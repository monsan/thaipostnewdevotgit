package com.ss.tp.model;

import java.io.Serializable;

public class VWgPrEmployeeSecurity implements Serializable {

	private VWgPrEmployeeSecurityPK pk;

	public VWgPrEmployeeSecurityPK getPk() {
		return pk;
	}

	public void setPk(VWgPrEmployeeSecurityPK pk) {
		this.pk = pk;
	}
}
