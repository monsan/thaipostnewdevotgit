/*
 * Created on 13 ?.?. 2549
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.ss.tp.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author bowpoko
 * 
 *         TODO To change the template for this generated type comment go to
 *         Window - Preferences - Java - Code Style - Code Templates
 */
public class VPrDailyDecOutAmtRep implements Serializable {
	private Integer keySeq;
	private String ouCode;
	

	private String divDesc;

	private String areaDesc;
	
	
	private String secDesc;

	
	private Double yearPr;
	private Double periodPr;
	private String refNo;
	private String empCode;
	private String fullName;
	
	private Integer codeSeq;
	private String incDecCode;
	private String groupCode;
	
	private Double allAmt;
	private Double outAmt;
	private Double netAmt;
	public Integer getKeySeq() {
		return keySeq;
	}
	public void setKeySeq(Integer keySeq) {
		this.keySeq = keySeq;
	}
	public String getOuCode() {
		return ouCode;
	}
	public void setOuCode(String ouCode) {
		this.ouCode = ouCode;
	}
	public String getDivDesc() {
		return divDesc;
	}
	public void setDivDesc(String divDesc) {
		this.divDesc = divDesc;
	}
	public String getAreaDesc() {
		return areaDesc;
	}
	public void setAreaDesc(String areaDesc) {
		this.areaDesc = areaDesc;
	}
	public String getSecDesc() {
		return secDesc;
	}
	public void setSecDesc(String secDesc) {
		this.secDesc = secDesc;
	}
	public Double getYearPr() {
		return yearPr;
	}
	public void setYearPr(Double yearPr) {
		this.yearPr = yearPr;
	}
	public Double getPeriodPr() {
		return periodPr;
	}
	public void setPeriodPr(Double periodPr) {
		this.periodPr = periodPr;
	}
	public String getRefNo() {
		return refNo;
	}
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}
	public String getEmpCode() {
		return empCode;
	}
	public void setEmpCode(String empCode) {
		this.empCode = empCode;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public Integer getCodeSeq() {
		return codeSeq;
	}
	public void setCodeSeq(Integer codeSeq) {
		this.codeSeq = codeSeq;
	}
	public String getIncDecCode() {
		return incDecCode;
	}
	public void setIncDecCode(String incDecCode) {
		this.incDecCode = incDecCode;
	}
	public String getGroupCode() {
		return groupCode;
	}
	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}
	public Double getAllAmt() {
		return allAmt;
	}
	public void setAllAmt(Double allAmt) {
		this.allAmt = allAmt;
	}
	public Double getOutAmt() {
		return outAmt;
	}
	public void setOutAmt(Double outAmt) {
		this.outAmt = outAmt;
	}
	public Double getNetAmt() {
		return netAmt;
	}
	public void setNetAmt(Double netAmt) {
		this.netAmt = netAmt;
	}
}
	
	
