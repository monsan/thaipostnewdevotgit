package com.ss.tp.model;

import java.io.Serializable;
import java.util.Date;

public class WlPeriodOfWork implements Serializable {
	private WlPeriodOfWorkPK pk;
	private Date billDate;
	private Date workStart;
	private Date workEnd;
	private String periodStatus;
	public WlPeriodOfWorkPK getPk() {
		return pk;
	}
	public void setPk(WlPeriodOfWorkPK pk) {
		this.pk = pk;
	}
	public Date getBillDate() {
		return billDate;
	}
	public void setBillDate(Date billDate) {
		this.billDate = billDate;
	}
	public Date getWorkStart() {
		return workStart;
	}
	public void setWorkStart(Date workStart) {
		this.workStart = workStart;
	}
	public Date getWorkEnd() {
		return workEnd;
	}
	public void setWorkEnd(Date workEnd) {
		this.workEnd = workEnd;
	}
	public String getPeriodStatus() {
		return periodStatus;
	}
	public void setPeriodStatus(String periodStatus) {
		this.periodStatus = periodStatus;
	}
	
	


}
