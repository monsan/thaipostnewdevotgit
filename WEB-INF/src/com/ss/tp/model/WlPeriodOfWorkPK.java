package com.ss.tp.model;

import java.io.Serializable;

public class WlPeriodOfWorkPK implements Serializable {
	private String ouCode;
	private double workYear;
	private double workPeriod;

	public WlPeriodOfWorkPK() {

	}

	public WlPeriodOfWorkPK(String ouCode, double workYear, double workPeriod) {
	
		this.ouCode = ouCode;
		this.workYear = workYear;
		this.workPeriod = workPeriod;
	}

	public String getOuCode() {
		return ouCode;
	}

	public void setOuCode(String ouCode) {
		this.ouCode = ouCode;
	}

	public double getWorkYear() {
		return workYear;
	}

	public void setWorkYear(double workYear) {
		this.workYear = workYear;
	}

	public double getWorkPeriod() {
		return workPeriod;
	}

	public void setWorkPeriod(double workPeriod) {
		this.workPeriod = workPeriod;
	}

	



}
