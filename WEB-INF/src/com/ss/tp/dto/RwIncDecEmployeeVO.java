package com.ss.tp.dto;

import java.io.Serializable;
import java.util.Date;

public class RwIncDecEmployeeVO implements Serializable {
	private Long keySeq;
	private String flagPr;
	private String empCode;
	private String name;
	private Double yearWork;
	private Double periodWork;
	private Double totalAmt;
	private Double seqData;
	private Long codeSeq;
	private Date startDate;
	private Date endDate;
	private String loanId;

	
	public RwIncDecEmployeeVO() {

	}


	public RwIncDecEmployeeVO(Long keySeq, String flagPr, String empCode,
			String name, Double yearWork, Double periodWork, Double totalAmt,
			Double seqData, Long codeSeq, Date startDate, Date endDate,
			String loanId) {
		
		this.keySeq = keySeq;
		this.flagPr = flagPr;
		this.empCode = empCode;
		this.name = name;
		this.yearWork = yearWork;
		this.periodWork = periodWork;
		this.totalAmt = totalAmt;
		this.seqData = seqData;
		this.codeSeq = codeSeq;
		this.startDate = startDate;
		this.endDate = endDate;
		this.loanId = loanId;
	}


	public Long getKeySeq() {
		return keySeq;
	}


	public void setKeySeq(Long keySeq) {
		this.keySeq = keySeq;
	}


	public String getFlagPr() {
		return flagPr;
	}


	public void setFlagPr(String flagPr) {
		this.flagPr = flagPr;
	}


	public String getEmpCode() {
		return empCode;
	}


	public void setEmpCode(String empCode) {
		this.empCode = empCode;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Double getYearWork() {
		return yearWork;
	}


	public void setYearWork(Double yearWork) {
		this.yearWork = yearWork;
	}


	public Double getPeriodWork() {
		return periodWork;
	}


	public void setPeriodWork(Double periodWork) {
		this.periodWork = periodWork;
	}


	public Double getTotalAmt() {
		return totalAmt;
	}


	public void setTotalAmt(Double totalAmt) {
		this.totalAmt = totalAmt;
	}


	public Double getSeqData() {
		return seqData;
	}


	public void setSeqData(Double seqData) {
		this.seqData = seqData;
	}


	public Long getCodeSeq() {
		return codeSeq;
	}


	public void setCodeSeq(Long codeSeq) {
		this.codeSeq = codeSeq;
	}


	public Date getStartDate() {
		return startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public String getLoanId() {
		return loanId;
	}


	public void setLoanId(String loanId) {
		this.loanId = loanId;
	}

}