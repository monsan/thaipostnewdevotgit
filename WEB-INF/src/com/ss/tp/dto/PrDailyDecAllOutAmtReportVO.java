package com.ss.tp.dto;

import java.io.Serializable;
import java.util.Date;

public class PrDailyDecAllOutAmtReportVO implements Serializable {

	private Integer keySeq;
	private String ouCode;

	private String divDesc;
	
	private String areaDesc;
	

	private String secDesc;
    private String orgDesc;
	
	private Double yearPr;
	private Double periodPr;
	private String refNo;
	private String empCode;
	private String fullName;
	private String flagPr;
	
	private Integer codeSeq;
	private String incDecCode;
	private String incDecName;
	private String subCode;
	private Double allAmt;
	private Double outAmt;
	private Double netAmt;
	public Integer getKeySeq() {
		return keySeq;
	}
	public void setKeySeq(Integer keySeq) {
		this.keySeq = keySeq;
	}
	public String getOuCode() {
		return ouCode;
	}
	public void setOuCode(String ouCode) {
		this.ouCode = ouCode;
	}
	public String getDivDesc() {
		return divDesc;
	}
	public void setDivDesc(String divDesc) {
		this.divDesc = divDesc;
	}
	public String getAreaDesc() {
		return areaDesc;
	}
	public void setAreaDesc(String areaDesc) {
		this.areaDesc = areaDesc;
	}
	public String getSecDesc() {
		return secDesc;
	}
	public void setSecDesc(String secDesc) {
		this.secDesc = secDesc;
	}
	public String getOrgDesc() {
		return orgDesc;
	}
	public void setOrgDesc(String orgDesc) {
		this.orgDesc = orgDesc;
	}
	public Double getYearPr() {
		return yearPr;
	}
	public void setYearPr(Double yearPr) {
		this.yearPr = yearPr;
	}
	public Double getPeriodPr() {
		return periodPr;
	}
	public void setPeriodPr(Double periodPr) {
		this.periodPr = periodPr;
	}
	public String getRefNo() {
		return refNo;
	}
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}
	public String getEmpCode() {
		return empCode;
	}
	public void setEmpCode(String empCode) {
		this.empCode = empCode;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getFlagPr() {
		return flagPr;
	}
	public void setFlagPr(String flagPr) {
		this.flagPr = flagPr;
	}
	public Integer getCodeSeq() {
		return codeSeq;
	}
	public void setCodeSeq(Integer codeSeq) {
		this.codeSeq = codeSeq;
	}
	public String getIncDecCode() {
		return incDecCode;
	}
	public void setIncDecCode(String incDecCode) {
		this.incDecCode = incDecCode;
	}
	public String getIncDecName() {
		return incDecName;
	}
	public void setIncDecName(String incDecName) {
		this.incDecName = incDecName;
	}
	public String getSubCode() {
		return subCode;
	}
	public void setSubCode(String subCode) {
		this.subCode = subCode;
	}
	public Double getAllAmt() {
		return allAmt;
	}
	public void setAllAmt(Double allAmt) {
		this.allAmt = allAmt;
	}
	public Double getOutAmt() {
		return outAmt;
	}
	public void setOutAmt(Double outAmt) {
		this.outAmt = outAmt;
	}
	public Double getNetAmt() {
		return netAmt;
	}
	public void setNetAmt(Double netAmt) {
		this.netAmt = netAmt;
	}
	
	
	
}
