package com.ss.tp.dto;

import java.io.Serializable;
import java.util.Date;

public class SrWflEmp09VO implements Serializable {
	private String keySeq;
	private String ouCode;
	private String empCodeN;
	private String fullNameN;
	private String contactNo;
	private String divDescN;
	private String divShortN;
	private String secDescN;
	private String secZipN;
	private String period;
	private String loanPrice;
	private String loanRate;
	private String empCodeG;
	private String fullNameG;
	private String divDescG;
	private String divShortG;
	private String firstDate;

	

	public SrWflEmp09VO() {
	}



	public SrWflEmp09VO(String keySeq, String ouCode, String empCodeN,
			String fullNameN, String contactNo, String divDescN,
			String divShortN, String secDescN, String secZipN, String period,
			String loanPrice, String loanRate, String empCodeG,
			String fullNameG, String divDescG, String divShortG,
			String firstDate) {
		
		this.ouCode = ouCode;
		this.empCodeN = empCodeN;
		this.fullNameN = fullNameN;
		this.contactNo = contactNo;
		this.divDescN = divDescN;
		this.divShortN = divShortN;
		this.secDescN = secDescN;
		this.secZipN = secZipN;
		this.period = period;
		this.loanPrice = loanPrice;
		this.loanRate = loanRate;
		this.empCodeG = empCodeG;
		this.fullNameG = fullNameG;
		this.divDescG = divDescG;
		this.divShortG = divShortG;
		this.firstDate = firstDate;
	}



	public String getKeySeq() {
		return keySeq;
	}



	public void setKeySeq(String keySeq) {
		this.keySeq = keySeq;
	}



	public String getOuCode() {
		return ouCode;
	}



	public void setOuCode(String ouCode) {
		this.ouCode = ouCode;
	}



	public String getEmpCodeN() {
		return empCodeN;
	}



	public void setEmpCodeN(String empCodeN) {
		this.empCodeN = empCodeN;
	}



	public String getFullNameN() {
		return fullNameN;
	}



	public void setFullNameN(String fullNameN) {
		this.fullNameN = fullNameN;
	}



	public String getContactNo() {
		return contactNo;
	}



	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}



	public String getDivDescN() {
		return divDescN;
	}



	public void setDivDescN(String divDescN) {
		this.divDescN = divDescN;
	}



	public String getDivShortN() {
		return divShortN;
	}



	public void setDivShortN(String divShortN) {
		this.divShortN = divShortN;
	}



	public String getSecDescN() {
		return secDescN;
	}



	public void setSecDescN(String secDescN) {
		this.secDescN = secDescN;
	}



	public String getSecZipN() {
		return secZipN;
	}



	public void setSecZipN(String secZipN) {
		this.secZipN = secZipN;
	}



	public String getPeriod() {
		return period;
	}



	public void setPeriod(String period) {
		this.period = period;
	}



	public String getLoanPrice() {
		return loanPrice;
	}



	public void setLoanPrice(String loanPrice) {
		this.loanPrice = loanPrice;
	}



	public String getLoanRate() {
		return loanRate;
	}



	public void setLoanRate(String loanRate) {
		this.loanRate = loanRate;
	}



	public String getEmpCodeG() {
		return empCodeG;
	}



	public void setEmpCodeG(String empCodeG) {
		this.empCodeG = empCodeG;
	}



	public String getFullNameG() {
		return fullNameG;
	}



	public void setFullNameG(String fullNameG) {
		this.fullNameG = fullNameG;
	}



	public String getDivDescG() {
		return divDescG;
	}



	public void setDivDescG(String divDescG) {
		this.divDescG = divDescG;
	}



	public String getDivShortG() {
		return divShortG;
	}



	public void setDivShortG(String divShortG) {
		this.divShortG = divShortG;
	}



	public String getFirstDate() {
		return firstDate;
	}



	public void setFirstDate(String firstDate) {
		this.firstDate = firstDate;
	}



	



	
}
