package com.ss.tp.dto;

import java.io.Serializable;

public class VWlrp0062VO implements Serializable {
	private Integer keySeq;
	private String contractNo;
	private String empCode;
	private String name;
	private String orgCode;
	private String secDesc;
	private Integer periodNo;
	private Double loanBalance;
	private Double interest;
	private String workYear;
	private String workPeriod;
	private String incDecCode;
	private String incDecName;
	public VWlrp0062VO() {

	}
	public Integer getKeySeq() {
		return keySeq;
	}
	public void setKeySeq(Integer keySeq) {
		this.keySeq = keySeq;
	}
	public String getContractNo() {
		return contractNo;
	}
	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}
	public String getEmpCode() {
		return empCode;
	}
	public void setEmpCode(String empCode) {
		this.empCode = empCode;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getOrgCode() {
		return orgCode;
	}
	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}
	public String getSecDesc() {
		return secDesc;
	}
	public void setSecDesc(String secDesc) {
		this.secDesc = secDesc;
	}
	public Integer getPeriodNo() {
		return periodNo;
	}
	public void setPeriodNo(Integer periodNo) {
		this.periodNo = periodNo;
	}
	public Double getLoanBalance() {
		return loanBalance;
	}
	public void setLoanBalance(Double loanBalance) {
		this.loanBalance = loanBalance;
	}
	public Double getInterest() {
		return interest;
	}
	public void setInterest(Double interest) {
		this.interest = interest;
	}
	public String getWorkYear() {
		return workYear;
	}
	public void setWorkYear(String workYear) {
		this.workYear = workYear;
	}
	public String getWorkPeriod() {
		return workPeriod;
	}
	public void setWorkPeriod(String workPeriod) {
		this.workPeriod = workPeriod;
	}
	public String getIncDecCode() {
		return incDecCode;
	}
	public void setIncDecCode(String incDecCode) {
		this.incDecCode = incDecCode;
	}
	public String getIncDecName() {
		return incDecName;
	}
	public void setIncDecName(String incDecName) {
		this.incDecName = incDecName;
	}
	
	
	

	
}
