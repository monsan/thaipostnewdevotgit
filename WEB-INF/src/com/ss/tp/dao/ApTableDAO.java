package com.ss.tp.dao;

import com.ss.tp.dto.ApEmployeeVO;
import com.ss.tp.dto.ApTableVO;
import com.ss.tp.dto.WeEmployeeVO;
import com.ss.tp.model.ApTable;



import java.util.Date;
import java.util.List;


public interface ApTableDAO {
	public void insertApTable(ApTableVO aptablevo)
			throws Exception;

	public void updateApTable(ApTableVO aptablevo)
			throws Exception;

	public void updateApTableApprove(ApTableVO aptablevo)
			throws Exception;


	public void deleteApTable(ApTableVO aptablevo)
			throws Exception;

	public void insertApTables(List aptablevolist)
			throws Exception;

	public List findByCriteria(String userId, String evaOuCode, Long evaYear,
			Long evaMonth, String evaEmpCodeFrom, String evaEmpCodeTo);

	public Integer countData(String userId, String evaOuCode, Long evaYear,
			Long evaMonth, Long evaVolume);

	public Integer countDataApprove(String userId, String evaOuCode, Long evaYear,
			Long evaMonth, Long evaVolume);

	public Integer countEmpData(String userId,String empCode, String evaOuCode, Long evaYear);

	public ApEmployeeVO findByEmpCodeDetail(String empCode, String ouCode);

	public List findByEmpCodeList(String userId, String ouCode, Long yearPn,
			Long monthPn, String empCode);

	public ApTable loadApTable(ApTableVO rpVo);

	// public List rwPremiumReportByOrg(String userId, String evaOuCode, Long
	// evaYear,Long evaPeriod,String evaFlag);
	public void addList(ApTableVO aptablevo);
	
	public void addListApprove(ApTableVO aptablevo);

	public void insertAndUpdateApTables() throws Exception;

	public void insertAndUpdateApTablesApprove() throws Exception;

	public boolean canDelete(String ouCode, String year, String month,
			String userId) throws Exception;

	public List findByCriteriaList(String userId, String evaOuCode,
			Long evaYear, Long evaMonth, Long evaVolume, int count,
			int countRecord);
	
	public List findByCriteriaListApprove(String userId, String evaOuCode,
			Long evaYear, Long evaMonth, Long evaVolume, int count,
			int countRecord);
	
	public List findByEmpCriteriaList(String userId,String empCode, String evaOuCode,
			Long evaYear, int count,int countRecord);


	public List findByCriteriaReport(String userId, String evaOuCode,
			String evaYear, String evaMonth, String evaVolume);

	public List findByCriteriaPromoteLevelReport(String userId,
			String evaOuCode, Long evaYear, Long evaMonth, Long evaVolume);

	public List findVolumeBySecurity(String userId, String ouCode,String year,String month)
			throws Exception;
	
	public List findVolumeBySecurityApprove(String userId, String ouCode,String year,String month)
			throws Exception;
	
	public List findVolumeBySecurityPm001(String userId, String ouCode,String year,String month)
			throws Exception;
	
	public List findVolumeBySecurityPm002(String userId, String ouCode,String year,String month)
			throws Exception;
	
	public List findVolumeBySecurityTransfer(String userId, String ouCode,String year,String month)
			throws Exception;
	
	
	public List findVolumeBySecurityReport1(String userId, String ouCode,String year,String month)
			throws Exception;
	public List findVolumeBySecurityApproveReport(String userId, String ouCode,String year,String month)
			throws Exception;
	
	public boolean isConfirmFlag(String ouCode, String year, String month,
			String userId) throws Exception;
	
	public String[] findMaxYearPeriod(String ouCode);
	
	public String[] findMaxYearPeriodAP001(String ouCode);
	public String[] findMaxYearPeriodAP002(String ouCode);
	public String[] findMaxYearPeriodBK(String ouCode);
	public String[] findMaxYearPeriodTR(String ouCode);
	
	
	
	
	public String[] findMaxMonthInYear(String ouCode,String year);
	
	public void deleteApTableInputError(String ouCode, String yearPn,
			String monthPn,String volumeSet, String userId) throws Exception;
	
	public Integer confirmData(String ouCode, Long yearPn, Long monthPn,
			String volumeSetFrom,String volumeSetTo, String userId);

	public Double sumAllMoneyData(String ouCode, Long yearPn, Long monthPn,
			String volumeSetFrom,String volumeSetTo, String userId);
	
	public Double sumMoneyData(String ouCode, Long yearPn, Long monthPn,
			String volumeSetFrom,String volumeSetTo, String userId,String flag);
	
	public void updateConfirmData(String ouCode, Long yearPn, Long monthPn,String volumeSetFrom,String volumeSetTo,
			String userId) throws Exception;
	
	public Integer confirmDataApprove(String ouCode, Long yearPn, Long monthPn,
			String volumeSetFrom,String volumeSetTo, String userId);

	public Double sumAllMoneyDataApprove(String ouCode, Long yearPn, Long monthPn,
			String volumeSetFrom,String volumeSetTo, String userId);
	
	public Double sumMoneyDataApprove(String ouCode, Long yearPn, Long monthPn,
			String volumeSetFrom ,String volumeSetTo, String userId,String flag);
	
	public void updateConfirmDataApprove(String ouCode, Long yearPn, Long monthPn,String volumeSetFrom,String volumeSetTo,
			String userId) throws Exception;

	public List apEmpListReport(String userId, String evaOuCode,
			String evaYear, String evaMonth,String volume);
	
	public List apEmpListApproveReport(String userId, String evaOuCode,
			String evaYear, String evaMonth,String volume,String approve);
	
	
	public List apEmpKtbBankReport(String userId, String evaOuCode,
			Integer evaYear, Integer evaMonth,String volumeFrom,String volumeTo);
	
	public List apEmpScbBankReport(String userId, String evaOuCode,
			Integer evaYear, Integer evaMonth,String volumeFrom,String volumeTo);
	
	public List apTransferReport(String evaYear, String evaMonth);
	
	public List apSumBatchApproveReport(String evaYear, String evaMonth,String volumeFrom,String volumeTo);
	
	public String apToGl( String userId, String evaOuCode,
			String evaYear,String evaMonth, String evaVolume1,String evaVolume2,String accDate);
	public void updateConfirmDataBankKTB(String ouCode, Long yearPn, Long monthPn,String volumeSetFrom,String volumeSetTo,
			String userId) throws Exception;
	
	public void updateConfirmDataBankSCB(String ouCode, Long yearPn, Long monthPn,String volumeSetFrom,String volumeSetTo,
			String userId) throws Exception;
	
	public void updateConfirmDataTransfer(String ouCode, Long yearPn, Long monthPn,String volumeSetFrom,String volumeSetTo,
			String userId) throws Exception;
	

}