package com.ss.tp.dao;

import java.util.List;

import com.ss.tp.dto.RwConfirmDataVO;

public interface RwConfirmDataDAO {
	public void insertRwConfirmData(RwConfirmDataVO vo) throws Exception;

	public boolean isConfirmMasterData(String ouCode, String year,
			String period, String userId) throws Exception;

	public boolean isConfirmRwData(String ouCode, String year, String period,
			String userId) throws Exception;
	public boolean isTransferGL(String ouCode, String year, String period,String userId) throws Exception;
	
	public List addPrDailyToGl(String userId, String evaOuCode, String evaYear,
			String evaMonth,String accDate);
	public List addPrDailyToGlHm(String userId, String evaOuCode, String evaYear,
			String evaMonth,String accDate);

}