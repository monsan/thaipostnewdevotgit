package com.ss.tp.dao;

import com.ss.tp.model.ApPeriodLine;

import java.util.List;


public interface ApPeriodLineDAO {
	/**
	 * Find Period Line
	 */
	public ApPeriodLine findPeriodLine(String ouCode, String year, String volumeSet)
			throws Exception;

	public List findYearInPeriodLine(String ouCode) throws Exception;

	public List findPeriodInPeriodLine(String ouCode, Double year)
			throws Exception;

	public boolean canDeleteData(String ouCode, String year, String month, String volumeSet)
			throws Exception;

	public boolean isCloseTranClose(String ouCode, String year, String month, String volumeSet)
			throws Exception;

	public boolean isCloseMasterClose(String ouCode, String year, String month, String volumeSet)
			throws Exception;
}