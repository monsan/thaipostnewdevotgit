package com.ss.tp.dao;

import java.util.List;

import com.ss.tp.model.WlPeriodOfWork;

public interface WlPeriodOfWorkDAO {
	/**
	 * Find Period Line
	 */
	public String[] findMaxYearPeriod(String ouCode);
	public WlPeriodOfWork findPeriodLine(String ouCode, String year, String period)
			throws Exception;

	public List findYearInPeriodLine(String ouCode) throws Exception;

	public List findPeriodInPeriodLine(String ouCode, Double year)
			throws Exception;

	public List findPeriodInPeriodLineMod(String ouCode, Double year)
			throws Exception;

	public boolean canDeleteData(String ouCode, String year, String period)
			throws Exception;

	public boolean isCloseTranClose(String ouCode, String year, String period)
			throws Exception;

	public boolean isCloseMasterClose(String ouCode, String year, String period)
			throws Exception;
	
	public WlPeriodOfWork findPeriodLineNext(String ouCode, String year, String period)
	throws Exception;
	
	public WlPeriodOfWork findPeriodLineMod(String ouCode, String year, String period)
			throws Exception;
	
	public List findPeriodInPeriodLineModOne(String ouCode, Double year)
			throws Exception;
	

	
}