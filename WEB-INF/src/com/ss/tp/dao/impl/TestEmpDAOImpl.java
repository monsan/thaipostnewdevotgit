package com.ss.tp.dao.impl;

import java.io.Serializable;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import com.ss.tp.dao.TestEmpDAO;
import com.ss.tp.model.TestEmp;

public class TestEmpDAOImpl extends HibernateDaoSupport implements TestEmpDAO,
		Serializable {

	public void loadTestEmp() {
		// String aaa = "aaa";
		TestEmp testEmp = new TestEmp();
		TestEmp testEmpData = new TestEmp();
		testEmp.setEmpCode("20");
		testEmpData = (TestEmp) this.getHibernateTemplate().load(TestEmp.class,
				testEmp.getEmpCode());
		System.out.println("Data is ");
		System.out.println(testEmpData.getEmpCode() + ' '
				+ testEmpData.getEmpName());
	}

}
