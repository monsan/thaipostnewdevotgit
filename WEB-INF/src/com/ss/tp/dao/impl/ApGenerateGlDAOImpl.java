package com.ss.tp.dao.impl;

import java.io.Serializable;

import java.sql.DriverManager;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;

import java.sql.ResultSet;
import java.sql.Types;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.map.ListOrderedMap;
import org.hibernate.Query;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.ss.tp.dao.ApGenerateGlDAO;
import com.ss.tp.dto.ApToGlVO;
import com.ss.tp.dto.DefaultYearSectionVO;
import com.ss.tp.dto.WeEmployeeVO;
import com.ss.tp.model.GlInterfaceErp;

public class ApGenerateGlDAOImpl extends HibernateDaoSupport implements
		ApGenerateGlDAO, Serializable {
	List rwList = new ArrayList();

	private JdbcTemplate jdbcTemplate;

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public ApGenerateGlDAOImpl() {
		Locale.setDefault(Locale.US);
	}

	public void insertApToGl(ApToGlVO aptoglvo) throws Exception {
		GlInterfaceErp rw = new GlInterfaceErp();
		try {
			BeanUtils.copyProperties(rw, aptoglvo);
			// rw.setCreDate(new Date());
			// rw.setUpdDate(new Date());
			// rw.setConfirmFlag("N");
			// rw.setApproveFlag("N");
			// rw.setTransferFlag("N");
			// rw.setBankFlag("N");
			this.getHibernateTemplate().save(rw);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public void deleteApToGl(ApToGlVO aptoglvo) throws Exception {
		GlInterfaceErp rw = new GlInterfaceErp();
		try {
			BeanUtils.copyProperties(rw, aptoglvo);
			this.getHibernateTemplate().delete(rw);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public void addList(ApToGlVO aptoglvo) {
		this.rwList.add(aptoglvo);
	}

	public void clearList() {
		this.rwList = new ArrayList();
	}

	public void insertApToGls(List aptoglvolist) throws Exception {
		GlInterfaceErp rw = new GlInterfaceErp();
		try {
			for (int i = 0; i < aptoglvolist.size(); i++) {
				ApToGlVO aptoglvo = (ApToGlVO) aptoglvolist.get(i);

				BeanUtils.copyProperties(rw, aptoglvo);
				// rw.setCreDate(new Date());
				// rw.setUpdDate(new Date());
				// rw.setConfirmFlag("N");
				// rw.setApproveFlag("N");
				// rw.setTransferFlag("N");
				// rw.setBankFlag("N");
				this.getHibernateTemplate().save(rw);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public List findByCriteria(String userId, String evaOuCode, Long evaYear,
			Long evaMonth, String evaEmpCodeFrom, String evaEmpCodeTo) {
		StringBuffer criteria = new StringBuffer();

		if (evaYear != null && !evaYear.equals("")) {
			criteria.append(" and rwPre.yearPn = ");
			criteria.append(evaYear);
		}

		if (evaMonth != null && !evaMonth.equals("")) {
			criteria.append(" and rwPre.monthPn = ");
			criteria.append(evaMonth);
		}

		if (evaEmpCodeFrom != null && !evaEmpCodeFrom.equals("")) {
			criteria.append(" and rwPre.empCode >= '");
			criteria.append(evaEmpCodeFrom);
			criteria.append("' ");
		}

		if (evaEmpCodeTo != null && !evaEmpCodeTo.equals("")) {
			criteria.append(" and rwPre.empCode <= '");
			criteria.append(evaEmpCodeTo);
			criteria.append("' ");
		}

		StringBuffer hql = new StringBuffer();

		hql.append(" select distinct rwPre.empCode, pnEmp.refDbPreSuff.prefixName, pnEmp.firstName, pnEmp.lastName, pnPos.positionShort, rwPre.codeSeq,pnEmp.adminCode,pnEmp.levelCode ");
		hql.append(" from ApToGl rwPre , PnEmployee pnEmp , PnPosition pnPos , PnOrganization pnOrg, VPnOrganizationSecurity v ");
		hql.append(" where rwPre.ouCode = '");
		hql.append(evaOuCode);
		hql.append("' ");
		hql.append(" and v.pk.userId = '" + userId + "' ");
		hql.append(" and rwPre.ouCode = v.pk.ouCode ");
		hql.append(" and rwPre.codeSeq = v.pk.codeSeq ");
		hql.append(criteria);
		hql.append(" and rwPre.ouCode   = pnEmp.pk.ouCode ");
		hql.append(" and rwPre.empCode = pnEmp.pk.empCode ");
		hql.append(" and pnEmp.pk.ouCode = pnPos.pk.ouCode ");
		hql.append(" and pnEmp.gworkCode = pnPos.pk.gworkCode ");
		hql.append(" and pnEmp.positionCode = pnPos.pk.positionCode ");
		hql.append(" and rwPre.ouCode = pnOrg.pk.ouCode ");
		hql.append(" and rwPre.codeSeq = pnOrg.pk.codeSeq ");
		hql.append(" order by rwPre.codeSeq,pnEmp.adminCode,pnEmp.levelCode desc, rwPre.empCode ");

		System.out.println("HQL findByCriteria ==> " + hql.toString());

		List empList = this.getSession().createQuery(hql.toString()).list();
		List retList = new ArrayList();

		for (Iterator it = empList.iterator(); it.hasNext();) {
			Object[] r = (Object[]) it.next();
			String empCode = (String) r[0];
			String prefixName = (String) r[1];
			String firstName = (String) r[2];
			String lastName = (String) r[3];
			// String positionShort = (String)r[4];

			WeEmployeeVO ret = new WeEmployeeVO();
			ret.setEmpCode(empCode);
			ret.setName(prefixName + " " + firstName + " " + lastName);
			// ret.setPositionShort(positionShort);

			retList.add(ret);
		}
		return retList;
	}

	public Integer countData(String userId, String evaOuCode, Long evaYear,
			Long evaMonth, Long evaVolume) {
		StringBuffer criteria = new StringBuffer();

		if (evaYear != null && !evaYear.equals("")) {
			criteria.append(" and rwPre.yearPn = ");
			criteria.append(evaYear);
		}

		if (evaMonth != null && !evaMonth.equals("")) {
			criteria.append(" and rwPre.monthPn = ");
			criteria.append(evaMonth);
		}

		if (evaVolume != null && !evaVolume.equals("")) {
			criteria.append(" and rwPre.volumeSet = ");
			criteria.append(evaVolume);
		}

		StringBuffer hql = new StringBuffer();

		hql.append(" select count(rwPre.seqData) ");
		hql.append(" from ApToGl rwPre , PnEmployee pnEmp , PnPosition pnPos , PnOrganization pnOrg, VPnOrganizationSecurity v ");
		hql.append(" where rwPre.ouCode = '");
		hql.append(evaOuCode);
		hql.append("' ");
		hql.append(" and v.pk.userId = '" + userId + "' ");
		hql.append(" and rwPre.ouCode = v.pk.ouCode ");
		hql.append(" and rwPre.codeSeq = v.pk.codeSeq ");
		hql.append(criteria);
		hql.append(" and rwPre.ouCode = pnEmp.pk.ouCode ");
		hql.append(" and rwPre.empCode = pnEmp.pk.empCode ");
		hql.append(" and pnEmp.pk.ouCode = pnPos.pk.ouCode ");
		hql.append(" and pnEmp.gworkCode = pnPos.pk.gworkCode ");
		hql.append(" and pnEmp.positionCode = pnPos.pk.positionCode ");
		hql.append(" and rwPre.ouCode = pnOrg.pk.ouCode ");
		hql.append(" and rwPre.codeSeq = pnOrg.pk.codeSeq ");

		List empList = this.getSession().createQuery(hql.toString()).list();

		return (Integer) empList.get(0);
	}

	public boolean canDelete(Date accountingDate) throws Exception {
		StringBuffer sql = new StringBuffer(0);
		sql.append(" select count(*) ");
		sql.append(" from GlInterfaceErpGl p ");
		sql.append(" where p.accountingDate = accountingDate ");

		List ls = this.getHibernateTemplate().find(sql.toString());

		if (ls != null && ls.size() > 0) {
			Integer i = (Integer) ls.get(0);

			if (i.intValue() > 0)
				return false;
			else
				return true;
		} else
			return true;
	}

	public List findByCriteriaList(String userId, String evaOuCode,
			Long evaYear, Long evaMonth, Long evaVolume, int count,
			int countRecord)

	{
		StringBuffer criteria = new StringBuffer();

		if (evaYear != null && !evaYear.equals("")) {
			criteria.append(" and rwInc.yearPn = ");
			criteria.append(evaYear);
		}

		if (evaMonth != null && !evaMonth.equals("")) {
			criteria.append(" and rwInc.monthPn = ");
			criteria.append(evaMonth);
		}

		if (evaVolume != null && !evaVolume.equals("")) {
			criteria.append(" and rwInc.volumeSet = ");
			criteria.append(evaVolume);
		}

		StringBuffer hql = new StringBuffer();

		hql.append(" select rwInc.keySeq, ");
		hql.append(" rwInc.empCode, ");
		hql.append(" pnEmp.refDbPreSuff.prefixName, ");
		hql.append(" pnEmp.firstName, ");
		hql.append(" pnEmp.lastName, ");
		// hql.append(" nvl(pa.adminDesc,' '), ");
		hql.append(" pnEmp.levelCode, ");
		hql.append(" pnPos.positionShort, ");
		hql.append(" pnOrg.pk.codeSeq, ");
		hql.append(" pnOrg.orgCode, ");
		hql.append(" pnOrg.divShort || ' ' || nvl(pnOrg.secDesc,pnOrg.areaDesc) || ' ' ||pnOrg.workDesc, ");
		hql.append(" rwInc.seqData, ");
		hql.append(" rwInc.totAmt, ");
		hql.append(" rwInc.newGworkCode ");
		hql.append(" from ApToGl rwInc , PnEmployee pnEmp ,PnAdmin pa,PnPosition pnPos, PnOrganization pnOrg,VPnOrganizationSecurity v ");
		hql.append(" where rwInc.ouCode = '");
		hql.append(evaOuCode);
		hql.append("' ");
		hql.append(criteria);
		hql.append(" and v.pk.userId = '" + userId + "' ");
		hql.append(" and rwInc.creBy = '" + userId + "' ");
		hql.append(" and rwInc.ouCode = v.pk.ouCode ");
		hql.append(" and rwInc.codeSeq = v.pk.codeSeq ");
		hql.append(" and rwInc.ouCode = pnEmp.pk.ouCode ");
		hql.append(" and rwInc.empCode = pnEmp.pk.empCode ");
		hql.append(" and pnEmp.pk.ouCode = pnOrg.pk.ouCode ");
		hql.append(" and pnEmp.codeSeq = pnOrg.pk.codeSeq ");
		hql.append(" and pnEmp.pk.ouCode = pa.pk.ouCode ");
		hql.append(" and NVL(pnEmp.adminCode,'N') = pa.pk.adminCode ");
		hql.append(" and pnEmp.positionCode = pnPos.pk.positionCode ");
		hql.append(" and pnEmp.gworkCode = pnPos.pk.gworkCode ");
		hql.append(" and rwInc.confirmFlag = 'N' ");
		// hql.append(" and rwInc.newCodeSeq = pnOrga.pk.codeSeq ");
		hql.append(" and pnOrg.inactive = 'N' ");
		// hql.append(" and pnOrga.inactive = 'N' ");
		hql.append(" order by rwInc.seqData ");
		System.out.println("HQL findByCriteriaList ==> " + hql.toString());

		Query q = this.getSession().createQuery(hql.toString());
		List empList = q.setFirstResult(countRecord * count)
				.setMaxResults(countRecord).list();
		List retList = new ArrayList();

		/*
		 * List empList = this.getSession().createQuery(hql.toString()).list();
		 * // List retList = new ArrayList();
		 */

		for (Iterator it = empList.iterator(); it.hasNext();) {
			Object[] r = (Object[]) it.next();

			Long keySeq = (Long) r[0];
			String empCode = (String) r[1];
			String prefixName = (String) r[2];
			String firstName = (String) r[3];
			String lastName = (String) r[4];
			// String oldDuty = (String) r[5];
			String oldLevel = (String) r[5];
			String oldPosition = (String) r[6];
			Long codeSeq = (Long) r[7];
			String orgCode = (String) r[8];
			String orgDesc = (String) r[9];
			Double seqData = (Double) r[10];
			Double totAmt = (Double) r[11];
			String newGworkCode = (String) r[12];
			int intLevelCode = Integer.parseInt(oldLevel.trim());

			WeEmployeeVO ret = new WeEmployeeVO();
			ret.setKeySeq(keySeq);
			ret.setEmpCode(empCode);
			ret.setName(prefixName + " " + firstName + " " + lastName);
			// ret.setOldDuty(oldDuty);
			ret.setOldPositionShort(oldPosition + " " + intLevelCode);
			ret.setCodeSeq(codeSeq);
			// ret.setOrgCode(orgCode);
			ret.setOrgDesc(orgDesc);
			// ret.setNewPositionCode(newPositionCode);
			// ret.setNewLevelCode(newLevelCode);
			// ret.setNewOldDuty(newOldDuty);
			// ret.setNewDuty(newDuty);
			// ret.setNewOrgCode(newOrgCode);
			// ret.setNewOrgDesc(newOrgDesc);
			ret.setSeqData(seqData);
			ret.setTotAmt(totAmt);
			ret.setNewGworkCode(newGworkCode);
			// ret.setNewCodeSeq(newCodeSeq);

			retList.add(ret);
		}

		return retList;
	}

	public void deleteApToGlInputError(String ouCode, String year,
			String month, String volumeSet, String userId) throws Exception {

		try {

			StringBuffer sql2 = new StringBuffer(0);
			sql2.append(" delete ap_table rw ");
			sql2.append(" where rw.ou_code = '" + ouCode + "' ");
			sql2.append(" and rw.year_pn = " + year);
			sql2.append(" and rw.month_pn = " + month);
			sql2.append(" and rw.code_seq is null ");
			sql2.append(" and rw.emp_code in ( ");
			sql2.append("	select v.emp_code ");
			sql2.append("	from v_pn_employee_security v ");
			sql2.append("	where v.user_id = '" + userId + "' ");
			sql2.append("	and v.code_seq = rw.code_seq ");
			sql2.append("	and ou_code = rw.ou_code ");
			sql2.append(" ) ");

			System.out.println("sql2 : " + sql2.toString());

			this.jdbcTemplate.execute(sql2.toString());

		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	public Integer confirmData(String ouCode, Long yearPn, Long monthPn,
			String volumeSet, String userId) {
		try {
			Integer countData = new Integer(0);

			StringBuffer hql1 = new StringBuffer();
			hql1.append(" select count(*) ");
			hql1.append(" from ApToGl pr ");
			hql1.append(" where pr.ouCode = '");
			hql1.append(ouCode);
			hql1.append("' ");
			hql1.append(" and pr.yearPn = ");
			hql1.append(yearPn);
			hql1.append(" and pr.monthPn = ");
			hql1.append(monthPn);
			hql1.append(" and pr.volumeSet =  '" + volumeSet + "' ");
			hql1.append("	and pr.creBy = '" + userId + "' ");
			hql1.append(" and ( pr.confirmFlag <> 'Y' or pr.confirmFlag is null ) ");
			hql1.append(" and  pr.empCode in ( ");
			hql1.append("	select pk.empCode ");
			hql1.append("	from VPnEmployeeSecurity ");
			hql1.append("	where pk.ouCode = '" + ouCode + "' ");
			hql1.append("	and pk.userId = '" + userId + "' ");
			hql1.append("	and pk.codeSeq = pr.codeSeq ");
			hql1.append(" 	) ");

			Object confirmDataObj = this.getSession()
					.createQuery(hql1.toString()).uniqueResult();

			if (confirmDataObj != null) {
				countData = (Integer) confirmDataObj;
			}

			return countData;
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public List addApToGl(String userId, String evaOuCode, String evaYear,
			String evaMonth, String volume1, String volume2, String accDate) {

		int intYear = Integer.parseInt(evaYear.trim());
		int intMonth = Integer.parseInt(evaMonth.trim());

		try {
			final Connection c = DriverManager.getConnection(
					"jdbc:oracle:thin:@10.254.8.22:1521:post", "hrpost",
					"hrpost");

			String plsql = "" + " begin " + "    AP_GENERATE_GL(?,?,?,?,?,?); "
					+ " end; ";

			CallableStatement cs = c.prepareCall(plsql);
			cs.setString(1, evaOuCode);
			cs.setLong(2, intYear);
			cs.setLong(3, intMonth);
			cs.setString(4, volume1);
			cs.setString(5, volume2);
			cs.setString(6, accDate);
			cs.execute();
			cs.close();
			c.close();

		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		return null;
	}
	
	public String[] findMaxDateGL(String ouCode) {

		String ddate = "";
		
		//String month = "";
		StringBuffer sql = new StringBuffer(0);
		sql.append(" SELECT MAX(a.accountingDate) ");
		sql.append(" FROM   GlInterfaceErp a ");
		//sql.append(" WHERE  a.ouCode = '" + ouCode + "' ");
		//sql.append(" and a.confirmFlag = 'Y' ");
		//sql.append(" and a.approveFlag = 'Y' ");
		//sql.append(" and a.approveClose = 'Y' ");
		//sql.append(" and a.bankClose = 'Y' ");
		//sql.append(" and a.transferFlag = 'N' ");
		//sql.append(" and a.transferClose = 'N' ");
		//sql.append(" and a.approveFlag = 'N' ");
		//sql.append(" and a.confirmFlag <> 'Y' ");
		//sql.append(" and a.yearPn = ( select max(b.yearPn) from ApTable b ) ");
		//sql.append(" group by a.yearPn ");
		List ls = this.getHibernateTemplate().find(sql.toString());
		if (ls != null && ls.size() > 0) {
			String rs = (String) ls.get(0);
			ddate = rs.substring(0, rs.length());
			
		}

		String[] val = new String[1];
		val[0] = ddate;
		
		System.out.println("VAL0" + val[0]);
		//System.out.println("VAL1" + val[1]);

		return val;
	}
	public List findDateBySecurity(String ouCode)
			throws Exception {
		String DATE_FORMAT = "dd/MM/yyyy";
	    SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);

		StringBuffer hql = new StringBuffer(0);
		hql.append("select max(a.accountingDate) from GlInterfaceErp a ");
		//hql.append(" where ");
		//hql.append(" a.ouCode = '" + ouCode + "' ");
		
		
		String ls = this.getHibernateTemplate().find(hql.toString()).toString();
		   
          List lss = new ArrayList();
          lss.add(sdf.format(ls));

		//List lss = sdf.format(ls);
		return lss;
	}
	
}
