package com.ss.tp.dao.impl;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.hibernate.*;
import org.hibernate.criterion.Restrictions;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.map.ListOrderedMap;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import com.ss.tp.dao.SrWflEmpDAO;


import com.ss.tp.dto.PrDailyDecAllOutAmtReportVO;
import com.ss.tp.dto.SrPvfEmpVO;
import com.ss.tp.dto.SrWflEmp09VO;
import com.ss.tp.dto.VPrGlTransferVO;
import com.ss.tp.dto.VWlrp0062VO;

public class SrWflEmpDAOImpl extends HibernateDaoSupport implements
		SrWflEmpDAO, Serializable {
	private JdbcTemplate jdbcTemplate;

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
	
	
	public List findMDate(String ouCode)
			throws Exception {
	
		StringBuffer hql = new StringBuffer(0);
		hql.append("select to_char(a.mDate,'dd/mm/yyyy') from VPvfMDate a ");
		//hql.append("group by to_char(a.mDate,'dd/mm/yyyy') ");
		hql.append(" order by a.mDate desc ");

		
		List ls = this.getHibernateTemplate().find(hql.toString());
	    System.out.println(ls);
		return ls;
	}
	
	public List findFDate(String ouCode)
			throws Exception {
	
		StringBuffer hql = new StringBuffer(0);
		hql.append("select to_char(a.rDate,'mm/yyyy') from VWflFirstDate09 a ");
		//hql.append("group by to_char(a.mDate,'dd/mm/yyyy') ");
		hql.append(" order by a.rDate desc ");

		
		List ls = this.getHibernateTemplate().find(hql.toString());
	    System.out.println(ls);
		return ls;
	}
	
	public List findChgRateDate(String ouCode)
			throws Exception {
		
		StringBuffer hql = new StringBuffer(0);
		hql.append("select to_char(a.rDate,'dd/mm/yyyy') from VPvfChgRateDate a ");
		hql.append("order by a.rDate desc ");

		
		List ls = this.getHibernateTemplate().find(hql.toString());
	    System.out.println(ls);
		return ls;
	}
	
	public List findEmpStatusDate(String ouCode)
			throws Exception {
		
		StringBuffer hql = new StringBuffer(0);
		hql.append("select to_char(a.rDate,'dd/mm/yyyy') from VPvfEmpStatusDate a ");
		hql.append("order by a.rDate desc ");

		
		List ls = this.getHibernateTemplate().find(hql.toString());
	    System.out.println(ls);
		return ls;
	}
	
	
	
	public List findChgMasterDate(String ouCode)
			throws Exception {
		
		StringBuffer hql = new StringBuffer(0);
		hql.append("select to_char(a.rDate,'dd/mm/yyyy') from VPvfChgMasterDate a ");
		//hql.append("group by to_char(a.rDate,'dd/mm/yyyy') ");
		hql.append("order by a.rDate desc ");

		

		List ls = this.getHibernateTemplate().find(hql.toString());
	    System.out.println(ls);
		return ls;
	}
	
	public List CTPFRP207(String ouCode,String ageYear,
			String dDate) {
		List resultList = new ArrayList();
		List returnList = new ArrayList();
		String sql = "";
		sql = " SELECT A.OU_CODE,C.DIV_DESC "
				+ " , nvl(c.sec_desc,c.area_desc) "
				+ " , C.WORK_DESC "
				+ " , C.ZIP_CODE "
				+ " , E.EMP_CODE||'/'||E.GWORK_CODE "
				+ " ,HRPVF.PN_GET_NAME(A.EMP_CODE,A.OU_CODE) "
				+ " ,Pn_Desc.get_emp_position_short(A.OU_CODE,A.EMP_CODE) ||' '||TO_NUMBER(E.LEVEL_CODE) "
				+ " ,A.CAT_DATE " + ",DECODE( '"
				+ ageYear
				+ "','10', ADD_MONTHS (A.CAT_DATE, 10 * 12),'20',ADD_MONTHS(A.CAT_DATE,20*12) ) "
				+ " ,to_char(A.COLLECTR) "
				+ " ,to_char(A.COLLECTR+1)   "
				+ " ,to_char(A.JOINR) "
				+ " ,DECODE('"
				+ ageYear
				+ "','10','10','20','11')  "
				+ " ,A.UPD_DATE "
				+ " FROM    HRPVF.PF_MEMBER A , PN_EMPLOYEE  E "
				+ " ,PN_ORGANIZATION C "
				+ " WHERE C.OU_CODE = '"
				+ ouCode
				+ "' "
				+ " AND C.INACTIVE = 'N' "
				+ " AND E.OU_CODE = C.OU_CODE "
				+ " AND E.CODE_SEQ_ACT = C.CODE_SEQ "
				+ " AND A.OU_CODE = E.OU_CODE "
				+ " AND A.EMP_CODE = E.EMP_CODE "
				+ " AND E.EMP_STATUS ='B' "
				+ " AND (  (TO_NUMBER('"
				+ ageYear
				+ "' ) = 10  AND  A.JOINR = 9          AND (TRUNC(MONTHS_BETWEEN(TO_DATE('"
				+ dDate
				+ "','DD/MM/YYYY'),A.CAT_DATE)/12)) >= 10 "
				+ " AND (TRUNC(MONTHS_BETWEEN(TO_DATE('"
				+ dDate
				+ "','DD/MM/YYYY'),A.CAT_DATE)/12)) <   20 )  OR "
				+ "(TO_NUMBER('"
				+ ageYear
				+ "' ) = 20  AND  A.JOINR IN(9,10)  AND (TRUNC(MONTHS_BETWEEN(TO_DATE('"
				+ dDate
				+ "','DD/MM/YYYY'),A.CAT_DATE)/12)) >= 20)  ) "
				+ " AND A.MEMBER_STATUS = 'B' "
				+ " group by  A.OU_CODE,C.DIV_DESC "
				+ " ,NVL(C.AREA_SEQ,0) "
				+ " ,NVL(C.SEC_DESC,C.AREA_DESC)"
				+ " ,C.SEC_CODE "
				+ " ,C.SEC_DESC "
				+ " ,NVL(C.SEC_SEQ,0) "
				+ " ,NVL(C.WORK_SEQ,0) "
				+ " ,C.WORK_DESC "
				+ " ,C.ZIP_CODE "
				+ " ,E.EMP_CODE||'/'||E.GWORK_CODE "
				+ " ,HRPVF.PN_GET_NAME(A.EMP_CODE,A.OU_CODE ) "
				+ " ,A.EMP_CODE,E.LEVEL_CODE ,"
				+ " E.EMP_STATUS , "
				+ " A.CAT_DATE, "
				+ " DECODE('"
				+ ageYear
				+ "','10', ADD_MONTHS (A.CAT_DATE, 10 * 12),ADD_MONTHS(A.CAT_DATE,20*12) ) ,"
				+ " to_char(A.COLLECTR), "
				+ " to_char(A.JOINR), "
				+ " A.UPD_DATE, "
				+ " DECODE('"
				+ ageYear
				+ "','10','10','20','11') ,"
				+ " to_char(A.COLLECTR+1) , "
				+ " Pn_Desc.get_emp_position_short('"
				+ ouCode
				+ "' ,A.EMP_CODE) ||'/'||TO_NUMBER(E.LEVEL_CODE) "
				+ " ORDER BY nvl(AREA_SEQ,0),nvl( SEC_SEQ,0),nvl(WORK_SEQ,0),A.EMP_CODE ";
		System.out.println(sql);
		resultList = this.jdbcTemplate.queryForList(sql);
		for (int i = 0; i < resultList.size(); i++) {
			ListOrderedMap map = (ListOrderedMap) resultList.get(i);
			String ouId = (String) map.getValue(0);
			String divDesc = (String) map.getValue(1);
			String secDesc = (String) map.getValue(2);
			String workDesc = (String) map.getValue(3);
			String zipCode = (String) map.getValue(4);
			String empCode = (String) map.getValue(5);
			String fullName = (String) map.getValue(6);
			String position = (String) map.getValue(7);
			Date iDate = (Date) map.getValue(8);
			Date duDate = (Date) map.getValue(9);
			String oldC = (String) map.getValue(10);
			String newC = (String) map.getValue(11);
			String oldJ = (String) map.getValue(12);
			String newJ = (String) map.getValue(13);

			SrPvfEmpVO vo = new SrPvfEmpVO();
			vo.setOuCode(ouId);
			vo.setDivDesc(divDesc);
			vo.setSecDesc(secDesc);
			vo.setWorkDesc(workDesc);
			vo.setZipCode(zipCode);
			vo.setEmpCode(empCode);
			vo.setFullName(fullName);
			vo.setPosition(position);
			vo.setiDate(iDate);
			vo.setdDate(duDate);
			vo.setOldC(oldC);
			vo.setNewC(newC);
			vo.setOldJ(oldJ);
			vo.setNewJ(newJ);

			returnList.add(vo);

		}
		return returnList;

	}

	public List CTPFRP070(String dDate) {
		// TODO Auto-generated method stub
		List resultList = new ArrayList();
		List returnList = new ArrayList();
		String sql = "";
		sql = " SELECT  HIS.EMP_CODE , "
				+ " HRPVF.GET_PRE_NAME(EMP.PRE_NAME)||' '||EMP.FIRST_NAME||' '||EMP.LAST_NAME , "
				+ " ORG.DIV_SHORT ,ORG.ZIP_CODE , "
				+ " M.CAT_DATE IDATE, "
				+ " to_char(HIS.COLLECTR) OLDC, "
				+ " to_char(HIS.NCOLLECTR) NEWC, "
				+ " to_char(HIS.JOINR) OLDJ, "
				+ " to_char(HIS.NJOINR) NEWJ, "
				+ " HIS.RESULT_DATE DDATE, "
				+ " HIS.UPD_DATE "
				+ " FROM     HRPVF.PF_CHANGE_RATE_HISTORY HIS,PN_EMPLOYEE EMP,PN_ORGANIZATION ORG,HRPVF.PF_MEMBER M "
				+ " WHERE  HIS.EMP_CODE               =             EMP.EMP_CODE "
				+ " AND        HIS.OU_CODE                 =             EMP.OU_CODE "
				+ " AND        HIS.EMP_CODE               =             M.EMP_CODE "
				+ " AND        HIS.OU_CODE                  =            M.OU_CODE "
				+ " AND        EMP.CODE_SEQ_ACT     =             ORG.CODE_SEQ "
				+ " AND        EMP.OU_CODE                =             ORG.OU_CODE "
				+ " AND        HIS.CONDITION               =             2"
				+ " AND        HIS.RESULT_DATE         =   TO_DATE('" + dDate + "','DD/MM/YYYY') "
				+ " AND        M.MEMBER_STATUS      =             'B'"
				+ " ORDER BY M.EMP_CODE ";
		System.out.println(sql);
		resultList = this.jdbcTemplate.queryForList(sql);
		for (int i = 0; i < resultList.size(); i++) {
			ListOrderedMap map = (ListOrderedMap) resultList.get(i);
			String empCode = (String) map.getValue(0);
			String fullName = (String) map.getValue(1);
			String divDesc = (String) map.getValue(2);
			String zipCode = (String) map.getValue(3);
			Date iDate = (Date) map.getValue(4);
			String oldC = (String) map.getValue(5);
			String newC = (String) map.getValue(6);
			String oldJ = (String) map.getValue(7);
			String newJ = (String) map.getValue(8);
			Date duDate = (Date) map.getValue(9);
			Date uDate = (Date) map.getValue(10);
			System.out.println(empCode);
			SrPvfEmpVO vo = new SrPvfEmpVO();
			vo.setEmpCode(empCode);
			vo.setFullName(fullName);
			vo.setDivDesc(divDesc);
			vo.setZipCode(zipCode);
			vo.setiDate(iDate);
			vo.setOldC(oldC);
		    vo.setNewC(newC);
			vo.setOldJ(oldJ);
			vo.setNewJ(newJ);
			vo.setdDate(duDate);
			vo.setUpdDate(uDate);
            
			returnList.add(vo);

		}
		return returnList;
	}

	public List CTPFRP029(String ouCode,String dDate) {
		// TODO Auto-generated method stub
		List resultList = new ArrayList();
		List returnList = new ArrayList();
		String sql = "";
		sql = " SELECT PN_EMPLOYEE.EMP_CODE, PN_DESC.GET_EMP_NAME(PN_EMPLOYEE.OU_CODE, PN_EMPLOYEE.EMP_CODE) FULLNAME, "
				+ " PN_ORGANIZATION.DIV_DESC,NVL(PN_ORGANIZATION.SEC_DESC,PN_ORGANIZATION.AREA_DESC) SEC_T,PN_ORGANIZATION.WORK_DESC,PN_ORGANIZATION.ZIP_CODE "
				+ " FROM         PN_EMPLOYEE, PN_ORGANIZATION "
				+ " WHERE     PN_EMPLOYEE.OU_CODE           = '"
				+ ouCode
				+ "' "
				+ " AND           PN_EMPLOYEE.FUND_STATUS IS NULL "
				+ " AND           PN_EMPLOYEE.EMP_STATUS = 'B' "
				+ " AND            PN_EMPLOYEE.EMP_CODE IN (SELECT E.EMP_CODE FROM PN_EMP_DATA_STATUS E WHERE E.STATUS_DATE =  TO_DATE('" + dDate + "','DD/MM/YYYY') group by E.EMP_CODE  ) "
				+ " AND            PN_EMPLOYEE.EMP_CODE NOT IN (SELECT PVF.EMP_CODE FROM HRPVF.PF_MEMBER PVF WHERE PVF.MEMBER_STATUS IS NOT NULL ) "
				+ " AND           PN_ORGANIZATION.OU_CODE   = PN_EMPLOYEE.OU_CODE "
				+ " AND           PN_ORGANIZATION.CODE_SEQ = PN_EMPLOYEE.CODE_SEQ "
				+ " AND           PN_ORGANIZATION.INACTIVE   = 'N' "
				+ " ORDER BY    pn_organization.div_seq, NVL(PN_ORGANIZATION.AREA_SEQ,0) , "
				+ "         NVL(PN_ORGANIZATION.SEC_SEQ,0),NVL(PN_ORGANIZATION.WORK_SEQ,0) ,PN_EMPLOYEE.EMP_CODE ";
		System.out.println(sql);
		resultList = this.jdbcTemplate.queryForList(sql);
		for (int i = 0; i < resultList.size(); i++) {
			ListOrderedMap map = (ListOrderedMap) resultList.get(i);
			String empCode = (String) map.getValue(0);
			String fullName = (String) map.getValue(1);
			String divDesc = (String) map.getValue(2);
			String secDesc = (String) map.getValue(3);
			String workDesc = (String) map.getValue(4);
			String zipCode = (String) map.getValue(5);

			SrPvfEmpVO vo = new SrPvfEmpVO();
			vo.setEmpCode(empCode);
			vo.setFullName(fullName);
			vo.setDivDesc(divDesc);
			vo.setSecDesc(secDesc);
			vo.setWorkDesc(workDesc);
			vo.setZipCode(zipCode);

			returnList.add(vo);

		}
		return returnList;
	}

	public List CTPFRP019(String dDate) {
		// TODO Auto-generated method stub
		List resultList = new ArrayList();
		List returnList = new ArrayList();
		String sql = "";
		sql = "	SELECT  REQ.EMP_CODE, "
				+ " HRPVF.PN_GET_NAME(REQ.EMP_CODE,REQ.OU_CODE) FULL_NAME, "
				+ "  ORG.DIV_DESC, "
				+ " NVL(ORG.SEC_DESC,ORG.AREA_DESC)  SEC_DESC, "
				+ " ORG.WORK_DESC ,ORG.ZIP_CODE , "
				+ " M.CAT_DATE IDATE, "
				+ " decode(REQ.MASTER,'1','������˹�� 100%','3','���ŧ�ع㹵����÷ع 20%','4','���ŧ�ع㹵����÷ع 30%') OLDP, "
				+ " decode(REQ.NMASTER,'1','������˹�� 100%','3','���ŧ�ع㹵����÷ع 20%','4','���ŧ�ع㹵����÷ع 30%') NEWP, "
				+ " REQ.RESULT_DATE    DDATE "
				+ " FROM     HRPVF.PF_REQUEST_CHANGE_MASTER REQ,PN_EMPLOYEE EMP,PN_ORGANIZATION ORG,HRPVF.PF_MEMBER M "
				+ " WHERE  REQ.EMP_CODE                =             EMP.EMP_CODE "
				+ " AND        REQ.OU_CODE                  =             EMP.OU_CODE "
				+ " AND        REQ.EMP_CODE                =             M.EMP_CODE "
				+ " AND        REQ.OU_CODE                  =             M.OU_CODE "
				+ " AND        EMP.CODE_SEQ_ACT       =             ORG.CODE_SEQ "
				+ " AND        EMP.OU_CODE                  =             ORG.OU_CODE "
				+ " AND        REQ.RESULT_DATE =             TO_DATE('"	+ dDate	+ "','DD/MM/YYYY')  "
				+ " ORDER BY " 
				+ "ORG.DIV_SEQ, "
				+ "       NVL(ORG.AREA_SEQ,0) , "
				+ "        NVL(ORG.SEC_SEQ,0) , "
				+ "         NVL(ORG.WORK_SEQ,0) , "
				+ " REQ.RESULT_DATE,REQ.EMP_CODE ";
		System.out.println(sql);
		resultList = this.jdbcTemplate.queryForList(sql);
		for (int i = 0; i < resultList.size(); i++) {
			ListOrderedMap map = (ListOrderedMap) resultList.get(i);
			String empCode = (String) map.getValue(0);
			String fullName = (String) map.getValue(1);
			String divDesc = (String) map.getValue(2);
			String secDesc = (String) map.getValue(3);
			String workDesc = (String) map.getValue(4);
			String zipCode = (String) map.getValue(5);
			Date iDate = (Date) map.getValue(6);
			String oldP = (String) map.getValue(7);
			String newP = (String) map.getValue(8);
			Date duDate = (Date) map.getValue(9);

			SrPvfEmpVO vo = new SrPvfEmpVO();
			vo.setEmpCode(empCode);
			vo.setFullName(fullName);
			vo.setDivDesc(divDesc);
			vo.setSecDesc(secDesc);
			vo.setWorkDesc(workDesc);
			vo.setZipCode(zipCode);
			vo.setiDate(iDate);
			vo.setOldP(oldP);
			vo.setNewP(newP);
			vo.setdDate(duDate);

			returnList.add(vo);

		}
		return returnList;
	}

	public List CTPFRP034(String dDate) {
		// TODO Auto-generated method stub
		List resultList = new ArrayList();
		List returnList = new ArrayList();
		String sql = "";
		sql = "	SELECT PF.EMP_CODE, "
				+ " HRPVF.PN_GET_NAME(PF.EMP_CODE, PF.OU_CODE) FULLNAME, "
				+ " PN_ORGANIZATION.DIV_CODE, PN_ORGANIZATION.DIV_SHORT, PN_ORGANIZATION.SEC_DESC, "
				+ " decode(PF.MASTER,'1','������˹��','3','��� 20%','4','��� 30%') MASTER,pn_organization.zip_code,PF.MDATE "
				+ " FROM           HRPVF.PF_MEMBER PF,  PN_EMPLOYEE, PN_ORGANIZATION "
				+ " WHERE       MDATE = TO_DATE('"+ dDate + "','DD/MM/YYYY')  "
				+ " AND              PF.OU_CODE =  PN_EMPLOYEE.OU_CODE "
				+ " AND              PF.EMP_CODE = PN_EMPLOYEE.EMP_CODE "
				+ " AND              PN_ORGANIZATION.OU_CODE =  PN_EMPLOYEE.OU_CODE "
				+ " AND              PN_EMPLOYEE.CODE_SEQ_ACT = PN_ORGANIZATION.CODE_SEQ "
				+ " ORDER BY   PF.MEMBER_STATUS, PF.EMP_CODE ";
		System.out.println(sql);
		resultList = this.jdbcTemplate.queryForList(sql);
		for (int i = 0; i < resultList.size(); i++) {
			ListOrderedMap map = (ListOrderedMap) resultList.get(i);
			String empCode = (String) map.getValue(0);
			String fullName = (String) map.getValue(1);
			String divCode = (String) map.getValue(2);
			String divDesc = (String) map.getValue(3);
			String secDesc = (String) map.getValue(4);
			String master = (String) map.getValue(5);
			String zipCode = (String) map.getValue(6);
			Date mDate = (Date) map.getValue(7);

			SrPvfEmpVO vo = new SrPvfEmpVO();
			vo.setEmpCode(empCode);
			vo.setFullName(fullName);
			vo.setDivCode(divCode);
			vo.setDivDesc(divDesc);
			vo.setSecDesc(secDesc);
			vo.setMaster(master);
			vo.setZipCode(zipCode);
			vo.setdDate(mDate);

			returnList.add(vo);
		}
		return returnList;
	}
	
	public List CTPFRP001(String ouCode, String status) {
		// TODO Auto-generated method stub
		List resultList = new ArrayList();
		List returnList = new ArrayList();
		String sql = "";
		sql = "	SELECT PF.EMP_CODE, "
				+ " HRPVF.PN_GET_NAME(PF.EMP_CODE, PF.OU_CODE) FULLNAME, "
				+ " PN_ORGANIZATION.DIV_CODE, PN_ORGANIZATION.DIV_SHORT, PN_ORGANIZATION.SEC_DESC, "
				+ " decode(PF.MASTER,'1','������˹��','3','��� 20%','4','��� 30%') MASTER,pn_organization.zip_code, "
				+ " Pn_Desc.get_emp_position_short(PF.OU_CODE,PF.EMP_CODE) ||' '||TO_NUMBER(PN_EMPLOYEE.LEVEL_CODE), "
				+ " to_char(PF.COLLECTR) "
				+ " ,to_char(PF.JOINR) "
				+ " ,PF.CAT_DATE "
				+ " ,PF.MDATE"
				+ " FROM           HRPVF.PF_MEMBER PF,  PN_EMPLOYEE, PN_ORGANIZATION "
				+ " WHERE   PF.OU_CODE = '"+ ouCode + "' "
				+ " AND              PN_EMPLOYEE.OU_CODE =  '"+ ouCode + "' "
				+ " AND              PF.EMP_CODE = PN_EMPLOYEE.EMP_CODE "
				+ " AND              PF.OU_CODE = PN_EMPLOYEE.OU_CODE "
				+ " AND              PN_ORGANIZATION.OU_CODE =  '"+ ouCode + "' "
				+ " AND              PF.MEMBER_STATUS =  '"+ status + "' "
				+ " AND              PN_EMPLOYEE.OU_CODE = PN_ORGANIZATION.OU_CODE "
				+ " AND              PN_EMPLOYEE.CODE_SEQ_ACT = PN_ORGANIZATION.CODE_SEQ "
				+ " ORDER BY   PF.MEMBER_STATUS, PF.EMP_CODE ";
		System.out.println(sql);
		resultList = this.jdbcTemplate.queryForList(sql);
		for (int i = 0; i < resultList.size(); i++) {
			ListOrderedMap map = (ListOrderedMap) resultList.get(i);
			String empCode = (String) map.getValue(0);
			String fullName = (String) map.getValue(1);
			String divCode = (String) map.getValue(2);
			String divDesc = (String) map.getValue(3);
			String secDesc = (String) map.getValue(4);
			String master = (String) map.getValue(5);
			String zipCode = (String) map.getValue(6);
			String position = (String) map.getValue(7);
			String newC = (String) map.getValue(8);
			String newJ = (String) map.getValue(9);
			Date iDate = (Date) map.getValue(10);
			Date dDate = (Date) map.getValue(11);

			SrPvfEmpVO vo = new SrPvfEmpVO();
			vo.setEmpCode(empCode);
			vo.setFullName(fullName);
			vo.setDivCode(divCode);
			vo.setDivDesc(divDesc);
			vo.setSecDesc(secDesc);
			vo.setMaster(master);
			vo.setZipCode(zipCode);
			vo.setPosition(position);
			vo.setNewC(newC);
			vo.setNewJ(newJ);
			vo.setiDate(iDate);
			vo.setdDate(dDate);
			
			

			returnList.add(vo);
		}
		return returnList;
	}

	public List findLastDate(String ouCode)
			throws Exception {
		
		StringBuffer hql = new StringBuffer(0);
		hql.append("select to_char(a.lDate,'dd/mm/yyyy') from VPvfLastDate a ");
		hql.append("order by a.lDate desc ");

		
		List ls = this.getHibernateTemplate().find(hql.toString());
	    System.out.println(ls);
		return ls;
	}
	
	public List CTWLRP009N(String mDate) {
		// TODO Auto-generated method stub
		List resultList = new ArrayList();
		List returnList = new ArrayList();
		String sql = "";
		/*sql = "	SELECT PF.EMP_CODE, "
				+ " HRPVF.PN_GET_NAME(PF.EMP_CODE, PF.OU_CODE) FULLNAME, "
				+ " PN_ORGANIZATION.DIV_CODE, PN_ORGANIZATION.DIV_SHORT, PN_ORGANIZATION.SEC_DESC, "
				+ " decode(PF.MASTER,'1','������˹��','3','��� 20%','4','��� 30%') MASTER,pn_organization.zip_code,PF.MDATE "
				+ " FROM           HRPVF.PF_MEMBER PF,  PN_EMPLOYEE, PN_ORGANIZATION "
				+ " WHERE       TO_CHAR(FIRST_PERIOD = TO_CHAR(TO_DATE('"+ fDate + "','MM/YYYY'))  "
				+ " AND              PF.OU_CODE =  PN_EMPLOYEE.OU_CODE "
				+ " AND              PF.EMP_CODE = PN_EMPLOYEE.EMP_CODE "
				+ " AND              PN_ORGANIZATION.OU_CODE =  PN_EMPLOYEE.OU_CODE "
				+ " AND              PN_EMPLOYEE.CODE_SEQ_ACT = PN_ORGANIZATION.CODE_SEQ "
				+ " ORDER BY   PF.MEMBER_STATUS, PF.EMP_CODE ";
		*/
		sql = " select  wl.emp_code emp_codeN,"
			  + " hrwfl.get_emp_name (wl.ou_code, wl.emp_code) full_nameN ,"
		      + " hrwfl.get_div_sect_name(wl.ou_code,wl.emp_code) div_descN ," 
			  + " WL.LOAN_CODE||'/'||WL.LOAN_DIVISION||'/'||WL.LOAN_NO||'/'||WL.LOAN_YEAR contact_no," 
		      + " hrwfl.get_div_name_short(wl.ou_code,wl.emp_code) div_shortN," 
			  + " hrwfl.get_sect_name(wl.ou_code,wl.emp_code) sec_descN , "
			  + " hrwfl.get_sect_zip(wl.ou_code,wl.emp_code) sec_zipN , "
		      + " to_char(WL.LOAN_PERIOD) period, " 
			  + " to_char(wl.loan_balance) loan_price, "
		      + " to_char(WL.INSTALL_AMOUNT) loan_rate,"
			  + " WG.EMP_CODE emp_codeG, " 
		      + " hrwfl.get_emp_name(wg.ou_code,wg.emp_code) full_nameG," 
			  + " hrwfl.get_sect_name(wg.ou_code,wg.emp_code) div_descG," 
		      + " hrwfl.get_div_name_short(wg.ou_code,wg.emp_code) div_shortG," 
			  + " to_char(WL.FIRST_PERIOD,'MM/YYYY') first_date" 
		      + " from hrwfl.wl_new_loan wl,hrwfl.wl_new_guarantor wg" 
			  + " where WL.OU_CODE = WG.OU_CODE" 
		      + " and WL.loan_no = WG.loan_no " 
			  + " and WL.LOAN_CODE = wg.loan_code" 
		      + " AND WG.LOAN_CODE ='09'" 
			  + " and WL.LOAN_YEAR = WG.LOAN_YEAR" 
		      + " and WL.FIRST_PERIOD =  TO_DATE('"	+ mDate	+ "','MM/YYYY')  "
		      + " order by wl.loan_no";
		      
		System.out.println(sql);
		resultList = this.jdbcTemplate.queryForList(sql);
		for (int i = 0; i < resultList.size(); i++) {
			ListOrderedMap map = (ListOrderedMap) resultList.get(i);
			String empCodeN = (String) map.getValue(0);
			String fullNameN = (String) map.getValue(1);
			String divDescN = (String) map.getValue(2);
			String contactNo = (String) map.getValue(3);
			String divShortN = (String) map.getValue(4);
			String secDescN = (String) map.getValue(5);
			String secZipN = (String) map.getValue(6);
			String period = (String) map.getValue(7); 
			String loanPrice = (String) map.getValue(8);
			String loanRate = (String) map.getValue(9);
			String empCodeG = (String) map.getValue(10);
			String fullNameG = (String) map.getValue(11);
			String divDescG = (String) map.getValue(12);
			String divShortG = (String) map.getValue(13);
			String firstDate = (String) map.getValue(14);
			//Date fDate = (Date) map.getValue(13);

			SrWflEmp09VO vo = new SrWflEmp09VO();
			
			vo.setEmpCodeN(empCodeN);
			vo.setFullNameN(fullNameN);
			vo.setDivDescN(divDescN);
			vo.setContactNo(contactNo);
			vo.setDivShortN(divShortN);
			vo.setSecDescN(secDescN);
			vo.setSecZipN(secZipN);
			vo.setPeriod(period);
			vo.setLoanPrice(loanPrice);
			vo.setLoanRate(loanRate);
			vo.setEmpCodeG(empCodeG);
			vo.setFullNameG(fullNameG);
			vo.setDivDescG(divDescG);
			vo.setDivShortG(divShortG);
			vo.setFirstDate(firstDate);
			returnList.add(vo);
		}
		return returnList;
	}
	public List glTransferReport(String evaYear,String evaMonth,String evaAccDate) {
		
		StringBuffer criteria = new StringBuffer();
		
	

		if (evaYear != null && !evaYear.equals("")) {
			criteria.append(" where rwPre.yearPr = ");
			criteria.append(evaYear);
		}

		if (evaMonth != null && !evaMonth.equals("")) {
			criteria.append(" and rwPre.monthPr = ");
			criteria.append(evaMonth);
		}
		
		if (evaAccDate != null && !evaAccDate.equals("")) {
			criteria.append(" and rwPre.evaAccDate = ");
			criteria.append(evaAccDate);
		}
		
		
		

		StringBuffer hql = new StringBuffer();

		hql.append(" select rwPre.seq, rwPre.yearPr,");
		hql.append(" rwPre.monthPr, ");
		hql.append(" rwPre.accountingDate, ");
		hql.append(" rwPre.accountCode, ");
		hql.append(" rwPre.accountName, ");
		hql.append(" rwPre.debit, ");
		hql.append(" rwPre.credit ");
		hql.append(" from VPrGlTransferRep rwPre ");
		hql.append(criteria);
		// hql.append(" and rwPre.creBy = '" + userId + "' ");
		hql.append(" order by rwPre.yearPr,rwPre.monthPr,rwPre.seq,rwPre.accountCode ");

		System.out.println("HQL GlTransferReport ==> " + hql.toString());

		List empList = this.getSession().createQuery(hql.toString()).list();
		List retList = new ArrayList();

		for (Iterator it = empList.iterator(); it.hasNext();) {
			Object[] r = (Object[]) it.next();
			Double seq = (Double) r[0];
			Double yearPr = (Double) r[1];
			Double monthPr = (Double) r[2];
			Date accountingDate = (Date) r[3];
			String accountCode = (String) r[4];
			String accountName = (String) r[5];
			Double debit = (Double) r[6];
			Double credit = (Double) r[7];
			
			
			VPrGlTransferVO ret = new VPrGlTransferVO();
			ret.setSeq(seq);
			ret.setYearPr(yearPr);
			ret.setMonthPr(monthPr);
			ret.setAccountingDate(accountingDate);
			ret.setAccountCode(accountCode);
			ret.setAccountName(accountName);
			ret.setDebit(debit);
			ret.setCredit(credit);
			
			
			
			retList.add(ret);
		}
		return retList;
	}
	public List decOutAllAmtReport(String userId, String evaOuCode, Long evaYear,
			Long evaPeriod, String incDecCode) {
		StringBuffer criteria = new StringBuffer();

		if (evaYear != null && !evaYear.equals("")) {
			criteria.append(" and rwOth.workYear = ");
			criteria.append(evaYear);
		}

		if (evaPeriod != null && !evaPeriod.equals("")) {
			criteria.append(" and rwOth.workPeriod = ");
			criteria.append(evaPeriod);
		}

		if (incDecCode != null && !incDecCode.equals("%")) {
			criteria.append(" and rwOth.incDecCode = '");
			criteria.append(incDecCode);
			criteria.append("'");
		}

		

		StringBuffer hql = new StringBuffer();

		hql.append(" select rwOth.incDecCode,prInc.incDecName,rwOth.loanBalance,rwOth.interest, ");
		hql.append(" rwOth.contractNo,rwOth.empCode, rwOth.name,");
		hql.append(" rwOth.secDesc  ");
		//hql.append(" rwOth.orgCode||' '||rwOth.divDesc||' '||nvl(rwOth.secDesc,rwOth.areaDesc)||' '||rwOth.workDesc");
		hql.append(" from VWlrp0062 rwOth , PrIncomeDeduct prInc ");
		hql.append(" where prInc.pk.ouCode = '");
		hql.append(evaOuCode);
		hql.append("' ");
		hql.append(criteria);
		//hql.append(" and rwOth.ouCode = prInc.pk.ouCode ");
		//hql.append(" and rwOth.empCode = '271884' ");
		//hql.append(" and rwOth.groupCode = prInc.pk.groupCode ");
		hql.append(" and rwOth.incDecCode = prInc.pk.incDecCode ");
		// hql.append(" order by rwOth.incDecCode,
		// nvl(pnOrg.secCode,pnOrg.divCode)||' '||pnOrg.divDesc||'
		// '||pnOrg.secDesc,rwOth.empCode ");
		hql.append(" order by rwOth.contractNo,rwOth.empCode ");
		System.out.println("HQL findByCriteria ==> " + hql.toString());

		List empList = this.getSession().createQuery(hql.toString()).list();
		List retList = new ArrayList();

		for (Iterator it = empList.iterator(); it.hasNext();) {
			Object[] r = (Object[]) it.next();
			String incDec = (String) r[0];
			String incDecName = (String) r[1];
			Double loanBalance = (Double) r[2];
			Double interest = (Double) r[3];
			
     		String contractNo = (String) r[4];
			String empCode = (String) r[5];
			String name = (String) r[6];
			String secDesc = (String) r[7];
			

			VWlrp0062VO ret = new VWlrp0062VO();
			ret.setIncDecCode(incDec);
			ret.setIncDecName(incDecName);
			ret.setLoanBalance(loanBalance);
			ret.setInterest(interest);
			ret.setContractNo(contractNo);
			ret.setEmpCode(empCode);
			ret.setName(name);
			ret.setSecDesc(secDesc);
			
			
			retList.add(ret);
		}
		return retList;
	}

	public List decOutAllAmtReportCountSheet(String userId, String evaOuCode,
			Long evaYear, Long evaPeriod, String incDecCode) {
		StringBuffer criteria = new StringBuffer();

		if (evaYear != null && !evaYear.equals("")) {
			criteria.append(" and rwOth.workYear = ");
			criteria.append(evaYear);
		}

		if (evaPeriod != null && !evaPeriod.equals("")) {
			criteria.append(" and rwOth.workPeriod = ");
			criteria.append(evaPeriod);
		}

		if (incDecCode != null && !incDecCode.equals("%")) {
			criteria.append(" and rwOth.incDecCode = '");
			criteria.append(incDecCode);
			criteria.append("'");
		}

		

		StringBuffer hql = new StringBuffer();

		hql.append(" select distinct(rwOth.incDecCode),prInc.incDecName");
		hql.append(" from  VWlrp0062 rwOth ,PrIncomeDeduct prInc");
		hql.append(" where prInc.pk.ouCode = '");
		hql.append(evaOuCode);
		hql.append("' ");
		hql.append(criteria);
		//hql.append(" and rwOth.ouCode = prInc.pk.ouCode ");
		//hql.append(" and rwOth.groupCode = prInc.pk.groupCode ");
		hql.append(" and rwOth.incDecCode = prInc.pk.incDecCode ");
		hql.append(" order by rwOth.incDecCode ");

		System.out.println("HQL findByCriteria ==> " + hql.toString());

		List empList = this.getSession().createQuery(hql.toString()).list();
		List retList = new ArrayList();

		for (Iterator it = empList.iterator(); it.hasNext();) {
			Object[] r = (Object[]) it.next();
			String incDec = (String) r[0];
			String incDecName = (String) r[1];

			VWlrp0062VO ret = new VWlrp0062VO();
			ret.setIncDecCode(incDec);
			ret.setIncDecName(incDecName);
			System.out.println(ret.getIncDecCode());
			System.out.println(ret.getIncDecName());
			
			retList.add(ret);
		}
		return retList;
	}
}
