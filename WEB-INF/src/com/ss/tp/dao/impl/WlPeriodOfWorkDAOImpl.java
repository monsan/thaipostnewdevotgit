package com.ss.tp.dao.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.ss.tp.dao.WlPeriodOfWorkDAO;
import com.ss.tp.dto.PrPeriodLineVO;
import com.ss.tp.model.WlPeriodOfWork;

public class WlPeriodOfWorkDAOImpl extends HibernateDaoSupport implements WlPeriodOfWorkDAO, Serializable {

	private Log logger = LogFactory.getLog(this.getClass());

	public WlPeriodOfWork findPeriodLine(String ouCode, String year, String period)
			throws Exception {

		StringBuffer sql = new StringBuffer(0);
		// sql.append(" SELECT l.periodName,l.pk.period");
		sql.append(" FROM WlPeriodOfWork l ");
		sql.append(" WHERE l.pk.ouCode = '" + ouCode + "' ");
		sql.append(" AND l.pk.workYear = " + year);
		sql.append(" AND l.pk.workPeriod	= " + period);

		List ls = this.getHibernateTemplate().find(sql.toString());

		if (ls != null && ls.size() > 0) {
			return (WlPeriodOfWork) ls.get(0);
		} else
			return null;
	}
	
	public WlPeriodOfWork findPeriodLineNext(String ouCode, String year, String period)
	throws Exception {
		Double d = new Double(period);
		Double y = new Double(year);
		int ye = 0;
		int yd = (y.intValue());
		int m = (d.intValue()) +1;
		if (m > 24)  {
			m=1;
			ye= yd + 1;
		}else {
			
			ye=yd;
		}
StringBuffer sql = new StringBuffer(0);
// sql.append(" SELECT l.periodName,l.pk.period");
sql.append(" FROM WlPeriodOfWork l ");
sql.append(" WHERE l.pk.ouCode = '" + ouCode + "' ");
sql.append(" AND l.pk.workYear = '" + ye + "'");
sql.append(" AND l.pk.workPeriod	= '" + m + "'");




List ls = this.getHibernateTemplate().find(sql.toString());

if (ls != null && ls.size() > 0) {
	return (WlPeriodOfWork) ls.get(0);
} else
	return null;
}
	
	public WlPeriodOfWork findPeriodLineMod(String ouCode, String year, String period)
			throws Exception {
		StringBuffer sql = new StringBuffer(0);
		// sql.append(" SELECT l.periodName,l.pk.period");
		sql.append(" FROM WlPeriodOfWork l ");
		sql.append(" WHERE l.pk.ouCode = '" + ouCode + "' ");
		sql.append(" AND l.pk.workYear = " + year);
		sql.append(" AND l.pk.workperiod	= " + period);


		List ls = this.getHibernateTemplate().find(sql.toString());

		if (ls != null && ls.size() > 0) {
			return (WlPeriodOfWork) ls.get(0);
		} else
			return null;
		}
	
	

	public List findYearInPeriodLine(String ouCode) throws Exception {

		StringBuffer sql = new StringBuffer(0);
		sql.append(" SELECT distinct l.pk.ouCode,l.pk.workYear");
		sql.append(" FROM WlPeriodOfWork l ");
		sql.append(" WHERE l.pk.ouCode = '" + ouCode + "' ");
		sql.append(" ORDER BY  l.pk.workYear");

		List yearList = this.getSession().createQuery(sql.toString()).list();
		List retList = new ArrayList();
		System.out.println(yearList);
		for (Iterator it = yearList.iterator(); it.hasNext();) {
			Object[] r = (Object[]) it.next();
			Double year = (Double) r[1];

			PrPeriodLineVO ret = new PrPeriodLineVO();
			ret.setOuCode(ouCode);
			ret.setYear(year);
			retList.add(ret);
		}
		return retList;
	}

	public boolean canDeleteData(String ouCode, String year, String period)
			throws Exception {

		StringBuffer sql = new StringBuffer(0);
		sql.append(" SELECT count(*) ");
		sql.append(" FROM WlPeriodOfWork l ");
		sql.append(" WHERE l.pk.ouCode = '" + ouCode + "' ");
		sql.append(" AND l.pk.workYear = " + year);
		sql.append(" AND l.pk.workPeriod = " + period);
		sql.append(" AND l.periodStatus = '2' ");

		List ls = this.getSession().createQuery(sql.toString()).list();

		// System.out.println("list of can delete data : " + ls.size());

		if (ls != null && ls.size() > 0) {
			Integer i = (Integer) ls.get(0);

			if (i.intValue() > 0)
				return true;
			else
				return false;
		} else
			return false;
	}

	public boolean isCloseTranClose(String ouCode, String year, String period)
			throws Exception {

		StringBuffer sql = new StringBuffer(0);
		sql.append(" SELECT count(*) ");
		sql.append(" FROM WlPeriodOfWork l ");
		sql.append(" WHERE l.pk.ouCode = '" + ouCode + "' ");
		sql.append(" AND l.pk.workYear = " + year);
		sql.append(" AND l.pk.workPeriod = " + period);
		sql.append(" AND l.periodStatus = '1' ");

		List ls = this.getSession().createQuery(sql.toString()).list();

		if (ls != null && ls.size() > 0) {
			Integer i = (Integer) ls.get(0);

			if (i.intValue() > 0)
				return true;
			else
				return false;
		} else
			return false;
	}
	public boolean isCloseMasterClose(String ouCode, String year, String period)
			throws Exception {

		StringBuffer sql = new StringBuffer(0);
		sql.append(" SELECT count(*) ");
		sql.append(" FROM WlPeriodOfWork l ");
		sql.append(" WHERE l.pk.ouCode = '" + ouCode + "' ");
		sql.append(" AND l.pk.workYear = " + year);
		sql.append(" AND l.pk.workPeriod = " + period);
		sql.append(" AND l.periodStatus = '1' ");

		List ls = this.getSession().createQuery(sql.toString()).list();

		if (ls != null && ls.size() > 0) {
			Integer i = (Integer) ls.get(0);

			if (i.intValue() > 0)
				return true;
			else
				return false;
		} else
			return false;
	}

	public List findPeriodInPeriodLine(String ouCode, Double year)
			throws Exception {

		StringBuffer sql = new StringBuffer(0);
		sql.append(" SELECT l.pk.ouCode,l.pk.workYear,l.pk.workPeriod,l.pk.workPeriod ");
		sql.append(" FROM WlPeriodOfWork l ");
		sql.append(" WHERE l.pk.ouCode = '" + ouCode + "' ");
		sql.append(" AND l.pk.workYear = " + year);
		sql.append(" ORDER BY  l.pk.workPeriod");

		List yearList = this.getSession().createQuery(sql.toString()).list();
		List retList = new ArrayList();

		for (Iterator it = yearList.iterator(); it.hasNext();) {
			Object[] r = (Object[]) it.next();
			Double period = (Double) r[2];
			Double periodT = (Double) r[2];
			int i = periodT.intValue(); 
			String periodName = Integer.toString(i);

			PrPeriodLineVO ret = new PrPeriodLineVO();
			ret.setOuCode(ouCode);
			ret.setYear(year);
			ret.setPeriod(period);
			ret.setPeriodName(periodName);
			retList.add(ret);
		}
		return retList;
	}
	
	public List findPeriodInPeriodLineMod(String ouCode, Double year)
			throws Exception {

		StringBuffer sql = new StringBuffer(0);
		sql.append(" SELECT l.pk.ouCode,l.pk.workYear,l.pk.workPeriod");
		sql.append(" FROM WlPeriodOfWork l ");
		sql.append(" WHERE l.pk.ouCode = '" + ouCode + "' ");
		sql.append(" AND l.pk.workYear = " + year);
		sql.append(" ORDER BY  l.pk.workPeriod");

		List yearList = this.getSession().createQuery(sql.toString()).list();
		List retList = new ArrayList();

		for (Iterator it = yearList.iterator(); it.hasNext();) {
			Object[] r = (Object[]) it.next();
			Double period = (Double) r[2];
			

			PrPeriodLineVO ret = new PrPeriodLineVO();
			ret.setOuCode(ouCode);
			ret.setYear(year);
			ret.setPeriod(period);
		

			retList.add(ret);
		}
		return retList;
	}
	
	public List findPeriodInPeriodLineModOne(String ouCode, Double year)
			throws Exception {

		StringBuffer sql = new StringBuffer(0);
		sql.append(" SELECT l.pk.ouCode,l.pk.workYear,l.pk.workPeriod");
		sql.append(" FROM WlPeriodOfWork l ");
		sql.append(" WHERE l.pk.ouCode = '" + ouCode + "' ");
		sql.append(" AND l.pk.workYear = " + year);
		sql.append(" ORDER BY  l.pk.workPeriod");

		List yearList = this.getSession().createQuery(sql.toString()).list();
		List retList = new ArrayList();

		for (Iterator it = yearList.iterator(); it.hasNext();) {
			Object[] r = (Object[]) it.next();
			Double period = (Double) r[2];
		

			PrPeriodLineVO ret = new PrPeriodLineVO();
			ret.setOuCode(ouCode);
			ret.setYear(year);
			ret.setPeriod(period);
			

			retList.add(ret);
		}
		return retList;
	}
	
	public String[] findMaxYearPeriod(String ouCode) {

		String year = "";
		String month = "";
		StringBuffer sql = new StringBuffer(0);
		sql.append(" SELECT a.pk.workYear || MAX(a.pk.workPeriod) ");
		sql.append(" FROM   WlPeriodOfWork a ");
		sql.append(" WHERE  a.pk.ouCode = '" + ouCode + "' ");
		sql.append(" and a.pk.workYear = ( select max(b.pk.workYear) from WlPeriodOfWork b ) ");
		sql.append(" group by a.pk.workYear ");
		List ls = this.getHibernateTemplate().find(sql.toString());
		if (ls != null && ls.size() > 0) {
			String rs = (String) ls.get(0);
			year = rs.substring(0, 4);
			month = rs.substring(4, rs.length());
		}

		String[] val = new String[2];
		val[0] = year;
		val[1] = month;

		return val;
	}
	

}
