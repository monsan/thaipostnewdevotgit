/* BALL
 * Created on 31 ?.?. 2549
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.ss.tp.control;

import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.CellFormat;
import jxl.write.Blank;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import com.ss.tp.dto.WgPrOvertimeVO;
import com.ss.tp.service.SuOrganizeService;
import com.ss.tp.service.WgPrOvertimeService;

/**
 * @author BALL
 * 
 *         TODO To change the template for this generated type comment go to
 *         Window - Preferences - Java - Code Style - Code Templates
 */
public class CTWGRP002Controller extends MultiActionController {

	/**
	 * @return Returns the taReportService.
	 */
	public WgPrOvertimeService getWgPrOvertimeService() {
		return (WgPrOvertimeService) this.getApplicationContext().getBean(
				"wgPrOvertimeService");
	}

	public SuOrganizeService getSuOrganizeService() {
		return (SuOrganizeService) this.getApplicationContext().getBean(
				"suOrganizeService");
	}

	/**
	 * method ����Ѻ�ʴ���§ҹ�������¹�ŧ��������ѡ�ͧ�١��ҧ��Ш�
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 * @return
	 */
	public ModelAndView doPrintReport(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String ouCode = "";
		String userId = "";
		int year = 0;
		int period = 0;
		String periodName = "";
		String otType = "";
		String otStatus = "";

		if (request.getParameter("ouCode") != null) {
			ouCode = request.getParameter("ouCode");
		}
		if (request.getParameter("userId") != null) {
			userId = request.getParameter("userId");
		}

		if (request.getParameter("year") != null
				&& !"".equals(request.getParameter("year"))) {
			year = Integer.parseInt(request.getParameter("year"));
		}
		if (request.getParameter("period") != null) {
			period = (int) Double.parseDouble(request.getParameter("period"));
		}
		if (request.getParameter("section") != null) {
			periodName = request.getParameter("section");
		}
		if (request.getParameter("otType") != null) {
			otType = request.getParameter("otType");
		}
		if (request.getParameter("otStatus") != null) {
			otStatus = request.getParameter("otStatus");
		}

		System.out.println("OTTYPE=" + otType);
		System.out.println("OTSTATUS=" + otStatus);

		List wgPrOvertimeList = this.getWgPrOvertimeService()
				.findWgPrOvertimeReport(ouCode, userId, year, period, otType,
						otStatus);
		System.out.println("list size=" + wgPrOvertimeList.size());

		// ---------------------------------Generate Report
		// Detail--------------------------------------------------

		WritableCellFormat headL4 = new WritableCellFormat();
		headL4.setBorder(Border.ALL, BorderLineStyle.THIN);
		WritableFont font2 = new WritableFont(WritableFont.ARIAL);
		font2.setBoldStyle(WritableFont.BOLD);
		font2.setPointSize(9);
		DecimalFormat df = new DecimalFormat("###,##0.00");
		Alignment data2 = Alignment.getAlignment(1);
		headL4.setAlignment(data2.LEFT);
		headL4.setFont(font2);

		WgPrOvertimeVO otvo = null;
		int startRow = 5;
		int i = 0;
		int j = 0;
		int s = 1; // Sheet's name
		String ouDesc = this.getSuOrganizeService().findOrganizeName(ouCode);

		String otTypeTemp = "";
		otType = "";

		String refNoTemp = "";
		String refNo = "";

		String divCodeTemp = "";// LV3
		String divCode = "";
		String secCodeTemp = "";// LV4
		String secCode = "";
		List otList = wgPrOvertimeList;

		System.out.println("\n\n\n size is ==>>" + wgPrOvertimeList.size());

		System.out.println("\n\n\n size2 is ==>>" + otList.size());
		WgPrOvertimeVO vo = null;
		WgPrOvertimeVO vo2 = null;

		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition",
				"attachment; filename=./page/report/CTWGRP002.xls");
		InputStream in = this.getServletContext().getResourceAsStream(
				"/page/report/CTWGRP002.xls");
		Workbook wb = Workbook.getWorkbook(in);
		WritableWorkbook ww = Workbook.createWorkbook(
				response.getOutputStream(), wb);
		WritableSheet sheet1 = ww.getSheet(0);

		// SheetSettings sst = new SheetSettings();
		// sst = wb.getSheet(0).getSettings();

		// ++++++++++Set Cell Format++++++++++
		CellFormat header = sheet1.getWritableCell(0, 0).getCellFormat();
		CellFormat header2 = sheet1.getWritableCell(0, 1).getCellFormat();
		CellFormat frHour = sheet1.getWritableCell(0, 7).getCellFormat();
		CellFormat normal = sheet1.getWritableCell(11, 6).getCellFormat();
		CellFormat normalLeft = sheet1.getWritableCell(11, 7).getCellFormat();
		CellFormat endLeft = sheet1.getWritableCell(1, 7).getCellFormat();
		CellFormat end = sheet1.getWritableCell(5, 6).getCellFormat();
		CellFormat tbcell = sheet1.getWritableCell(6, 7).getCellFormat();

		WritableCellFormat subHead = new WritableCellFormat();
		subHead.setBorder(Border.LEFT, BorderLineStyle.THIN);
		subHead.setAlignment(Alignment.LEFT);
		subHead.setFont(font2);
		// ------------------//
		WritableCellFormat borderNumber2 = new WritableCellFormat();
		borderNumber2.setBorder(Border.ALL, BorderLineStyle.THIN);
		borderNumber2.setAlignment(Alignment.RIGHT);
		borderNumber2.setFont(font2);

		WritableCellFormat borderNumber = new WritableCellFormat();
		borderNumber.setBorder(Border.ALL, BorderLineStyle.THIN);
		borderNumber.setAlignment(Alignment.RIGHT);
		borderNumber.setFont(font2);
		// ------------------//
		WritableCellFormat borderEnd = new WritableCellFormat();
		borderEnd.setBorder(Border.TOP, BorderLineStyle.THIN);

		WritableCellFormat noData = new WritableCellFormat();
		noData.setBorder(Border.ALL, BorderLineStyle.THIN);
		noData.setAlignment(Alignment.CENTRE);

		WritableFont fontBold = new WritableFont(WritableFont.ARIAL);
		fontBold.setBoldStyle(WritableFont.BOLD);
		fontBold.setPointSize(9);

		WritableCellFormat header3 = new WritableCellFormat();
		header3.setAlignment(Alignment.RIGHT);
		header3.setFont(fontBold);

		GregorianCalendar gd = new GregorianCalendar();
		SimpleDateFormat sdfPrint = new SimpleDateFormat("dd/MM/yyyy HH:mm",
				new java.util.Locale("th", "TH"));

		if (otList.size() > 0) {
			try {

				otTypeTemp = "";
				refNoTemp = "";
				divCodeTemp = "";
				secCodeTemp = "";
				String headDesc = "";
				double sumTotDay15 = 0.0;
				double sumTotDay1 = 0.0;
				double sumTotDay3 = 0.0;
				double sumTotAmt15 = 0.0;
				double sumTotAmt1 = 0.0;
				double sumTotAmt3 = 0.0;
				String flagTotalHour = "";
				String refDesc = "";
				sheet1.addCell(new Label(11, 4, "", tbcell));
				for (int k = 0; k < otList.size(); k++) {
					System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-"
							+ otList.get(k).toString());
					vo = (WgPrOvertimeVO) otList.get(k);
					if (k + 1 < otList.size())
						vo2 = (WgPrOvertimeVO) otList.get(k + 1);

					System.out.println(vo.getRefNo() + ", " + vo.getOtType());
					// if (((!refNoTemp.equalsIgnoreCase(vo.getRefNo())) ||
					// (!otTypeTemp.equalsIgnoreCase(vo.getOtType()))) &&
					// vo.getRefNo() != null && vo.getOtType() != null){
					if (!otTypeTemp.equalsIgnoreCase(vo.getOtType())
							&& vo.getOtType() != null) {
						// --------------------Start Show sum hour--
						// bybow-----------------------------//
						if (!flagTotalHour.equals("")) {
							sheet1.mergeCells(0, startRow + i, 4, startRow + i);
							sheet1.addCell(new Label(0, startRow+i , "���", borderNumber2));
							sheet1.addCell(new Label(5, startRow + i,
									convertFormatHour(new Double(sumTotDay1)),
									borderNumber));
							sheet1.addCell(new Label(6, startRow + i,
									convertFormatHour(new Double(sumTotDay15)),
									borderNumber));
							sheet1.addCell(new Label(7, startRow + i,
									convertFormatHour(new Double(sumTotDay3)),
									borderNumber));
							sheet1.addCell(new Label(8, startRow + i,
									convertFormatHour(new Double(sumTotAmt1)),
									borderNumber));
							sheet1.addCell(new Label(9, startRow + i,
									convertFormatHour(new Double(sumTotAmt15)),
									borderNumber));
							sheet1.addCell(new Label(10, startRow + i,
									convertFormatHour(new Double(sumTotAmt3)),
									borderNumber));
							sheet1.addCell(new Label(11, startRow + i, "",
									borderNumber2));
							sumTotDay15 = 0.0;
							sumTotDay1 = 0.0;
							sumTotDay3 = 0.0;
							sumTotAmt15 = 0.0;
							sumTotAmt1 = 0.0;
							sumTotAmt3 = 0.0;
							flagTotalHour = "";
							i++;
						}
						// --------------------End Show sum
						// hour-------------------------------//
						System.out.println("Num Sheet ==>"
								+ ww.getNumberOfSheets());
						ww.copySheet("Sheet1", "�蹧ҹ"+s, ww.getNumberOfSheets());
						s++;
						i = 0;
						j++;
						if (j > 0) {
							sheet1.getSettings().setPassword("123");
							sheet1.getSettings().setProtected(true);
						}
						sheet1 = ww.getSheet(j);
						String otDesc = "";

						System.out.println("OtType ==>" + vo.getOtType() + "  "
								+ vo.getRefNo());
						if (vo.getOtType() != null) {
							if(vo.getOtType().equalsIgnoreCase("1") || vo.getOtType() == "1")
								otDesc = "����� CTWGRP002                                                                                            ��§ҹ�����Ť����ǧ����";
							else if(vo.getOtType().equalsIgnoreCase("2") || vo.getOtType() == "2")
								otDesc = "����� CTWGRP002                                                                                ��§ҹ�����Ť�ҷӧҹ��ѹ��ش�ѡ��͹";
						}

						headDesc = ChgNullToEmpty(vo.getDivCode(), "") + "   "
								+ ChgNullToEmpty(vo.getDivDesc(), "");
						if (vo.getSecCode() != null)
							headDesc = ChgNullToEmpty(vo.getSecCode(), "")
									+ "   "
									+ ChgNullToEmpty(vo.getDivDesc(), "")
									+ ChgNullToEmpty(vo.getSecDesc(), "");
						// refDesc = "";
						// if(vo.getRefNo() != null){
						// refDesc = "�ӴѺ��� �.1 "+vo.getRefNo();
						// }
						// sheet1.addCell(new Blank(5, 5, tbcell));
						sheet1.addCell(new Label(0, 0, ChgNullToEmpty(ouDesc,
								""), header));
						sheet1.addCell(new Label(0, 1, ChgNullToEmpty(otDesc,
								""), header2));
						sheet1.addCell(new Label(0, 2,"��ШӧǴ��� "+ periodName + " �.�." + year+"                                                                              �ѹ������� : "+sdfPrint.format(gd.getTime()),header3));
//						sheet1.addCell(new Label(0, 3,refDesc,header));
						// refNoTemp = ChgNullToEmpty(vo.getRefNo(),"");
						otTypeTemp = ChgNullToEmpty(vo.getOtType(), "");
						secCodeTemp = "";
						divCodeTemp = "";
						flagTotalHour = "";
					}

					System.out.println("DIV = " + vo.getDivCode() + ", SEC = "
							+ vo.getSecCode());
					if (!refNoTemp.equalsIgnoreCase(vo.getRefNo())
							&& vo.getRefNo() != null) {
						if (!flagTotalHour.equals("")) {
							sheet1.mergeCells(0, startRow + i, 4, startRow + i);
							sheet1.addCell(new Label(0, startRow+i , "���", borderNumber2));
							sheet1.addCell(new Label(5, startRow + i,
									convertFormatHour(new Double(sumTotDay1)),
									borderNumber));
							sheet1.addCell(new Label(6, startRow + i,
									convertFormatHour(new Double(sumTotDay15)),
									borderNumber));
							sheet1.addCell(new Label(7, startRow + i,
									convertFormatHour(new Double(sumTotDay3)),
									borderNumber));
							sheet1.addCell(new Label(8, startRow + i,
									convertFormatHour(new Double(sumTotAmt1)),
									borderNumber));
							sheet1.addCell(new Label(9, startRow + i,
									convertFormatHour(new Double(sumTotAmt15)),
									borderNumber));
							sheet1.addCell(new Label(10, startRow + i,
									convertFormatHour(new Double(sumTotAmt3)),
									borderNumber));
							sheet1.addCell(new Label(11, startRow + i, "",
									borderNumber2));
							sumTotDay15 = 0.0;
							sumTotDay1 = 0.0;
							sumTotDay3 = 0.0;
							sumTotAmt15 = 0.0;
							sumTotAmt1 = 0.0;
							sumTotAmt3 = 0.0;
							flagTotalHour = "";
							secCodeTemp = "";
							divCodeTemp = "";
							i++;
						}
						refDesc = "";
						if (vo.getRefNo() != null
								&& !vo.getRefNo().equals(" - ")) {
							refDesc = "�ӴѺ��� �.1 "+vo.getRefNo();
							sheet1.addCell(new Label(0, startRow + i, refDesc,
									subHead));
							sheet1.addCell(new Blank(1, startRow + i, normal));
							sheet1.addCell(new Blank(2, startRow + i, normal));
							sheet1.addCell(new Blank(3, startRow + i, normal));
							sheet1.addCell(new Blank(4, startRow + i, normal));
							sheet1.addCell(new Blank(5, startRow + i, normal));
							sheet1.addCell(new Blank(6, startRow + i, normal));
							sheet1.addCell(new Blank(7, startRow + i, normal));
							sheet1.addCell(new Blank(8, startRow + i, normal));
							sheet1.addCell(new Blank(9, startRow + i, normal));
							sheet1.addCell(new Blank(10, startRow + i, normal));
							sheet1.addCell(new Blank(11, startRow + i, normal));
							i++;
						}
						refNoTemp = ChgNullToEmpty(vo.getRefNo(), "");

					}
					if ((!divCodeTemp.equalsIgnoreCase(ChgNullToEmpty(
							vo.getDivCode(), "")))
							|| (!secCodeTemp.equalsIgnoreCase(ChgNullToEmpty(
									vo.getSecCode(), "")))) {
						// -----------------Start Show sum
						// hours----bybow------------------------//
						if (!flagTotalHour.equals("")) {
							sheet1.mergeCells(0, startRow + i, 4, startRow + i);
							sheet1.addCell(new Label(0, startRow+i , "���", borderNumber2));
							sheet1.addCell(new Label(5, startRow + i,
									convertFormatHour(new Double(sumTotDay1)),
									borderNumber));
							sheet1.addCell(new Label(6, startRow + i,
									convertFormatHour(new Double(sumTotDay15)),
									borderNumber));
							sheet1.addCell(new Label(7, startRow + i,
									convertFormatHour(new Double(sumTotDay3)),
									borderNumber));
							sheet1.addCell(new Label(8, startRow + i,
									convertFormatHour(new Double(sumTotAmt1)),
									borderNumber));
							sheet1.addCell(new Label(9, startRow + i,
									convertFormatHour(new Double(sumTotAmt15)),
									borderNumber));
							sheet1.addCell(new Label(10, startRow + i,
									convertFormatHour(new Double(sumTotAmt3)),
									borderNumber));
							sheet1.addCell(new Label(11, startRow + i, "",
									borderNumber2));
							sumTotDay15 = 0.0;
							sumTotDay1 = 0.0;
							sumTotDay3 = 0.0;
							sumTotAmt15 = 0.0;
							sumTotAmt1 = 0.0;
							sumTotAmt3 = 0.0;
							flagTotalHour = "";
							i++;
						}
						// -----------------End Show sum
						// hours----------------------------//
						headDesc = ChgNullToEmpty(vo.getDivCode(), "") + "   "
								+ ChgNullToEmpty(vo.getDivDesc(), "");
						if (vo.getSecCode() != null) {
							headDesc = ChgNullToEmpty(vo.getSecCode(), "")
									+ " " + ChgNullToEmpty(vo.getDivDesc(), "")
									+ " " + ChgNullToEmpty(vo.getSecDesc(), "");
							System.out.println("--------------------Lv.4**"
									+ vo.getSecCode() + ", " + vo.getDivCode());
						} else {
							System.out.println("--------------------Lv.3**"
									+ vo.getSecCode() + ", " + vo.getDivCode());
						}
						sheet1.addCell(new Label(0, startRow + i, headDesc,
								subHead));
						sheet1.addCell(new Blank(1, startRow + i, normal));
						sheet1.addCell(new Blank(2, startRow + i, normal));
						sheet1.addCell(new Blank(3, startRow + i, normal));
						sheet1.addCell(new Blank(4, startRow + i, normal));
						sheet1.addCell(new Blank(5, startRow + i, normal));
						sheet1.addCell(new Blank(6, startRow + i, normal));
						sheet1.addCell(new Blank(7, startRow + i, normal));
						sheet1.addCell(new Blank(8, startRow + i, normal));
						sheet1.addCell(new Blank(9, startRow + i, normal));
						sheet1.addCell(new Blank(10, startRow + i, normal));
						sheet1.addCell(new Blank(11, startRow + i, normal));
						divCodeTemp = ChgNullToEmpty(vo.getDivCode(), "");
						secCodeTemp = ChgNullToEmpty(vo.getSecCode(), "");
						i++;

					}
					String EmpName = "";
					if (vo.getFlagWork() != null
							&& vo.getFlagWork().equals("Y")) {
						EmpName += "*";
					}
					EmpName += vo.getPrefixName() + " " + vo.getFirstName()
							+ " " + vo.getLastName();
					String flagDesc = "";
					if(vo.getFlagPr() != null){
						if(vo.getFlagPr().equals("A"))
							flagDesc="��Ѻ��ا";
						else if(vo.getFlagPr().equals("R"))
							flagDesc="���¡�׹";
						else if(vo.getFlagPr().equals("B"))
							flagDesc="���ԡ��Ѻ��ا��¡���Ѻ";
						else if(vo.getFlagPr().equals("S"))
							flagDesc="���ԡ��Ѻ��ا��¡�����¡�׹";
						else if(vo.getFlagPr().equals("N"))
							flagDesc="";
					}
					double t15 = 0;
					double t1 = 0;
					double t3 = 0;

					double amt15 = 0;
					double amt1 = 0;
					double amt3 = 0;

					System.out.println("A15 =>" + vo.getTotDay15());
					System.out.println("A1 =>" + vo.getTotDay1());
					System.out.println("A3 =>" + vo.getTotDay3());

					if (vo.getTotDay15() != null) {
						t15 = vo.getTotDay15().intValue();
						sumTotDay15 = sumTotDay15
								+ vo.getTotDay15().doubleValue();
						flagTotalHour = "sum";

					}
					if (vo.getTotDay1() != null) {
						t1 = vo.getTotDay1().intValue();
						sumTotDay1 = sumTotDay1 + vo.getTotDay1().doubleValue();
						flagTotalHour = "sum";
					}
					if (vo.getTotDay3() != null) {
						t3 = vo.getTotDay3().intValue();
						sumTotDay3 = sumTotDay3 + vo.getTotDay3().doubleValue();
						flagTotalHour = "sum";
					}

					// /////////////////////////////////////////
					if (vo.getAmtDay1() != null) {
						amt1 = vo.getAmtDay1().doubleValue();
						sumTotAmt1 = sumTotAmt1 + vo.getAmtDay1().doubleValue();
						flagTotalHour = "sum";

					}
					if (vo.getAmtDay15() != null) {
						amt15 = vo.getAmtDay15().doubleValue();
						sumTotAmt15 = sumTotAmt15
								+ vo.getAmtDay15().doubleValue();
						flagTotalHour = "sum";
					}
					if (vo.getAmtDay3() != null) {
						amt3 = vo.getAmtDay3().doubleValue();
						sumTotAmt3 = sumTotAmt3 + vo.getAmtDay3().doubleValue();
						flagTotalHour = "sum";
					}

					otTypeTemp = ChgNullToEmpty(vo.getOtType(), "");
					refNoTemp = ChgNullToEmpty(vo.getRefNo(), "");
					divCodeTemp = ChgNullToEmpty(vo.getDivCode(), "");
					secCodeTemp = ChgNullToEmpty(vo.getSecCode(), "");

					if (k + 1 < otList.size()) {
						if (((!refNoTemp.equalsIgnoreCase(vo2.getRefNo())) || (!otTypeTemp
								.equalsIgnoreCase(vo2.getOtType())))
								&& vo.getRefNo() != null
								&& vo.getOtType() != null) {
							sheet1.addCell(new Label(0, startRow + i,
									ChgNullToEmpty(vo.getEmpCode(), ""), end));
							sheet1.addCell(new Label(1, startRow + i, EmpName,
									endLeft));
							sheet1.addCell(new Label(2, startRow + i, vo
									.getYearPeriod(), end));
							sheet1.addCell(new Label(3, startRow + i, vo
									.getStartDateTemp(), end));
							sheet1.addCell(new Label(4, startRow + i, vo
									.getEndDateTemp(), end));
							sheet1.addCell(new Label(5, startRow + i, String
									.valueOf(df.format(t1)), frHour));
							sheet1.addCell(new Label(6, startRow + i, String
									.valueOf(df.format(t15)), frHour));
							sheet1.addCell(new Label(7, startRow + i, String
									.valueOf(df.format(t3)), frHour));
							sheet1.addCell(new Label(8, startRow + i, String
									.valueOf(df.format(amt1)), frHour));
							sheet1.addCell(new Label(9, startRow + i, String
									.valueOf(df.format(amt15)), frHour));
							sheet1.addCell(new Label(10, startRow + i, String
									.valueOf(df.format(amt3)), frHour));
							sheet1.addCell(new Label(11, startRow + i,
									flagDesc, endLeft));
						} else {
							sheet1.addCell(new Label(0, startRow + i,
									ChgNullToEmpty(vo.getEmpCode(), ""), end));
							sheet1.addCell(new Label(1, startRow + i, EmpName,
									endLeft));
							sheet1.addCell(new Label(2, startRow + i, vo
									.getYearPeriod(), end));
							sheet1.addCell(new Label(3, startRow + i, vo
									.getStartDateTemp(), end));
							sheet1.addCell(new Label(4, startRow + i, vo
									.getEndDateTemp(), end));
							sheet1.addCell(new Label(5, startRow + i, String
									.valueOf(df.format(t1)), frHour));
							sheet1.addCell(new Label(6, startRow + i, String
									.valueOf(df.format(t15)), frHour));
							sheet1.addCell(new Label(7, startRow + i, String
									.valueOf(df.format(t3)), frHour));
							sheet1.addCell(new Label(8, startRow + i, String
									.valueOf(df.format(amt1)), frHour));
							sheet1.addCell(new Label(9, startRow + i, String
									.valueOf(df.format(amt15)), frHour));
							sheet1.addCell(new Label(10, startRow + i, String
									.valueOf(df.format(amt3)), frHour));
							sheet1.addCell(new Label(11, startRow + i,
									flagDesc, endLeft));
						}
					} else {
						sheet1.addCell(new Label(0, startRow + i,
								ChgNullToEmpty(vo.getEmpCode(), ""), end));
						sheet1.addCell(new Label(1, startRow + i, EmpName,
								endLeft));
						sheet1.addCell(new Label(2, startRow + i, vo
								.getYearPeriod(), end));
						sheet1.addCell(new Label(3, startRow + i, vo
								.getStartDateTemp(), end));
						sheet1.addCell(new Label(4, startRow + i, vo
								.getEndDateTemp(), end));
						sheet1.addCell(new Label(5, startRow + i, String
								.valueOf(df.format(t1)), frHour));
						sheet1.addCell(new Label(6, startRow + i, String
								.valueOf(df.format(t15)), frHour));
						sheet1.addCell(new Label(7, startRow + i, String
								.valueOf(df.format(t3)), frHour));
						sheet1.addCell(new Label(8, startRow + i, String
								.valueOf(df.format(amt1)), frHour));
						sheet1.addCell(new Label(9, startRow + i, String
								.valueOf(df.format(amt15)), frHour));
						sheet1.addCell(new Label(10, startRow + i, String
								.valueOf(df.format(amt3)), frHour));
						sheet1.addCell(new Label(11, startRow + i, flagDesc,
								endLeft));
						System.out
								.println("<<<<<<<<<<<<<<<<<END REPORT>>>>>>>>>>>>>>>>>");
					}
					i++;
					if (!flagTotalHour.equals("") && (k + 1 == otList.size())) {

						sheet1.mergeCells(0, startRow + i, 4, startRow + i);
						sheet1.addCell(new Label(0, startRow+i , "���", borderNumber2));
						sheet1.addCell(new Label(5, startRow + i,
								convertFormatHour(new Double(sumTotDay1)),
								borderNumber));
						sheet1.addCell(new Label(6, startRow + i,
								convertFormatHour(new Double(sumTotDay15)),
								borderNumber));
						sheet1.addCell(new Label(7, startRow + i,
								convertFormatHour(new Double(sumTotDay3)),
								borderNumber));
						sheet1.addCell(new Label(8, startRow + i,
								convertFormatHour(new Double(sumTotAmt1)),
								borderNumber));
						sheet1.addCell(new Label(9, startRow + i,
								convertFormatHour(new Double(sumTotAmt15)),
								borderNumber));
						sheet1.addCell(new Label(10, startRow + i,
								convertFormatHour(new Double(sumTotAmt3)),
								borderNumber));
						sheet1.addCell(new Label(11, startRow + i, "",
								borderNumber2));
						sumTotDay15 = 0.0;
						sumTotDay1 = 0.0;
						sumTotDay3 = 0.0;
						flagTotalHour = "";
						i++;
					}
				}

				sheet1.getSettings().setPassword("123");
				sheet1.getSettings().setProtected(true);

				if (ww.getNumberOfSheets() > 1) {
					ww.removeSheet(0);
				}

				// sheet1 = ww.getSheet(0);
				// sheet1.getSettings().setHidden(true);
				ww.setProtected(true);
				ww.write();
				ww.close();
				// sheet1.getSettings().setHidden(false);

				wb.close();
				in.close();

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		} else {
			ww.copySheet("Sheet1", "�蹧ҹ1" , ww.getNumberOfSheets());	
			sheet1 = ww.getSheet(1);

			sheet1.addCell(new Label(0, 0, ouDesc, header));

			if(request.getParameter("otType").equals("1"))
				sheet1.addCell(new Label(0, 1,"����� CTWGRP002                                                                                            ��§ҹ�����Ť����ǧ����", header2));
			else if(request.getParameter("otType").equals("2"))
				sheet1.addCell(new Label(0, 1,"����� CTWGRP002                                                                                ��§ҹ�����Ť�ҷӧҹ��ѹ��ش�ѡ��͹", header2));
			sheet1.addCell(new Label(0, 2, "��ШӧǴ " + periodName + "   �.�. " + year, header));
			
			sheet1.addCell(new Blank(5, 8, normal));
			sheet1.mergeCells(0, startRow + i, 11, startRow + i);
			sheet1.addCell(new Label(0, startRow+i,"��辺������", noData));
			sheet1.addCell(new Blank(1, startRow + i, noData));
			sheet1.addCell(new Blank(2, startRow + i, noData));
			sheet1.addCell(new Blank(3, startRow + i, noData));
			sheet1.addCell(new Blank(4, startRow + i, noData));
			sheet1.addCell(new Blank(5, startRow + i, noData));
			sheet1.addCell(new Blank(6, startRow + i, noData));
			sheet1.addCell(new Blank(7, startRow + i, noData));
			sheet1.addCell(new Blank(8, startRow + i, noData));
			sheet1.addCell(new Blank(9, startRow + i, noData));
			sheet1.addCell(new Blank(10, startRow + i, noData));
			sheet1.addCell(new Blank(11, startRow + i, noData));
			sheet1.getSettings().setPassword("123");
			sheet1.getSettings().setProtected(true);
			ww.removeSheet(0);
			ww.setProtected(true);
			ww.write();
			ww.close();
			wb.close();
			in.close();
			return null;
		}

	}

	private String ChgNullToEmpty(String str1, String str2) {
		if (str1 == null) {
			str1 = str2;
		}
		return str1;
	}

	/**
	 * method ����Ѻ�ŧ Time Format
	 * 
	 * @param hour
	 * @return
	 */
	private String convertFormatHour(Double hour) {
		String rlst = "";
		DecimalFormat dec = new DecimalFormat("###,##0.00");
		rlst = dec.format(hour != null ? hour.doubleValue() : 0);
		return rlst;
	}
}
