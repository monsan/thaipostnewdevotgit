/* BALL
 * Created on 31 ?.?. 2549
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.ss.tp.control;

import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.CellFormat;
import jxl.format.Colour;
import jxl.format.VerticalAlignment;
import jxl.write.Blank;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import com.ss.tp.dto.CTRWRP201VO;
import com.ss.tp.service.SuOrganizeService;
import com.ss.tp.service.WgPrEmployeeService;

/**
 * @author BALL
 * 
 *         TODO To change the template for this generated type comment go to
 *         Window - Preferences - Java - Code Style - Code Templates
 */
public class CTWGRP006Controller extends MultiActionController {

	/**
	 * @return Returns the taReportService.
	 */
	public WgPrEmployeeService getWgPrEmployeeService() {
		return (WgPrEmployeeService) this.getApplicationContext().getBean(
				"wgPrEmployeeService");
	}

	public SuOrganizeService getSuOrganizeService() {
		return (SuOrganizeService) this.getApplicationContext().getBean(
				"suOrganizeService");
	}

	/**
	 * method ����Ѻ�ʴ���§ҹ�������¹�ŧ��������ѡ�ͧ�١��ҧ��Ш�
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 * @return
	 */
	public ModelAndView doPrintReport(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String ouCode = "";
		String userId = "";
		String year = "";
		String period = "";
		String periodName = "";

		DecimalFormat df = new DecimalFormat("###,##0.00");

		String key = null;
		CTRWRP201VO value = null;

		Hashtable hash = new Hashtable();

		if (request.getParameter("ouCode") != null) {
			ouCode = request.getParameter("ouCode");
		}
		if (request.getParameter("userId") != null) {
			userId = request.getParameter("userId");
		}

		if (request.getParameter("year") != null
				&& !"".equals(request.getParameter("year"))) {
			year = String.valueOf(request.getParameter("year"));
		}
		if (request.getParameter("period") != null) {
			period = String.valueOf(request.getParameter("period"));
		}
		if (request.getParameter("section") != null) {
			periodName = request.getParameter("section");
		}

		List ctrwrp201 = this.getWgPrEmployeeService().findDetailPaySlip(
				ouCode, userId, year, period);

		// ---------------------------------Generate Report
		// Detail--------------------------------------------------

		WritableCellFormat headL4 = new WritableCellFormat();
		headL4.setBorder(Border.ALL, BorderLineStyle.THIN);
		WritableFont font2 = new WritableFont(WritableFont.ARIAL);
		font2.setBoldStyle(WritableFont.BOLD);
		font2.setPointSize(9);

		WritableFont font = new WritableFont(WritableFont.ARIAL);
		font.setPointSize(9);

		Alignment data2 = Alignment.getAlignment(1);
		headL4.setAlignment(data2.LEFT);
		headL4.setFont(font2);

		// RwOvertimeVO otvo=null;
		int startRow = 6;
		int i = 0;
		int j = 0;
		int s = 1; // Sheet's name
		String ouDesc = this.getSuOrganizeService().findOrganizeName(ouCode);

		String divCodeChk = "";
		String areaCodeChk = "";
		String incDecCodeChk = "";

		List rpList = ctrwrp201;

		CTRWRP201VO vo = null;
		CTRWRP201VO vo2 = null;

		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition",
				"attachment; filename=./page/report/CTWGRP006.xls");
		InputStream in = this.getServletContext().getResourceAsStream(
				"/page/report/CTWGRP006.xls");
		Workbook wb = Workbook.getWorkbook(in);
		WritableWorkbook ww = Workbook.createWorkbook(
				response.getOutputStream(), wb);
		WritableSheet sheet1 = ww.getSheet(0);

		// SheetSettings sst = new SheetSettings();
		// sst = wb.getSheet(0).getSettings();

		// ++++++++++Set Cell Format++++++++++
		CellFormat header = sheet1.getWritableCell(0, 0).getCellFormat();
		// CellFormat normal = sheet1.getWritableCell(11,6).getCellFormat();
		CellFormat tbcell = sheet1.getWritableCell(8, 5).getCellFormat();

		WritableCellFormat subHead = new WritableCellFormat();
		subHead.setBorder(Border.LEFT, BorderLineStyle.THIN);
		subHead.setAlignment(Alignment.LEFT);
		subHead.setFont(font2);

		WritableFont fontBold = new WritableFont(WritableFont.ARIAL);
		fontBold.setBoldStyle(WritableFont.BOLD);
		fontBold.setPointSize(9);
		// ------------------//
		WritableCellFormat borderNumber2 = new WritableCellFormat();
		borderNumber2.setBorder(Border.RIGHT, BorderLineStyle.THIN);
		borderNumber2.setBorder(Border.LEFT, BorderLineStyle.THIN);
		borderNumber2.setAlignment(Alignment.RIGHT);
		borderNumber2.setFont(font);

		WritableCellFormat borderString = new WritableCellFormat();
		borderString.setBorder(Border.LEFT, BorderLineStyle.THIN);
		borderString.setAlignment(Alignment.LEFT);
		borderString.setFont(font);

		WritableCellFormat HeadFormat = new WritableCellFormat();
		HeadFormat.setAlignment(Alignment.LEFT);
		HeadFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		HeadFormat.setFont(fontBold);
		// ------------------//

		WritableCellFormat mergeCenterBoldBGGrayFormat = new WritableCellFormat();
		mergeCenterBoldBGGrayFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
		mergeCenterBoldBGGrayFormat.setFont(fontBold);
		mergeCenterBoldBGGrayFormat.setBackground(Colour.GRAY_50);
		mergeCenterBoldBGGrayFormat.setAlignment(Alignment.CENTRE);
		mergeCenterBoldBGGrayFormat
				.setVerticalAlignment(VerticalAlignment.CENTRE);
		mergeCenterBoldBGGrayFormat.setWrap(true);

		WritableCellFormat borderEnd = new WritableCellFormat();
		borderEnd.setBorder(Border.TOP, BorderLineStyle.THIN);

		WritableCellFormat noData = new WritableCellFormat();
		noData.setBorder(Border.ALL, BorderLineStyle.THIN);
		noData.setAlignment(Alignment.CENTRE);
		if (rpList.size() > 0) {
			try {

				divCodeChk = "";
				areaCodeChk = "";

				sheet1.addCell(new Label(12, 5, "", tbcell));
				for (int k = 0; k < rpList.size(); k++) {
					vo = (CTRWRP201VO) rpList.get(k);
					if (k + 1 < rpList.size())
						vo2 = (CTRWRP201VO) rpList.get(k + 1);

					// loop set Hash
					if (!divCodeChk.equals(vo.getDivCode())
							|| !incDecCodeChk.equals(vo.getIncDecCode())) {
						key = vo.getDivCode().trim()
								+ vo.getIncDecCode().trim();
						value = (CTRWRP201VO) hash.get(key);
						if (value != null) {
							// sum value
							value.setCntEmp(new Long(Integer.parseInt(value
									.getCntEmp().toString())
									+ Integer.parseInt(vo.getCntEmp()
											.toString())));
							value.setEmpA(new Long(Integer.parseInt(value
									.getEmpA().toString())
									+ Integer.parseInt(vo.getEmpA().toString())));
							value.setEmpI(new Long(Integer.parseInt(value
									.getEmpI().toString())
									+ Integer.parseInt(vo.getEmpI().toString())));
							value.setEmpD(new Long(Integer.parseInt(value
									.getEmpD().toString())
									+ Integer.parseInt(vo.getEmpD().toString())));

							value.setCntRec(new Long(Integer.parseInt(value
									.getCntRec().toString())
									+ Integer.parseInt(vo.getCntRec()
											.toString())));
							value.setAmtRec(Double.valueOf(String.valueOf(value
									.getAmtRec().doubleValue()
									+ vo.getAmtRec().doubleValue())));
							value.setCntN(new Long(Integer.parseInt(value
									.getCntN().toString())
									+ Integer.parseInt(vo.getCntN().toString())));
							value.setAmtN(Double.valueOf(String.valueOf(value
									.getAmtN().doubleValue()
									+ vo.getAmtN().doubleValue())));
							value.setCntA(new Long(Integer.parseInt(value
									.getCntA().toString())
									+ Integer.parseInt(vo.getCntA().toString())));
							value.setAmtA(Double.valueOf(String.valueOf(value
									.getAmtA().doubleValue()
									+ vo.getAmtA().doubleValue())));
							value.setCntR(new Long(Integer.parseInt(value
									.getCntR().toString())
									+ Integer.parseInt(vo.getCntR().toString())));
							value.setAmtR(Double.valueOf(String.valueOf(value
									.getAmtR().doubleValue()
									+ vo.getAmtR().doubleValue())));
							value.setCntB(new Long(Integer.parseInt(value
									.getCntB().toString())
									+ Integer.parseInt(vo.getCntB().toString())));
							value.setAmtB(Double.valueOf(String.valueOf(value
									.getAmtB().doubleValue()
									+ vo.getAmtB().doubleValue())));
							value.setCntS(new Long(Integer.parseInt(value
									.getCntS().toString())
									+ Integer.parseInt(vo.getCntS().toString())));
							value.setAmtS(Double.valueOf(String.valueOf(value
									.getAmtS().doubleValue()
									+ vo.getAmtS().doubleValue())));
						} else {
							hash.put(key, vo);
						}

					}

					if (!divCodeChk.equals(vo.getDivCode())
							|| !areaCodeChk.equals(vo.getAreaCode())) {
						divCodeChk = vo.getDivCode();
						areaCodeChk = vo.getAreaCode();
						if (i == 0) {
							ww.copySheet("Sheet1", "�蹧ҹ"+s, ww.getNumberOfSheets());
							s++;
							i = 0;
							j = 1;
							if (j > 0) {
								sheet1.getSettings().setPassword("123");
								sheet1.getSettings().setProtected(true);
							}
							sheet1 = ww.getSheet(j);
							String otDesc="����� CTWGRP006                                                                                         ��§ҹ�ʹ��ػ㹡�èѴ�� Pay Slip";
							
							
							
							
							sheet1.addCell(new Label(0, 0,ChgNullToEmpty(ouDesc,""),header));
							sheet1.addCell(new Label(0, 1,ChgNullToEmpty(otDesc,""),HeadFormat));
							sheet1.addCell(new Label(0, 2,"��ШӧǴ��� "+ periodName + " �.�." + year,header));
							
							if(vo.getAreaCode() != null && !vo.getAreaCode().equals("") && vo.getAreaName() != null && !vo.getAreaName().equals("")){
								sheet1.addCell(new Label(0, 3,"�ͧ   "+ vo.getDivCode()+" : "+vo.getDivName() + "    Ἱ�   " + vo.getAreaCode()+" : "+vo.getAreaName(),header));
							}else{
								sheet1.addCell(new Label(0, 3,"�ͧ   "+ vo.getDivCode()+" : "+vo.getDivName(),header));
							}
							
					//		sheet1.addCell(new Label(0, 4,"�ӹǹ��ѡ�ҹ������  "+ vo.getCntEmp() + " ��       ���  " + vo.getEmpA()+" ��¡��    ����  "+vo.getEmpI()+" ��¡��    ź  "+vo.getEmpD()+" ��¡��",header));
							sheet1.addCell(new Label(0, 4,"�ӹǹ��ѡ�ҹ������  "+ vo.getCntEmp() + " ��       ���  " + vo.getEmpA()+" ��¡��    ź  "+vo.getEmpD()+" ��¡��",header));
							i++;
							
					}else{
//						 begin new table	
//						sheet1 = ww.getSheet(j);
//						i++;
						sheet1.mergeCells(0,startRow+i, 12,startRow+i);
						sheet1.addCell(new Label(0, startRow+i,"",borderEnd));
						
						i++;
						sheet1.mergeCells(0,startRow+i, 12,startRow+i);
						if(vo.getAreaCode() != null && !vo.getAreaCode().equals("") && vo.getAreaName() != null && !vo.getAreaName().equals("")){
							sheet1.addCell(new Label(0, startRow+i,"�ͧ   "+ vo.getDivCode()+" : "+vo.getDivName() + "    Ἱ�   " + vo.getAreaCode()+" : "+vo.getAreaName(),header));
						}else{
							sheet1.addCell(new Label(0, startRow+i,"�ͧ   "+ vo.getDivCode()+" : "+vo.getDivName() ,header));
						}
						i++;
						sheet1.mergeCells(0,startRow+i, 12,startRow+i);
						//sheet1.addCell(new Label(0, startRow+i,"�ӹǹ��ѡ�ҹ������  "+ vo.getCntEmp() + " ��       ���  " + vo.getEmpA()+" ��¡��    ����  "+vo.getEmpI()+" ��¡��    ź  "+vo.getEmpD()+" ��¡��",header));
						sheet1.addCell(new Label(0, startRow+i,"�ӹǹ��ѡ�ҹ������  "+ vo.getCntEmp() + " ��       ���  " + vo.getEmpA()+" ��¡��    ź  "+vo.getEmpD()+" ��¡��",header));
						
						i++;
						
						//sheet1.setRowView(startRow+i, 550);
						sheet1.mergeCells(0,startRow+i, 0,startRow+i+1);
						sheet1.addCell(new Label(0, startRow+i, "��¡��", mergeCenterBoldBGGrayFormat));
						sheet1.mergeCells(1,startRow+i, 2,startRow+i);
						sheet1.addCell(new Label(1, startRow+i, "�����ŷ�����", mergeCenterBoldBGGrayFormat));
						sheet1.mergeCells(3,startRow+i, 4,startRow+i);
						sheet1.addCell(new Label(3, startRow+i, "����", mergeCenterBoldBGGrayFormat));
						sheet1.mergeCells(5,startRow+i, 6,startRow+i);
						sheet1.addCell(new Label(5, startRow+i, "��Ѻ��ا", mergeCenterBoldBGGrayFormat));
						sheet1.mergeCells(7,startRow+i, 8,startRow+i);
						sheet1.addCell(new Label(7, startRow+i, "���¡�׹", mergeCenterBoldBGGrayFormat));
						sheet1.mergeCells(9,startRow+i, 10,startRow+i);
						sheet1.addCell(new Label(9, startRow+i, "���ԡ��Ѻ��ا", mergeCenterBoldBGGrayFormat));
						sheet1.mergeCells(11,startRow+i, 12,startRow+i);
						sheet1.addCell(new Label(11, startRow+i, "���ԡ���¡�׹", mergeCenterBoldBGGrayFormat));
						
						i++;
						
						sheet1.addCell(new Label(0, startRow+i, "", mergeCenterBoldBGGrayFormat));
						sheet1.addCell(new Label(1, startRow+i, "��¡��", mergeCenterBoldBGGrayFormat));
						sheet1.addCell(new Label(2, startRow+i, "�ҷ", mergeCenterBoldBGGrayFormat));
						sheet1.addCell(new Label(3, startRow+i, "��¡��", mergeCenterBoldBGGrayFormat));
						sheet1.addCell(new Label(4, startRow+i, "�ҷ", mergeCenterBoldBGGrayFormat));
						sheet1.addCell(new Label(5, startRow+i, "��¡��", mergeCenterBoldBGGrayFormat));
						sheet1.addCell(new Label(6, startRow+i, "�ҷ", mergeCenterBoldBGGrayFormat));
						sheet1.addCell(new Label(7, startRow+i, "��¡��", mergeCenterBoldBGGrayFormat));
						sheet1.addCell(new Label(8, startRow+i, "�ҷ", mergeCenterBoldBGGrayFormat));
						sheet1.addCell(new Label(9, startRow+i, "��¡��", mergeCenterBoldBGGrayFormat));
						sheet1.addCell(new Label(10, startRow+i, "�ҷ", mergeCenterBoldBGGrayFormat));
						sheet1.addCell(new Label(11, startRow+i, "��¡��", mergeCenterBoldBGGrayFormat));
						sheet1.addCell(new Label(12, startRow+i, "�ҷ", mergeCenterBoldBGGrayFormat));
						i++;
				//	 end new table
						
					}
				}
					if (!vo.getIncDecCode().equals("00")) {
						if (k + 1 < rpList.size()) {
							sheet1.addCell(new Label(0, startRow + i, vo
									.getIncDecCode()
									+ "  "
									+ vo.getIncDecName(), borderString));
							sheet1.addCell(new Label(1, startRow + i, String
									.valueOf(vo.getCntRec()), borderNumber2));
							sheet1.addCell(new Label(2, startRow + i,
									convertFormatNumber(vo.getAmtRec()),
									borderNumber2));
							sheet1.addCell(new Label(3, startRow + i, String
									.valueOf(vo.getCntN()), borderNumber2));
							sheet1.addCell(new Label(4, startRow + i,
									convertFormatNumber(vo.getAmtN()),
									borderNumber2));
							sheet1.addCell(new Label(5, startRow + i, String
									.valueOf(vo.getCntA()), borderNumber2));
							sheet1.addCell(new Label(6, startRow + i,
									convertFormatNumber(vo.getAmtA()),
									borderNumber2));
							sheet1.addCell(new Label(7, startRow + i, String
									.valueOf(vo.getCntR()), borderNumber2));
							sheet1.addCell(new Label(8, startRow + i,
									convertFormatNumber(vo.getAmtR()),
									borderNumber2));
							sheet1.addCell(new Label(9, startRow + i, String
									.valueOf(vo.getCntB()), borderNumber2));
							sheet1.addCell(new Label(10, startRow + i,
									convertFormatNumber(vo.getAmtB()),
									borderNumber2));
							sheet1.addCell(new Label(11, startRow + i, String
									.valueOf(vo.getCntS()), borderNumber2));
							sheet1.addCell(new Label(12, startRow + i,
									convertFormatNumber(vo.getAmtS()),
									borderNumber2));

						} else {
							sheet1.addCell(new Label(0, startRow + i, vo
									.getIncDecCode()
									+ "  "
									+ vo.getIncDecName(), borderString));
							sheet1.addCell(new Label(1, startRow + i, String
									.valueOf(vo.getCntRec()), borderNumber2));
							sheet1.addCell(new Label(2, startRow + i,
									convertFormatNumber(vo.getAmtRec()),
									borderNumber2));
							sheet1.addCell(new Label(3, startRow + i, String
									.valueOf(vo.getCntN()), borderNumber2));
							sheet1.addCell(new Label(4, startRow + i,
									convertFormatNumber(vo.getAmtN()),
									borderNumber2));
							sheet1.addCell(new Label(5, startRow + i, String
									.valueOf(vo.getCntA()), borderNumber2));
							sheet1.addCell(new Label(6, startRow + i,
									convertFormatNumber(vo.getAmtA()),
									borderNumber2));
							sheet1.addCell(new Label(7, startRow + i, String
									.valueOf(vo.getCntR()), borderNumber2));
							sheet1.addCell(new Label(8, startRow + i,
									convertFormatNumber(vo.getAmtR()),
									borderNumber2));
							sheet1.addCell(new Label(9, startRow + i, String
									.valueOf(vo.getCntB()), borderNumber2));
							sheet1.addCell(new Label(10, startRow + i,
									convertFormatNumber(vo.getAmtB()),
									borderNumber2));
							sheet1.addCell(new Label(11, startRow + i, String
									.valueOf(vo.getCntS()), borderNumber2));
							sheet1.addCell(new Label(12, startRow + i,
									convertFormatNumber(vo.getAmtS()),
									borderNumber2));
							System.out
									.println("<<<<<<<<<<<<<<<<<END REPORT>>>>>>>>>>>>>>>>>");

						}
						i++;

					}
				}

				// ��鹻Դ
				// sheet1.mergeCells(0,startRow+i, 12,startRow+i);
				sheet1.addCell(new Label(0, startRow + i, "", borderEnd));
				sheet1.addCell(new Label(1, startRow + i, "", borderEnd));
				sheet1.addCell(new Label(2, startRow + i, "", borderEnd));
				sheet1.addCell(new Label(3, startRow + i, "", borderEnd));
				sheet1.addCell(new Label(4, startRow + i, "", borderEnd));
				sheet1.addCell(new Label(5, startRow + i, "", borderEnd));
				sheet1.addCell(new Label(6, startRow + i, "", borderEnd));
				sheet1.addCell(new Label(7, startRow + i, "", borderEnd));
				sheet1.addCell(new Label(8, startRow + i, "", borderEnd));
				sheet1.addCell(new Label(9, startRow + i, "", borderEnd));
				sheet1.addCell(new Label(10, startRow + i, "", borderEnd));
				sheet1.addCell(new Label(11, startRow + i, "", borderEnd));
				sheet1.addCell(new Label(12, startRow + i, "", borderEnd));
				// ��鹻Դ
				// ˹���ʴ������

				divCodeChk = "";
				areaCodeChk = "";
				incDecCodeChk = "";
				i = 0;

				for (Iterator it = new TreeSet(hash.keySet()).iterator(); it
						.hasNext();) {
					key = (String) it.next();
					value = (CTRWRP201VO) hash.get(key);

					if (!divCodeChk.equals(value.getDivCode())
							|| (value.getIncDecCode().equals("00"))) {
						divCodeChk = value.getDivCode();
						if (i == 0) {
							ww.copySheet("Sheet1", "�蹧ҹ2", ww.getNumberOfSheets());

							// j++;
							j = 2;
							if (j > 0) {
								sheet1.getSettings().setPassword("123");
								sheet1.getSettings().setProtected(true);
							}
							sheet1 = ww.getSheet(j);
							String otDesc="����� CTWGRP006                                                                                         ��§ҹ�ʹ��ػ㹡�èѴ�� Pay Slip";
							
							
							sheet1.addCell(new Label(0, 0,ChgNullToEmpty(ouDesc,""),header));
							sheet1.addCell(new Label(0, 1,ChgNullToEmpty(otDesc,""),HeadFormat));
							sheet1.addCell(new Label(0, 2,"��ШӧǴ��� "+ periodName + " �.�." + year,header));
							sheet1.addCell(new Label(0, 3,"�ͧ   "+ value.getDivCode()+" : "+value.getDivName(),header));
							sheet1.addCell(new Label(0, 4,"�ӹǹ��ѡ�ҹ������  "+ value.getCntEmp() + " ��       ���  " + value.getEmpA()+" ��¡��    ź  "+value.getEmpD()+" ��¡��",header));
							i++;
					}else{
							// begin new table
							// sheet1 = ww.getSheet(j);
							sheet1.mergeCells(0, startRow + i, 12, startRow + i);
							sheet1.addCell(new Label(0, startRow + i, "",
									borderEnd));

							i++;
							sheet1.mergeCells(0, startRow + i, 12, startRow + i);
							sheet1.addCell(new Label(0, startRow+i,"�ͧ   "+ value.getDivCode()+" : "+value.getDivName(),header));
							i++;
							sheet1.mergeCells(0,startRow+i, 12,startRow+i);
							sheet1.addCell(new Label(0, startRow+i,"�ӹǹ��ѡ�ҹ������  "+ value.getCntEmp() + " ��       ���  " + value.getEmpA()+" ��¡��    ź  "+value.getEmpD()+" ��¡��",header));
							
							i++;
							
							//sheet1.setRowView(startRow+i, 550);
							sheet1.mergeCells(0,startRow+i, 0,startRow+i+1);
							sheet1.addCell(new Label(0, startRow+i, "��¡��", mergeCenterBoldBGGrayFormat));
							sheet1.mergeCells(1,startRow+i, 2,startRow+i);
							sheet1.addCell(new Label(1, startRow+i, "�����ŷ�����", mergeCenterBoldBGGrayFormat));
							sheet1.mergeCells(3,startRow+i, 4,startRow+i);
							sheet1.addCell(new Label(3, startRow+i, "����", mergeCenterBoldBGGrayFormat));
							sheet1.mergeCells(5,startRow+i, 6,startRow+i);
							sheet1.addCell(new Label(5, startRow+i, "��Ѻ��ا", mergeCenterBoldBGGrayFormat));
							sheet1.mergeCells(7,startRow+i, 8,startRow+i);
							sheet1.addCell(new Label(7, startRow+i, "���¡�׹", mergeCenterBoldBGGrayFormat));
							sheet1.mergeCells(9,startRow+i, 10,startRow+i);
							sheet1.addCell(new Label(9, startRow+i, "���ԡ��Ѻ��ا", mergeCenterBoldBGGrayFormat));
							sheet1.mergeCells(11,startRow+i, 12,startRow+i);
							sheet1.addCell(new Label(11, startRow+i, "���ԡ���¡�׹", mergeCenterBoldBGGrayFormat));
							
							i++;
							
							sheet1.addCell(new Label(0, startRow+i, "", mergeCenterBoldBGGrayFormat));
							sheet1.addCell(new Label(1, startRow+i, "��¡��", mergeCenterBoldBGGrayFormat));
							sheet1.addCell(new Label(2, startRow+i, "�ҷ", mergeCenterBoldBGGrayFormat));
							sheet1.addCell(new Label(3, startRow+i, "��¡��", mergeCenterBoldBGGrayFormat));
							sheet1.addCell(new Label(4, startRow+i, "�ҷ", mergeCenterBoldBGGrayFormat));
							sheet1.addCell(new Label(5, startRow+i, "��¡��", mergeCenterBoldBGGrayFormat));
							sheet1.addCell(new Label(6, startRow+i, "�ҷ", mergeCenterBoldBGGrayFormat));
							sheet1.addCell(new Label(7, startRow+i, "��¡��", mergeCenterBoldBGGrayFormat));
							sheet1.addCell(new Label(8, startRow+i, "�ҷ", mergeCenterBoldBGGrayFormat));
							sheet1.addCell(new Label(9, startRow+i, "��¡��", mergeCenterBoldBGGrayFormat));
							sheet1.addCell(new Label(10, startRow+i, "�ҷ", mergeCenterBoldBGGrayFormat));
							sheet1.addCell(new Label(11, startRow+i, "��¡��", mergeCenterBoldBGGrayFormat));
							sheet1.addCell(new Label(12, startRow+i, "�ҷ", mergeCenterBoldBGGrayFormat));
							i++;
					//	 end new table
							
						}
						
					}
					if (!value.getIncDecCode().equals("00")) {
						sheet1.addCell(new Label(0, startRow + i,
								value.getIncDecCode() + "  "
										+ value.getIncDecName(), borderString));
						sheet1.addCell(new Label(1, startRow + i, String
								.valueOf(value.getCntRec()), borderNumber2));
						sheet1.addCell(new Label(2, startRow + i,
								convertFormatNumber(value.getAmtRec()),
								borderNumber2));
						sheet1.addCell(new Label(3, startRow + i, String
								.valueOf(value.getCntN()), borderNumber2));
						sheet1.addCell(new Label(4, startRow + i,
								convertFormatNumber(value.getAmtN()),
								borderNumber2));
						sheet1.addCell(new Label(5, startRow + i, String
								.valueOf(value.getCntA()), borderNumber2));
						sheet1.addCell(new Label(6, startRow + i,
								convertFormatNumber(value.getAmtA()),
								borderNumber2));
						sheet1.addCell(new Label(7, startRow + i, String
								.valueOf(value.getCntR()), borderNumber2));
						sheet1.addCell(new Label(8, startRow + i,
								convertFormatNumber(value.getAmtR()),
								borderNumber2));
						sheet1.addCell(new Label(9, startRow + i, String
								.valueOf(value.getCntB()), borderNumber2));
						sheet1.addCell(new Label(10, startRow + i,
								convertFormatNumber(value.getAmtB()),
								borderNumber2));
						sheet1.addCell(new Label(11, startRow + i, String
								.valueOf(value.getCntS()), borderNumber2));
						sheet1.addCell(new Label(12, startRow + i,
								convertFormatNumber(value.getAmtS()),
								borderNumber2));

						i++;
					}

				}
				// ��鹻Դ
				// sheet1.mergeCells(0,startRow+i, 12,startRow+i);
				sheet1.addCell(new Label(0, startRow + i, "", borderEnd));
				sheet1.addCell(new Label(1, startRow + i, "", borderEnd));
				sheet1.addCell(new Label(2, startRow + i, "", borderEnd));
				sheet1.addCell(new Label(3, startRow + i, "", borderEnd));
				sheet1.addCell(new Label(4, startRow + i, "", borderEnd));
				sheet1.addCell(new Label(5, startRow + i, "", borderEnd));
				sheet1.addCell(new Label(6, startRow + i, "", borderEnd));
				sheet1.addCell(new Label(7, startRow + i, "", borderEnd));
				sheet1.addCell(new Label(8, startRow + i, "", borderEnd));
				sheet1.addCell(new Label(9, startRow + i, "", borderEnd));
				sheet1.addCell(new Label(10, startRow + i, "", borderEnd));
				sheet1.addCell(new Label(11, startRow + i, "", borderEnd));
				sheet1.addCell(new Label(12, startRow + i, "", borderEnd));
				// ��鹻Դ
				// ˹���ʴ������

				sheet1.getSettings().setPassword("123");
				sheet1.getSettings().setProtected(true);

				if (ww.getNumberOfSheets() > 1) {
					ww.removeSheet(0);
				}

				// sheet1 = ww.getSheet(0);
				// sheet1.getSettings().setHidden(true);
				ww.setProtected(true);
				ww.write();
				ww.close();
				// sheet1.getSettings().setHidden(false);

				wb.close();
				in.close();

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		} else {
			ww.copySheet("Sheet1", "�蹧ҹ1" , ww.getNumberOfSheets());	
			String otDesc="����� CTWGRP006                                                                                         ��§ҹ�ʹ��ػ㹡�èѴ�� Pay Slip";
			sheet1 = ww.getSheet(1);
			sheet1.addCell(new Label(0, 0, ChgNullToEmpty(ouDesc, ""), header));
			sheet1.addCell(new Label(0, 1, ChgNullToEmpty(otDesc, ""),
					HeadFormat));
			sheet1.addCell(new Label(0, 2,"��ШӧǴ��� "+ periodName + " �.�." + year,header));
			sheet1.addCell(new Label(0, 3,"�ͧ   .... : ..........    Ἱ�   ........ : ............."));
			sheet1.addCell(new Label(0, 4,"�ӹǹ��ѡ�ҹ������  0 ��       ���  0 ��¡��    ����  0 ��¡��    ź  0 ��¡��",header));
	
//			sheet1.addCell(new Blank(5, 8, normal));
			sheet1.mergeCells(0,startRow+i,12,startRow+i);
			sheet1.addCell(new Label(0, startRow+i,"��辺������", noData));
			sheet1.addCell(new Blank(1, startRow + i, noData));
			sheet1.addCell(new Blank(2, startRow + i, noData));
			sheet1.addCell(new Blank(3, startRow + i, noData));
			sheet1.addCell(new Blank(4, startRow + i, noData));
			sheet1.addCell(new Blank(5, startRow + i, noData));
			sheet1.addCell(new Blank(6, startRow + i, noData));
			sheet1.addCell(new Blank(7, startRow + i, noData));
			sheet1.addCell(new Blank(8, startRow + i, noData));
			sheet1.addCell(new Blank(9, startRow + i, noData));
			sheet1.addCell(new Blank(10, startRow + i, noData));
			sheet1.addCell(new Blank(11, startRow + i, noData));
			sheet1.addCell(new Blank(12, startRow + i, noData));
			sheet1.getSettings().setPassword("123");
			sheet1.getSettings().setProtected(true);
			ww.removeSheet(0);
			ww.setProtected(true);
			ww.write();
			ww.close();
			wb.close();
			in.close();
			return null;
		}

	}

	private String ChgNullToEmpty(String str1, String str2) {
		if (str1 == null) {
			str1 = str2;
		}
		return str1;
	}

	/**
	 * method ����Ѻ�ŧ Time Format
	 * 
	 * @param hour
	 * @return
	 */
	private String convertFormatNumber(Double hour) {
		String rlst = "";
		DecimalFormat dec = new DecimalFormat("###,##0.00");
		rlst = dec.format(hour != null ? hour.doubleValue() : 0);
		return rlst;
	}
}
