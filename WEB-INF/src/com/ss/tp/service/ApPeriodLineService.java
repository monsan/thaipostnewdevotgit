package com.ss.tp.service;

import com.ss.tp.dto.DefaultYearSectionVO;
import com.ss.tp.model.ApPeriodLine;

import java.util.List;


public interface ApPeriodLineService {
	public DefaultYearSectionVO getDefaultYearAndSection(String ouCode,
			String userId) throws Exception;

	public List findYearInPeriodLine(String ouCode) throws Exception;

	public List findPeriodInPeriodLine(String ouCode, double year)
			throws Exception;

	public boolean canDeleteData(String ouCode, String year, String month ,String volumeSet)
			throws Exception;

	public boolean isCloseTranClose(String ouCode, String year, String month,String volumeSet)
			throws Exception;
	
	public boolean isCloseMasterClose(String ouCode, String year, String month ,String volumeSet)
			throws Exception;

	public ApPeriodLine findPeriodLine(String ouCode, String year, String volumeSet)
			throws Exception;
}
