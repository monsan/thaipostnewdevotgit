package com.ss.tp.service;

import com.ss.tp.dto.ApConfirmDataVO;


public interface ApConfirmDataService {
	public void insertApConfirmData(ApConfirmDataVO vo) throws Exception;
	



	public boolean isConfirmInputData(String ouCode, String year,
			String month,String volumeSet, String userId) throws Exception;

	public boolean isConfirmApData(String ouCode, String year, String month,String volumeSet,
			String userId) throws Exception;
}