package com.ss.tp.service.impl;


import com.ss.tp.dto.DefaultYearSectionVO;
import com.ss.tp.dto.WeEmployeeVO;

import com.ss.tp.dto.ApToGlVO;

import com.ss.tp.service.ApGenerateGlService;

import java.io.Serializable;

import java.util.Date;
import java.util.List;
import com.ss.tp.dao.ApGenerateGlDAO;
import com.ss.tp.dao.ApTableDAO;


//import com.ss.tp.dto.PayRollEmployeeVO;


public class ApGenerateGlServiceImpl implements ApGenerateGlService,
		Serializable {

	private ApGenerateGlDAO ApGenerateGlDAO;

	public ApGenerateGlDAO getApGenerateGlDAO() {
		return ApGenerateGlDAO;
	}

	public void setApGenerateGlDAO(ApGenerateGlDAO ApGenerateGlDAO) {
		this.ApGenerateGlDAO = ApGenerateGlDAO;
	}

	public void insertApToGl(ApToGlVO aptoglvo)
			throws Exception {
		this.ApGenerateGlDAO.insertApToGl(aptoglvo);
	}

	
	
	public void deleteApToGl(ApToGlVO aptoglvo)
			throws Exception {
		this.ApGenerateGlDAO.deleteApToGl(aptoglvo);
	}

	public void insertApToGls(List aptoglvolist)
			throws Exception {
		this.ApGenerateGlDAO.insertApToGls(aptoglvolist);
	}

	public List findByCriteria(String userId, String evaOuCode, long evaYear,
			long evaMonth, String evaEmpCodeFrom, String evaEmpCodeTo) {
		return this.ApGenerateGlDAO.findByCriteria(userId, evaOuCode,
				new Long(evaYear), new Long(evaMonth), evaEmpCodeFrom,
				evaEmpCodeTo);
	}

	public Integer countData(String userId, String evaOuCode, long evaYear,
			long evaMonth, long evaVolume) {
		return this.ApGenerateGlDAO.countData(userId, evaOuCode, new Long(
				evaYear), new Long(evaMonth), new Long(evaVolume));
	}
	

	
	
	public void addList(ApToGlVO rpVo, boolean isSave) {
		this.ApGenerateGlDAO.addList(rpVo);

		if (isSave) {
			try {
				this.ApGenerateGlDAO.insertApToGl(rpVo);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	


	


	public boolean canDelete(Date accountingDate) throws Exception {
		return this.ApGenerateGlDAO.canDelete(accountingDate);
	}

	public List findByCriteriaList(String userId, String evaOuCode,
			long evaYear, long evaMonth, long evaVolume, int count,
			int countRecord) {
		return this.ApGenerateGlDAO.findByCriteriaList(userId, evaOuCode,
				new Long(evaYear), new Long(evaMonth), new Long(evaVolume),
				count, countRecord);
	}
	
	

	

	
	 
	public void deleteApToGlInputError(String ouCode, String yearPn,
			String monthPn,String volumeSet,String userId) throws Exception {
		this.ApGenerateGlDAO.deleteApToGlInputError(ouCode, yearPn, monthPn,volumeSet,userId);
	}
	
	public Integer confirmData(String ouCode, long yearPn, long monthPn,
			String volumeSet, String userId) {
		return this.ApGenerateGlDAO.confirmData(ouCode, new Long(yearPn),
				new Long(monthPn), volumeSet, userId);
	}

	public List addApToGl(String userId, String evaOuCode, String evaYear,
			String evaMonth, String volume1, String volume2, String accDate) {
		// TODO Auto-generated method stub
		return this.ApGenerateGlDAO.addApToGl(userId, evaOuCode,evaYear,evaMonth,volume1,volume2, accDate);
	}
	
	public DefaultYearSectionVO getDefaultYearAndSectionGL(String ouCode,
			String userId) throws Exception {
		String[] val = ApGenerateGlDAO.findMaxDateGL(ouCode);
		
		//boolean isConfirm = ApTableDAO.isConfirmFlag(ouCode, val[0],
		//		val[1], userId);
	

		DefaultYearSectionVO vo = new DefaultYearSectionVO();
		vo.setYear(val[0]);
		//vo.setMonth(val[1]);
		//vo.setConfirm(isConfirm);

	
		//System.out.println("month : " + vo.getMonth());

		return vo;
	}
	
	
	
	public String[] findMaxDateGL(String ouCode) {
		return this.ApGenerateGlDAO.findMaxDateGL(ouCode);
	}
    
	public List findDateBySecurity(String ouCode)
			throws Exception {
		return this.ApGenerateGlDAO.findDateBySecurity(ouCode);
			}
	
	

}
