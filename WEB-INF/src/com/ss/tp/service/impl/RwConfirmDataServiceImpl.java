/*
 * Created on 30 ?.?. 2549
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.ss.tp.service.impl;

import java.io.Serializable;
import java.util.List;

import com.ss.tp.dao.RwConfirmDataDAO;
import com.ss.tp.dto.RwConfirmDataVO;
import com.ss.tp.service.RwConfirmDataService;

/**
 * @author Administrator
 * 
 *         TODO To change the template for this generated type comment go to
 *         Window - Preferences - Java - Code Style - Code Templates
 */
public class RwConfirmDataServiceImpl implements RwConfirmDataService,
		Serializable {
	private RwConfirmDataDAO rwConfirmData;

	public void insertRwConfirmData(RwConfirmDataVO vo) throws Exception {
		this.rwConfirmData.insertRwConfirmData(vo);
	}

	public RwConfirmDataDAO getRwConfirmData() {
		return rwConfirmData;
	}

	public void setRwConfirmData(RwConfirmDataDAO rwConfirmData) {
		this.rwConfirmData = rwConfirmData;
	}

	public boolean isConfirmMasterData(String ouCode, String year,
			String period, String userId) throws Exception {
		return this.rwConfirmData.isConfirmMasterData(ouCode, year, period,
				userId);
	}

	public boolean isConfirmRwData(String ouCode, String year, String period,
			String userId) throws Exception {
		return this.rwConfirmData.isConfirmRwData(ouCode, year, period, userId);
	}
	public boolean isTransferGL(String ouCode, String year,
			String period, String userId) throws Exception {
		return this.rwConfirmData.isTransferGL(ouCode, year, period,userId);
	}

	public List addPrDailyToGl(String userId, String evaOuCode, String evaYear,
			String evaMonth,String accDate) {
		// TODO Auto-generated method stub
		return this.rwConfirmData.addPrDailyToGl(userId, evaOuCode,evaYear,evaMonth, accDate);
	}
	public List addPrDailyToGlHm(String userId, String evaOuCode, String evaYear,
			String evaMonth,String accDate) {
		// TODO Auto-generated method stub
		return this.rwConfirmData.addPrDailyToGlHm(userId, evaOuCode,evaYear,evaMonth, accDate);
	}
	
}
