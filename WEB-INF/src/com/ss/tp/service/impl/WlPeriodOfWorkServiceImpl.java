package com.ss.tp.service.impl;

import java.io.Serializable;
import java.util.List;

import com.ss.tp.dao.PrEmployeeDAO;
import com.ss.tp.dao.PrEmployeeTextDAO;
import com.ss.tp.dao.WlPeriodOfWorkDAO;
import com.ss.tp.dto.DefaultYearSectionVO;
import com.ss.tp.model.WlPeriodOfWork;
import com.ss.tp.service.WlPeriodOfWorkService;

public class WlPeriodOfWorkServiceImpl implements WlPeriodOfWorkService,
		Serializable {

	private PrEmployeeDAO prEmployeeDAO;
	private PrEmployeeTextDAO prEmployeeTextDAO;
	private WlPeriodOfWorkDAO wlPeriodOfWorkDAO;

	public PrEmployeeDAO getPrEmployeeDAO() {
		return prEmployeeDAO;
	}

	public void setPrEmployeeDAO(PrEmployeeDAO prEmployeeDAO) {
		this.prEmployeeDAO = prEmployeeDAO;
	}

	public PrEmployeeTextDAO getPrEmployeeTextDAO() {
		return prEmployeeTextDAO;
	}

	public void setPrEmployeeTextDAO(PrEmployeeTextDAO prEmployeeTextDAO) {
		this.prEmployeeTextDAO = prEmployeeTextDAO;
	}

	public WlPeriodOfWorkDAO getWlPeriodOfWorkDAO() {
		return wlPeriodOfWorkDAO;
	}

	public void setWlPeriodOfWorkDAO(WlPeriodOfWorkDAO wlPeriodOfWorkDAO) {
		this.wlPeriodOfWorkDAO = wlPeriodOfWorkDAO;
	}

	public DefaultYearSectionVO getDefaultYearAndSection(String ouCode,
			String userId) throws Exception {
		String[] val = wlPeriodOfWorkDAO.findMaxYearPeriod(ouCode);
		// System.out.println("1234");
		//boolean isConfirm = prEmployeeTextDAO.isConfirmFlag(ouCode, val[0],val[1], userId);
		// System.out.println("5678");
		//WlPeriodOfWork wlPeriodOfWork = wlPeriodOfWorkDAO.findPeriodLine(ouCode,val[0], val[1]);
		// System.out.println("11111");

		DefaultYearSectionVO vo = new DefaultYearSectionVO();
		vo.setYear(val[0]);
		vo.setMonth(val[1]);
		vo.setPeriod(val[1]);
		//vo.setConfirm(isConfirm);

		//Double d = new Double(wlPeriodOfWork.getPk().getWorkPeriod());
		//int m = (d.intValue());
		//vo.setPeriod(String.valueOf(d.intValue()));
		//vo.setMonth(String.valueOf(m));
		System.out.println("year : " + vo.getYear());
		System.out.println("month : " + vo.getMonth());

		return vo;
	}

	public List findYearInPeriodLine(String ouCode) throws Exception {
		return this.wlPeriodOfWorkDAO.findYearInPeriodLine(ouCode);
	}

	public List findPeriodInPeriodLine(String ouCode, double year)
			throws Exception {
		return this.wlPeriodOfWorkDAO.findPeriodInPeriodLine(ouCode, new Double(
				year));
	}

	public List findPeriodInPeriodLineMod(String ouCode, double year)
			throws Exception {
		return this.wlPeriodOfWorkDAO.findPeriodInPeriodLineMod(ouCode, new Double(
				year));
	}
	public List findPeriodInPeriodLineModOne(String ouCode, double year)
			throws Exception {
		return this.wlPeriodOfWorkDAO.findPeriodInPeriodLineModOne(ouCode, new Double(
				year));
	}
	
	public boolean canDeleteData(String ouCode, String year, String period)
			throws Exception {
		return this.wlPeriodOfWorkDAO.canDeleteData(ouCode, year, period);
	}

	public boolean isCloseTranClose(String ouCode, String year, String period)
			throws Exception {
		return this.wlPeriodOfWorkDAO.isCloseTranClose(ouCode, year, period);
	}
	
	public boolean isCloseMasterClose(String ouCode, String year, String period)
			throws Exception {
		return this.wlPeriodOfWorkDAO.isCloseMasterClose(ouCode, year, period);
	}


	public WlPeriodOfWork findPeriodLine(String ouCode, String year, String period)
			throws Exception {
		return this.wlPeriodOfWorkDAO.findPeriodLine(ouCode, year, period);
	}
	
	public DefaultYearSectionVO getDefaultYearAndSectionNext(String ouCode,
			String userId) throws Exception {
		String[] val = prEmployeeDAO.findMaxYearPeriod(ouCode);
		// System.out.println("1234");
		boolean isConfirm = prEmployeeTextDAO.isConfirmFlag(ouCode, val[0],
				val[1], userId);
		// System.out.println("5678");
		WlPeriodOfWork wlPeriodOfWork = wlPeriodOfWorkDAO.findPeriodLineNext(ouCode,
				val[0], val[1]);
		// System.out.println("11111");

		DefaultYearSectionVO vo = new DefaultYearSectionVO();
		//vo.setYear(val[0]);
		
		vo.setConfirm(isConfirm);

		Double d = new Double(wlPeriodOfWork.getPk().getWorkPeriod());
		//int m = (d.intValue()) / 2;
		Double y = new Double(wlPeriodOfWork.getPk().getWorkYear());
		vo.setYear(String.valueOf(y.intValue()));
		vo.setPeriod(String.valueOf(d.intValue()));
		//vo.setMonth(String.valueOf(m));
		System.out.println("year : " + vo.getYear());
		System.out.println("period : " + vo.getPeriod());
		//System.out.println("month : " + vo.getMonth());

		return vo;
	}
	public DefaultYearSectionVO getDefaultYearAndSectionMod(String ouCode,
			String userId) throws Exception {
		String[] val = prEmployeeDAO.findMaxYearPeriodMod(ouCode);
		// System.out.println("1234");
		boolean isConfirm = prEmployeeTextDAO.isConfirmFlag(ouCode, val[0],val[1], userId);
		// System.out.println("5678");
		WlPeriodOfWork wlPeriodOfWork = wlPeriodOfWorkDAO.findPeriodLineMod(ouCode,val[0], val[1]);
		// System.out.println("11111");

		DefaultYearSectionVO vo = new DefaultYearSectionVO();
		vo.setYear(val[0]);
		vo.setConfirm(isConfirm);

		Double d = new Double(wlPeriodOfWork.getPk().getWorkPeriod());
		//int m = (d.intValue()) / 2;
		vo.setPeriod(String.valueOf(d.intValue()));
		//vo.setMonth(String.valueOf(m));
		System.out.println("period : " + vo.getPeriod());
		//System.out.println("month : " + vo.getMonth());

		return vo;
	}
	public String[] findMaxYearPeriod(String ouCode) {
		return this.wlPeriodOfWorkDAO.findMaxYearPeriod(ouCode);
	}

}
