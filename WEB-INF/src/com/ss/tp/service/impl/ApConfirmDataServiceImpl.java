/*
 * Created on 30 ?.?. 2549
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.ss.tp.service.impl;

import com.ss.tp.dao.ApConfirmDataDAO;
import com.ss.tp.dto.ApConfirmDataVO;
import com.ss.tp.service.ApConfirmDataService;

import java.io.Serializable;


/**
 * @author Administrator
 * 
 *         TODO To change the template for this generated type comment go to
 *         Window - Preferences - Java - Code Style - Code Templates
 */
public class ApConfirmDataServiceImpl implements ApConfirmDataService,
		Serializable {
	private ApConfirmDataDAO apConfirmData;

	public void insertApConfirmData(ApConfirmDataVO vo) throws Exception {
		this.apConfirmData.insertApConfirmData(vo);
	}
	
	
	public ApConfirmDataDAO getApConfirmData() {
		return apConfirmData;
	}

	public void setApConfirmData(ApConfirmDataDAO apConfirmData) {
		this.apConfirmData = apConfirmData;
	}

	public boolean isConfirmInputData(String ouCode, String year,
			String month,String volumeSet, String userId) throws Exception {
		return this.apConfirmData.isConfirmInputData(ouCode, year, month,volumeSet,
				userId);
	}

	public boolean isConfirmApData(String ouCode, String year, String month,String volumeSet,
			String userId) throws Exception {
		return this.apConfirmData.isConfirmApData(ouCode, year, month,volumeSet, userId);
	}
}
