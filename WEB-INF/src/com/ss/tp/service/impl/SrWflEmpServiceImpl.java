package com.ss.tp.service.impl;

import java.util.List;


import com.ss.tp.dao.SrWflEmpDAO;
import com.ss.tp.dto.DefaultYearSectionVO;
import com.ss.tp.dto.SrPvfEmpVO;
import com.ss.tp.service.SrWflEmpService;

public class SrWflEmpServiceImpl implements SrWflEmpService {

	private SrWflEmpDAO srWflEmpDAO;
	
	public SrWflEmpDAO getSrWflEmpDAO() {
		return srWflEmpDAO;
	}

	public void setSrWflEmpDAO(SrWflEmpDAO srWflEmpDAO) {
		this.srWflEmpDAO = srWflEmpDAO;
	}
	public List findMDate(String ouCode)
			throws Exception {
		return this.srWflEmpDAO.findMDate(ouCode);
	}
	public List findChgRateDate(String ouCode)
			throws Exception {
		return this.srWflEmpDAO.findChgRateDate(ouCode);
	}
	public List findChgMasterDate(String ouCode)
			throws Exception {
		return this.srWflEmpDAO.findChgMasterDate(ouCode);
	}
	public List findEmpStatusDate(String ouCode)
			throws Exception {
		return this.srWflEmpDAO.findEmpStatusDate(ouCode);
	}
	public List findLastDate(String ouCode)
			throws Exception {
		return this.srWflEmpDAO.findLastDate(ouCode);
	}
	public List findFDate(String ouCode)
			throws Exception {
		return this.srWflEmpDAO.findFDate(ouCode);
		}
	public List CTPFRP207(String ouCode,String ageYear, String dDate) {
		return this.srWflEmpDAO.CTPFRP207(ouCode, ageYear, dDate);
	}

	public List CTPFRP070( String dDate) {
		// TODO Auto-generated method stub
		return this.srWflEmpDAO.CTPFRP070( dDate);
	}

	public List CTPFRP029(String ouCode,String dDate) {
		// TODO Auto-generated method stub
		return this.srWflEmpDAO.CTPFRP029(ouCode,dDate);
	}

	public List CTPFRP019(String dDate) {
		// TODO Auto-generated method stub
		return this.srWflEmpDAO.CTPFRP019(dDate);
	}

	public List CTPFRP034(String dDate) {
		// TODO Auto-generated method stub
		return this.srWflEmpDAO.CTPFRP034(dDate);
	}
	public List CTPFRP001(String ouCode,  String status) {
		// TODO Auto-generated method stub
		return this.srWflEmpDAO.CTPFRP001(ouCode, status);
	}
	
	public List CTWLRP009N(String fDate) {
		// TODO Auto-generated method stub
		return this.srWflEmpDAO.CTWLRP009N(fDate);
	}
	public List decOutAllAmtReport(String userId, String evaOuCode, long evaYear,
			long evaPeriod, String incDecCode) {
		return this.srWflEmpDAO.decOutAllAmtReport(userId, evaOuCode,
				new Long(evaYear), new Long(evaPeriod), incDecCode);
	}

	public List decOutAllAmtReportCountSheet(String userId, String evaOuCode,
			long evaYear, long evaPeriod, String incDecCode) {
		return this.getSrWflEmpDAO().decOutAllAmtReportCountSheet(userId,
				evaOuCode, new Long(evaYear), new Long(evaPeriod), incDecCode);
	}
}
