package com.ss.tp.service;



import java.util.Date;
import java.util.List;

import com.ss.tp.dto.ApEmployeeVO;
import com.ss.tp.dto.ApTableVO;
import com.ss.tp.dto.DefaultYearSectionVO;
import com.ss.tp.dto.WeEmployeeVO;


public interface ApTableService {
	
	public DefaultYearSectionVO getDefaultYearAndSection(String ouCode,
			String userId) throws Exception;
	
	
	public DefaultYearSectionVO getDefaultYearAndSectionAP001(String ouCode,
			String userId) throws Exception;
	
	public DefaultYearSectionVO getDefaultYearAndSectionAP002(String ouCode,
			String userId) throws Exception;

	public DefaultYearSectionVO getDefaultYearAndSectionBK(String ouCode,
			String userId) throws Exception;

	
	public DefaultYearSectionVO getDefaultYearAndSectionTR(String ouCode,
			String userId) throws Exception;

	
	
	public void insertApTable(ApTableVO aptablevo)
			throws Exception;

	public void updateApTable(ApTableVO aptablevo)
			throws Exception;
	
	public void updateApTableApprove(ApTableVO aptablevo)
			throws Exception;


	public void deleteApTable(ApTableVO aptablevo)
			throws Exception;

	public void insertApTables(List aptablevolist)
			throws Exception;

	public List findByCriteria(String userId, String evaOuCode, long evaYear,
			long evaMonth, String evaEmpCodeFrom, String evaEmpCodeTo);

	public Integer countData(String userId, String evaOuCode, long evaYear,
			long evaMonth, long evaVolume);
	
	
	
	public Integer countDataApprove(String userId, String evaOuCode, long evaYear,
			long evaMonth, long evaVolume);

	public Integer countEmpData(String userId,String empCode, String evaOuCode, long evaYear
			);
	
	public ApEmployeeVO findByEmpCodeDetail(String empCode, String ouCode);

	public List findByEmpCodeList(String userId, String ouCode, long yearPn,
			long monthPn, String empCode);

	// public List rwPremiumReportByOrg(String userId, String evaOuCode, long
	// evaYear,long evaPeriod,String evaFlag);
	public void addList(ApTableVO rpVo, boolean isSave);
	
	public void addListApprove(ApTableVO rpVo, boolean isSave);

	public void insertAndUpdate() throws Exception;
	

	public boolean canDelete(String ouCode, String year, String month,
			String userId) throws Exception;

	public List findByCriteriaList(String userId, String evaOuCode,
			long evaYear, long evaMonth, long evaVolume, int count,
			int countRecord);

	public List findByCriteriaListApprove(String userId, String evaOuCode,
			long evaYear, long evaMonth, long evaVolume, int count,
			int countRecord);
	
	public List findByEmpCriteriaList(String userId,String empCode, String evaOuCode,
			long evaYear, int count,int countRecord);


	public List findByCriteriaReport(String userId, String evaOuCode,
			String evaYear, String evaMonth, String evaVolume);

	public List findByCriteriaPromoteLevelReport(String userId,
			String evaOuCode, long evaYear, long evaMonth, long evaVolume);

	public List findVolumeBySecurity(String userId, String evaOuCode,String evaYear,String evaMonth)
			throws Exception;
	
	public List findVolumeBySecurityApprove(String userId, String evaOuCode,String evaYear,String evaMonth)
			throws Exception;
	public List findVolumeBySecurityReport1(String userId, String evaOuCode,String evaYear,String evaMonth)
			throws Exception;
	
	public List findVolumeBySecurityApproveReport(String userId, String evaOuCode,String evaYear,String evaMonth)
			throws Exception;
	
	public List findVolumeBySecurityPm001(String userId, String evaOuCode,String evaYear,String evaMonth)
			throws Exception;
	
	public List findVolumeBySecurityPm002(String userId, String evaOuCode,String evaYear,String evaMonth)
			throws Exception;
		
	public List findVolumeBySecurityTransfer(String userId, String evaOuCode,String evaYear,String evaMonth)
			throws Exception;
	
	public boolean isConfirmFlag(String ouCode, String year, String month,
			String userId) throws Exception;
	
	public void deleteApTableInputError(String ouCode, String yearPn,
			String monthPn,String volumeSet,String userId) 
			throws Exception;
	
	public Integer confirmData(String ouCode, long yearPn, long monthPn,
			String volumeSetFrom,String volumeSetTo, String userId);
    
	public Double sumAllMoneyData(String ouCode, long yearPn, long monthPn,
			String volumeSetFrom,String volumeSetTo, String userId);

	public Double sumMoneyData(String ouCode, long yearPn, long monthPn,
			String volumeSetFrom,String volumeSetTo, String userId,String flag);
	
	public void updateConfirmData(String ouCode, long yearPn, long monthPn,String volumeSetFrom,String volumeSetTo,
			String userId) throws Exception;
	
	public Integer confirmDataApprove(String ouCode, long yearPn, long monthPn,
			String volumeSetFrom,String volumeSetTo, String userId);
    
	public Double sumAllMoneyDataApprove(String ouCode, long yearPn, long monthPn,
			String volumeSetFrom,String volumeSetTo, String userId);

	public Double sumMoneyDataApprove(String ouCode, long yearPn, long monthPn,
			String volumeSetFrom,String volumeSetTo, String userId,String flag);
	
	public void updateConfirmDataApprove(String ouCode, long yearPn, long monthPn,String volumeSetFrom,String volumeSetTo,
			String userId) throws Exception;
	
	public List apEmpListReport(String userId, String evaOuCode, String evaYear,
			String evaMonth,String volume);
	
	public List apEmpListApproveReport(String userId, String evaOuCode, String evaYear,
			String evaMonth,String volume,String approve);
	
	
	public List apEmpKtbBankReport(String userId, String evaOuCode, int evaYear,
			int evaMonth,String volumeFrom,String volumeTo);
	
	public List apEmpScbBankReport(String userId, String evaOuCode, int evaYear,
			int evaMonth,String volumeFrom,String volumeTo);	
	
	public List apTransferReport( String evaYear,String evaMonth);	
	
	public List apSumBatchApproveReport( String evaYear,String evaMonth,String volumeFrom,String volumeTo);	
	
	public String apToGl( String userId, String evaOuCode,String evaYear,
			String evaMonth, String volume1, String volume2,String accDate );

	public void updateConfirmDataBankKTB(String ouCode, long yearPn, long monthPn,String volumeSetFrom,String volumeSetTo,
			String userId) throws Exception;
	
	public void updateConfirmDataBankSCB(String ouCode, long yearPn, long monthPn,String volumeSetFrom,String volumeSetTo,
			String userId) throws Exception;
	
	public void updateConfirmDataTransfer(String ouCode, long yearPn, long monthPn,String volumeSetFrom,String volumeSetTo,
			String userId) throws Exception;
	
}