<%@ page language="java" contentType="text/html;charset=TIS-620" %>
<%@ page import="com.ss.tp.security.UserInfo" %>
<html>
<head>
<title>Update taMonthEmpWork</title>
<!-- Include -->
<script type="text/javascript" src="dwr/engine.js"></script>
<script type="text/javascript" src="dwr/util.js"></script>
<!-- Javascript Script File -->
<script type="text/javascript" src="dwr/interface/TaMonthEmpWorkService.js"></script>
<script type="text/javascript" src="script/gridScript.js"></script>
<script type="text/javascript">	
</script>
</head>
<body>
<%
	UserInfo uf =  (UserInfo)request.getSession().getAttribute("UserLogin");
	String ouCode =  uf.getOuCode();
	String userId =  uf.getUserId();
	String areaCode = request.getParameter("areaCode");
	String secCode = request.getParameter("secCode");
	String workCode = request.getParameter("workCode");
	String areaLabel = request.getParameter("areaLabel");
	String secLabel = request.getParameter("secLabel");
	String workLabel = request.getParameter("workLabel");
%>
<form name="hidForm" method="post">		
	<input type="hidden" name="hidOuCode" value="<%=ouCode%>"/>
	<input type="hidden" name="hidWorkYear" value="<%=request.getParameter("workYear")%>" />
	<input type="hidden" name="hidWorkMonth" value="<%=request.getParameter("workMonth")%>" />
	<input type="hidden" name="hidEmpCode" value="<%=request.getParameter("hidEmpCode")%>" />
	<input type="hidden" name="hidPage" value="update"/>
	<input type="hidden" name="pageEdit" value="<%=request.getParameter("page")%>"/>
	<input type="hidden" name="hidUserId" value="<%=userId%>"/><br>
	<input type="hidden" name="areaCode" value="<%=areaCode%>"/>
	<input type="hidden" name="secCode" value="<%=secCode%>"/>
	<input type="hidden" name="workCode" value="<%=workCode%>"/>
	<input type="hidden" name="areaLabel" value="<%=areaLabel%>"/>
	<input type="hidden" name="secLabel" value="<%=secLabel%>"/>
	<input type="hidden" name="workLabel" value="<%=workLabel%>"/>
	<input type="hidden" name="hidPage" value="update"/>
</form>
<table width="100%" >
<tr>
		<td class="font-head">[ CTTAQY001_1 ]  �ͺ�����������´�����š���һ�Ш���͹�ͧ��ѡ�ҹ/�١��ҧ��Ш�</td>
</tr>
<tr>
<form name="form1" action="security.htm" method="post">
<table width="400" border="0" align="center" cellspacing="1">
	<tr>
		<td class="font-field">��Шӻ�</td>
		<td><input type="text" name="workYear" size="2" readonly="readonly" style="width: 100;text-align: center;background-color: silver;"/></td>
		<td class="font-field">��͹</td>
		<td><input type="text" name="workMonth" size="5" readonly="readonly" style="width: 100;text-align: center;background-color: silver;"/></td>
	</tr>
	<tr>
		<td class="font-field">�Ţ��Шӵ��</td>
		<td><input type="text" name="empCode" size="10" readonly="readonly" style="width: 100;text-align: center;background-color: silver;"/></td>
		<td class="font-field">����</td>
		<td><input type="text" name="empName" size="20" readonly="readonly" style="width: 150;text-align: left;background-color: silver;"/><td>
</table>
<br/>
<div style="height:390px;overflow:auto;">
<table bordercolor="#6699CC" cellpadding="2" cellspacing="0"  width="600" align="center" border="1">
	<thead>
		<tr CLASS="TABLEBULE2">
		<th>���ʡ����</th>
		<th>��������´</th>
		<th>�ӹǹ�ѹ</th>
	</thead>
	<tbody id="dataTable">
	</tbody>
</table>
</div>
<table width="100%" CLASS="TABLEBULE2">
	<tr CLASS="TABLEBULE2">
		<td>
			<input type="button" value="   �͡   " name="exit" class="button" onclick="whenCallback('goBack');"/>
		</td>
	</tr>
</table>
</form>
</body>
</html>
<script>
	onInitPage()
	function onInitPage(){
		var frm = document.forms[0]
		var hidWorkYear = frm.elements["hidWorkYear"].value;
 		var hidWorkMonth = frm.elements["hidWorkMonth"].value;
		var hidEmpCode = frm.elements["hidEmpCode"].value;
		var hidOuCode = frm.elements["hidOuCode"].value;
		DWRUtil.setValue("workYear", hidWorkYear);
		DWRUtil.setValue("workMonth", convertMonth(hidWorkMonth));
		DWRUtil.setValue("empCode", hidEmpCode);
		TaMonthEmpWorkService.findEmpByKey(hidWorkYear, hidWorkMonth, hidEmpCode, {callback:whenShowEmpName});
		TaMonthEmpWorkService.findByKey(hidWorkYear, hidWorkMonth, hidEmpCode, hidOuCode, {callback:whenListDataTableHandler});		
	}
	function convertMonth(month){
		if (month == 1)
			return "���Ҥ�";
		else if (month == 2)
			return "����Ҿѹ��";
		else if (month == 3)
			return "�չҤ�";
		else if (month == 4)
			return "����¹";
		else if (month == 5)
			return "����Ҥ�";
		else if (month == 6)
			return "�Զع�¹";
		else if (month == 7)
			return "�á�Ҥ�";
		else if (month == 8)
			return "�ԧ�Ҥ�";
		else if (month == 9)
			return "�ѹ��¹";
		else if (month == 10)
			return "���Ҥ�";
		else if (month == 11)
			return "��Ȩԡ�¹";
		else if (month == 12)
			return "�ѹ�Ҥ�";
	}
	function whenShowEmpName(data){
		DWRUtil.setValue("empName", data.prefixName + data.firstName + ' ' + data.lastName);
		DWRUtil.setValue("hidOuCode", data.ouCode);
	}
	function whenListDataTableHandler(data){
		DWRUtil.removeAllRows("dataTable");		
		DWRUtil.addRows("dataTable", data, cellFuncs);
	}
	
	var cellFuncs = [	
		function(data) { return "<div align='center'>"+data.workCode+ "</div>";},
		function(data) { return data.workDesc;},
		function(data) { return "<div align='right'>"+data.totalDays+ "</div>";}
	];

	
	function whenCallback(val){
		var frm = document.forms[0];
		frm.action = "security.htm?reqCode=CTTAQY001";
		frm.hidPage.value = val;
		frm.submit();		
	}
	
</script>