<%@ page language="java" contentType="text/html;charset=TIS-620"%>
<%@ page import="com.ss.tp.security.UserInfo" %>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="/WEB-INF/tld/c.tld"%>
<%
	UserInfo userInfo = (UserInfo)session.getAttribute("UserLogin");
	String userId = userInfo.getUserId();
	String ouCode = userInfo.getOuCode();
	Calendar now = Calendar.getInstance(Locale.US);	
	String year =String.valueOf(now.get(Calendar.YEAR)+543);
	
%>
<html>
<head>
<title>View Time Attendance</title>
<!-- You have to include these two JavaScript files from DWR -->
<script type="text/javascript" src="dwr/engine.js"></script>
<script type="text/javascript" src="dwr/util.js"></script>
<script type="text/javascript" src="script/dojo.js"></script>
<script type="text/javascript" src="script/payroll_util.js"></script>
<!-- This JavaScript file is generated specifically for your application -->
<script type="text/javascript" src="dwr/interface/TaViewService.js"></script>
</head>
<script type="text/javascript">
	dojo.require("dojo.widget.*");
	dojo.require("dojo.widget.Menu2");
	dojo.require("dojo.widget.Button");
	dojo.require("dojo.widget.ComboBox");
	dojo.require("dojo.widget.DropDownButton");
	dojo.require("dojo.widget.SortableTable");
	dojo.require("dojo.widget.ContentPane");
	dojo.require("dojo.widget.LayoutContainer");
	dojo.require("dojo.widget.SortableTable");
	dojo.require("dojo.widget.Toolbar");
	dojo.require("dojo.widget.html.*");
	dojo.require("dojo.widget.Menu2");
	dojo.hostenv.writeIncludes();
//Event
	dojo.require("dojo.event.*");
	var orgName=[] ;
	var codeSeqData =[];
	var empCodeDataFrom=[];
	var empCodeDataTo=[];
function init(){
	var cboEmpFrom = dojo.widget.byId("empCodeFrom");
	dojo.event.connect(cboEmpFrom, "selectOption", "whenSelectEmpOption");
	
	}
function onLoadOrganization(){
	var cboOrgF = dojo.widget.byId("orgCboFrom");
	var cboOrgT = dojo.widget.byId("orgCboTo");
	var cboSource = [];
		<c:forEach items="${OrganizationInSecurity}" var="result" >		 
			cboSource.push(["<c:out value='${result.orgCode}' />"+" "+"<c:out value='${result.orgShowDesc}' />","<c:out value='${result.orgCode}' />"]);
			orgName.push(["<c:out value='${result.orgCode}' />"+" "+"<c:out value='${result.orgShowDesc}' />"]);
			codeSeqData.push(["<c:out value='${result.orgCode}' />"]);
		</c:forEach>
		cboOrgF.dataProvider.setData(cboSource);
		cboOrgT.dataProvider.setData(cboSource);
	}

function onLoadEmployee(){
	
		var frm=document.forms[0];
		var comB1=dojo.widget.byId("empCodeFrom");
		var comB2=dojo.widget.byId("empCodeTo");
		var arrData=[]
			<c:forEach items="${PnEmployeeInSecurity}" var="result" >		 
				arrData.push(["<c:out value='${result}' />","<c:out value='${result}' />"]);
				empCodeDataFrom.push(["<c:out value='${result}' />"]);
			</c:forEach>
		comB1.dataProvider.setData(arrData);
		comB2.dataProvider.setData(arrData);
		
	}
function initMonth(){
		var workYear = DWRUtil.getValue("workYear");
		var monthFrom='';
		var monthTo='';
		var tempMonth='';
		
		tempMonth=workYear.substring(2,workYear.length);
		monthTo ='10/'+(parseInt(tempMonth)-1);
		monthFrom = '9/'+tempMonth;
		DWRUtil.setValue("monthFrom",monthTo);
		DWRUtil.setValue("monthTo",monthFrom);
}
function initNameMonth(){
		DWRUtil.setValue("tempBudget1","�.�.");
		DWRUtil.setValue("tempBudget2","�.�.");
		DWRUtil.setValue("tempBudget3","�.�.");
		DWRUtil.setValue("tempBudget4","�.�.");
		DWRUtil.setValue("tempBudget5","�.�.");
		DWRUtil.setValue("tempBudget6","��.�.");
		DWRUtil.setValue("tempBudget7","��.�.");
		DWRUtil.setValue("tempBudget8","�.�.");
		DWRUtil.setValue("tempBudget9","��.�");
		DWRUtil.setValue("tempBudget10","�.�.");
		DWRUtil.setValue("tempBudget11","�.�.");
		DWRUtil.setValue("tempBudget12","�.�.");
}	
//     Start DOJO	
//	dojo.addOnLoad(init); comment bybow no use
	dojo.addOnLoad(initMonth);
//	dojo.addOnLoad(initNameMonth);
	dojo.addOnLoad(onLoadOrganization);
	dojo.addOnLoad(onLoadEmployee);
// Even ComboBox valueChange
function whenSelectEmpOption(){
	DWRUtil.useLoadingMessage("Loading ...");
	var cbo = dojo.widget.byId("empCodeFrom");
	whenFetchEmployeeTo(cbo.textInputNode.value);
	}
function whenFetchEmployeeTo(empCode){
	DWRUtil.useLoadingMessage("Loading ...");
	var cboTo = dojo.widget.byId("empCodeTo");
	var arrData=[]
	if( empCode > cboTo.comboBoxSelectionValue.value ){
			     	cboTo.textInputNode.value = '';
			     	cboTo.comboBoxSelectionValue.value = '';
	}

	<c:forEach items="${PnEmployeeInSecurity}" var="result" >		 
		if("<c:out value='${result}' />" >= empCode){
			arrData.push(["<c:out value='${result}' />","<c:out value='${result}' />"]);
			empCodeDataTo.push(["<c:out value='${result}' />"]);
		}
	</c:forEach>
	cboTo.dataProvider.setData(arrData);
	//PnEmployeeService.findToEmpBySecurity('<%=userId%>','<%=ouCode%>',empCode , {callback:whenFetchEmployeeToCallback});
}
function whenFetchEmployeeToCallback(data){
		    	try{
		    		var arrData=[];
			     	var cboTo = dojo.widget.byId("empCodeTo");
			     	for(i=0;i<data.length;i++){
						arrData.push([data[i].empCode,data[i].empCode]);
						empCodeDataTo.push([data[i].empCode]);
						}
			     	cboTo.dataProvider.setData(arrData);
		     	}catch(e){
		     		alert(e.message);
		     	}
}
</script>
<body>
<form  method="post" name="mainform" ><div style="height:400px;">
<table  width="100%" align="center" border="0">
<tr>
	<td align="left" valign="top" class="font-head">[ CTTAQY002 ]  �ͺ��������š���һ�Шӻբͧ��ѡ�ҹ/�١��ҧ��Ш�</td>
</tr>
<tr>
	<td aling ="center"> <input type="hidden" name="ouCode" value="<%=ouCode%>"><input type="hidden" name="userId" value="<%=userId%>">
 		<table width="770" align="center" border="0" cellpadding="2" cellspacing="2">
			<tr>
				<td class="font-field"  align="right">��Шӻ�</td>
				<td  colspan="4" ><input type="text" name="workYear" style="text-align: center;"  value="<%=year%>" size="4" maxlength="4" onkeyup="if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);"  onChange="onChangeMonth(this.value);" /></td>
				
			</tr>
			<tr>
				<td class="font-field"  align="right">�������͹/��</td>
				<td><input type="text" name="monthFrom"   size="5" maxlength="5" style="text-align: center;" /></td>
				<td class="font-field">�֧�״�͹/��</td>
				<td colspan="2"  ><input type="text" name="monthTo"   size="5" maxlength="5" style="text-align: center;" /></td>
			</tr>
			<tr>
            		<td class="font-field"  align="right">������ѧ�Ѵ��Ժѵԧҹ</td>
            		<td colspan="4"><select  dojotype="ComboBox"  widgetid="orgCboFrom" style="width:570"></select></td>
            		
          	</tr>
			<tr>
            		<td class="font-field"  align="right">�֧�ѧ�Ѵ��Ժѵԧҹ</td>
            		<td colspan="4"><select  dojotype="ComboBox"  widgetid="orgCboTo" style="width:570"></select></td>
            		
          	</tr>
			<tr>
				<td width="130" class="font-field"  align="right">������Ţ��Шӵ��</td>
				<td width="150"><SELECT  widgetId="empCodeFrom" dojoType="ComboBox" style="width:120" ></SELECT></td>
				<td width="100" class="font-field">	�֧�Ţ��Шӵ��</td>
				<td width="150"><SELECT  widgetId="empCodeTo" dojoType="ComboBox" style="width:120"></SELECT></td>
				<td width="220" align="right"><input type="button" class=" button "  value=" ���� " onclick="whenLoadTaView();" /></td>
			</tr>
			
 		</table>
	</td>
</tr>
<tr>
	<td aling ="center"><div style="height:400px;">
		<table width="992" aling ="center"  border="1" bordercolor="#6699CC"  cellpadding="2" cellspacing="0">
		<thead>
		<tr CLASS="TABLEBULE" >
				<th CLASS="TABLEBULE" colspan="2" >&nbsp;</th>
				<th CLASS="TABLEBULE" align="center" colspan="14" >�ӹǹ�ѹ</th>
				
		</tr>
		<tr CLASS="TABLEBULE2">
			<th CLASS="TABLEBULE2">��ѡ�ҹ</th>
			<th CLASS="TABLEBULE2">��÷ӧҹ</th>
			<th CLASS="TABLEBULE2"><input type="text" name="tempBudget1"  readonly="readonly" style="width:45;text-align: center;center;font-weight:bold;color: #FFFFFF;font-size:10pt;border-style : none;background-color: #3399CC;" /></th>
			<th CLASS="TABLEBULE2"><input type="text" name="tempBudget2"  readonly="readonly" style="width:45;text-align: center;center;font-weight:bold;color: #FFFFFF;font-size:10pt;border-style : none;background-color: #3399CC;" /></th>
			<th CLASS="TABLEBULE2"><input type="text" name="tempBudget3"   readonly="readonly" style="width:45;text-align: center;center;font-weight:bold;color: #FFFFFF;font-size:10pt;border-style : none;background-color: #3399CC;" /></th>
			<th CLASS="TABLEBULE2"><input type="text" name="tempBudget4"  readonly="readonly" style="width:45;text-align: center;center;font-weight:bold;color: #FFFFFF;font-size:10pt;border-style : none;background-color: #3399CC;" /></th>
			<th CLASS="TABLEBULE2"><input type="text" name="tempBudget5"  readonly="readonly" style="width:45;text-align: center;center;font-weight:bold;color: #FFFFFF;font-size:10pt;border-style : none;background-color: #3399CC;" /></th>
			<th CLASS="TABLEBULE2"><input type="text" name="tempBudget6"   readonly="readonly" style="width:45;text-align: center;center;font-weight:bold;color: #FFFFFF;font-size:10pt;border-style : none;background-color: #3399CC;" /></th>
			<th CLASS="TABLEBULE2"><input type="text" name="tempBudget7"  readonly="readonly" style="width:45;text-align: center;center;font-weight:bold;color: #FFFFFF;font-size:10pt;border-style : none;background-color: #3399CC;" /></th>
			<th CLASS="TABLEBULE2"><input type="text" name="tempBudget8"  readonly="readonly" style="width:45;text-align: center;center;font-weight:bold;color: #FFFFFF;font-size:10pt;border-style : none;background-color: #3399CC;" /></th>
			<th CLASS="TABLEBULE2"><input type="text" name="tempBudget9"  readonly="readonly" style="width:45;text-align: center;center;font-weight:bold;color: #FFFFFF;font-size:10pt;border-style : none;background-color: #3399CC;" /></th>
			<th CLASS="TABLEBULE2"><input type="text" name="tempBudget10"  readonly="readonly" style="width:45;text-align: center;center;font-weight:bold;color: #FFFFFF;font-size:10pt;border-style : none;background-color: #3399CC;" /></th>
			<th CLASS="TABLEBULE2"><input type="text" name="tempBudget11"  readonly="readonly" style="width:45;text-align: center;center;font-weight:bold;color: #FFFFFF;font-size:10pt;border-style : none;background-color: #3399CC;" /></th>
			<th CLASS="TABLEBULE2"><input type="text" name="tempBudget12"  readonly="readonly" style="width:45;text-align: center;center;font-weight:bold;color: #FFFFFF;font-size:10pt;border-style : none;background-color: #3399CC;" /></th>
			<th CLASS="TABLEBULE2">���</th>
		</tr>
		</thead>
		<tbody id="taViewTable" >
		</tbody>

		</table>
		</div>
	</td>
</tr>
<tr>
	<td>
		<table width="100%" CLASS="TABLEBULE2" >
					<tr CLASS="TABLEBULE2" >
						<td align="left" >&nbsp;
							
						</td>
					</tr>
			</table>
	</td>
</tr>
</table>
</form>
</body>
<script>
	
	function onChangeMonth(month){
		var monthFrom='';
		var monthTo='';
		var tempMonth='';
		if (month.length < 4){
			alert("�վ.�.��ͧ�� 4 ��ѡ");
		}else{
			tempMonth=month.substring(2,month.length);
			monthTo ='10/'+(parseInt(tempMonth)-1);
			monthFrom = '9/'+tempMonth;
			DWRUtil.setValue("monthFrom",monthTo);
			DWRUtil.setValue("monthTo",monthFrom);
			
		}
	}
	function whenLoadTaView(){
		var frm = document.forms[0];
		DWRUtil.useLoadingMessage("Loading ...");
		var workYear = DWRUtil.getValue("workYear");
		var monthFrom = DWRUtil.getValue("monthFrom");
		var monthTo  =DWRUtil.getValue("monthTo");
		var comB1=dojo.widget.byId("empCodeFrom").textInputNode.value;
		var comB2=dojo.widget.byId("empCodeTo").textInputNode.value;
		var nameOrgFrom = dojo.widget.byId("orgCboFrom").textInputNode.value;
		var nameOrgTo = dojo.widget.byId("orgCboTo").textInputNode.value;
		var orgCodeFrom = "";
		var orgCodeTo = "";
		var check = false;
		var codeFrom="";
		var codeTo="";
//   for check Month
		var tempMonthFrom ='';
		var tempMonthTo = '';
		
		var tempYearFrom;
		var tempYearTo;
		var tempYear ='';
		var tempYearFront ='';
		var calYear = '';
		
		var calMonthFrom =0;
		var calMonthTo =0;
		var sumMonth = 0;
//     fix choice sql
		var choice=''; 	
		if(workYear==''){
			alert("��سҡ�͡��");
			return false;
		}else if (workYear.length < 4){
			alert("�վ.�.��ͧ�� 4 ��ѡ");
			return false;
		}
		if(monthFrom == '' || monthTo == ''){
			alert("��سҡ�͡���  ��͹/��");
			return false;
		}else{
			tempYear=workYear.substring(2,workYear.length);
			tempYearFront = workYear.substring(0,workYear.length-2);
			if( checkMonth(monthFrom)){
				alert("��سҡ�͡ ��͹/�� ���� ");
				return false;
			}
			
			if( checkMonth(monthTo)){
				alert("��سҡ�͡ ��͹/�� ���� ");
				return false;
			}
			
			if(monthFrom.length ==5 ){
				tempYearFrom = monthFrom.substring(3,monthFrom.length);
				tempMonthFrom = monthFrom.substring(0,monthFrom.length-3);
				if(!isNumber(tempMonthFrom)){
					alert("��͹/�� ��ͧ�繵���Ţ");
					return false;
				}
				if(!isNumber(tempYearFrom)){
					alert("��͹/�� ��ͧ�繵���Ţ");
					return false;
				}
				
			}else if(monthFrom.length == 4){
				tempYearFrom = monthFrom.substring(2,monthFrom.length);
				tempMonthFrom = monthFrom.substring(0,monthFrom.length-3);
				if(!isNumber(tempMonthFrom)){
					alert("��͹/�� ��ͧ�繵���Ţ");
					return false;
				}
				if(!isNumber(tempYearFrom)){
					alert("��͹/�� ��ͧ�繵���Ţ");
					return false;
				}
			}
			
			if(monthTo.length ==5 ){
				tempYearTo = monthTo.substring(3,monthTo.length);
				tempMonthTo = monthTo.substring(0,monthTo.length-3);
				if(!isNumber(tempMonthTo)){
					alert("��͹/�� ��ͧ�繵���Ţ");
					return false;
				}
				if(!isNumber(tempYearTo)){
					alert("��͹/�� ��ͧ�繵���Ţ");
					return false;
				}
				
			}else if(monthTo.length == 4){
				tempYearTo = monthTo.substring(2,monthTo.length);
				tempMonthTo = monthTo.substring(0,monthTo.length-3);
				if(!isNumber(tempMonthTo)){
					alert("��͹/�� ��ͧ�繵���Ţ");
					return false;
				}
				if(!isNumber(tempYearTo)){
					alert("��͹/�� ��ͧ�繵���Ţ");
					return false;
				}
			}
			
			if(tempYearFrom > tempYearTo){
				alert("��͹/�� ������鹵�ͧ���¡��� ��͹�� ����ش");
				return false;
			}
			if(tempYearFrom == tempYearTo){
				if(tempYearTo != tempYear){
					alert("�״�͹/�� ��ͧ�繻����ǡѹ�Ѻ ��Шӻ�");
					return false;
				}
				if((tempMonthTo-tempMonthFrom)< 0  ){
					alert("��͹ ������鹵�ͧ���¡�����͹����ش");
					return false;
				}else{
					for(var i=tempMonthFrom;i <= tempMonthTo;i++){
						sumMonth++;
						
					}
				}
				choice=1;
			}
			if(tempYearTo > tempYearFrom ){
				calYear = tempYear  - tempYearFrom;
			
				if(calYear>1){
					alert("��͹/�� ������鹵�ҧ�Ѻ ��Шӻ� ������Թ 1 ��");	
					return false;
				}
				calYear = tempYearTo - tempYear;
				if(calYear>1){
					alert("��͹/�� ����ش��ҧ�Ѻ ��Шӻ� ������Թ 1 ��");	
					return false;
				}
				calYear = tempYearTo - tempYearFrom;
				if(calYear>1){
					alert("��͹/�� ��ҧ�ѹ������Թ 1 ��");
					return false;
				}else{
					for(var k = tempMonthFrom; k < 13;k++){
						calMonthFrom ++;
					}
					 for(var y =1 ;y <= tempMonthTo;y++){
						calMonthTo++;
					}
					sumMonth = 	calMonthFrom + calMonthTo;
					if(sumMonth>12){
						alert("�ӹǹ ��͹ ������ǵ�ͧ����Թ 12 ��͹");
						return false;
					}
				}
				choice=2;
			}
			tempYearFrom =tempYearFront+tempYearFrom; 
			tempYearTo = tempYearFront+tempYearTo; 
		
		}
			
	

		if(nameOrgFrom!=''){
			dojo.widget.byId("orgCboFrom").comboBoxSelectionValue.value = splitCombo(dojo.widget.byId("orgCboFrom").textInputNode.value);
			orgCodeFrom = splitCombo(dojo.widget.byId("orgCboFrom").textInputNode.value);
			
		}
		if(nameOrgTo !=''){
			dojo.widget.byId("orgCboTo").comboBoxSelectionValue.value = splitCombo(dojo.widget.byId("orgCboTo").textInputNode.value);
			orgCodeTo =  splitCombo(dojo.widget.byId("orgCboTo").textInputNode.value);
			
		}
		
		if(dojo.widget.byId("orgCboTo").textInputNode.value!=""){
			if(dojo.widget.byId("orgCboFrom").textInputNode.value==""){
				alert("��سҡ�͡ �ѧ�Ѵ��Ժѵԧҹ������� ");
				return false;
			}
		}
//check emp
		if(comB1!=''){
			dojo.widget.byId("empCodeFrom").comboBoxSelectionValue.value = splitCombo(dojo.widget.byId("empCodeFrom").textInputNode.value);
			codeFrom = splitCombo(dojo.widget.byId("empCodeFrom").textInputNode.value);
			
		}
		if(comB2 !=''){
			dojo.widget.byId("empCodeTo").comboBoxSelectionValue.value = splitCombo(dojo.widget.byId("empCodeTo").textInputNode.value);
			codeTo =  splitCombo(dojo.widget.byId("empCodeTo").textInputNode.value);
			
		}
		if(dojo.widget.byId("empCodeTo").textInputNode.value!=""){
			if(dojo.widget.byId("empCodeFrom").textInputNode.value==""){
				alert("��سҡ�͡ �Ţ��Шӵ��������� ");
				return false;
			}
		}
		
		
		TaViewService.findListEmpDetailNew(DWRUtil.getValue("ouCode"),
											DWRUtil.getValue("userId"),
											DWRUtil.getValue("workYear"),
											tempYearFrom,
											tempYearTo,
											tempMonthFrom,
											tempMonthTo,
											orgCodeFrom,
											orgCodeTo,
											codeFrom,
											codeTo,
											choice,{callback:onListTaView});
	}
	var cellFuncs = [		
		function(data) {
						var emp =data.empCodeTemp+" "+data.fullName;
							return emp; },
		function(data) {var work= data.workCode +" "+data.workDesc;
						 return work; },
		function(data) { return "<div align='right'>"+data.month1+ "</div>" ;},
		function(data) { return "<div align='right'>"+data.month2+"</div>"; },
		function(data) { return "<div align='right'>"+data.month3+"</div>"; },
		function(data) { return "<div align='right'>"+data.month4+"</div>"; },
		function(data) { return "<div align='right'>"+data.month5+"</div>"; },
		function(data) { return "<div align='right'>"+data.month6+"</div>"; },
		function(data) { return "<div align='right'>"+data.month7+"</div>"; },
		function(data) { return "<div align='right'>"+data.month8+"</div>"; },
		function(data) { return "<div align='right'>"+data.month9+"</div>"; },
		function(data) { return "<div align='right'>"+data.month10+"</div>"; },
		function(data) { return "<div align='right'>"+data.month11+"</div>"; },
		function(data) { return "<div align='right'>"+data.month12+"</div>"; },
		function(data) { return "<div align='right'>"+data.total+"</div>"; }
	];
	function onListTaView(data){
		if(data!=''){
		DWRUtil.setValue("tempBudget1",data[0].monthName1);
		DWRUtil.setValue("tempBudget2",data[0].monthName2);
		DWRUtil.setValue("tempBudget3",data[0].monthName3);
		DWRUtil.setValue("tempBudget4",data[0].monthName4);
		DWRUtil.setValue("tempBudget5",data[0].monthName5);
		DWRUtil.setValue("tempBudget6",data[0].monthName6);
		DWRUtil.setValue("tempBudget7",data[0].monthName7);
		DWRUtil.setValue("tempBudget8",data[0].monthName8);
		DWRUtil.setValue("tempBudget9",data[0].monthName9);
		DWRUtil.setValue("tempBudget10",data[0].monthName10);
		DWRUtil.setValue("tempBudget11",data[0].monthName11);
		DWRUtil.setValue("tempBudget12",data[0].monthName12);
		}else{
		DWRUtil.setValue("tempBudget1","-");
		DWRUtil.setValue("tempBudget2","-");
		DWRUtil.setValue("tempBudget3","-");
		DWRUtil.setValue("tempBudget4","-");
		DWRUtil.setValue("tempBudget5","-");
		DWRUtil.setValue("tempBudget6","-");
		DWRUtil.setValue("tempBudget7","-");
		DWRUtil.setValue("tempBudget8","-");
		DWRUtil.setValue("tempBudget9","-");
		DWRUtil.setValue("tempBudget10","-");
		DWRUtil.setValue("tempBudget11","-");
		DWRUtil.setValue("tempBudget12","-");
		}
		DWRUtil.removeAllRows("taViewTable");
		DWRUtil.addRows("taViewTable", data, cellFuncs);

	}
	
	
	function isNumber(num){
		var realNumber = "1234567890";
		var length = num.length;		
		var temp;
		var check = true;
		for(var i=0;i<length;i++){
			temp = num.substring(i,i+1);
			if ( realNumber.indexOf(temp) == -1 ){
				check = false;
				break;
			}
		}
		return check;
	}
	
	function checkMonth(str){
		var tempStr ="/";
		var sizeIs = str.length;
		var temp;
		var check = true;
		for(var i=0;i<sizeIs;i++){
			temp = str.substring(i,i+1);
			if( temp == '/'){
				check = false;
				break;
			}
		}
		
		return check;
	}
	
	
	
</script>
</html>