<%@ page language="java" contentType="text/html;charset=TIS-620"%>
<%@ page import="com.ss.tp.security.UserInfo" %>
<%@ page import="java.util.List" %>
<%@ taglib prefix="c" uri="/WEB-INF/tld/c.tld"%>
<%
	UserInfo userInfo = (UserInfo)session.getAttribute("UserLogin");
	String userId = userInfo.getUserId();
	String ouCode = userInfo.getOuCode();
	
	String year = (String)session.getAttribute("wgDraftYear");
	//System.out.println("year : " + year);
	
	String yearEdit = request.getParameter("yearEdit");
	String orgFromEdit = request.getParameter("orgFromEdit");
	String orgToEdit = request.getParameter("orgToEdit");
	String empCodeFromEdit = request.getParameter("empCodeFromEdit");
	String empCodeToEdit = request.getParameter("empCodeToEdit");
	//String pageEdit = request.getParameter("pageEdit");
	
	// ***** declare for paging  **********
	String pageEdit = request.getParameter("pageEdit")==null?"-1":request.getParameter("pageEdit");
%>
<html>
	<head>
		<title>��âͻ�Ѻ��Ҩ�ҧ�ͧ�١��ҧ��Шӻ�</title>
		<!-- Include -->
		<script type="text/javascript" src="dwr/engine.js"></script>
		<script type="text/javascript" src="dwr/util.js"></script>
		<script type="text/javascript" src="script/payroll_util.js"></script>
		<!-- Javascript Script File -->
		<SCRIPT type="text/javascript" src="dwr/interface/SuUserOrganizationService.js"></SCRIPT>
		<SCRIPT type="text/javascript" src="dwr/interface/WgPrEmployeeService.js"></SCRIPT>
		<SCRIPT type="text/javascript" src="dwr/interface/WgDraftService.js"></SCRIPT>

		<script type="text/javascript" src="script/gridScript.js"></script>
		<script type="text/javascript" src="page/NavigatePage.jsp"></script>
		<script type="text/javascript" src="script/dojo.js"></script>

		<script type="text/javascript">
			//Load Dojo's code relating to widget managing functions
			dojo.require("dojo.widget.*");
			dojo.require("dojo.widget.Menu2");
			dojo.require("dojo.widget.Button");
			dojo.require("dojo.widget.ComboBox");
			dojo.require("dojo.widget.DropDownButton");
			dojo.require("dojo.widget.SortableTable");
			dojo.require("dojo.widget.ContentPane");
			dojo.require("dojo.widget.LayoutContainer");
			dojo.require("dojo.widget.SortableTable");
			dojo.require("dojo.widget.Toolbar");
			dojo.require("dojo.widget.html.*");
			dojo.require("dojo.widget.Menu2");
			dojo.hostenv.writeIncludes();
			
			//Event
			dojo.require("dojo.event.*");
		</script>
		<script type="text/javascript">
			var tempSource;
		
			function onLoadOrganization(){
		     	DWRUtil.useLoadingMessage("Loading ...");
		     	onLoadOrganizationCallback();
		    }
		    function onLoadOrganizationCallback(){
		     	try{
			     	var cboSource = [];
			     	var cboFrom = dojo.widget.byId("orgFromCbo");
			     	var cboTo = dojo.widget.byId("orgToCbo");
			     	
			     	<c:forEach items="${OrganizationInSecurity}" var="result" >		 
						cboSource.push(["<c:out value='${result.orgCode} ${result.orgShowDesc}' />","<c:out value='${result.orgCode}' />"]);
					</c:forEach>
			     	
			     	cboFrom.dataProvider.setData(cboSource);
			     	cboTo.dataProvider.setData(cboSource);		
			     		     	
			     	onLoadEmployee();
			     	
		     	}catch(e){
		     		alert(e.message);
		     	}
		    }
		    
		    /****** Begin  OrgCodeFrom Select ********/
		    function whenOrgFromSelectOption(){
		    	DWRUtil.useLoadingMessage("Loading ...");
				var cbo = dojo.widget.byId("orgFromCbo");
				whenFetchOrganizationTo( splitCombo( cbo.textInputNode.value ) );
			}
			function whenFetchOrganizationTo(orgCode){
				if( orgCode != '' ){
			     	DWRUtil.useLoadingMessage("Loading ...");
			     	var cboTo = dojo.widget.byId("orgToCbo");
			     	//alert( orgCode + " " + cboTo.comboBoxSelectionValue.value + " " + (orgCode > cboTo.comboBoxSelectionValue.value));
			     	if( orgCode > cboTo.comboBoxSelectionValue.value ){
				     	cboTo.textInputNode.value = '';
				     	cboTo.comboBoxSelectionValue.value = '';
			     	}
			     	
			     	var cboSource = [];
		     	
			     	<c:forEach items="${OrganizationInSecurity}" var="result" >		 
						if( <c:out value='${result.orgCode}' /> >= orgCode )
							cboSource.push(["<c:out value='${result.orgCode} ${result.orgDesc}' />","<c:out value='${result.orgCode}' />"]);
					</c:forEach>
			     	
			     	cboTo.dataProvider.setData(cboSource);
			     	
			     	//SuUserOrganizationService.findOrganizationByUserIdAndOuCodeToOrgCode('<%=userId%>','<%=ouCode%>',orgCode , {callback:whenFetchOrganizationToCallback,errorHandler:function(message) { alert('�Դ��ͼԴ��Ҵ����ǡѺ��õԴ����������');}});
		     	}
		    }
		    //function whenFetchOrganizationToCallback(data){
		    //	try{
			//     	var cboSource = [];
			//     	var cboTo = dojo.widget.byId("orgToCbo");
			//     	for(var i=0; i<data.length; i++){
			//     		var org = data[i];
			//     		cboSource.push([org.orgCode, org.orgCode]);
			//     	}
			//     	cboTo.dataProvider.setData(cboSource);
		    // 	}catch(e){
		    // 		alert(e.message);
		    // 	}
		    //}
		    /****** End  OrgCodeFrom Select ********/
		    
		    function onLoadEmployee(){
				onLoadEmployeeCallback();
			}
			function onLoadEmployeeCallback(){
		     	try{
			     	var cboSource = [];
			     	var cboFrom = dojo.widget.byId("empFromCbo");
			     	var cboTo = dojo.widget.byId("empToCbo");

			     	
			     	<c:forEach items="${WgDraftEmployeeInSecurity}" var="result" >		 
						cboSource.push(["<c:out value='${result.empCode}' />","<c:out value='${result.empCode}' />"]);
					</c:forEach>			     	
			     	
			     	cboFrom.dataProvider.setData(cboSource);
			     	cboTo.dataProvider.setData(cboSource);
			     	
			     	<%
			     		if( yearEdit != null && !yearEdit.trim().equals("") ){
							System.out.println("yearEdit : " + yearEdit);
							System.out.println("orgFromEdit : " + orgFromEdit);
							System.out.println("orgToEdit : " + orgToEdit);
							System.out.println("empCodeFromEdit : " + empCodeFromEdit);
							System.out.println("empCodeToEdit : " + empCodeToEdit);
							System.out.println("pageEdit : " + pageEdit);
							
			     	%>
			     	
			     			var cboOrgFrom = dojo.widget.byId("orgFromCbo");
			     			var cboOrgTo = dojo.widget.byId("orgToCbo");
			     			var year = document.getElementById("years");
			     			
			     			cboOrgFrom.textInputNode.value = '<%= orgFromEdit %>';
			     			cboOrgTo.textInputNode.value = '<%= orgToEdit %>';
			     			cboFrom.textInputNode.value = '<%= empCodeFromEdit %>';
			     			cboTo.textInputNode.value = '<%= empCodeToEdit %>';
			     			year.value = '<%= yearEdit %>';
			     			$("page").value = '<%= pageEdit %>';
			     			
			     			whenQueryEmpBefore();
			     			
			     	<%
			     		}
			     	%>
			     	
		     	}catch(e){
		     		alert(e.message);
		     	}
		    }
		    
		    /****** Begin  EmpCodeFrom Select ********/
			function whenEmpFromSelectOption(){
				DWRUtil.useLoadingMessage("Loading ...");
				var cbo = dojo.widget.byId("empFromCbo");
				whenFetchEmployeeTo(cbo.textInputNode.value);
			}
			function whenFetchEmployeeTo(empCode){
				if( empCode != '' ){
			     	DWRUtil.useLoadingMessage("Loading ...");
			     	var cboTo = dojo.widget.byId("empToCbo");
			     	//alert( empCode + " " + cboTo.comboBoxSelectionValue.value + " " + (empCode > cboTo.comboBoxSelectionValue.value));
			     	if( empCode > cboTo.comboBoxSelectionValue.value ){
				     	cboTo.textInputNode.value = '';
				     	cboTo.comboBoxSelectionValue.value = '';
			     	}
			     	
			     	var cboSource = [];
		     	
		     		for(var i=0; i<tempSource.length; i++){
		     			if( empCode <= tempSource[i] ){
		     				var emp = tempSource[i];
		     				cboSource.push([emp.empCode, emp.empCode]);
		     			}
		     		}
		     	
		     		cboTo.dataProvider.setData(cboSource);
			     	
			     	//WgDraftService.findFromEmployee( $("year").value, '<%//=ouCode%>', '<%//=userId%>', empCode, {callback:whenFetchEmployeeToCallback,errorHandler:function(message) { alert('�Դ��ͼԴ��Ҵ����ǡѺ��õԴ����������');}});
		     	}
		    }
		    //function whenFetchEmployeeToCallback(data){
		    //	try{
		    		//alert( data.length );
			//     	var cboSource = [];
			//     	var cboTo = dojo.widget.byId("empToCbo");
			//     	for(var i=0; i<data.length; i++){
			//     		var emp = data[i];
			//     		cboSource.push([emp.empCode, emp.empCode]);
			//     	}
			//     	cboTo.dataProvider.setData(cboSource);
		    // 	}catch(e){
		    // 		alert(e.message);
		    // 	}
		    //}
		    /******  End EmpCodeFrom Select ********/
		    
		    var cellFuncs = [
				function(data) { return "<div align='center'>"+data.empCode+"</div>";},
				function(data) { return data.firstName + ' ' + data.lastName;},
//				function(data) { return "<div align='center'>"+data.positionShort+"</div>";},
				function(data) { return writeButton("edit",data.empCode);}
			];
			
			function writeButton(inname,emp)
			{
				return "<div align='center'><input type='button' class='button' name = '"+inname+"' value='...' onclick='preEdit(this.empId);' empId='"+emp+"' /></div>";
			}
		    
			function init(){
				var cboOrgFrom = dojo.widget.byId("orgFromCbo");
				dojo.event.connect(cboOrgFrom, "selectOption", "whenOrgFromSelectOption");
				var cboEmpFrom = dojo.widget.byId("empFromCbo");
				dojo.event.connect(cboEmpFrom, "selectOption", "whenEmpFromSelectOption");
				
			}
		    
		   //dojo.addOnLoad(init);
		    dojo.addOnLoad(onLoadOrganization);
			//dojo.addOnLoad(onLoadOrganization);
			//dojo.addOnLoad(onLoadEmployee);
		
			function preEdit(empId){ 
				var frm=document.forms["editForm"];
				var cboOrgFrom = dojo.widget.byId("orgFromCbo");
				var cboOrgTo = dojo.widget.byId("orgToCbo");
				var cboEmpFrom = dojo.widget.byId("empFromCbo");
				var cboEmpTo = dojo.widget.byId("empToCbo");
				var showPage = document.getElementById("showPage");
				
				var yearVal = document.getElementById("years").value;
				
				//alert(empId);
				
				$("empCodeEdit").value = empId;
				$("yearEdit").value = yearVal;
				$("orgFromEdit").value = cboOrgFrom.textInputNode.value;
				$("orgToEdit").value =cboOrgTo.textInputNode.value;
				$("empCodeFromEdit").value = cboEmpFrom.textInputNode.value;
			 	$("empCodeToEdit").value = cboEmpTo.textInputNode.value;
				$("pageEdit").value = showPage.value;
				
				frm.submit();
			}
			
			function whenBlurYear(){
				SuUserOrganizationService.findOrganizationByUserIdAndOuCode('<%= userId %>', '<%= ouCode %>', {callback:whenFetchOrganizationCallback,errorHandler:function(message) { alert('�Դ��ͼԴ��Ҵ����ǡѺ��õԴ����������');}});
			}
			function whenFetchOrganizationCallback(data){
		    	try{
			     	var cboSource = [];
			     	var cboFrom = dojo.widget.byId("orgFromCbo");
			     	var cboTo = dojo.widget.byId("orgToCbo");
			     	
			     	for(var i=0; i<data.length; i++){
			     		var org = data[i];
			     		cboSource.push([org.orgCode + ' ' + org.orgShowDesc, org.orgCode]);
			     	}
			     	
			     	cboFrom.dataProvider.setData(cboSource);
			     	cboTo.dataProvider.setData(cboSource);	
			     	
			     	var yearVal = document.getElementById("years").value;
			     	
			     	WgDraftService.findEmployee(yearVal, '<%= ouCode %>', '<%= userId %>', {callback:whenFetchEmployeeCallback,errorHandler:function(message) { alert('�Դ��ͼԴ��Ҵ����ǡѺ��õԴ����������');}});
		     	}catch(e){
		     		alert(e.message);
		     	}
		    }
		    function whenFetchEmployeeCallback(data){
		     	try{
			     	var cboSource = [];
			     	var cboFrom = dojo.widget.byId("empFromCbo");
			     	var cboTo = dojo.widget.byId("empToCbo");

			     	
			     	for(var i=0; i<data.length; i++){
			     		var emp = data[i];
			     		cboSource.push([emp.empCode, emp.empCode]);
			     	}		     	
			     	
			     	tempSource = data;
			     	
			     	cboFrom.dataProvider.setData(cboSource);
			     	cboTo.dataProvider.setData(cboSource);
		     	}catch(e){
		     		alert(e.message);
		     	}
		    }
		    
		    function whenQueryEmpBefore(){
		    	DWRUtil.useLoadingMessage("Loading ...");
		     	var orgFromCbo = dojo.widget.byId("orgFromCbo");
				var orgToCbo = dojo.widget.byId("orgToCbo");
				var	empFromCbo = dojo.widget.byId("empFromCbo");
				var	empToCbo = dojo.widget.byId("empToCbo");
				
				//alert(dojo.widget.byId("orgFromCbo"));
				var orgFromVal = splitCombo( orgFromCbo.textInputNode.value );
				var orgToVal   = splitCombo( orgToCbo.textInputNode.value );
				var empFromVal = empFromCbo.textInputNode.value;
				var empToVal   = empToCbo.textInputNode.value;		
				//alert('page : ' +  page + ' ' + DWRUtil.getValue("dataPerPage") );
				
				var yearVal = document.getElementById("years").value;
				
				WgDraftService.findWgDraftByCriteria(
		     		yearVal,
		     		'<%= ouCode %>', 
		     		'<%= userId %>',
		     		orgFromVal,
					orgToVal,
					empFromVal,
					empToVal,
					DWRUtil.getValue("page"),
					DWRUtil.getValue("dataPerPage"),
					{callback:whenQueryEmpCallBack,errorHandler:function(message) { alert('�Դ��ͼԴ��Ҵ����ǡѺ��õԴ����������');}}
				);
				
		    }
		    function whenQueryEmpCallBack(data){
		     	//alert(data.length);
		     	if(data.length > 0){
					DWRUtil.removeAllRows("dataTable");
					DWRUtil.addRows("dataTable",data,cellFuncs);
					if(DWRUtil.getValue("showMaxPage") == ''){
						countData();
					}else{
						onCheckButt("searchForm");
					}
				}else{
					alert('��辺������');
					DWRUtil.removeAllRows("dataTable");
					if(DWRUtil.getValue("showMaxPage") == ''){
						countData();
					}else{
						onCheckButt("searchForm");
					}
				}
		    }
		    
		    function countData(){
		    	DWRUtil.useLoadingMessage("Loading ...");
		     	var orgFromCbo = dojo.widget.byId("orgFromCbo");
				var orgToCbo = dojo.widget.byId("orgToCbo");
				var	empFromCbo = dojo.widget.byId("empFromCbo");
				var	empToCbo = dojo.widget.byId("empToCbo");
				
				var orgFromVal = splitCombo( orgFromCbo.textInputNode.value );
				var orgToVal   = splitCombo( orgToCbo.textInputNode.value );
				var empFromVal = empFromCbo.textInputNode.value;
				var empToVal   = empToCbo.textInputNode.value;	
				
				var yearVal = document.getElementById("years").value;
				
				WgDraftService.countWgDraftByCriteria(
		     		yearVal,
		     		'<%= ouCode %>', 
		     		'<%= userId %>',
		     		orgFromVal,
					orgToVal,
					empFromVal,
					empToVal,
					{callback:whenCountDataCallBack,errorHandler:function(message) { alert('�Դ��ͼԴ��Ҵ����ǡѺ��õԴ����������');}}
				);
				
		    }
		    function whenCountDataCallBack(data){
		    	DWRUtil.setValue("countData",data);
				onCheckButt("searchForm");
		    }
		
		</script>

	</head>
	<body>
		<table width="100%">
			<tr>
				<td class="font-head">
					[ CTWGMT101 ] ��âͻ�Ѻ��Ҩ�ҧ�ͧ�١��ҧ��Шӻ�
				</td>
			</tr>
		</table>
		<form name="searchForm" action="" method="post">
			<table width="750" border="0" align="center" cellspacing="0">
				<tr><td colspan="4" align="center">
					<table>
						<tr>
							<td class="font-field" align="right">��âͻ�Ѻ��Ҩ�ҧ�ͧ�١��ҧ��Шӻ�&nbsp;</td>
							<td>
								<SELECT name="years" onchange="whenBlurYear();">
								<%
									//System.out.println("xxxxx test xxxxx");
									List ls = (List)session.getAttribute("wgDraftAllYear");
									for(int i=0; i<ls.size(); i++){
										Integer tmpYear = (Integer)ls.get(i);
										if( tmpYear.intValue() == Integer.parseInt(year) ){
								%>
									<OPTION value="<%= tmpYear %>" selected="selected"><%= tmpYear %></OPTION>
								<%
										}else{
								%>
									<OPTION value="<%= tmpYear %>"><%= tmpYear %></OPTION>
								<%
										}
									}
								%>
								</SELECT>							
							</td>
						</tr>
					</table>
					</td>
				</tr>
			  	<tr>
			    	<td class="font-field" align="right">������ѧ�Ѵ��Ժѵԧҹ&nbsp;</td>
			    	<%--<td align="left" colspan="4"><SELECT  dojoType="ComboBox" widgetId="orgFromCbo" style="width:462px" onBlurInput="();"></SELECT></td>--%>
					<td align="left" colspan="4"><SELECT  dojoType="ComboBox" widgetId="orgFromCbo" style="width:570px" ></SELECT></td>
			  	</tr>
			  	<tr>
			  		<td  class="font-field" align="right">�֧�ѧ�Ѵ��Ժѵԧҹ&nbsp;</td>
			    	<td align="left" colspan="4"><SELECT  dojoType="ComboBox" widgetId="orgToCbo" style="width:570px"></td>
			  	</tr>
  				<tr>
    				<td class="font-field" align="right">������Ţ��Шӵ��&nbsp;</td>
					<%--<td align="left"><SELECT  dojoType="ComboBox" widgetId="empFromCbo" style="width:140" onBlurInput="whenEmpFromSelectOption();"></td>--%>
    				<td align="left"><SELECT  dojoType="ComboBox" widgetId="empFromCbo" style="width:140" ></td>
				    <td  class="font-field" align="right">�֧�Ţ��Шӵ��&nbsp;</td>
				    <td align="left"><SELECT  dojoType="ComboBox" widgetId="empToCbo" style="width:140"></td>
				    <td><INPUT type="button" class="button" value="����" onclick="onQuery(whenQueryEmpBefore);"></td>
				</tr>
			</table>
			<div style="height:340px;">
			<table width="770"  border="1" bordercolor="#6699CC"  align="center"  cellpadding="2" cellspacing="0">
			<thead>
				<tr CLASS="TABLEBULE2">
				<th CLASS="TABLEBULE2" width="100">�Ţ��Шӵ��</th>
				<th CLASS="TABLEBULE2">���� - ���ʡ��</th>
				<th CLASS="TABLEBULE2" width="50">���</th>
				<tr>
			</thead>
			<tbody id="dataTable">
			</tbody>
			</table>
			</div>
			<!-- Begin Declare Paging -->
			<table width="770" align="center"  cellpadding="2" cellspacing="0" >
				<tr>
					<td align="right">
						<input type="hidden" name="page" value="<%=pageEdit%>">
						<input type="hidden" name="maxPage">
						<input type="hidden" name="countData" >
						<input type="hidden" name="dataPerPage" value="10">
						<input type="button" disabled="disabled" class=" button " value="First" name="first" onclick="onFirst(whenQueryEmpBefore)"/>
						<input type="button" disabled="disabled" class=" button " value="<<" name="previous" onclick="onPrevious(whenQueryEmpBefore)"/>
						<input type="text"  name="showPage" style="text-align:right;width: 30;" 
							    onkeyup="onCheckPageNAN(this.value);" onchange="onChangeGoPage(whenQueryEmpBefore);" onkeypress="onKeyGoPage(event,whenQueryEmpBefore);" 
						/>
						/
						<input type="text"  name="showMaxPage" readonly="readonly" style="width: 30;border-style : none;background-color : transparent;text-align:right;font-weight:bold;"/>
						<input type="button" disabled="disabled" class=" button " value=">>" name="next" onclick="onNext(whenQueryEmpBefore);" />
						<input type="button" disabled="disabled" class=" button " value="Last" name="last" onclick="onLast(whenQueryEmpBefore);"/>
					</td>
				</tr>
			</table>
			<!-- End Declare Paging -->
			<table width="100%" CLASS="TABLEBULE2">
				<tr CLASS="TABLEBULE2" >
					<td align="left" >&nbsp;
						<!-- <input type="button" class=" button "  value="����������" name="add" onclick="preIns();"/> -->
					</td>
				</tr>
			</table>
		</form>
		<form name="editForm" action="security.htm?reqCode=CTWGUP101" method="post">
			<input type="hidden" name="empCodeEdit" />
			<input type="hidden" name="yearEdit" />
			<input type="hidden" name="orgFromEdit" />
			<input type="hidden" name="orgToEdit" />
			<input type="hidden" name="empCodeFromEdit" />
			<input type="hidden" name="empCodeToEdit" />
			<input type="hidden" name="pageEdit" />
		</form>
	</body>
	<script type="text/javascript">
		if (DWRUtil.getValue("page") >= 0){
			//whenQueryEmp(DWRUtil.getValue("page"));
		}
	</script>
</html>
