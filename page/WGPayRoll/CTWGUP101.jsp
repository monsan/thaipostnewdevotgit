<%@ page language="java" contentType="text/html;charset=TIS-620"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="javax.swing.text.Document"%>
<%@ page import="com.ss.tp.security.UserInfo" %>
<%@ page import="sun.security.krb5.internal.i" %>
<%@ taglib prefix="c" uri="/WEB-INF/tld/c.tld"%>
<%
	UserInfo userInfo = (UserInfo)session.getAttribute("UserLogin");
	String userId = userInfo.getUserId();
	String ouCode = userInfo.getOuCode();
	
	String year = request.getParameter("yearEdit");
	String empCode = request.getParameter("empCodeEdit");
	String orgFromEdit = request.getParameter("orgFromEdit");
	String orgToEdit = request.getParameter("orgToEdit");
	String empCodeFromEdit = request.getParameter("empCodeFromEdit");
	String empCodeToEdit = request.getParameter("empCodeToEdit");
	String pageEdit = request.getParameter("pageEdit");
	
	
	boolean isConfirm = ((Boolean)session.getAttribute("isConfirm")).booleanValue();

	
	//isConfirm = true;
	//System.out.println("year : " + year);
	//System.out.println("isConfirm : " + isConfirm);
	
	//isConfirm = "true";
%>
<html>
	<head>
		<title>��âͻ�Ѻ��Ҩ�ҧ�ͧ�١��ҧ��Шӻ�</title>
		<!-- Include -->
		<script type="text/javascript" src="dwr/engine.js"></script>
		<script type="text/javascript" src="dwr/util.js"></script>
		<!-- Javascript Script File -->
		<SCRIPT type="text/javascript" src="dwr/interface/WgDraftService.js"></SCRIPT>
		
		<script type="text/javascript" src="script/gridScript.js"></script>
		<script type="text/javascript" src="page/NavigatePage.jsp"></script>
		<script type="text/javascript" src="script/dojo.js"></script>
		<script type="text/javascript" src="script/json.js"></script>
		<script type="text/javascript" src="script/payroll_util.js"></script>

		<script type="text/javascript">
			//Load Dojo's code relating to widget managing functions
			dojo.require("dojo.widget.*");
			dojo.require("dojo.widget.Menu2");
			dojo.require("dojo.widget.Button");
			dojo.require("dojo.widget.ComboBox");
			dojo.require("dojo.widget.DropDownButton");
			dojo.require("dojo.widget.SortableTable");
			dojo.require("dojo.widget.ContentPane");
			dojo.require("dojo.widget.LayoutContainer");
			dojo.require("dojo.widget.SortableTable");
			dojo.require("dojo.widget.Toolbar");
			dojo.require("dojo.widget.html.*");
			dojo.require("dojo.widget.Menu2");
			dojo.hostenv.writeIncludes();
			
			//Event
			dojo.require("dojo.event.*");
		</script>
		<script type="text/javascript">
			
			function loadDraft(){
				WgDraftService.findWgDraft('<%= ouCode %>', '<%= year %>', '<%= empCode %>', {callback:whenLoadDraft, errorHandler:function(message) { alert('�Դ��ͼԴ��Ҵ����ǡѺ��õԴ����������');}});
			}
			function whenLoadDraft(data){
			
				//alert( data.toJSONString() );
			
				var empCode = document.getElementById("empCode");
				var iDate = document.getElementById("iDate");
				var empName = document.getElementById("empName");
				var areaDesc = document.getElementById("areaDesc");
				var secDesc = document.getElementById("secDesc");
				var workDesc = document.getElementById("workDesc");
				var draftStatus = document.getElementById("draftStatus");
				var draftStatusText = document.getElementById("draftStatusText");
				var punishStatus = document.getElementById("punishStatus");
				var score = document.getElementById("score");
				var hisSeq = document.getElementById("hisSeq");
				var perDraft = document.getElementById("perDraft");
				var remarkYes = document.getElementById("remarkYes");
				
				var cause1 = document.getElementById("cause1");
				var cause2 = document.getElementById("cause2");
				var cause3 = document.getElementById("cause3");
				var cause4 = document.getElementById("cause4");
				var cause5 = document.getElementById("cause5");
				var cause6 = document.getElementById("cause6");
				var cause9 = document.getElementById("cause9");
				var remarkNo = document.getElementById("remarkNo");
				
				var draftYes = document.getElementById("draftStatusYes");
				var draftNo = document.getElementById("draftStatusNo");
				
				empCode.value = checkNull(data.empCode,'STRING');
				iDate.value = checkNull(data.IDateStr,'STRING');
				
				empName.value = checkNull(data.firstName,'STRING') + ' ' + checkNull(data.lastName,'STRING');
				areaDesc.value = checkNull(data.areaDesc,'STRING');
				secDesc.value = checkNull(data.secDesc,'STRING');
				workDesc.value = checkNull(data.workDesc,'STRING');
				
				
				punishStatus.value = checkNull(data.punishStatus,'STRING');
				score.value = checkNull(data.score,'STRING');
				hisSeq.value = checkNull(data.hisSeq,'STRING');
				perDraft.value = checkNull(data.perDraft,'STRING');
				draftStatus.value = checkNull(data.draftStatus,'STRING');

				if( draftStatus.value == 'Y' )
					remarkYes.value = checkNull(data.remark,'STRING');
				else
					remarkNo.value = checkNull(data.remark,'STRING');
					
				var noDraft = data.noDraft.split(',');
				for(var i=0; i<noDraft.length; i++){
					switch(noDraft[i])
					{
						case "�":
					  		cause1.checked = true;
					  		//alert( cause1.checked );
					  	break    
						case "�":
					  		cause2.checked = true;
					  		//alert( cause2.checked );
					  	break
					  	case "�":
					  		cause3.checked = true;
					  		//alert( cause3.checked );
					  	break
					  	case "�":
					  		cause4.checked = true;
					  		//alert( cause4.checked );
					  	break
					  	case "�":
					  		cause5.checked = true;
					  		//alert( cause5.checked );
					  	break
					  	case "�":
					  		cause6.checked = true;
					  		//alert( cause6.checked );
					  	break
					  	case "�":
					  		cause9.checked = true;
					  		//alert( cause9.checked );
					  	break
						//default:
					  	//	alert("default");
					}
				}
				
				whenChangeDraftStatus();
				validateCanInputHisSeq();
				
				if( <%= isConfirm %> ){
					disableComponent();
					if(data.draftStatus != null && data.draftStatus == 'Y'){
						draftStatusText.value = '���Է��';
						remarkYes.value = checkNull(data.remark,'STRING');
						draftYes.style.visibility = "visible";
						draftNo.style.visibility = "hidden";
						draftNo.style.bottom = "-146px";
					}else if(data.draftStatus != null && data.draftStatus == 'N'){
						draftStatusText.value = '������Է��';
						remarkNo.value = checkNull(data.remark,'STRING');
						draftYes.style.visibility = "hidden";
						draftNo.style.visibility = "visible";
						draftNo.style.position = "relative";
						draftNo.style.bottom = "+146px";
					}
				}
			}
			
			function disableComponent(){
				document.getElementById("empCode").readonly = true;
				document.getElementById("iDate").readonly = true;
				document.getElementById("empName").readonly = true;
				document.getElementById("areaDesc").readonly = true;
				document.getElementById("secDesc").readonly = true;
				document.getElementById("workDesc").readonly = true;
				document.getElementById("draftStatus").disabled = true;
				document.getElementById("draftStatus").readonly = true;  
				document.getElementById("punishStatus").readonly = true;
				document.getElementById("score").readonly = true;
				document.getElementById("hisSeq").readonly = true;
				document.getElementById("perDraft").readonly = true;
				document.getElementById("remarkYes").readonly = true;
				
				document.getElementById("cause1").disabled = true;
				document.getElementById("cause2").disabled = true;
				document.getElementById("cause3").disabled = true;
				document.getElementById("cause4").disabled = true;
				document.getElementById("cause5").disabled = true;
				document.getElementById("cause6").disabled = true;
				document.getElementById("cause9").disabled = true;
				document.getElementById("remarkNo").disabled = true;
			
				document.getElementById("ok").disabled = true;
			}
			
			var wgDraftVo = {	
				ouCode:null,
				year:null,
				empCode:null,
				codeSeq:null,
				hisSeq:null,
				preName:null,
				firstName:null,
				lastName:null,
				gworkCode:null,
				dutyCode:null,
				wage:null,
				wageDraft:null,
				score:null,
				perDraft:null,
				remark:null,
				draftStatus:null,
				punishStatus:null,
				noDraft:null,
				idate:null,
				instructNo:null,
				updateFlag:null,
				confirmFlag:null,
				statusDate:null,
				lastPerDraft1:null,
				lastPerDraft2:null,
				lastPerDraft3:null,
				lastPerDraft4:null,
				lastPerDraft5:null,
				illDay:null,
				busDay:null,
				illDocDay:null,
				lateDay:null,
				bornDay:null,
				bornBusDay:null,
				budthDay:null,
				absenceDay:null,
				creDate:null,
				creBy:'<%=userId%>',
				updBy:'<%=userId%>',
				updDate:null,
				areaDesc:null,
				secDesc:null,
				workDesc:null,
				iDateStr:null
			};
			
			
			function preSave(){
				var empCode = document.getElementById("empCode");
				var hisSeq = document.getElementById("hisSeq");
				var perDraft = document.getElementById("perDraft");
				var draftStatus = document.getElementById("draftStatus");
			
				var canSave = false;
				if (perDraft.value == ''){
					perDraft.value = 0;
				} 
				if( draftStatus.value == 'Y' ){
					if( perDraft.value > 3 && hisSeq.value >= 1 ){
						canSave = true;
				    }
					else if( perDraft.value == 0 ){
						canSave = true;
					}
					else if( perDraft.value == 2 ){
						canSave = true;
					}
					else if( perDraft.value == 3 ){
						canSave = true;
					}else{
						alert('��سҵ�Ǩ�ͺ������ ��Ѻ��Ҩ�ҧ ��� �ӴѺ����');
					}
				
					if( canSave ){
						WgDraftService.findDupHisSeq('<%= ouCode %>', '<%= year %>', perDraft.value, hisSeq.value, draftStatus.value,empCode.value, {callback:save, errorHandler:function(message) { alert('�������ö�ѹ�֡��');}});
					}
				}else{
					WgDraftService.findDupHisSeq('<%= ouCode %>', '<%= year %>', perDraft.value, hisSeq.value, draftStatus.value,empCode.value, {callback:save, errorHandler:function(message) { alert('�������ö�ѹ�֡��');}});
				}
			}
			
			function save(data){
				//alert( data );
			
				var empCode = document.getElementById("empCode");
				var iDate = document.getElementById("iDate");
				var empName = document.getElementById("empName");
				var areaDesc = document.getElementById("areaDesc");
				var secDesc = document.getElementById("secDesc");
				var workDesc = document.getElementById("workDesc");
				var draftStatus = document.getElementById("draftStatus");
				var punishStatus = document.getElementById("punishStatus");
				var score = document.getElementById("score");
				var hisSeq = document.getElementById("hisSeq");
				var perDraft = document.getElementById("perDraft");
				var remarkYes = document.getElementById("remarkYes");
				
				var cause1 = document.getElementById("cause1");
				var cause2 = document.getElementById("cause2");
				var cause3 = document.getElementById("cause3");
				var cause4 = document.getElementById("cause4");
				var cause5 = document.getElementById("cause5");
				var cause6 = document.getElementById("cause6");
				var cause9 = document.getElementById("cause9");
				var remarkNo = document.getElementById("remarkNo");
				
				
					
				if( (draftStatus.value == 'Y' && data == 'N') || draftStatus.value == 'N' ){
				
					// clear lower section
					if( draftStatus.value == 'Y' ){
						clearDraftStatusNo();
						
						wgDraftVo.remark = remarkYes.value;
							
					}else{
						clearDraftStatusYes();
						
						wgDraftVo.remark = remarkNo.value;
					}
					
					wgDraftVo.ouCode = '<%= ouCode %>';
					wgDraftVo.year = '<%= year %>';
					wgDraftVo.empCode = '<%= empCode %>';
					wgDraftVo.hisSeq = hisSeq.value;
					wgDraftVo.score = score.value;
					if(perDraft.value == ''){
		    			perDraft.value = 0;
		    		}
					wgDraftVo.perDraft = perDraft.value;
					
					wgDraftVo.draftStatus = draftStatus.value;
					wgDraftVo.punishStatus = punishStatus.value;
					
					var noDraft = '';
					
					if( cause1.checked )
						noDraft += cause1.value;
					if( cause2.checked )
						noDraft += cause2.value;
					if( cause3.checked )
						noDraft += cause3.value;
					if( cause4.checked )
						noDraft += cause4.value;
					if( cause5.checked )
						noDraft += cause5.value;
					if( cause6.checked )
						noDraft += cause6.value;
					if( cause9.checked )
						noDraft += cause9.value;
						
					wgDraftVo.noDraft = noDraft;
					
					//alert( wgDraftVo.toJSONString() );
					
					WgDraftService.updateWgDraft( wgDraftVo, {callback:whenUpdateWgDraft, errorHandler:function(message) { alert('�������ö�ѹ�֡��');}});
				}else{
					alert("�ӴѺ���ͫ�ӫ�͹");
				}
			}
			function whenUpdateWgDraft(data){
				alert("�ѹ�֡���������º����");
				
				gotoMTPage();
			}
			
			function whenChangeDraftStatus(){
				var cboDraft = document.getElementById("draftStatus");
				var draftYes = document.getElementById("draftStatusYes");
				var draftNo = document.getElementById("draftStatusNo");
				
				if( cboDraft.value == 'Y' ){
					draftYes.style.visibility = "visible";
					draftNo.style.visibility = "hidden";
					draftNo.style.bottom = "-146px";
					
					//clearDraftStatusNo();
					
				}else if( cboDraft.value == 'N' ){
					draftYes.style.visibility = "hidden";
					draftNo.style.visibility = "visible";
					draftNo.style.position = "relative";
					draftNo.style.bottom = "+146px";
					
					//clearDraftStatusYes();
					
				}
			}
			
			function clearDraftStatusYes(){
				//var punishStatus = document.getElementById("punishStatus");
				var score = document.getElementById("score");
				var hisSeq = document.getElementById("hisSeq");
				var perDraft = document.getElementById("perDraft");
				var remarkYes = document.getElementById("remarkYes");
				
				//punishStatus.value = "";
			//	score.value = "";
				hisSeq.value = "";
				perDraft.value = "";
				remarkYes.value = "";
			}
			
			function clearDraftStatusNo(){
				var cause1 = document.getElementById("cause1");
				var cause2 = document.getElementById("cause2");
				var cause3 = document.getElementById("cause3");
				var cause4 = document.getElementById("cause4");
				var cause5 = document.getElementById("cause5");
				var cause6 = document.getElementById("cause6");
				var cause9 = document.getElementById("cause9");
				var remark = document.getElementById("remarkNo");
				
				cause1.checked = false;
				cause2.checked = false;
				cause3.checked = false;
				cause4.checked = false;
				cause5.checked = false;
				cause6.checked = false;
				cause9.checked = false;
				remark.value = "";
			}
			
			function gotoMTPage(){
				document.getElementById("yearEdit").value = '<%= year %>';
				document.getElementById("orgFromEdit").value = '<%= orgFromEdit %>';
				document.getElementById("orgToEdit").value = '<%= orgToEdit %>';
				document.getElementById("empCodeFromEdit").value = '<%= empCodeFromEdit %>';
				document.getElementById("empCodeToEdit").value = '<%= empCodeToEdit %>';
				document.getElementById("pageEdit").value = '<%= Integer.parseInt(pageEdit) - 1 %>';
			
		    	document.forms["goMT"].submit();
		    }
		    
		    function validatePerDraft(){
		    	var perDraft = document.getElementById("perDraft");
		    	var punishStatus = document.getElementById("punishStatus");
		    	if(perDraft.value == ''){
		    		perDraft.value = 0;
		    	}
		    	if( isNaN(Number(perDraft.value)) ){ 
		    		perDraft.value = perDraft.value.substring(0,perDraft.value.length - 1);
		    	}else{
		    		if( punishStatus.value == 'N' &&
		    				( parseInt( perDraft.value ) <= 0 || 
		    				  parseInt( perDraft.value ) >= 6 ) ){
		    			perDraft.value = perDraft.value.substring(0,perDraft.value.length - 1);
		    			//alert("Y " + perDraft.value);
		    			
		    		}else if( punishStatus.value == 'Y' && (parseInt(perDraft.value) > 2 )){
		    			//alert("N " + perDraft.value);
		    			perDraft.value = perDraft.value.substring(0,perDraft.value.length - 1);
		    			//WgDraftService.findAdjustNotOver('<%= ouCode %>',  '<%= empCode %>','<%= year %>'-1 , {callback:whenFindAdjustNotOver, errorHandler:function(message) { alert('�������ö�ѹ�֡��');}});
		    		}else{
		    			//alert("yo");
		    		}
		    	}
		    }
		    function whenFindAdjustNotOver(data){
		    var maxVal = data*1;
		    var perDraft = document.getElementById("perDraft");
		    if(perDraft.value == ''){
		    	perDraft.value = 0;
		    }
		   	var punishStatus = document.getElementById("punishStatus");
		    
		    	 if( punishStatus.value == 'Y' &&
		    				( parseInt( perDraft.value ) <= 0 || 
		    				  parseInt( perDraft.value ) <= maxVal ) ){
		    			perDraft.value = perDraft.value.substring(0,perDraft.value.length - 1);
		    		}
		    		
		    		
		    }		    
		    function toNumber(comp){
		    	 if( !isNaN(parseInt(comp.value)) )
		    	 	comp.value = parseInt(comp.value);
		    }
		    
		    function validateCanInputHisSeq(){
		    	var perDraft = document.getElementById("perDraft");
		    	var hisSeq = document.getElementById("hisSeq");
		    	//alert perDraft;
		    	if( perDraft.value >3 ){
		    		hisSeq.style.background = 'white';
		    		hisSeq.readOnly = false;
		    	}else{
		    		hisSeq.style.background = 'silver';
		    		hisSeq.readOnly = true;
		    		hisSeq.value = "";
		    	}
		    }
			
			function init(){
			
				var draftYes = document.getElementById("draftStatusYes");
				var draftNo = document.getElementById("draftStatusNo");
				draftYes.style.visibility = "hidden";
				draftNo.style.visibility = "hidden";
				
				loadDraft();
			}
			
			dojo.addOnLoad(init);
			
		</script>

	</head>
	<body>
		<table width="100%">
			<tr>
				<td class="font-head">
					[ CTWGUP101 ] ��âͻ�Ѻ��Ҩ�ҧ�ͧ�١��ҧ��Шӻ�
				</td>
			</tr>
		</table>
		<form name="searchForm" action="" method="post">
		<br/>
		<table width="100%" align="center" border="0">
		<tr><td align="center">
			<TABLE border="0" align="center" width="600px" cellspacing="2">
				<TR>
					<TD class="font-field" style="text-align:right;" >�Ţ��Шӵ��</TD>
					<TD width="130px" ><INPUT type="text" id="empCode" readonly="readonly" style="width:130px;text-align: left;background-color:silver;" maxlength="10" /></TD>
					<TD class="font-field" style="text-align:right;" >�ѹ�����ҷӧҹ</TD>
					<TD width="100px"><INPUT type="text" id="iDate" readonly="readonly" style="width:130px;text-align: left;background-color:silver;" /></TD>
				</TR>
				<TR>
					<TD class="font-field" style="text-align:right;">���� - ���ʡ��</TD>
					<TD colspan="3"><INPUT type="text" id="empName" readonly="readonly" style="width:200px;text-align: left; background-color:silver;" /></TD>
				</TR>
				<TR>
					<TD class="font-field" style="text-align:right;">ʾ./��.</TD>
					<TD colspan="3"><INPUT type="text" id="areaDesc" readonly="readonly" style="width:250px;text-align: left; background-color:silver;" /></TD>
				</TR>
				<TR>
					<TD class="font-field" style="text-align:right;">��ǹ/���ӡ��</TD>
					<TD colspan="3"><INPUT type="text" id="secDesc" readonly="readonly" style="width:250px;text-align: left; background-color:silver;" /></TD>
				</TR>
				<TR>
					<TD class="font-field" style="text-align:right;">Ἱ�</TD>
					<TD colspan="3"><INPUT type="text" id="workDesc" readonly="readonly" style="width:250px;text-align: left; background-color:silver;" /></TD>
				</TR>
				<TR>
					<TD class="font-field" style="text-align:right;">�Է��</TD>
					<TD colspan="3">
						<c:choose>
							<c:when test="${isConfirm}">
								<INPUT type="text" id="draftStatusText" readonly="readonly" style="width:70px;text-align: left; background-color:silver;" />
								<input type="hidden" name="draftStatus" />
							</c:when>
							<c:otherwise>
								<input type="hidden" name="draftStatusText">
							    <select name="draftStatus" onchange="whenChangeDraftStatus();" >
								<!--<select name="draftStatus" disabled=true;>-->
									<OPTION value="Y">���Է��</OPTION>
									<OPTION value="N">������Է��</OPTION>
								</select>	
							</c:otherwise>
						</c:choose>			
					</TD>
				</TR>
			</TABLE>
		</td></tr>
		</table>
		<br/>
		<CENTER>
			<div id="draftStatusYes" style="width: 600px; height: 130px;">
			<FIELDSET>
				<br/>
				<TABLE width="400"   cellpadding="2" cellspacing="2"  border="0">
					<TR>
						<td class="font-field" style="text-align:right; ">�����Ҥ�ѳ�� (Y/N)</td>
						<TD colspan="3" >
                        <!--<input type="text" id="punishStatus" readonly="readonly" style="text-align: right; width: 50px; background-color:silver;">-->
						<select id="punishStatus" name="punishStatus">
						<option value="Y">��</option>
						<option value="N">�����</option>
						</select>
						</TD>
					</TR>
					<TR>
						<td class="font-field" style="text-align:right;">��ṹ�����Թ�š�÷ӧҹ</td>
						<!--<TD colspan="3" ><input type="text" id="score" readonly="readonly" style="text-align: right; width: 100px;" onblur="toNumber(this);" onkeyup="if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);"></TD>-->
						<TD colspan="3" ><input type="text" id="score" readonly="readonly" style="text-align: right; width: 100px; background-color:silver;"></TD>
						</TR>
					<TR>
						<td class="font-field" style="text-align:right;">���������Ѻ��Ҩ�ҧ</td>
						<TD><input type="text" id="perDraft" style="text-align: right; width: 50px" onkeyup="validatePerDraft();" onblur="validateCanInputHisSeq();toNumber(this);">&nbsp; %</TD>
						<td class="font-field" style="text-align:right;">�ӴѺ����</td>
						<TD><input type="text" id="hisSeq" readonly="readonly" style="text-align: right; width: 50px; background-color:silver;" onblur="toNumber(this);" onkeyup="if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);"></TD>
					</TR>
					<TR>
						<td class="font-field" style="text-align:right;">�����˵�</td>
						<TD colspan="3" ><input type="text" id="remarkYes" style="text-align: left; width: 200px;"></TD>
					</TR>
				</TABLE>
				<br/>
			</FIELDSET>
			</div>
			<div id="draftStatusNo" style="width: 600px; position: relative; bottom: +146px;">
			<FIELDSET>
				<br/>
				<TABLE>
					<THEAD>
						<TR><TD class="font-field" style="text-align: left;">���˵ط��������Է������Ѻ��Ҩ�ҧ</TD></TR>
					</THEAD>
					<TBODY>
					<TR>
						<td><input type="checkbox" name="cause1" value="�">�������ҡ�÷ӧҹ���֧ 8 ��͹</td>
					</TR>
					<TR>
						<td><input type="checkbox" name="cause2" value="�">����ҹࡳ������Թ�š�÷ӧҹ</td>
					</TR>
					<TR>
						<td><input type="checkbox" name="cause3" value="�">�١ŧ�ɵѴ��Ҩ�ҧ</td>
					</TR>
					<TR>
						<td><input type="checkbox" name="cause4" value="�">�ѹ�� ����� �Թ��˹�</td>
					</TR>
					<TR>
						<td><input type="checkbox" name="cause5" value="�">�Ҵ�ҹ</td>
					</TR>
					<TR>
						<td><input type="checkbox" name="cause6" value="�">�� ���ѧ����դ����</td>
					</TR>
					<TR>
						<td><input type="checkbox" name="cause9" value="�">��� �</td>
						<td style="text-align:right;">
							&nbsp;&nbsp; �����˵� &nbsp;&nbsp;<input type="text" id="remarkNo" >
						</td>
					</TR>
					</TBODY>
				</TABLE>
				<br/>
			</FIELDSET>
			</div>
		</CENTER>
		<TABLE width="100%" CLASS="TABLEBULE2" style="position: relative; bottom: +120px">
			<TR CLASS="TABLEBULE2" >
				<TD align="left" >&nbsp;
					<input type="button" class=" button "  value="��ŧ" name="ok" onclick="preSave();"/>
					<input type="button" class=" button "  value="�͡" name="back" onclick="gotoMTPage();"/>
				</TD>
			</TR>
		</TABLE>
		</form>
		<FORM name="goMT" action="security.htm?reqCode=CTWGMT101" method="post">
			<input type="hidden" name="yearEdit" />
			<input type="hidden" name="orgFromEdit" />
			<input type="hidden" name="orgToEdit" />
			<input type="hidden" name="empCodeFromEdit" />
			<input type="hidden" name="empCodeToEdit" />
			<input type="hidden" name="pageEdit" />
		</FORM>
	</body>
</html>
