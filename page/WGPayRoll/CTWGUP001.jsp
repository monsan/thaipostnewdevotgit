<%@ page language="java" contentType="text/html;charset=TIS-620"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="javax.swing.text.Document"%>
<%@ page import="com.ss.tp.security.UserInfo" %>
<%@ page import="sun.security.krb5.internal.i" %>
<%@ taglib prefix="c" uri="/WEB-INF/tld/c.tld"%>
<%
	UserInfo userInfo = (UserInfo)session.getAttribute("UserLogin");
	String userId = userInfo.getUserId();
	String ouCode = userInfo.getOuCode();
	
	String year = (String)request.getParameter("yearEdit");
	String period = (String)request.getParameter("periodEdit");
	String section = (String)request.getParameter("sectionEdit");
	String isConfirm = (String)request.getParameter("isConfirmEdit");
	String empCode = (String)request.getParameter("empCodeEdit");
	String orgFromLabel = (String) request.getParameter("orgFromLabel");
	String orgFromValue = (String) request.getParameter("orgFromValue");
	String orgToLabel = (String) request.getParameter("orgToLabel");
	String orgToValue = (String) request.getParameter("orgToValue");
	String empFrom = (String) request.getParameter("empFrom");
	String empTo = (String) request.getParameter("empTo");
	String pageNow = (String) request.getParameter("pageNow");
%>
<html>
	<head>
		<title>�ѹ�֡��������ѡ㹡�èѴ���Թ��͹</title>
		<!-- Include -->
		<script type="text/javascript" src="dwr/engine.js"></script>
		<script type="text/javascript" src="dwr/util.js"></script>
		<!-- Javascript Script File -->
		<SCRIPT type="text/javascript" src="dwr/interface/SuUserOrganizationService.js"></SCRIPT>
		<SCRIPT type="text/javascript" src="dwr/interface/VPnOrganizationSecurityService.js"></SCRIPT>
		<SCRIPT type="text/javascript" src="dwr/interface/WgPrEmployeeService.js"></SCRIPT>
		<SCRIPT type="text/javascript" src="dwr/interface/WgPrEmployeeTextService.js"></SCRIPT>
		
		<script type="text/javascript" src="script/gridScript.js"></script>
		<script type="text/javascript" src="page/NavigatePage.jsp"></script>
		<script type="text/javascript" src="script/dojo.js"></script>
		<script type="text/javascript" src="script/json.js"></script>
		<script type="text/javascript" src="script/payroll_util.js"></script>

		<script type="text/javascript">
			//Load Dojo's code relating to widget managing functions
			dojo.require("dojo.widget.*");
			dojo.require("dojo.widget.Menu2");
			dojo.require("dojo.widget.Button");
			dojo.require("dojo.widget.ComboBox");
			dojo.require("dojo.widget.DropDownButton");
			dojo.require("dojo.widget.SortableTable");
			dojo.require("dojo.widget.ContentPane");
			dojo.require("dojo.widget.LayoutContainer");
			dojo.require("dojo.widget.SortableTable");
			dojo.require("dojo.widget.Toolbar");
			dojo.require("dojo.widget.html.*");
			dojo.require("dojo.widget.Menu2");
			dojo.hostenv.writeIncludes();
			
			//Event
			dojo.require("dojo.event.*");
		</script>
		<script type="text/javascript">
			
			/**** BEGIN When Employee Select ****/
		    function onLoad(){
		    	DWRUtil.useLoadingMessage("Loading ...");
		    	$("year").value = '<%=year%>';
		    	$("period").value = '<%=period%>';
		    	$("section").value = '<%=section%>';
		    	$("isConfirm").value = '<%=isConfirm%>';

		    	
		    }
		    
		    function whenEmpBlur(){
		    	if( $("empCode").value != '' ){
		    		DWRUtil.useLoadingMessage("Loading ...");
		    		WgPrEmployeeService.findWgPrEmpInSecue($("userId").value,$("ouCode").value,$("empCode").value,
		    		 <c:out value='${yearPrevious}' />, <c:out value='${periodPrevious}' />,
		    		 {callback:whenLoadPnEmployeeDeatilCallback,errorHandler:function(message) { alert('�Դ��ͼԴ��Ҵ����ǡѺ��õԴ����������');}});
		    	}
		    }
		    
		    function whenLoadPnEmployeeDeatilCallback(data){
		    if( data.firstName != null )
		    	{
			    	$("empName").value = checkNull(data.prefixName,'STRING') + ' ' + checkNull(data.firstName,'STRING') + ' ' + checkNull(data.lastName,'STRING');
			    	$("account").value = checkNull(data.account,'STRING');
			    	$("dutyDesc").value = checkNull(data.dutyDesc,'STRING');
			    	$("pDate").value = checkNull(data.PDate,'STRING');
			    	$("orgCode").value = checkNull(data.orgCode,'STRING');
			    	$("orgDesc").value = checkNull(data.orgDesc,'STRING'); // + ' ' + checkNull(data.orgDesc,'STRING');   	
			    	$("codeSeqAct").value = checkNull(data.codeSeqAct,'STRING');
			    	$("codeSeq").value = checkNull(data.codeSeq,'STRING');
			    	$("codeSeqName").value = checkNull(data.divDesc,'STRING') + ' ' + checkNull(data.secDesc,'STRING');
			    	
			    	WgPrEmployeeService.findWgPrEmp('<%=ouCode%>', $("year").value, $("period").value,
			    	<c:out value='${yearPrevious}' />, <c:out value='${periodPrevious}' />,
			    	$("empCode").value, $("userId").value,{callback:whenPrEmployeeLoadCallback,errorHandler:function(message) { alert('�Դ��ͼԴ��Ҵ����ǡѺ��õԴ����������');}});
			    }
			 else { alert("��辺�������Ţ��Шӵ��"); }
			    	
		    }
		    function whenPrEmployeeLoadCallback(data){
		    
		    	var empName = $("empName").value.replace(' ','space');
		    	if( empName != 'space' ){
			    	
			    	// �Ţ��Шӵ�Ǽ����������
			    	$("taxId").value = checkNull(data.taxId,'STRING');
			    	$("hTaxId").value = checkNull(data.taxId,'STRING');
			    	
			    	$("bankId").value = checkNull(data.bankId,'STRING');
			    	$("hBankId").value = checkNull(data.bankId,'STRING');
			    	
			    	// ʶҹС���Ѻ�Թ��͹
	 		    	var payStatus = document.forms["searchForm"].elements["payStatus"];
	 		    	//alert("payStatus : " + data.payStatus );
	 		    	if( data.payStatus == '1' ){
	 		    		payStatus[0].checked = true;
	 		    		$("hPayStatus").value = data.payStatus;
	 		    	}else if( data.payStatus == '2' ){
	 		    		payStatus[1].checked = true;
	 		    		$("hPayStatus").value = data.payStatus;
	 		    	}else{
	 		    		payStatus[0].checked = true;
	 		    	}
	 		    	
	 		    	// ʶҹС�è����Թ��͹
	 		    	var flagPr = document.getElementById("flagPr");
	 		    	if( data.flagPr == '0' ){
	 		    		flagPr.selectedIndex = 0;
	 		    		$("hFlagPr").value = data.flagPr;
	 		    	}else if( data.flagPr == '1' ){
	 		    		flagPr.selectedIndex = 1;
	 		    		$("hFlagPr").value = data.flagPr;
	 		    	}else if( data.flagPr == '2' ){
	 		    		flagPr.selectedIndex = 2;
	 		    		$("hFlagPr").value = data.flagPr;
	 		    	}
	 		    	
	 		    	// ʶҹ��ҾŴ���͹
	 		    	var marriedStatus = document.getElementById("marriedStatus");
	 		    	//alert("marriedStatus : " + data.marriedStatus);
	 		    	if( data.marriedStatus == '1' ){
	 		    		marriedStatus.selectedIndex = 0;
	 		    		$("hMarriedStatus").value = data.marriedStatus;
	 		    	}else if( data.marriedStatus == '2' ){
	 		    		marriedStatus.selectedIndex = 1;
	 		    		$("hMarriedStatus").value = data.marriedStatus;
	 		    	}else if( data.marriedStatus == '3' ){
	 		    		marriedStatus.selectedIndex = 2;
	 		    		$("hMarriedStatus").value = data.marriedStatus;
	 		    	}else if( data.marriedStatus == '4' ){
	 		    		marriedStatus.selectedIndex = 3;
	 		    		$("hMarriedStatus").value = data.marriedStatus;
	 		    	}
			    	
			    	$("costChild").value = checkNull(data.costChild,'STRING');
			    	$("hCostChild").value = checkNull(data.costChild,'STRING');
			    	
			    	$("childStudy").value = checkNull(data.childStudy,'STRING');
			    	$("hChildStudy").value = checkNull(data.childStudy,'STRING');
			    	
			    	$("childNoStudy").value = checkNull(data.childNoStudy,'STRING');
			    	$("hChildNoStudy").value = checkNull(data.childNoStudy,'STRING');
			    	
			    	var flagFather = document.getElementById("flagFather");
			    	if( data.flagFather == 'Y' ){
			    		flagFather.checked = true;
			    		$("hFlagFather").value = data.flagFather;
			    	}else{
			    		flagFather.checked = false;
			    		$("hFlagFather").value = data.flagFather;
			    	}
			    		
			    	var flagFatherSpouse = document.getElementById("flagFatherSpouse");
			    	if( data.flagFatherSpouse == 'Y' ){
			    		flagFatherSpouse.checked = true;
			    		$("hFlagFatherSpouse").value = data.flagFather;
			    	}else{
			    		flagFatherSpouse.checked = false;
			    		$("hFlagFatherSpouse").value = data.flagFather;
			    	}
			    	
			    	var flagMother = document.getElementById("flagMother");
			    	if( data.flagMother == 'Y' ){
			    		flagMother.checked = true;
			    		$("hFlagMother").value = data.flagMother;
			    	}else{
			    		flagMother.checked = false;
			    		$("hFlagMother").value = data.flagMother;
			    	}
			    	
			    	var flagMotherSpouse = document.getElementById("flagMotherSpouse");
			    	if( data.flagMotherSpouse == 'Y' ){
			    		flagMotherSpouse.checked = true;
			    		$("hFlagMotherSpouse").value = data.flagMotherSpouse;
			    	}else{
			    		flagMotherSpouse.checked = false;
			    		$("hFlagMotherSpouse").value = data.flagMotherSpouse;
			    	}
			    	
			    	var gundanFlag = document.getElementById("gundanFlag");
		    		if( data.gundanFlag == 'Y' ){
		    			gundanFlag.checked = true;
		    		}
		    		else{
		    			gundanFlag.checked = false;
		    		}
		    			
			    	$("costLife").value = checkNull(data.costLife,'STRING');
			    	$("hCostLife").value = checkNull(data.costLife,'STRING');
			    	
			    	$("debtLife").value = checkNull(data.debtLife,'STRING');
			    	$("hDebtLife").value = checkNull(data.debtLife,'STRING');
			    	
			    	$("debtLoan").value = checkNull(data.debtLoan,'STRING');
			    	$("hDebtLoan").value = checkNull(data.debtLoan,'STRING');
			    	
			    	$("donate").value = checkNull(data.donate,'STRING');
			    	$("hDonate").value = checkNull(data.donate,'STRING');
			    	
			    	$("incomeTax").value = checkNull(data.incomeTax,'STRING');
			    	$("hIncomeTax").value = checkNull(data.incomeTax,'STRING');
			    	
			    	$("pensionFund").value = checkNull(data.pensionFund,'STRING');
			    	$("hPensionFund").value = checkNull(data.pensionFund,'STRING');
			    	
			    	$("rmf").value = checkNull(data.rmf,'STRING');
			    	$("hRmf").value = checkNull(data.rmf,'STRING');
			    	
			    	$("ltf").value = checkNull(data.ltf,'STRING');
			    	$("hLtf").value = checkNull(data.ltf,'STRING');
			    	
			    	$("teacherFund").value = checkNull(data.teacherFund,'STRING');
			    	$("hTeacherFund").value = checkNull(data.teacherFund,'STRING');
			    	
			    	$("compensateLabour").value = checkNull(data.compensateLabour,'STRING');
			    	$("hCompensateLabour").value = checkNull(data.compensateLabour,'STRING');
			    	
			    	$("overage").value = checkNull(data.overage,'STRING');
			    	$("hOverage").value = checkNull(data.overage,'STRING');
			    	
			    	$("overageSpouse").value = checkNull(data.overageSpouse,'STRING');
			    	$("hOverageSpouse").value = checkNull(data.overageSpouse,'STRING');
			    }
		    }
		    
		    function clearScreen(){
		    	$("empCode").value = '';
		    	$("empName").value = '';
		    	$("account").value = '';
		    	$("dutyDesc").value = '';
		    	$("pDate").value = '';
		        $("orgCode").value = '';
		    	$("orgDesc").value = '';
		    	$("taxId").value = '';
		    	$("codeSeq").value = '';
		        $("codeSeqName").value = '';
		    	
 		    	var payStatus = document.forms["searchForm"].elements["payStatus"];
 		    	payStatus[0].checked = false;
 		    	payStatus[1].checked = false;
 		    	
 		    	$("bankId").value = '';
 		    	
 		    	// ʶҹС�è����Թ��͹
 		    	var flagPr = document.getElementById("flagPr");
 		    	flagPr.selectedIndex = 0;
 		    	
 		    	// ʶҹ��ҾŴ���͹
 		    	var marriedStatus = document.getElementById("marriedStatus");
 		    	marriedStatus.selectedIndex = 0;
 		    	
 		    	
 		    	$("costChild").value = '';
		    	$("childStudy").value = '';
		    	$("childNoStudy").value = '';
		    	
		    	var gundanFlag = document.getElementById("gundanFlag");
		    	gundanFlag.checked = false;
		    	
		    	var flagFather = document.getElementById("flagFather");
		    	flagFather.checked = false;
		    		
		    	var flagFatherSpouse = document.getElementById("flagFatherSpouse");
		    	flagFatherSpouse.checked = false;
		    	
		    	var flagMother = document.getElementById("flagMother");
		    	flagMother.checked = false;
		    	
		    	var flagMotherSpouse = document.getElementById("flagMotherSpouse");
		    	flagMotherSpouse.checked = false;
		    	
		    	$("costLife").value = '';
		    	$("debtLife").value = '';
		    	$("debtLoan").value = '';
		    	$("donate").value = '';
		    	$("incomeTax").value = '';
		    	$("pensionFund").value = '';
		    	$("rmf").value = '';
		    	$("ltf").value = '';
		    	$("teacherFund").value = '';
		    	$("compensateLabour").value = '';
		    	$("overage").value = '';
		    	$("overageSpouse").value = '';
		    }
		    /**** END When Employee Select ****/
		    
		   
		    	
		    var wgPrEmpTextVo = {	keySeq:null, 
		    					ouCode:null,
								year:null,
								period:null,
								empCode:null,
								codeSeqWork:null,
								taxId:null,
								marriedStatus:null,
								payStatus:null,
								bankId:null,
								costChild:null,
								childStudy:null,
								childNoStudy:null,
								costLife:null,
								gundanFlag:null,
								debtLife:null,
								debtLoan:null,
								donate:null,
								other:null,
								incomeTax:null,
								oldSalary:null,
								newSalary:null,
								adjOldsal:null,
								adjNewsal:null,
								gundanAmt:null,
								flagPr:null,
								deductAmt:null,
								flagStatus:null,
								seqData:null,
								creBy:'<%=userId%>',
								updBy:'<%=userId%>',
								flagFather:null,
								flagMother:null,
								flagFatherSpouse:null,
								flagMotherSpouse:null,
								rmf:null,
								ltf:null,
								pensionFund:null,
								teacherFund:null,
								overage:null,
								overageSpouse:null,
								compensateLabour:null,
								confirmFlag:'N'
							  };
		    
		   function checkSave(){
		    	if(confirm("��س��׹�ѹ��úѹ�֡������")){
		    		save();
		    	}
		    }
		    function save()
		    {

				 wgPrEmpTextVo = {	keySeq:null, 
		    					ouCode:null,
								year:null,
								period:null,
								empCode:null,
								codeSeqWork:null,
								taxId:null,
								marriedStatus:null,
								payStatus:null,
								bankId:null,
								costChild:null,
								childStudy:null,
								childNoStudy:null,
								costLife:null,
								gundanFlag:null,
								debtLife:null,
								debtLoan:null,
								donate:null,
								other:null,
								incomeTax:null,
								oldSalary:null,
								newSalary:null,
								adjOldsal:null,
								adjNewsal:null,
								gundanAmt:null,
								flagPr:null,
								deductAmt:null,
								flagStatus:null,
								seqData:null,
								creBy:'<%=userId%>',
								updBy:'<%=userId%>',
								flagFather:null,
								flagMother:null,
								flagFatherSpouse:null,
								flagMotherSpouse:null,
								rmf:null,
								ltf:null,
								pensionFund:null,
								teacherFund:null,
								overage:null,
								overageSpouse:null,
								compensateLabour:null,
								confirmFlag:'N'
							  };
						
						var payStatus = document.forms["searchForm"].elements["payStatus"];
						var payStatusVal ="";
				    	wgPrEmpTextVo.keySeq = '';
				    	wgPrEmpTextVo.ouCode = $("ouCode").value;
						wgPrEmpTextVo.year = $("year").value/1; 
						wgPrEmpTextVo.period = $("period").value/1;
						wgPrEmpTextVo.empCode = $("empCode").value;
						
						
						if( !isEqualsValue($("taxId").value, $("hTaxId").value) )
							wgPrEmpTextVo.taxId = $("taxId").value;
						
						if( !isEqualsValue($("marriedStatus").value, $("hMarriedStatus").value) )
							wgPrEmpTextVo.marriedStatus = $("marriedStatus").value;
							

						if (payStatus[0].checked == true)	
							payStatusVal = payStatus[0].value;
						if (payStatus[1].checked == true)	
							payStatusVal = payStatus[1].value;
						
						if( !isEqualsValue(payStatusVal, $("hPayStatus").value) )
							wgPrEmpTextVo.payStatus =payStatusVal;

						if( !isEqualsValue($("bankId").value, $("hBankId").value) )
							wgPrEmpTextVo.bankId = $("bankId").value;
						
						if( !isEqualsValue($("costChild").value, $("hCostChild").value) )
							wgPrEmpTextVo.costChild = $("costChild").value;
						
						if( !isEqualsValue($("childStudy").value, $("hChildStudy").value) )
							wgPrEmpTextVo.childStudy = $("childStudy").value;
							
						if( !isEqualsValue($("childNoStudy").value, $("hChildNoStudy").value) )
							wgPrEmpTextVo.childNoStudy = $("childNoStudy").value;
							
						if( !isEqualsValue($("costLife").value, $("hCostLife").value) )
							wgPrEmpTextVo.costLife = $("costLife").value;
	
						if( $("gundanFlag").checked ){
							if( !isEqualsValue('Y', $("hGundanFlag").value) )
								wgPrEmpTextVo.gundanFlag = 'Y';
						}else{
							if( !isEqualsValue('N', $("hGundanFlag").value) )
								wgPrEmpTextVo.gundanFlag = 'N';
						}
						
						if( !isEqualsValue($("debtLife").value, $("hDebtLife").value) )
							wgPrEmpTextVo.debtLife = $("debtLife").value;
	
						if( !isEqualsValue($("debtLoan").value, $("hDebtLoan").value) )
							wgPrEmpTextVo.debtLoan = $("debtLoan").value;
	
						if( !isEqualsValue($("donate").value, $("hDonate").value) )
							wgPrEmpTextVo.donate = $("donate").value;
	
						//if( !isEqualsValue($("other").value, $("hOther").value) )
						//	prEmpTextVo.other = $("other").value;
	
						if( !isEqualsValue($("incomeTax").value, $("hIncomeTax").value) )
							wgPrEmpTextVo.incomeTax = $("incomeTax").value;
	
						//if( !isEqualsValue($("oldSalary").value, $("oldSalary").value) )
						//	prEmpTextVo.oldSalary = '';
	
						//if( !isEqualsValue($("debtLoan").value, $("hDebtLoan").value) )
						//	prEmpTextVo.newSalary = '';
	
						//if( !isEqualsValue($("debtLoan").value, $("hDebtLoan").value) )
						//	prEmpTextVo.adjOldsal = '';
	
						//if( !isEqualsValue($("debtLoan").value, $("hDebtLoan").value) )
						//	prEmpTextVo.adjNewsal = '';
	
						//prEmpTextVo.gundanAmt = '';
						
						if( !isEqualsValue($("flagPr").value, $("hFlagPr").value) )
							wgPrEmpTextVo.flagPr = $("flagPr").value;
	
						//prEmpTextVo.deductAmt = '';
						//prEmpTextVo.flagStatus = '';
						
						//if( !isEqualsValue($("flagPr").value, $("hFlagPr").value) )
						//	prEmpTextVo.seqData = '';
	
						if( $("flagFather").checked )
							wgPrEmpTextVo.flagFather = 'Y';
						else
							wgPrEmpTextVo.flagFather = 'N';
						
						if( $("flagMother").checked )
							wgPrEmpTextVo.flagMother = 'Y';
						else
							wgPrEmpTextVo.flagMother = 'N';
						
						if( $("flagFatherSpouse").checked )
							wgPrEmpTextVo.flagFatherSpouse = 'Y';
						else
							wgPrEmpTextVo.flagFatherSpouse = 'N';
							
						if( $("flagMotherSpouse").checked )
							wgPrEmpTextVo.flagMotherSpouse = 'Y';
						else
							wgPrEmpTextVo.flagMotherSpouse = 'N';
	
						if( !isEqualsValue($("rmf").value, $("hRmf").value) )
							wgPrEmpTextVo.rmf = $("rmf").value;
	
						if( !isEqualsValue($("ltf").value, $("hLtf").value) )
							wgPrEmpTextVo.ltf = $("ltf").value;
	
						if( !isEqualsValue($("pensionFund").value, $("hPensionFund").value) )
							wgPrEmpTextVo.pensionFund = $("pensionFund").value;
						
						if( !isEqualsValue($("teacherFund").value, $("hTeacherFund").value) )
							wgPrEmpTextVo.teacherFund = $("teacherFund").value;
						
						if( !isEqualsValue($("overage").value, $("hOverage").value) )
							wgPrEmpTextVo.overage = $("overage").value;
						
						if( !isEqualsValue($("overageSpouse").value, $("hOverageSpouse").value) )
							wgPrEmpTextVo.overageSpouse = $("overageSpouse").value;
						
						if( !isEqualsValue($("compensateLabour").value, $("hCompensateLabour").value) )
							wgPrEmpTextVo.compensateLabour = $("compensateLabour").value;
						
						//alert( prEmpTextVo.toJSONString() );
						
						wgPrEmpTextVo.flagStatus = 'A';
						
						WgPrEmployeeTextService.insertWgPrEmployeeText(wgPrEmpTextVo, {callback:whenInsertPrEmployeeText});
		    }
		    
		    function whenInsertPrEmployeeText(data)
		    {
		    	alert("�ѹ�֡���������º����");
		    	clearScreen();
		    //	gotoMTPage();
		    }
		    
		    function checkDelete(){
		    	if(confirm("��س��׹�ѹ���ź������")){
		    		onDelete();
		    	}
		    }
		    function onDelete(){
		    	var empName = $("empName").value.replace(' ','space');
		    	//alert( empName );
		    	if( empName != 'space' && $("orgDesc").value != '' ){
			    	wgPrEmpTextVo.keySeq = '';
			    	wgPrEmpTextVo.ouCode = '<%=ouCode%>';
					wgPrEmpTextVo.year = $("year").value/1; 
					wgPrEmpTextVo.period = $("period").value/1;
					wgPrEmpTextVo.empCode = $("empCode").value;
					
					
					if( !isEqualsValue($("taxId").value, $("hTaxId").value) )
						wgPrEmpTextVo.taxId = $("taxId").value;
					
					if( !isEqualsValue($("marriedStatus").value, $("hMarriedStatus").value) )
						wgPrEmpTextVo.marriedStatus = $("marriedStatus").value;
						
					if( !isEqualsValue($("payStatus").value, $("hPayStatus").value) )
						wgPrEmpTextVo.payStatus = $("payStatus").value;

					if( !isEqualsValue($("bankId").value, $("hBankId").value) )
						wgPrEmpTextVo.bankId = $("bankId").value;
					
					if( !isEqualsValue($("costChild").value, $("hCostChild").value) )
						WgPrEmpTextVo.costChild = $("costChild").value;
					
					if( !isEqualsValue($("childStudy").value, $("hChildStudy").value) )
						wgPrEmpTextVo.childStudy = $("childStudy").value;
						
					if( !isEqualsValue($("childNoStudy").value, $("hChildNoStudy").value) )
						wgPrEmpTextVo.childNoStudy = $("childNoStudy").value;
						
					if( !isEqualsValue($("costLife").value, $("hCostLife").value) )
						wgPrEmpTextVo.costLife = $("costLife").value;

					if( $("gundanFlag").checked ){
						if( !isEqualsValue('Y', $("hGundanFlag").value) )
							wgPrEmpTextVo.gundanFlag = 'Y';
					}else{
						if( !isEqualsValue('N', $("hGundanFlag").value) )
							wgPrEmpTextVo.gundanFlag = 'N';
					}
					
					if( !isEqualsValue($("debtLife").value, $("hDebtLife").value) )
						wgPrEmpTextVo.debtLife = $("debtLife").value;

					if( !isEqualsValue($("debtLoan").value, $("hDebtLoan").value) )
						wgPrEmpTextVo.debtLoan = $("debtLoan").value;

					if( !isEqualsValue($("donate").value, $("hDonate").value) )
						wgPrEmpTextVo.donate = $("donate").value;

					//if( !isEqualsValue($("other").value, $("hOther").value) )
					//	prEmpTextVo.other = $("other").value;

					if( !isEqualsValue($("incomeTax").value, $("hIncomeTax").value) )
						wgPrEmpTextVo.incomeTax = $("incomeTax").value;

					//if( !isEqualsValue($("oldSalary").value, $("oldSalary").value) )
					//	prEmpTextVo.oldSalary = '';

					//if( !isEqualsValue($("debtLoan").value, $("hDebtLoan").value) )
					//	prEmpTextVo.newSalary = '';

					//if( !isEqualsValue($("debtLoan").value, $("hDebtLoan").value) )
					//	prEmpTextVo.adjOldsal = '';

					//if( !isEqualsValue($("debtLoan").value, $("hDebtLoan").value) )
					//	prEmpTextVo.adjNewsal = '';

					//prEmpTextVo.gundanAmt = '';
					
					if( !isEqualsValue($("flagPr").value, $("hFlagPr").value) )
						wgPrEmpTextVo.flagPr = $("flagPr").value;

					//prEmpTextVo.deductAmt = '';
					//prEmpTextVo.flagStatus = '';
					
					//if( !isEqualsValue($("flagPr").value, $("hFlagPr").value) )
					//	prEmpTextVo.seqData = '';

					if( $("flagFather").checked )
						wgPrEmpTextVo.flagFather = 'Y';
					else
						wgPrEmpTextVo.flagFather = 'N';
					
					if( $("flagMother").checked )
						wgPrEmpTextVo.flagMother = 'Y';
					else
						wgPrEmpTextVo.flagMother = 'N';
					
					if( $("flagFatherSpouse").checked )
						wgPrEmpTextVo.flagFatherSpouse = 'Y';
					else
						wgPrEmpTextVo.flagFatherSpouse = 'N';
						
					if( $("flagMotherSpouse").checked )
						wgPrEmpTextVo.flagMotherSpouse = 'Y';
					else
						wgPrEmpTextVo.flagMotherSpouse = 'N';

					if( !isEqualsValue($("rmf").value, $("hRmf").value) )
						wgPrEmpTextVo.rmf = $("rmf").value;

					if( !isEqualsValue($("ltf").value, $("hLtf").value) )
						wgPrEmpTextVo.ltf = $("ltf").value;

					if( !isEqualsValue($("pensionFund").value, $("hPensionFund").value) )
						wgPrEmpTextVo.pensionFund = $("pensionFund").value;
					
					if( !isEqualsValue($("teacherFund").value, $("hTeacherFund").value) )
						wgPrEmpTextVo.teacherFund = $("teacherFund").value;
					
					if( !isEqualsValue($("overage").value, $("hOverage").value) )
						wgPrEmpTextVo.overage = $("overage").value;
					
					if( !isEqualsValue($("overageSpouse").value, $("hOverageSpouse").value) )
						wgPrEmpTextVo.overageSpouse = $("overageSpouse").value;
					
					if( !isEqualsValue($("compensateLabour").value, $("hCompensateLabour").value) )
						wgPrEmpTextVo.compensateLabour = $("compensateLabour").value;
					
					//alert( prEmpTextVo.toJSONString() );
					
					wgPrEmpTextVo.flagStatus = 'D';
					
					WgPrEmployeeTextService.insertWgPrEmployeeText(wgPrEmpTextVo, {callback:whenDeletePrEmployeeText,errorHandler:function(message) { alert('�������ö�ѹ�֡��');}});
				}else{
					alert("�ô��͡ �Ţ��Шӵ�� ���� �ѧ�Ѵ��Ժѵԧҹ���١��ͧ");
				}
		    }
		    function whenDeletePrEmployeeText(data){
		    	alert("ź���������º����");
		    	gotoMTPage();
		    }		    
		    
		    function gotoMTPage(){
		    	document.forms["goMT"].submit();
			}
			
			// this function use for compare 2 value is Equals ?
			function isEqualsValue(val1, val2){
				if( val1 == val2 )
					return true;
				else
					return false;
			}
		

			function init(){
	
				onLoad();
				
				if( <%= isConfirm %> ){
					document.forms['searchForm'].elements['taxId'].focus();
					
					document.forms['searchForm'].elements['ok'].disabled = true;
				//	document.forms['searchForm'].elements['delete'].disabled = true;
					
					
					document.forms['searchForm'].elements['taxId'].readOnly = true;
					document.forms['searchForm'].elements['taxId'].style.background = 'silver';
					
					document.forms['searchForm'].elements['flagPr'].disabled = true;
					document.forms['searchForm'].elements['flagPr'].style.background = 'silver';
										
					document.forms['searchForm'].elements['marriedStatus'].disabled = true;
					document.forms['searchForm'].elements['marriedStatus'].style.background = 'silver';
					
					document.forms['searchForm'].elements['payStatus'][0].disabled = true;
					document.forms['searchForm'].elements['payStatus'][1].disabled = true;
					
					document.forms['searchForm'].elements['bankId'].readOnly = true;
					document.forms['searchForm'].elements['bankId'].style.background = 'silver';
					
					document.forms['searchForm'].elements['costChild'].readOnly = true;
					document.forms['searchForm'].elements['costChild'].style.background = 'silver';
					
					document.forms['searchForm'].elements['childStudy'].readOnly = true;
					document.forms['searchForm'].elements['childStudy'].style.background = 'silver';
					
					document.forms['searchForm'].elements['childNoStudy'].readOnly = true;
					document.forms['searchForm'].elements['childNoStudy'].style.background = 'silver';
					
					document.forms['searchForm'].elements['gundanFlag'].disabled = true;
					document.forms['searchForm'].elements['gundanFlag'].style.background = 'silver';
					
					document.forms['searchForm'].elements['flagFather'].disabled = true;
					document.forms['searchForm'].elements['flagFatherSpouse'].disabled = true;
					document.forms['searchForm'].elements['flagMother'].disabled = true;
					document.forms['searchForm'].elements['flagMotherSpouse'].disabled = true;
					
					document.forms['searchForm'].elements['costLife'].readOnly = true;
					document.forms['searchForm'].elements['costLife'].style.background = 'silver';
					
					document.forms['searchForm'].elements['debtLife'].readOnly = true;
					document.forms['searchForm'].elements['debtLife'].style.background = 'silver';
					
					document.forms['searchForm'].elements['debtLoan'].readOnly = true;
					document.forms['searchForm'].elements['debtLoan'].style.background = 'silver';
					
					document.forms['searchForm'].elements['donate'].readOnly = true;
					document.forms['searchForm'].elements['donate'].style.background = 'silver';
					
					document.forms['searchForm'].elements['pensionFund'].readOnly = true;
					document.forms['searchForm'].elements['pensionFund'].style.background = 'silver';
					
					document.forms['searchForm'].elements['incomeTax'].readOnly = true;
					document.forms['searchForm'].elements['incomeTax'].style.background = 'silver';
					
					document.forms['searchForm'].elements['rmf'].readOnly = true;
					document.forms['searchForm'].elements['rmf'].style.background = 'silver';
					
					document.forms['searchForm'].elements['ltf'].readOnly = true;
					document.forms['searchForm'].elements['ltf'].style.background = 'silver';
					
					document.forms['searchForm'].elements['teacherFund'].readOnly = true;
					document.forms['searchForm'].elements['teacherFund'].style.background = 'silver';
					
					document.forms['searchForm'].elements['compensateLabour'].readOnly = true;
					document.forms['searchForm'].elements['compensateLabour'].style.background = 'silver';
					
					document.forms['searchForm'].elements['overage'].readOnly = true;
					document.forms['searchForm'].elements['overage'].style.background = 'silver';
					
					document.forms['searchForm'].elements['overageSpouse'].readOnly = true;
					document.forms['searchForm'].elements['overageSpouse'].style.background = 'silver';
				}
			}
		    
		    
		    dojo.addOnLoad(init);
		
		</script>

	</head>
	<body>
		<table width="100%">
			<tr>
				<td class="font-head">
					[ CTWGUP001 ] �ѹ�֡��������ѡ㹡�èѴ�Ӥ�Ҩ�ҧ
				</td>
			</tr>
		</table>
		<form name="searchForm" action="" method="post">
			<!-- Begin Declare Paging -->
			<!-- End Declare Paging -->
		<table width="100%" align="center">
		<tr><td align="center">
			<TABLE border="0" align="left" cellspacing="1">
				<TR>
					<TD width="354px" class="font-field" align="right">��Шӻ�&nbsp;</TD>
					<TD align="left"><INPUT type="text" name="year" value="<%= year %>" readonly="readonly" style="width: 70px;text-align: center;background-color:silver;"></TD>
				    <TD width="46px" class="font-field" align="right">�Ǵ&nbsp;</TD>
				    <TD align="left">
				    	<INPUT type="text" name="section" value="<%= section %>" readonly="readonly" style="width: 70px;text-align: center;background-color:silver;">
				    	<INPUT type="hidden" name="period" value="<%= period %>">
				    	<INPUT type="hidden" name="ouCode" value="<%= ouCode %>">
				    	<INPUT type="hidden" name="userId" value="<%= userId %>">
				    	<INPUT type="hidden" name="isConfirm" value="<%= isConfirm %>">
				    </TD>
				</TR>
			</TABLE>
		</td></tr>
		<tr><td align="center">
			<TABLE border="0" align="center" width="920px" cellspacing="1">
				<TR>
					<TD class="font-field" style="text-align:right;" width="200px">�Ţ��Шӵ��</TD>
					<TD width="130px"><INPUT type="text" id="empCode"  style="width:130px;text-align: left;" maxlength="10" onblur="whenEmpBlur();"/></TD>
					<TD colspan="4" width="470px"><INPUT type="text" id="empName" readonly="readonly" style="width:380px;text-align: left;background-color:silver;" /></TD>
					<TD colspan="2" align="right" class="font-field" style="text-align:right;">�ѵ���Ţ���
					<INPUT type="text" id="account" readonly="readonly" style="width:100px;text-align: left;background-color:silver;" />
					</TD>
				</TR>
				<TR>
				 	<TD class="font-field" style="text-align:right;" width="200px">��Ժѵ�˹�ҷ��</TD>
					<TD colspan="5" ><INPUT type="text" id="dutyDesc" readonly="readonly" style="width: 130px;text-align: left;background-color:silver;" /></TD>
					<TD colspan="2" align="right" class="font-field" style="text-align:right;">�ѹ����è�
					<INPUT type="text" id="pDate" readonly="readonly" style="width: 100px;text-align: center;background-color:silver;" />
					</TD>
				</TR>
				  
				<TR>
					<TD class="font-field" style="text-align:right;" width="200px">�ѧ�Ѵ�觵��</TD>
					<TD colspan="7" width="800px">
						<INPUT type="hidden" id="codeSeq">
						<INPUT type="text" id="codeSeqName" readonly="readonly" style="width:100%;text-align: left;background-color:silver;" />
					</TD>
				</TR>
				
				<TR>
					<TD class="font-field" style="text-align:right;" width="200px">�ѧ�Ѵ��Ժѵԧҹ</TD>
					<TD width="130px">
					 
					    <INPUT type="text" id="orgCode" readonly="readonly" style="width:130px;text-align: center;background-color:silver;" />
						<INPUT type="hidden" id="codeSeqAct" />
						
					</TD>
					<TD colspan="6" width="590px"><INPUT type="text" id="orgDesc" readonly="readonly" style="width:100%;text-align: left;background-color:silver;" /></TD>
				</TR>
				<TR>
					<TD class="font-field" style="text-align:right;" width="200px">�Ţ��Шӵ�Ǽ����������</TD>
					<TD width="130px">
						<INPUT type="text" id="taxId" maxlength="30" style="width:130px" onkeyup="if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);" />
						<INPUT type="hidden" id="hTaxId">
					</TD>
					<TD colspan="6" width="670px">
						<TABLE width="100%" border="0" cellspacing="0">
						<TR>
							<TD class="font-field" style="text-align:right;" width="166px">ʶҹС�è��¤�Ҩ�ҧ</TD>
							<TD width="160px">
								<SELECT id="flagPr">
									<OPTION value="0">����</OPTION>
									<OPTION value="1">�ЧѺ��è��·�����</OPTION>
									<OPTION value="2">�������੾���Թ��</OPTION>
								</SELECT>
								<INPUT type="hidden" id="hFlagPr">
							</TD>
							<TD class="font-field" style="text-align:right;width:159px">ʶҹС��Ŵ���͹</TD>
							<TD align="right">
								<SELECT id="marriedStatus" style="width:150px">
									<OPTION value="1">�ʴ</OPTION>
									<OPTION value="2">�������������Թ��</OPTION>
									<OPTION value="3">����������Թ��</OPTION>
									<OPTION value="4">�����-����-�ʴ�պصúح����</OPTION>
								</SELECT>
								<INPUT type="hidden" id="hMarriedStatus">
							</TD>
						</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD class="font-field" style="text-align:right;" width="200px">ʶҹС���Ѻ��Ҩ�ҧ<INPUT type="hidden" id="hPayStatus"> </TD>
					<TD><INPUT type="radio" value="1" name="payStatus"/><FONT class="font-field">���¼�ҹ��Ҥ��</FONT></TD>
					<TD><input type="radio" value="2" name="payStatus"/><FONT class="font-field">�Թʴ</FONT></TD>
					<TD colspan="3"></TD>
					<TD width="140px" class="font-field" style="text-align:right;">�Ţ���ѭ�ո�Ҥ��</TD>
					<TD width="154px">
						<input type="text" id="bankId" maxlength="30" style="width=150px;" onkeyup="if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);"/>
						<INPUT type="hidden" id="hBankId">
					</TD>
				</TR>
				<TR>
					<TD class="font-field" style="text-align:right;" width="200px">�Թ�ѧ�վ</TD>
					<TD width="130px">
						<!-- <SELECT id="costLife" >
							<OPTION value=''></OPTION>
							<OPTION value='800'>800</OPTION>
							<OPTION value='1600'>1600</OPTION>
							<OPTION value='2400'>2400</OPTION>
						</SELECT>-->
						<INPUT id="costLife" type="text" maxlength="7" style="width:100px;text-align:right;" onkeyup="if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);" /> 
						<INPUT type="hidden" id="hCostLife">
					</TD>						
					<TD class="font-field" style="text-align:left;">�ӹǹ�ص÷�����Ѻ��ê�������� 
						<INPUT type="text" name="costChild" maxlength="5" style="width:123px;text-align:right;" onkeyup="if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);" />
						<INPUT type="hidden" id="hCostChild">
					</TD>
					<TD colspan="3"></TD>
					<TD width="140px" class="font-field" style="text-align:right;">���¡ѹ���</TD>
					<TD width="154px">
						<input type="checkbox" name="gundanFlag" />
						<INPUT type="hidden" id="hGundanFlag">
					</TD>
				</TR>				
			</TABLE>
		</td></tr>
		</TABLE>
			<TABLE border="0" align="center" width="950px" cellspacing="1">
				<TR>
					<TD colspan="8">
						<FIELDSET><LEGEND class="font-field">���������´�ӹǳ����</LEGEND>
						<TABLE border="0" width="100%">
							<TR><TD></TD><TD></TD><TD></TD><TD></TD><TD></TD><TD></TD></TR>
							<TR>
								<TD class="font-field" style="text-align:right;">�ص÷���֡��</TD>
								<TD>
									<INPUT id="childStudy" type="text" maxlength="5" style="width:100px;text-align:right;" onkeyup="if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);">
									<INPUT type="hidden" id="hChildStudy">
								</TD>
								<TD class="font-field" style="text-align: right;" width="200px">�ص÷������֡��</TD>
								<TD colspan="3">
									<INPUT id="childNoStudy" type="text" maxlength="5" style="width:100px;text-align:right;" onkeyup="if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);">
									<INPUT id="hChildNoStudy" type="hidden">
								</TD>
							</TR>
							<TR>
								<TD></TD>
								<TD align="left">
									<INPUT id="flagFather" type="checkbox"><FONT class="font-field">�ѡŴ���͹�Դ�</FONT>
									<INPUT type="hidden" id="hFlagFather">
								</TD>
								<TD colspan="4" align="left">
									<INPUT id="flagFatherSpouse" type="checkbox"><FONT class="font-field">�ѡŴ���͹�ԴҢͧ������ʷ�����Թ�� ����ӹǳ��������������Թ��</FONT>
									<INPUT type="hidden" id="hFlagFatherSpouse">
								</TD>
							</TR>
							<TR>
								<TD></TD>
								<TD align="left">
									<INPUT id="flagMother" type="checkbox"><FONT class="font-field">�ѡŴ���͹��ô�</FONT>
									<INPUT type="hidden" id="hFlagMother">
								</TD>
								<TD colspan="4" align="left">
									<INPUT id="flagMotherSpouse" type="checkbox"><FONT class="font-field">�ѡŴ���͹��ôҢͧ������ʷ�����Թ�� ����ӹǳ��������������Թ��</FONT>
									<INPUT type="hidden" id="hFlagMotherSpouse">
								</TD>
							</TR>
							<TR>
								<TD style="text-align: right;" class="font-field">&nbsp;</TD>
								<TD>
								&nbsp;
								<!--  
									<INPUT id="costLife" type="text" maxlength="7" style="width:100px;text-align:right;" onkeyup="if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);">
									<INPUT type="hidden" id="hCostLife"> 
								-->
								</TD>
								<TD style="text-align: right;" class="font-field" width="200px">���»�Сѹ���Ե</TD>
								<TD>
									<INPUT id="debtLife" type="text" maxlength="7" style="width:100px;text-align:right;" onkeyup="if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);">
									<INPUT type="hidden" id="hDebtLife">
								</TD>
								<TD style="text-align: right;" class="font-field">�͡�����Թ������͡������</TD>
								<TD>
									<INPUT id="debtLoan" type="text" maxlength="7" style="width:100px;text-align:right;" onkeyup="if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);">
									<INPUT type="hidden" id="hDebtLoan">
								</TD>
							</TR>
							<TR>
								<TD style="text-align: right;" class="font-field">�Թ��ԨҤ</TD>
								<TD>
									<INPUT id="donate" type="text" maxlength="7" style="width:100px;text-align:right;" onkeyup="if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);">
									<INPUT type="hidden" id="hDonate">
								</TD>
								<TD style="text-align:right;" class="font-field" width="200px">�Թ���� ���.</TD>
								<TD>
									<INPUT id="pensionFund" type="text" maxlength="7" style="width:100px;text-align:right;" onkeyup="if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);">
									<INPUT type="hidden" id="hPensionFund">
								</TD>
								<TD style="text-align: right;" class="font-field">�Թ����� � ����ͧ��������</TD>
								<TD>
									<INPUT id="incomeTax" type="text" maxlength="7" style="width:100px;text-align:right;" onkeyup="if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);">
									<INPUT type="hidden" id="hIncomeTax">
								</TD>
							</TR>
							<TR>
								<TD style="text-align:right;" class="font-field" width="410px" colspan="3" align="right">��ҫ���˹���ŧ�ع㹡ͧ�ع������͡������§�վ (RMF)</TD>
								<TD>
									<INPUT id="rmf" type="text" maxlength="7" style="width:100px;text-align:right;" onkeyup="if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);">
									<INPUT type="hidden" id="hRmf">
								</TD>
								<TD style="text-align:right;" class="font-field">��ҫ���˹���ŧ�ع㹡ͧ�ع������������� (LTF)</TD>
								<TD>
									<INPUT id="ltf" type="text" maxlength="7" style="width:100px;text-align:right;" onkeyup="if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);">
									<INPUT type="hidden" id="hLtf">
								</TD>
							</TR>
						</TABLE>
						<hr/>
						<TABLE border="0" width="100%">
							<TR>
								<TD style="text-align: right;" class="font-field">��� � 1</TD>
								<TD>
									<INPUT id="teacherFund" type="text" maxlength="7" style="width:100px;text-align:right;" onkeyup="if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);">
									<INPUT type="hidden" id="hTeacherFund">
								</TD>
								<TD style="text-align: right;" class="font-field">��� � 2</TD>
								<TD width="150px">
									<INPUT id="compensateLabour" type="text" maxlength="7" style="width:100px;text-align:right;" onkeyup="if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);">
									<INPUT type="hidden" id="hCompensateLabour">
								</TD>
							</TR>
							<TR>
								<TD style="text-align: right;" class="font-field">��� � 3</TD>
								<TD>
									<INPUT id="overage" type="text" maxlength="7" style="width:100px;text-align:right;" onkeyup="if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);">
									<INPUT type="hidden" id="hOverage">
								</TD>
								<TD style="text-align: right;" class="font-field">��� � 4</TD>
								<TD>
									<INPUT id="overageSpouse" type="text" maxlength="7" style="width:100px;text-align:right;" onkeyup="if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);">
									<INPUT type="hidden" id="hOverageSpouse">
								</TD>
							</TR>
						</TABLE>
						</FIELDSET>
					</TD>
				</TR>
			</TABLE>
			<TABLE width="100%" CLASS="TABLEBULE2">
				<TR CLASS="TABLEBULE2" >
					<TD align="left" >&nbsp;
					<!--  
						<input type="button" class=" button "  value="ź" name="delete" onclick="checkDelete();"/>
					-->
						<input type="button" class=" button " style="width: 60px" value="��ŧ" name="ok" onclick="checkSave();"/>
						<input type="button" class=" button " style="width: 60px" value="�͡" name="back" onclick="gotoMTPage();"/>
						<!-- <input type="button" class=" button "  value="Clear" name="Clear" onclick="clearScreen();"/> -->
					</TD>
				</TR>
			</TABLE>
		</form>
		<FORM name="goMT" action="security.htm?reqCode=CTWGMT001" method="post">
			<input type="hidden" name="orgFromLabel" value="<%=orgFromLabel%>">
			<input type="hidden" name="orgFromValue" value="<%=orgFromValue%>">
			<input type="hidden" name="orgToLabel" value="<%=orgToLabel%>">
			<input type="hidden" name="orgToValue" value="<%=orgToValue%>">
			<input type="hidden" name="empFrom" value="<%=empFrom%>">
			<input type="hidden" name="empTo" value="<%=empTo%>">
			<input type="hidden" name="pageNow" value="<%=pageNow%>">
			<input type="hidden" name="search" value="search">
		</FORM>
	</body>
</html>
