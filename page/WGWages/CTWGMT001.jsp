<%@ page language="java" contentType="text/html;charset=TIS-620"%>
<%@ page import="com.ss.tp.security.UserInfo" %>
<%@ taglib prefix="c" uri="/WEB-INF/tld/c.tld"%>
<%
	UserInfo userInfo = (UserInfo)session.getAttribute("UserLogin");
	String userId = userInfo.getUserId();
	String ouCode = userInfo.getOuCode();

	// ***** declare for paging  **********
	String pageEdit = request.getParameter("pageEdit")==null?"-1":request.getParameter("pageEdit");

	// *** comback from edit page *** //
	String orgFromLabel = (String) request.getParameter("orgFromLabel");
	String orgFromValue = (String) request.getParameter("orgFromValue");
	String orgToLabel = (String) request.getParameter("orgToLabel");
	String orgToValue = (String) request.getParameter("orgToValue");
	String empFrom = (String) request.getParameter("empFrom");
	String empTo = (String) request.getParameter("empTo");
	String pageNow = (String) request.getParameter("pageNow");
	String search = (String) request.getParameter("search");
%>
<html>
	<head>
		<title>�ѹ�֡��������ѡ㹡�èѴ���Թ��͹</title>
		<!-- Include -->
		<script type="text/javascript" src="dwr/engine.js"></script>
		<script type="text/javascript" src="dwr/util.js"></script>
		<!-- Javascript Script File -->
		<SCRIPT type="text/javascript" src="dwr/interface/SuUserOrganizationService.js"></SCRIPT>
		<SCRIPT type="text/javascript" src="dwr/interface/WgPrEmployeeService.js"></SCRIPT>
		<script type="text/javascript" src="script/gridScript.js"></script>
		<script type="text/javascript" src="page/NavigatePage.jsp"></script>
		<script type="text/javascript" src="script/dojo.js"></script>		
		<script type="text/javascript" src="script/payroll_util.js"></script>

		<script type="text/javascript">
			//Load Dojo's code relating to widget managing functions
			dojo.require("dojo.widget.*");
			dojo.require("dojo.widget.Menu2");
			dojo.require("dojo.widget.Button");
			dojo.require("dojo.widget.ComboBox");
			dojo.require("dojo.widget.DropDownButton");
			dojo.require("dojo.widget.SortableTable");
			dojo.require("dojo.widget.ContentPane");
			dojo.require("dojo.widget.LayoutContainer");
			dojo.require("dojo.widget.SortableTable");
			dojo.require("dojo.widget.Toolbar");
			dojo.require("dojo.widget.html.*");
			dojo.require("dojo.widget.Menu2");
			dojo.hostenv.writeIncludes();
			
			//Event
			dojo.require("dojo.event.*");
		</script>
		<script type="text/javascript"><!--
			
			function onLoadYearSection(){
				onLoadYearSectionCallback();
			}
			
			function onLoadYearSectionCallback(){
				
				$("year").value =   "<c:out value='${WgDefaultYearAndSection.year}' /> "  ;
				$("section").value = "<c:out value='${WgDefaultYearAndSection.section}' /> "  ;
				$("period").value = ""+"<c:out value='${WgDefaultYearAndSection.period}' /> "  ;
			
			
				onLoadOrganization();				
			}
		
			function onLoadOrganization(){
		     	DWRUtil.useLoadingMessage("Loading ...");
		     	onLoadOrganizationCallback();
		    }
			var orgDataDesc=[];
			var orgDataValue=[];  
		    function onLoadOrganizationCallback(){
		     	try{
			     	var cboSource = [];
			     	var cboFrom = dojo.widget.byId("orgFromCbo");
			     	var cboTo = dojo.widget.byId("orgToCbo");
			     	
			     	<c:forEach items="${OrganizationInSecurity}" var="result" >		 
						cboSource.push(["<c:out value='${result.orgCode}' />"+" "+"<c:out value='${result.orgShowDesc}' />","<c:out value='${result.orgCode}' />"]);
						orgDataDesc.push(["<c:out value='${result.orgCode}' />"+" "+"<c:out value='${result.orgShowDesc}' />"]);
						orgDataValue.push(["<c:out value='${result.orgCode}' />"]);											
					</c:forEach>
			     	
			     	cboFrom.dataProvider.setData(cboSource);
			     	cboTo.dataProvider.setData(cboSource);		
			     		     	
			     	onLoadEmployee();
			     	
		     	}catch(e){
		     		alert(e.message);
		     	}
		    }
		    
		    function onLoadEmployee(){
				onLoadEmployeeCallback();
			}
			function onLoadEmployeeCallback(){
		     	try{
			     	var cboSource = [];
			     	var cboFrom = dojo.widget.byId("empFromCbo");
			     	var cboTo = dojo.widget.byId("empToCbo");

			     	
			     	<c:forEach items="${WgPrEmployeeInSecurity}" var="result" >		 
						cboSource.push(["<c:out value='${result.empCode}' />","<c:out value='${result.empCode}' />"]);
					</c:forEach>			     	
			     	
			     	cboFrom.dataProvider.setData(cboSource);
			     	cboTo.dataProvider.setData(cboSource);
		     	}catch(e){
		     		alert(e.message);
		     	}
				//air
				var frm = document.forms["editForm"];
				if (frm.search.value == 'search'){
					whenComeBackFromEditPage();
				}
				var frm = document.forms["deleteForm"];
				if (frm.search.value == 'search'){
					whenComeBackFromDeletetPage();
				}
		    }
		    
		    /****** Begin  OrgCodeFrom Select ********/
		    function whenOrgFromSelectOption(){
		    	DWRUtil.useLoadingMessage("Loading ...");
				var cbo = dojo.widget.byId("orgFromCbo");
				whenFetchOrganizationTo(splitCombo(cbo.textInputNode.value));
			}
			function whenFetchOrganizationTo(orgCode){
		     	DWRUtil.useLoadingMessage("Loading ...");
		     	var cboTo = dojo.widget.byId("orgToCbo");
		     	//alert( orgCode + " " + cboTo.comboBoxSelectionValue.value + " " + (orgCode > cboTo.comboBoxSelectionValue.value));
		     	if( orgCode > splitCombo( cboTo.textInputNode.value ) ){
			     	cboTo.textInputNode.value = '';
		     	}
		     	
		     	var cboSource = [];
		     	
		     	<c:forEach items="${OrganizationInSecurity}" var="result" >		 
					if( <c:out value='${result.orgCode}' /> >= orgCode )
						cboSource.push(["<c:out value='${result.orgCode} ${result.divShort} ${result.areaDesc} ${result.secDesc} ${result.workDesc}' />","<c:out value='${result.orgCode}' />"]);
				</c:forEach>
		     	
		     	cboTo.dataProvider.setData(cboSource);
		     	
		     	//SuUserOrganizationService.findOrganizationByUserIdAndOuCodeToOrgCode('<%=userId%>','<%=ouCode%>',orgCode , {callback:whenFetchOrganizationToCallback,errorHandler:function(message) { alert('�Դ��ͼԴ��Ҵ����ǡѺ��õԴ����������');}});
		    }
		    function whenFetchOrganizationToCallback(data){
		    	try{
			     	var cboSource = [];
			     	var cboTo = dojo.widget.byId("orgToCbo");
			     	for(var i=0; i<data.length; i++){
			     		var org = data[i];
			     		cboSource.push([org.orgCode, org.orgCode]);
			     	}
			     	cboTo.dataProvider.setData(cboSource);
		     	}catch(e){
		     		alert(e.message);
		     	}
		    }
		    /****** End  OrgCodeFrom Select ********/
			
			/****** Begin  EmpCodeFrom Select ********/
			function whenEmpFromSelectOption(){
				DWRUtil.useLoadingMessage("Loading ...");
				var cbo = dojo.widget.byId("empFromCbo");
				whenFetchEmployeeTo(splitCombo( cbo.textInputNode.value));
			}
			function whenFetchEmployeeTo(empCode){
		     	DWRUtil.useLoadingMessage("Loading ...");
		     	var cboSource = [];
		     	var cboTo = dojo.widget.byId("empToCbo");
		     	//alert( empCode + " " + cboTo.comboBoxSelectionValue.value + " " + (empCode > cboTo.comboBoxSelectionValue.value));
		     	if( empCode > cboTo.comboBoxSelectionValue.value ){
			     	cboTo.textInputNode.value = '';
			     	cboTo.comboBoxSelectionValue.value = '';
		     	}
	
			     	<c:forEach items="${WgPrEmployeeInSecurity}" var="result" >		
			     		if ( "<c:out value='${result.empCode}' />" >= empCode)
						cboSource.push(["<c:out value='${result.empCode}' />","<c:out value='${result.empCode}' />"]);
					</c:forEach>			     	

			     	cboTo.dataProvider.setData(cboSource);
		   //  	WgPrEmployeeService.findWgPrToEmpBySecurity('<%=userId%>', '<%=ouCode%>', empCode, $("year").value, $("period").value , {callback:whenFetchEmployeeToCallback,errorHandler:function(message) { alert('�Դ��ͼԴ��Ҵ����ǡѺ��õԴ����������');}});
		    }
		    function whenFetchEmployeeToCallback(data){
		    	try{
			     	var cboSource = [];
			     	var cboTo = dojo.widget.byId("empToCbo");
			     	for(var i=0; i<data.length; i++){
			     		var emp = data[i];
			     		cboSource.push([emp.empCode, emp.empCode]);
			     	}
			     	cboTo.dataProvider.setData(cboSource);
		     	}catch(e){
		     		alert(e.message);
		     	}
		    }
		    /******  End EmpCodeFrom Select ********/
			
				
			    
		    function whenQueryEmpBefore(){
		    	DWRUtil.useLoadingMessage("Loading ...");
		     	var orgFromCbo = dojo.widget.byId("orgFromCbo");
				var orgToCbo = dojo.widget.byId("orgToCbo");
				var	empFromCbo = dojo.widget.byId("empFromCbo");
				var	empToCbo = dojo.widget.byId("empToCbo");
				
				var orgFrom = orgFromCbo.textInputNode.value;
				var orgTo   = orgToCbo.textInputNode.value;
				var empFromVal = empFromCbo.textInputNode.value;
				var empToVal   = empToCbo.textInputNode.value;	

				var orgFromVal = splitCombo( orgFromCbo.textInputNode.value );
				var orgToVal   = splitCombo( orgToCbo.textInputNode.value );				
				var frm = document.forms["editForm"];
				if(frm.search.value == 'search'){
				    if (frm.pageNow.value  < 0 ){DWRUtil.setValue("page",0);}
				    else{DWRUtil.setValue("page", frm.pageNow.value);}
					frm.search.value = null;
				}
				var frm = document.forms["deleteForm"];
				if(frm.search.value == 'search'){
				    if (frm.pageNow.value  < 0 ){DWRUtil.setValue("page",0);}
				    else{DWRUtil.setValue("page", frm.pageNow.value);}
					frm.search.value = null;
				}
		     		
		     
		     		
				WgPrEmployeeService.findByCriteria(
		     		'<%= ouCode %>', 
			     	$("year").value, 
			     	$("period").value,
		     		'<%= userId %>',
		     		orgFromVal,
					orgToVal,
					empFromVal,
					empToVal,
					DWRUtil.getValue("page"),
					DWRUtil.getValue("dataPerPage"),
					{callback:whenQueryEmpCallBack}
				);
				
		    }
		    function whenQueryEmpCallBack(data){
		     	//alert(data.length);
		     	if(data.length > 0){
					DWRUtil.removeAllRows("dataTable");
					DWRUtil.addRows("dataTable",data,cellFuncs);
					if(DWRUtil.getValue("showMaxPage") == ''){
						countData();
					}else{
						onCheckButt("searchForm");
					}
				}else{
					alert('��辺������');
					DWRUtil.removeAllRows("dataTable");
					if(DWRUtil.getValue("showMaxPage") == ''){
						countData();
					}else{
						onCheckButt("searchForm");
					}
				}
		    }
		    
		    function countData(){
		    	DWRUtil.useLoadingMessage("Loading ...");
		     	var orgFromCbo = dojo.widget.byId("orgFromCbo");
				var orgToCbo = dojo.widget.byId("orgToCbo");
				var	empFromCbo = dojo.widget.byId("empFromCbo");
				var	empToCbo = dojo.widget.byId("empToCbo");
				
				var orgFrom = orgFromCbo.textInputNode.value;
				var orgToVal   = orgToCbo.textInputNode.value;
				var empFromVal = empFromCbo.textInputNode.value;
				var empToVal   = empToCbo.textInputNode.value;	
				
				var orgFromVal = splitCombo( orgFromCbo.textInputNode.value );
				var orgToVal   = splitCombo( orgToCbo.textInputNode.value );

			
				WgPrEmployeeService.getCountByCriteria(
		     		'<%= ouCode %>', 
			     	$("year").value, 
			     	$("period").value,
		     		'<%= userId %>',
		     		orgFromVal,
					orgToVal,
					empFromVal,
					empToVal,
					{callback:whenCountDataCallBack,errorHandler:function(message) { alert('�Դ��ͼԴ��Ҵ����ǡѺ��õԴ����������');}}
				);
				
		    }
		    function whenCountDataCallBack(data){
		    	DWRUtil.setValue("countData",data);
				onCheckButt("searchForm");
		    }
			
			var cellFuncs = [
				function(data) { return "<div align='center'>"+data.empCode+"</div>";},
				function(data) { return data.name;},
				function(data) { return "<div align='left'>"+data.dutyDesc+"</div>";},
				function(data) { return "<div ><table border='0' width='100%'><tr><td align='center'>"+data.statusText+"</td></tr></table></div>";}
			//	function(data) { return writeButton("edit",data.empCode);}
			];
			
			function writeButton(inname,emp)
			{
				return "<div align='center'><input type='button' class='button' name = '"+inname+"' value='...' onclick='preEdit(this.empId);' empId='"+emp+"' /></div>";
			}
			
			function preIns(){ 
				var frm=document.forms["insertForm"];
				$("yearIns").value = $("year").value;
				$("sectionIns").value = $("section").value;
				$("periodIns").value = $("period").value;
				$("isConfirmIns").value = $("isConfirm").value;
				
				frm.submit();
			}	
			
			function preEdit(){ 
				var frm=document.forms["editForm"];
				$("yearEdit").value = $("year").value;
				$("sectionEdit").value = $("section").value;
				$("periodEdit").value = $("period").value;
				$("isConfirmEdit").value = $("isConfirm").value;
				//	$("empCodeEdit").value = empId;
				var cboOrgFrom = dojo.widget.byId("orgFromCbo");
				var cboOrgTo = dojo.widget.byId("orgToCbo");
				var cboEmpFrom = dojo.widget.byId("empFromCbo");
				var cboEmpTo = dojo.widget.byId("empToCbo");
				frm.orgFromLabel.value = cboOrgFrom.textInputNode.value;
				frm.orgFromValue.value = splitCombo(cboOrgFrom.textInputNode.value);
				frm.orgToLabel.value = cboOrgTo.textInputNode.value;
				frm.orgToValue.value = splitCombo(cboOrgTo.textInputNode.value);
				frm.empFrom.value = cboEmpFrom.textInputNode.value;
				frm.empTo.value = cboEmpTo.textInputNode.value;
				frm.pageNow.value = $("page").value
				frm.submit();
			}
			
			function preDelete(){ 
				var frm=document.forms["deleteForm"];
				$("yearDel").value = $("year").value;
				$("sectionDel").value = $("section").value;
				$("periodDel").value = $("period").value;
				$("isConfirmDel").value = $("isConfirm").value;
				//	$("empCodeEdit").value = empId;
				var cboOrgFrom = dojo.widget.byId("orgFromCbo");
				var cboOrgTo = dojo.widget.byId("orgToCbo");
				var cboEmpFrom = dojo.widget.byId("empFromCbo");
				var cboEmpTo = dojo.widget.byId("empToCbo");
				frm.orgFromLabel.value = cboOrgFrom.textInputNode.value;
				frm.orgFromValue.value = splitCombo(cboOrgFrom.textInputNode.value);
				frm.orgToLabel.value = cboOrgTo.textInputNode.value;
				frm.orgToValue.value = splitCombo(cboOrgTo.textInputNode.value);
				frm.empFrom.value = cboEmpFrom.textInputNode.value;
				frm.empTo.value = cboEmpTo.textInputNode.value;
				frm.pageNow.value = $("page").value
				frm.submit();
			}
			
			
			function init(){
				alert("init");
				var cboOrgFrom = dojo.widget.byId("orgFromCbo");
				dojo.event.connect(cboOrgFrom, "selectOption", "whenOrgFromSelectOption");
				var cboEmpFrom = dojo.widget.byId("empFromCbo");
				dojo.event.connect(cboEmpFrom, "selectOption", "whenEmpFromSelectOption");
			}
		    
		
		    dojo.addOnLoad(onLoadYearSection);
		
		
		--></script>

	</head>
	<body>
		<table width="100%">
			<tr>
				<td class="font-head">
					[ CTWGMT001 ] �ѹ�֡��������ѡ㹡�èѴ�Ӥ�Ҩ�ҧ
				</td>
			</tr>
		</table>
		<form name="searchForm" action="" method="post">
			<table width="760" border="0" align="center" cellspacing="0">
				<tr>
					<td class="font-field" align="right">��Шӻ�&nbsp;</td>
					<td align="left"><input type="text" name="year"  value="" readonly="readonly" style="width: 70px;text-align: center;background-color:silver;"/></td>
				    <td  class="font-field" align="right">�Ǵ&nbsp;</td>
				    <td align="left">
				    	<input type="text" name="section"  value="" readonly="readonly" style="width: 70px;text-align: center;background-color:silver;"/>
				    	<input type="hidden" name="period" value="" />
				    	<INPUT type="hidden" name="isConfirm" value="" />
				    </td>
				</tr>
			  	<tr>
			    	<td class="font-field" align="right">������ѧ�Ѵ��Ժѵԧҹ&nbsp;</td>
			    	<%//onBlurInput="whenOrgFromSelectOption();" %>
			    	<td align="left" colspan="3"><SELECT  dojoType="ComboBox" widgetId="orgFromCbo" style="width:570" onBlurInput="whenOrgFromSelectOption();"></SELECT></td>
			   	</tr>
			   	<tr>
			    	<td  class="font-field" align="right">�֧�ѧ�Ѵ��Ժѵԧҹ&nbsp;</td>
			    	<td align="left" colspan="3"><SELECT  dojoType="ComboBox" widgetId="orgToCbo" style="width:570"></td>
				</tr>
  				<tr>
    				<td class="font-field" align="right">������Ţ��Шӵ��&nbsp;</td>
    				<%//onBlurInput="whenEmpFromSelectOption();"%>
    				<td align="left"><SELECT  dojoType="ComboBox" widgetId="empFromCbo" style="width:150" onBlurInput="whenEmpFromSelectOption();"></td>
				    <td  class="font-field" align="right">�֧�Ţ��Шӵ��&nbsp;</td>
				    <td align="left"><SELECT  dojoType="ComboBox" widgetId="empToCbo" style="width:150"></td>
				    <td><INPUT type="button" class="button" value="����" onclick="onQuery(whenQueryEmpBefore);"></td>
				</tr>
			</table>
			<div style="height:310px;">
			<table width="770"  border="1" bordercolor="#6699CC"  align="center"  cellpadding="2" cellspacing="0">
			<thead>
				<tr CLASS="TABLEBULE2">
				<th CLASS="TABLEBULE2" width="100">�Ţ��Шӵ��</th>
				<th CLASS="TABLEBULE2">���� - ���ʡ��</th>
				<th CLASS="TABLEBULE2" width="150">��Ժѵ�˹�ҷ��</th>
				<th CLASS="TABLEBULE2" width="50">Status</th>
				<tr>
			</thead>
			<tbody id="dataTable">
			</tbody>
			</table>
			</div>
			<!-- Begin Declare Paging -->
			<table width="770" align="center"  cellpadding="2" cellspacing="0" >
				<tr>
					<td align="right">
						<input type="hidden" name="page" value="<%=pageEdit%>">
						<input type="hidden" name="maxPage">
						<input type="hidden" name="countData" >
						<input type="hidden" name="dataPerPage" value="10">
						<input type="button" disabled="disabled" class=" button " value="First" name="first" onclick="onFirst(whenQueryEmpBefore)"/>
						<input type="button" disabled="disabled" class=" button " value="<<" name="previous" onclick="onPrevious(whenQueryEmpBefore)"/>
						<input type="text"  name="showPage" style="text-align:right;width: 30;" 
							    onkeyup="onCheckPageNAN(this.value);" onchange="onChangeGoPage(whenQueryEmpBefore);" onkeypress="onKeyGoPage(event,whenQueryEmpBefore);" 
						/>
						/
						<input type="text"  name="showMaxPage" readonly="readonly" style="width: 30;border-style : none;background-color : transparent;text-align:right;font-weight:bold;"/>
						<input type="button" disabled="disabled" class=" button " value=">>" name="next" onclick="onNext(whenQueryEmpBefore);" />
						<input type="button" disabled="disabled" class=" button " value="Last" name="last" onclick="onLast(whenQueryEmpBefore);"/>
					</td>
				</tr>
			</table>
			<!-- End Declare Paging -->
			<table width="100%" CLASS="TABLEBULE2">
				<tr CLASS="TABLEBULE2" >
					<td align="left" >
						<input type="button" class=" button " style="width: 60px" value="��ӡ�䢢�����" name="edit" onclick="preEdit();"/>
						<input type="button" class=" button " style="width: 60px" value="ź������" name="del" onclick="preDelete();"/>
					</td>
				</tr>
			</table>
		</form>
		<form name="insertForm" action="security.htm?reqCode=CTWGIN001" method="post">
			<input type="hidden" name="yearIns" />
			<input type="hidden" name="sectionIns" />
			<input type="hidden" name="periodIns" />
			<input type="hidden" name="isConfirmIns" />
		</form>
		<form name="editForm" action="security.htm?reqCode=CTWGUP001" method="post">
			<input type="hidden" name="yearEdit" />
			<input type="hidden" name="periodEdit" />
			<input type="hidden" name="sectionEdit" />
			<input type="hidden" name="isConfirmEdit" />
			<input type="hidden" name="empCodeEdit" />
			<input type="hidden" name="orgFromLabel" value="<%=orgFromLabel%>"/>
			<input type="hidden" name="orgFromValue" value="<%=orgFromValue%>"/>
			<input type="hidden" name="orgToLabel" value="<%=orgToLabel%>"/>
			<input type="hidden" name="orgToValue" value="<%=orgToValue%>"/>
			<input type="hidden" name="empFrom" value="<%=empFrom%>"/>
			<input type="hidden" name="empTo" value="<%=empTo%>"/>
			<input type="hidden" name="pageNow" value="<%=pageNow%>"/>
			<input type="hidden" name="search" value="<%=search%>"/>
		</form>
		<form name="deleteForm" action="security.htm?reqCode=CTWGDL001" method="post">
			<input type="hidden" name="yearDel" />
			<input type="hidden" name="periodDel" />
			<input type="hidden" name="sectionDel" />
			<input type="hidden" name="isConfirmDel" />
			<input type="hidden" name="empCodeEdit" />
			<input type="hidden" name="orgFromLabel" value="<%=orgFromLabel%>"/>
			<input type="hidden" name="orgFromValue" value="<%=orgFromValue%>"/>
			<input type="hidden" name="orgToLabel" value="<%=orgToLabel%>"/>
			<input type="hidden" name="orgToValue" value="<%=orgToValue%>"/>
			<input type="hidden" name="empFrom" value="<%=empFrom%>"/>
			<input type="hidden" name="empTo" value="<%=empTo%>"/>
			<input type="hidden" name="pageNow" value="<%=pageNow%>"/>
			<input type="hidden" name="search" value="<%=search%>"/>
		</form>
	</body>
</html>
<script>
	function whenComeBackFromEditPage(){
		var frm = document.forms["editForm"];
		var orgFromCbo = dojo.widget.byId("orgFromCbo");
		var orgToCbo = dojo.widget.byId("orgToCbo");
		var	empFromCbo = dojo.widget.byId("empFromCbo");
		var	empToCbo = dojo.widget.byId("empToCbo");
		if (frm.orgFromLabel.value != ''){
			orgFromCbo.textInputNode.value = frm.orgFromLabel.value;
			orgFromCbo.comboBoxSelectionValue.value = splitCombo(frm.orgFromLabel.value);
		}
		if (frm.orgToLabel.value != ''){
			orgToCbo.textInputNode.value = frm.orgToLabel.value;
			orgToCbo.comboBoxSelectionValue.value = splitCombo(frm.orgToLabel.value);
		}
		if (frm.empFrom.value != ''){
			empFromCbo.textInputNode.value = frm.empFrom.value;
			empFromCbo.comboBoxSelectionValue.value = frm.empFrom.value;		
		}
		if (frm.empTo.value != ''){
			empToCbo.textInputNode.value = frm.empTo.value;
			empToCbo.comboBoxSelectionValue.value = frm.empTo.value;		
		}
		onQuery(whenQueryEmpBefore);
	}
	function whenComeBackFromDeletetPage(){
		var frm = document.forms["deleteForm"];
		var orgFromCbo = dojo.widget.byId("orgFromCbo");
		var orgToCbo = dojo.widget.byId("orgToCbo");
		var	empFromCbo = dojo.widget.byId("empFromCbo");
		var	empToCbo = dojo.widget.byId("empToCbo");
		if (frm.orgFromLabel.value != ''){
			orgFromCbo.textInputNode.value = frm.orgFromLabel.value;
			orgFromCbo.comboBoxSelectionValue.value = splitCombo(frm.orgFromLabel.value);
		}
		if (frm.orgToLabel.value != ''){
			orgToCbo.textInputNode.value = frm.orgToLabel.value;
			orgToCbo.comboBoxSelectionValue.value = splitCombo(frm.orgToLabel.value);
		}
		if (frm.empFrom.value != ''){
			empFromCbo.textInputNode.value = frm.empFrom.value;
			empFromCbo.comboBoxSelectionValue.value = frm.empFrom.value;		
		}
		if (frm.empTo.value != ''){
			empToCbo.textInputNode.value = frm.empTo.value;
			empToCbo.comboBoxSelectionValue.value = frm.empTo.value;		
		}
		onQuery(whenQueryEmpBefore);
	}
</script>
