<%@ page language="java" contentType="text/html;charset=TIS-620" %>
<%@page import="java.text.DecimalFormat"%>
<%@page import="javax.swing.text.Document"%>
<%@ taglib prefix="c" uri="/WEB-INF/tld/c.tld"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.Date"%>
<%@ page import="com.ss.tp.security.ProcessResult" %>
<%@ page import="com.ss.tp.security.UserInfo" %>
<%@ page import="com.ss.tp.dto.DefaultYearSectionVO" %>

<%
	Calendar now = Calendar.getInstance(Locale.US);	
	UserInfo uf =  (UserInfo)request.getSession().getAttribute("UserLogin");	
	DefaultYearSectionVO defaultYear = (DefaultYearSectionVO)session.getAttribute("DefaultYearAndSection");
	
	java.util.Date dd = new java.util.Date();
	java.text.SimpleDateFormat fmd = new java.text.SimpleDateFormat("dd/MM/yyyy",new java.util.Locale("th","TH"));
	String date = fmd.format(dd);
	String userId =  uf.getUserId();
	String ouCode =  uf.getOuCode();
	String hidPage = request.getParameter("hidPage")==null?"":request.getParameter("hidPage");
	String pageEdit = request.getParameter("pageEdit")==null?"0":request.getParameter("pageEdit");
	//String year = request.getParameter("hidWorkYear")==null?String.valueOf(now.get(Calendar.YEAR)+543):request.getParameter("hidWorkYear");
	//String month = request.getParameter("hidWorkMonth")==null?String.valueOf(now.get(Calendar.MONTH)+1):request.getParameter("hidWorkMonth");
	String approveF ="";
	if (request.getSession().getAttribute("processResult") != null){
		ProcessResult processResult = (ProcessResult) request.getSession().getAttribute("processResult");
		//year =  request.getParameter("hidYear")==null?String.valueOf(now.get(Calendar.YEAR)+543):request.getParameter("hidYear");
		//month =  request.getParameter("hidMonth")==null?String.valueOf(now.get(Calendar.MONTH)+1):request.getParameter("hidMonth");
	}
%>
<html>
<head>
<title>�ѹ�֡�Ѻ�ͧ����ԡ����ѡ�Ҿ�Һ��</title>
<!-- Include -->
<script type="text/javascript" src="dwr/engine.js"></script>
<script type="text/javascript" src="dwr/util.js"></script>
<script type="text/javascript" src="script/payroll_util.js"></script>
<!-- Javascript Script File -->
<SCRIPT type="text/javascript" src="dwr/interface/SuUserOrganizationService.js"></SCRIPT>
<SCRIPT type="text/javascript" src="dwr/interface/PnEmployeeService.js"></SCRIPT>
<SCRIPT type="text/javascript" src="dwr/interface/ApTableService.js"></SCRIPT>


<script type="text/javascript" src="script/gridScript.js"></script>
<script type="text/javascript" src="script/payrollComboBox.js"></script>
<script type="text/javascript" src="page/NavigatePage.jsp"></script>
<script type="text/javascript" src="script/dojo.js"></script>
<script type="text/javascript" src="script/dateCalendar.js"></script>
<script type="text/javascript" src="script/json.js"></script>

<script type="text/javascript">
	//Load Dojo's code relating to widget managing functions
	dojo.require("dojo.widget.*");
	dojo.require("dojo.widget.Menu2");
	dojo.require("dojo.widget.Button");
	dojo.require("dojo.widget.ComboBox");
	dojo.require("dojo.widget.DropDownButton");
	dojo.require("dojo.widget.SortableTable");
	dojo.require("dojo.widget.ContentPane");
	dojo.require("dojo.widget.LayoutContainer");
	dojo.require("dojo.widget.SortableTable");
	dojo.require("dojo.widget.Toolbar");
	dojo.require("dojo.widget.html.*");
	dojo.require("dojo.widget.Menu2");
	dojo.hostenv.writeIncludes();
	
	//Event
	dojo.require("dojo.event.*");
</script>

<script type="text/javascript">

	// =========================== Start LOV ===========================
    var myUpdate = new Array();
	var count = 0;
	var rowModify ;
	var rowModifyAMT ;
	var lRowNumber;
	var canSave = true;
	
    function onLoadVolumeCallback()
    {
    	try
    	{
	    	var cboSource = [];
	     	var cbo = dojo.widget.byId("volumeCbo");
	     	//var workYear = DWRUtil.getValue("workYear");
			//var workMonth = DWRUtil.getValue("workMonth");
	     
	     
			     	
			     	<c:forEach items="${VolumeInSecurity}" var="result" >		 
						cboSource.push(["<c:out value='${result}' />","<c:out value='${result}' />"]);
					</c:forEach>
	     	
	     	cbo.dataProvider.setData(cboSource);
	     
	   
     	}catch(e)
     	{
     		alert(e.message);
    	}
	} 
     
   
 
     
   
   function whenSelectVolumeOption()
	{
    	DWRUtil.useLoadingMessage("Loading ...");
		var cbo = dojo.widget.byId("volumeCbo");
		whenFetchVolumeTo(cbo.textInputNode.value);
	}
	
	function whenFetchVolumeTo(volume)
	{
     	DWRUtil.useLoadingMessage("Loading ...");
     	var cboTo = dojo.widget.byId("volumeCbo");
		     	
		    
		     	
		     	var cboSource = [];
		     	
		     	<c:forEach items="${VolumeInSecurity}" var="result" >			     	
						cboSource.push(["<c:out value='${result}' />","<c:out value='${result}' />"]);
				</c:forEach>	
		     	
		     	cboTo.dataProvider.setData(cboSource);
     	
    }
    
  
    
	
    
   

 
	function init()
	{
		$("workYear").value = '<%= defaultYear.getYear() %>';
		$("workMonth").value = '<%= defaultYear.getMonth() %>';
		//var cboEmpFrom = dojo.widget.byId("empFromCbo");
		var cboVolume = dojo.widget.byId("volumeCbo");
	
		//dojo.event.connect(cboEmpFrom, "selectOption", "whenSelectEmpOption");
		dojo.event.connect(cboVolume, "selectOption", "whenSelectVolumeOption");
	
		
	}
	
  	dojo.addOnLoad(init);

    dojo.addOnLoad(onLoadVolumeCallback);
	//dojo.addOnLoad(onLoadEmployeeCallback);

	
	// =========================== End LOV ===========================
	
	
	
	
	function whenShowDataTable()
	{	
		myUpdate = new Array();
		
	    	var frm = document.forms[0];
			var workYear = DWRUtil.getValue("workYear");
			var workMonth = DWRUtil.getValue("workMonth");
			
			var volumeCbo = '';
			
		
			
	
			if(dojo.widget.byId("volumeCbo").textInputNode.value != '')
			{
				volumeCbo = dojo.widget.byId("volumeCbo").textInputNode.value;
			}else
			{
				volumeCbo = '';
			}
			//alert('query');
			// Query by Criteria
			ApTableService.findByCriteriaListApprove
			(
				'<%=userId%>',
				'<%=ouCode%>',
				workYear,
				workMonth,
				volumeCbo,
				DWRUtil.getValue("page"),
				DWRUtil.getValue("dataPerPage"),
				{callback:whenListDataTableHandler,errorHandler:function(message) { alert('�Դ��ͼԴ��Ҵ����ǡѺ��õԴ����������');}}
			);
		
	}
	
	var cellFuncs = [
		function(data) { return writeHidden("seqData",data.seqData,200,12,"right","keySeq",data.keySeq);},		
		function(data) { return writeTextID("empCode",data.empCode,10,"left",data.keySeq);},
		function(data) { return writeNameTextDisplay("name",data.name,200,"left","codeSeq",data.codeSeq);},
		function(data) { return writeTextDisplay("oldPositionShort",data.oldPositionShort,200,"left");},
		function(data) { return writeTextDisplay("orgDesc",data.orgDesc,200,"left");},
		function(data) { return writeText("totAmt",data.totAmt,12,"right",data.keySeq);},
		function(data) { return writeGDisplay("newGwork",data.newGworkCode,200,"left");},
		function(data) {  return writeApp("approveF",data.approveFlag,data.keySeq);}		
	];
	
	function writeCheckBox()
	{	
		return "<div align='center'><input type='checkbox' name ='chk'  /></div>";	
	}

	function writeText(inname,emp,maxlength,textalign,key)
	{
		return "<div align='center' ><input type='text' name = '"+inname+"' onChange='addListUpdate("+key+");checkValueInRowUpdate("+key+");' readonly='true'   value='"+emp+"' align='center' maxlength='"+maxlength+"' style='text-align:"+textalign+";width:100%;' onkeyup='if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);'/></div>";
	}
	function writeNameTextDisplay(inname,emp,maxlength,textalign,nameSeq,codeseq)
	{
		return "<div align='center' style='background-color:#CCCCCC;'><input type='text' readonly='true' name = '"+inname+"' value='"+emp+"' align='center' maxlength='"+maxlength+"' style='text-align:"+textalign+";width:100%;background-color:transparent;' /><input type='hidden' name = '"+nameSeq+"' value='"+codeseq+"'  /></div>";
	}
	function writeTextDisplay(inname,emp,maxlength,textalign)
	{
		return "<div align='center' style='background-color:#CCCCCC;'><input type='text' readonly='true' name = '"+inname+"' value='"+emp+"' align='center' maxlength='"+maxlength+"' style='text-align:"+textalign+";width:100%;background-color:transparent;' /></div>";
	}
	function writeTextID(inname,emp,maxlength,textalign,key)
	{
		return "<div align='center' ><input type='text' name = '"+inname+"' maxlength='6' onChange='whenSelectEmpOptionInRowUpdate("+key+");addListUpdate("+key+")'  readonly='true' value='"+emp+"' align='center' maxlength='"+maxlength+"' style='text-align:"+textalign+";width:100%;'  onkeyup='if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);'/></div>";
	}
	
	
	function writeHidden(inname,emp,size,maxlength,textalign,nameHide,empHide)
	{
				return "<div align='center'><input type='text' name = '"+inname+"' onchange='addListUpdate("+empHide+")'  readonly='true' value='"+emp+"' align='center' maxlength='"+maxlength+"' style='text-align:"+textalign+";width:100%' onkeyup='if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);'/><input type='hidden' name = '"+nameHide+"' value='"+empHide+"'  /></div>";
	}
	
	
	function writeGDisplay(inname,emp,maxlength,textalign)
	{
        var lmp ;
		
		if(emp != null && emp != ''){			
				lmp = emp;		
		}else{
			lmp = '';			
		}
		var mm0 = '';
		var mm1 = '';
		var mm2 = '';
		
		
		 if(lmp=='1'){
			return "<div align='center' style='background-color:#CCCCCC;'><input type='text' readonly='true' name = '"+inname+"' value='���ͧ' align='center' maxlength='"+maxlength+"' style='text-align:"+textalign+";width:100%;background-color:transparent;' /></div>";
		} else if(lmp=='2'){
			return "<div align='center' style='background-color:#CCCCCC;'><input type='text' readonly='true' name = '"+inname+"' value='��ͺ����' align='center' maxlength='"+maxlength+"' style='text-align:"+textalign+";width:100%;background-color:transparent;' /></div>";	
		}
		
	}
	
	
	
	function writeG(newGwork,key)
	{
		var lmp ;
		
		if(newGwork != null && newGwork != ''){			
				lmp = newGwork;		
		}else{
			lmp = '';			
		}
		var mm0 = '';
		var mm1 = '';
		var mm2 = '';
		
		
		if(lmp==''){
			mm0 = 'selected';
		} else if(lmp=='1'){
			mm1 = 'selected';
		} else if(lmp=='2'){
			mm2 = 'selected';		
		}
		
		return "<div align='center' style='background-color:#CCCCCC;'><input type='text' readonly='true' name = 'newGwork' value='"+emp+"' align='center' maxlength='"+maxlength+"' style='text-align:"+textalign+";width:100%;background-color:transparent;' /><input type='hidden' name = '"+nameSeq+"' value='"+codeseq+"'  /></div>";
		
		return "<div align='center' style='background-color:#CCCCCC;' ><select name='newGwork' readonly='true' background-color:transparent; >" +
													"<option value='1' "+mm1+" >���ͧ</option>" +
												    "<option value='2' "+mm2+" >��ͺ����</option>" +											 							
							"</select></div>";		
	}
	
	
	function writeApprove(approveF,key)
	{
		var lmp ;
		
		if(approveF != null && approveF != ''){			
				lmp = approveF;		
		}else{
			lmp = '';			
		}
		var mm0 = '';
		var mm1 = '';
		var mm2 = '';
		
		//alert("lmp :",lmp);
		if(lmp==''){
			mm0 = 'selected';
		} else if(lmp=='Y'){
			mm1 = 'selected';
		} else if(lmp=='N'){
			mm2 = 'selected';		
		}
		
		
		return "<div align='center' ><select name='approveF' onchange='addListUpdate("+key+");'>" +
													"<option value='Y' "+mm1+" >�Ѻ�ͧ</option>" +
												    "<option value='N' "+mm2+" >����Ѻ�ͧ</option>" +											 							
							"</select></div>";		
	}
	
	function writeApp(inname,emp,key)
	{
			
		var mm1 = '';
		var mm2 = '';
		
	
		
		if(emp=='Y'){
		mm1 = 'selected';
		}else if(emp=='N'){
		mm2 = 'selected';
		}
		
		return "<div align='center' ><select onchange='addListUpdate("+key+")' name='"+inname+"'>" +
													"<option value='Y' "+mm1+" >�Ѻ�ͧ</option>" +
												    "<option value='N' "+mm2+" >����Ѻ�ͧ</option>" +																							 							
							"</select></div>";
	
	
	}
	
	
	function whenListDataTableHandler(data)
	{
		$("dataLength").value = data.length;
		countData();
		
		if(data.length > 0){
			DWRUtil.removeAllRows("dataTable");
			DWRUtil.addRows("dataTable",data,cellFuncs);
			//countData();
			if(DWRUtil.getValue("showMaxPage") == ''){
				countData();
			}else{
				onCheckButt("searchForm");
			}
		}else{
			alert('��辺������');
			DWRUtil.removeAllRows("dataTable");
			if(DWRUtil.getValue("showMaxPage") == ''){
				countData();
			}else{
				onCheckButt("searchForm");
			}
		}
	}
	
	
	var apTable = {keySeq:null, ouCode:null, yearPn:null, monthPn:null,volumeSet:null,empCode:null,codeSeq:null,seqData:null,newGworkCode:null,totAmt:null,approveFlag:null,updBy:null,updDate:null,creBy:null,creDate:null,approveBy:null,approveDate:null};
	
	var allRowUpdate = 0;
	
	function onUpdate(){
		
		var table = document.getElementById("table");
		var aRows = table.rows;
		var num = 1 + parseInt(DWRUtil.getValue("dataLength"));
		
		var empNull = true;
		canSave = true ;
		var tab = $('dataTable');
		var row;
		var update ;
		var empList=[];
		var frm   = document.forms["searchForm"];
		var volumeCbo ='';
		
		
		if(parseInt(DWRUtil.getValue("dataLength")) != null && parseInt(DWRUtil.getValue("dataLength")) > 0){
			num = 1 + parseInt(DWRUtil.getValue("dataLength"));
		}else{
			num = 1 ;
		}
		if(dojo.widget.byId("volumeCbo").textInputNode.value != '')
			{
				volumeCbo = dojo.widget.byId("volumeCbo").textInputNode.value;
			}else
			{
				volumeCbo = '';
			}
	
		for(var a=aRows.length-1;a > num;a--){
				if (aRows[a].cells["empCode"].childNodes[0].value == null || aRows[a].cells["empCode"].childNodes[0].value == '' || aRows[a].cells["name"].childNodes[0].value == null || aRows[a].cells["name"].childNodes[0].value == ''){
					empNull = false;
					break;
				}
		}
		
		
		if(tab.rows.length > 0){
		
			var keySeq 		= frm.elements["keySeq"];
			var empCode 	= frm.elements["empCode"];
			var name		= frm.elements["name"];
			var codeSeq 	= frm.elements["codeSeq"];
			var oldPositionShort = frm.elements["oldPositionShort"];
			var orgDesc		= frm.elements["orgDesc"];
			var seqData 	= frm.elements["seqData"];
		    var totAmt      = frm.elements["totAmt"];
			var newGwork = frm.elements["newGwork"];
			var approveF = frm.elements["approveF"];
			
			var sst = "";
			var sed = ""; 
			
			for(var c=0; c<tab.rows.length; c++){
				if (empCode[c].value == null || empCode[c].value == ''){
					empNull = false;
				}
				
			
			}

			
			if(canSave){
				if(empNull){
				
					DWREngine.beginBatch();
					
					//alert('UPDATE '+tab.rows.length);
					for(var i=0; i<tab.rows.length; i++){
						//alert('value in row :'+keySeq[i].value+' : '+empCode[i].value+' : '+codeSeq[i].value+' : '+seqData[i].value+' : '+newCodeSeq[i].value+' : '+orgCode[i].value+' : '+newOrgCode[i].value+': '+newOldDuty[i].value+' : '+newPositionCode[i].value+':'+newLevelCode[i].value+':'+newDuty[i].value );
						//alert('value in row :'+keySeq[i].value+' : '+empCode[i].value+': '+newGworkCode[i].value );
						update = false;
						row = tab.rows[i];
						apTable.keySeq = parseInt(keySeq[i].value);
					    apTable.volumeSet = dojo.widget.byId("volumeCbo").textInputNode.value;
					    
					    if (empCode[i].value != '' && empCode[i].value != null){
							apTable.empCode  = empCode[i].value;
						}
						else{
							apTable.empCode  = null;
						}
						
					    if (name[i].value != '' && name[i].value != null){
					    	apTable.name  = name[i].value;
						}else{
							apTable.name  = null;
						}
					    apTable.codeSeq = codeSeq[i].value;
						
						
						if (orgDesc[i].value != '' && orgDesc[i].value != null){
							apTable.orgDesc  = orgDesc[i].value;
						}
						else{
							apTable.orgDesc  = null;
						}
						//else{
						//	apTable.newCodeSeq = null;
						//}
						
						if (seqData[i].value != '' && seqData[i].value != null){
							apTable.seqData  = parseInt(seqData[i].value);
						}else{
							apTable.seqData  = null;
						}
						if (totAmt[i].value != '' && totAmt[i].value != null){
							apTable.totAmt  = totAmt[i].value;
						}
						
						
						if (newGwork[i].value != '' && newGwork[i].value != null){
							apTable.newGworkCode  = newGwork[i].value;
						}
						else{
							apTable.newGworkCode  = null;
						}
						//alert(approveFlag[i].value);
						if (approveF[i].value != '' && approveF[i].value != null){
							apTable.approveFlag  = approveF[i].value;
						}
						else{
							apTable.approveFlag  = null;
						}
						
						apTable.approveBy = '<%=userId%>';
						apTable.updBy = '<%=userId%>';
						for(var x = 0 ; x < myUpdate.length ;x++){
							if(myUpdate[x] == parseInt(keySeq[i].value)){
								update = true;
								break;
							}
						}
						
						
						if(update){
							allRowUpdate++;
							if(aRows.length  > num + 1 ){
								if( allRowUpdate == myUpdate.length )
									ApTableService.addListApprove(apTable, false, {callback:onInsertCallback,errorHandler:function(message) { alert('�Դ��ͼԴ��Ҵ����ǡѺ��õԴ����������');}});
								else
									ApTableService.addListApprove(apTable, false);
					
								}else{
								if( allRowUpdate == myUpdate.length )
									ApTableService.addListApprove(apTable, true, {callback:ClearData,errorHandler:function(message) { alert('�������ö�ѹ�֡�� �Ҩ�բ����ž�ѡ�ҹ���觵�駫��');}});
								else
									ApTableService.addListApprove(apTable, false);
								//allRowUpdate++;
							}
						}
						
						
					}

				
					DWREngine.endBatch();
				}
			}
		}
		

		if(empNull){
			if(canSave){
				if(myUpdate.length == 0){
						insertNewData();
				}
			}
		}else{
					alert('�Ţ��Шӵ��  ���١��ͧ �ѹ�֡�����');
				}
	}
	

	function ClearData(){
		alert("�ѹ�֡���������º����");
			var table = document.getElementById("table");
			var tdName;
			var chkName;
			var num;
			if(parseInt(DWRUtil.getValue("dataLength")) != null && parseInt(DWRUtil.getValue("dataLength")) > 0){
				num = 1 + parseInt(DWRUtil.getValue("dataLength"));
			}else{
				num = 1;
			}
			
			var oRows = table.rows;
			if(tdName == null)tdName="flag";
			if(chkName == null)chkName="chk";
			for(i=oRows.length-1;i > num;i--){
					table.deleteRow(i);		
			}
		DWRUtil.removeAllRows("dataTable");
		whenShowDataTable();
	}
	
	function onInsertCallback(){
		insertNewData();
		allRowUpdate = 0;
	}
	
	function insertNewData()
	{
		var frm   = document.forms[0];
		var table = document.getElementById("table");
		var tdName;
		var chkName;
		var num;
		var empNull = true;
		
		var workMonth = DWRUtil.getValue("workMonth");
		var volumeCbo ='';
		
		if(parseInt(DWRUtil.getValue("dataLength")) != null && parseInt(DWRUtil.getValue("dataLength")) > 0){
			num = 1 + parseInt(DWRUtil.getValue("dataLength"));
		}else{
			num = 1 ;
		}
		if(dojo.widget.byId("volumeCbo").textInputNode.value != '')
			{
				volumeCbo = dojo.widget.byId("volumeCbo").textInputNode.value;
			}else
			{
				volumeCbo = '';
			}
		var oRows = table.rows;
		var insertStatus = false;
		if(tdName == null)tdName="flag";
		if(chkName == null)chkName="chk";
		
	
		
		for(var c=oRows.length-1;c > num;c--){
				if (oRows[c].cells["empCode"].childNodes[0].value == null || oRows[c].cells["empCode"].childNodes[0].value == '' || oRows[c].cells["name"].childNodes[0].value == null || oRows[c].cells["name"].childNodes[0].value == ''){
					empNull = false;
				}
		}
		
		
		//alert( empNull );
		
		if(empNull){
		
			DWREngine.beginBatch();
			//alert( oRows.length + ' ' + num );
			for(var i=oRows.length-1;i > num;i--)
			{
					insertStatus = true;
					apTable.keySeq = null;
					apTable.ouCode = '<%=ouCode%>';
					apTable.yearPn = workYear;
					apTable.monthPn = workMonth;
					apTable.volumeSet = volumeCbo;
					apTable.empCode = oRows[i].cells["empCode"].childNodes[0].value;
					apTable.name = oRows[i].cells["name"].childNodes[0].value;
					apTable.codeSeq = oRows[i].cells["name"].childNodes[1].value;					
					apTable.oldPositionShort = oRows[i].cells["oldPositionShort"].childNodes[0].value;
					apTable.orgDesc = oRows[i].cells["orgDesc"].childNodes[0].value;apTable.seqData = oRows[i].cells["seqData"].childNodes[0].value;
					apTable.totAmt = oRows[i].cells["totAmt"].childNodes[0].value;
					apTable.newGworkCode = oRows[i].cells["newGwork"].childNodes[0].value;
					apTable.approveFlag = oRows[i].cells["approveF"].childNodes[0].value;
					apTable.approveBy = '<%=userId%>';
					apTable.updBy = '<%=userId%>';
					apTable.creBy = '<%=userId%>';
					apTable.creDate = getDateFromFormat(<%=date%>,"dd/MM/yyyy");
				  		if( i == (num +1) )
							ApTableService.addListApprove(apTable, true, {callback:ClearData,errorHandler:function(message) { alert('�������ö�ѹ�֡�� �Ҩ�բ����ž�ѡ�ҹ���觵�駫��');}});
						else
							ApTableService.addListApprove(apTable, false);
			}
			
			DWREngine.endBatch();
			if (!insertStatus){alert("�ѹ�֡���������º����");}
			
		}else{
			alert('�Ţ��Шӵ�� ���� �ѧ�Ѵ���� ���١��ͧ');
		}
	
	}
	
	function onDeleteCallback()
	{
		alert("Delete Complete");
		whenShowDataTable();
	}
	
	
	 
	 function addListUpdate(data){
		var add = true;
		for(var i = 0 ; i < myUpdate.length ;i++){
			if(myUpdate[i] == data){
				add = false;
				break;
			}
		}
		if(add){
			myUpdate[count] = data;
			count++;
		}
	}
	

	function whenSelectEmpOptionInRow(object)
     {
     
     	 while (object.tagName !=  'TR')
		 {
		 object = object.parentNode;
		 }
	
		 lRowNumber = object.rowIndex;
		 
		DWRUtil.useLoadingMessage("Loading ...");
		var table = document.getElementById("table");
		var tdName;
		var chkName;
		
		var oRows = table.rows;
		var i = oRows.length-1;
		//alert(oRows[i].cells["empCode"].childNodes[0].value);
		var empCode = oRows[lRowNumber].cells["empCode"].childNodes[0].value;
		//alert(empCode);
		//var cbo = dojo.widget.byId("empCbo");
		
		if(empCode != null && empCode != ''){
			whenFetchEmployeeDetailInRow(empCode)
		}else{
			oRows[lRowNumber].cells["name"].childNodes[0].value = '';
			oRows[lRowNumber].cells["name"].childNodes[1].value = '';
		}
	 } 
	  function whenFetchEmployeeDetailInRow(empCode)
	 {
		DWRUtil.useLoadingMessage("Loading ...");
		//alert(empCode+' : '+<%=ouCode%>+' : ' + DWRUtil.getValue("year")+' : '+ DWRUtil.getValue("period"))
		PnEmployeeService.findByApEmpCodeDetail(empCode,'<%=ouCode%>', {callback:whenFetchEmployeeDetailInRowCallback,errorHandler:function(message) { alert('�Դ��ͼԴ��Ҵ����ǡѺ��õԴ���������� wee ');}});
	 }
	 
	 function whenFetchEmployeeDetailInRowCallback(data)
	 {
		// alert(data.empCode);
		var table = document.getElementById("table");
		var tdName;
		var chkName;
		
		var oRows = table.rows;
		var i = oRows.length-1;
	 	if(data.empCode != null && data.empCode != ''){
	 				oRows[lRowNumber].cells["name"].childNodes[0].value = data.name;
					oRows[lRowNumber].cells["oldPositionShort"].childNodes[0].value = data.oldPositionShort;
					oRows[lRowNumber].cells["orgDesc"].childNodes[0].value = data.orgDesc;
					oRows[lRowNumber].cells["name"].childNodes[1].value = data.codeSeq ;
		}else{
			oRows[lRowNumber].cells["name"].childNodes[0].value = '';
			oRows[lRowNumber].cells["oldPositionShort"].childNodes[0].value = '';
			oRows[lRowNumber].cells["orgDesc"].childNodes[0].value ='';
			oRows[lRowNumber].cells["name"].childNodes[1].value = '';
			alert('�Ţ��Шӵ�����١��ͧ');
			oRows[lRowNumber].cells["empCode"].childNodes[0].focus();	
		}
	 }
	 
	 function whenSelectEmpOptionInRowUpdate(data){
	 	
	 	var tab = $('dataTable');
		var row;
		var update ;
		var empList=[];
		var frm   = document.forms["searchForm"];
		
		
		if(tab.rows.length > 0){
			var keySeq 		= frm.elements["keySeq"];
			var empCode 	= frm.elements["empCode"];
			var name		= frm.elements["name"];
			var codeSeq 	= frm.elements["codeSeq"];
			var oldPositionShort  = frm.elements["oldPositionShort"];
			var orgDesc = frm.elements["orgDesc"];
		
			var seqData 	= frm.elements["seqData"];
			
			for(var c=0; c<tab.rows.length; c++){
				if(keySeq[c].value == data ){
					rowModify = c;
					break;
				}
			}
			alert(rowModify);
			var empCode = empCode[rowModify].value;
			//alert(empCode);
			if(empCode != null && empCode != ''){
				whenFetchEmployeeDetailInRowUpdate(empCode)
			}else{
				name[rowModify].value = '';
				codeSeq[rowModify].value = '';
			}
			
		}
	 	
	 }
	 
	 function whenFetchEmployeeDetailInRowUpdate(empCode)
	 {
		DWRUtil.useLoadingMessage("Loading ...");
		//alert(empCode+' : '+<%=ouCode%>+' : ' + DWRUtil.getValue("year")+' : '+ DWRUtil.getValue("period"))
		PnEmployeeService.findByApEmpCodeDetail(empCode, '<%=ouCode%>', {callback:whenFetchEmployeeDetailInRowUpdateCallback,errorHandler:function(message) { alert('�Դ��ͼԴ��Ҵ����ǡѺ��õԴ����������');}});
	 }
	 
	 function whenFetchEmployeeDetailInRowUpdateCallback(data)
	 {
		// alert(data.empCode);
		
		var tab = $('dataTable');
		var row;
		var update ;
		var empList=[];
		var frm   = document.forms["searchForm"];
		var oRows = table.rows;
		
		if(tab.rows.length > 0){
			var keySeq 		= frm.elements["keySeq"];
			var empCode 	= frm.elements["empCode"];
			var name		= frm.elements["name"];
			var codeSeq     = frm.elements["codeSeq"];
			var oldPositionShort  = frm.elements["oldPositionShort"];
			var orgDesc = frm.elements["orgDesc"];
			var seqData 	= frm.elements["seqData"];
			
			if(data.empCode != null && data.empCode != ''){
				name[rowModify].value = data.name;
				codeSeq[rowModify].value = data.codeSeq;
				oldPositionShort[rowModify].value = data.oldPositionShort;
				orgDesc[rowModify].value = data.orgDesc;
			
				
			}else{
				name[rowModify].value = '';
				oldPositionShort[rowModify].value =  '';
				orgDesc[rowModify].value =  '';
				codeSeq[rowModify].value = '';
				alert('�Ţ��Шӵ�����١��ͧ');
				empCode[rowModify].focus();	
			}
			
		}
	
	 }
	 
	 function countData(){
		myUpdate = new Array();
		
	    	var frm = document.forms[0];
			var workYear = DWRUtil.getValue("workYear");
			var workMonth = DWRUtil.getValue("workMonth");
			
			var volumeCbo = '';
			
		
			
	
			if(dojo.widget.byId("volumeCbo").textInputNode.value != '')
			{
				volumeCbo = dojo.widget.byId("volumeCbo").textInputNode.value;
			}else
			{
				volumeCbo = '';
			}
			//alert('query');
			// Query by Criteria
		ApTableService.countDataApprove
		(
				'<%=userId%>',
				'<%=ouCode%>',
				workYear,
				workMonth,
				volumeCbo,
			{callback:countDataHandler,errorHandler:function(message) { alert('�Դ��ͼԴ��Ҵ����ǡѺ��õԴ����������');}}
		);
	}
	
	function countDataHandler(data)
	{
		DWRUtil.setValue("countData",data);
		onCheckButt("searchForm");
	    
	}
	 
</script>
<%
	
	
	String keySeq  = request.getParameter("keySeq");
%>
</head>
<body>

<table width="100%">
	<tr>
		<td class="font-head">
			[ CTAPPM002 ] �ѹ�֡�Ѻ�ͧ��¡���ԡ����ѡ�Ҿ�Һ�� ��.
		</td>
	</tr>
</table>
<form name="searchForm" action="" method="post">

<input type="hidden" name="dataLength"> 
 
<table width="770" border="0" align="center" cellspacing="1">

  		<tr>
    	<td class="font-field" align="right">��Шӻ�&nbsp</td>
    	<td align="left">
    		<input type="text" name="workYear" size="2" maxlength="4" style="width: 70px;text-align: center;" onkeyup="if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);"/>
    	</td>
	    <td class="font-field" align="right">��͹</td>
		<td align="left">
			<select name="workMonth" >	
				<option value="0">- - - - - - - - - -</option>
				<option value="1">���Ҥ�</option>
				<option value="2">����Ҿѹ��</option>
				<option value="3">�չҤ�</option>
				<option value="4">����¹</option>
				<option value="5">����Ҥ�</option>
				<option value="6">�Զع�¹</option>
				<option value="7">�á�Ҥ�</option>
				<option value="8">�ԧ�Ҥ�</option>
				<option value="9">�ѹ��¹</option>
				<option value="10">���Ҥ�</option>
				<option value="11">��Ȩԡ�¹</option>
				<option value="12">�ѹ�Ҥ�</option>
			</select>
			
		</td>
		<td>&nbsp</td>
  	</tr>
  	
   
 	<tr>
  	<td class="font-field" align="right">�ش���&nbsp;</td>
	    <td align="left"><SELECT  dojoType="ComboBox" widgetId="volumeCbo" style="width:100"  forceValidOption="true"  ></SELECT></td>
	    <input type="hidden" name="volumeSet" >
	    <td><input type="Button" value="����" class=" button " onclick="whenShowDataTable();" /></td>
  	    	<input type="hidden" name="hidSearchMonth"/>	
			<input type="hidden" name="hidSearchYear" />	
     </tr>
</table>
<br/>

<table  width="900" border="0" cellspacing="0" cellpadding="0" align="center" >
	<tr>
		<td>
		<div style="height:320px;width:900;overflow:auto;vertical-align: top;" align="center" >
		<table id="table" width="900"  border="1" bordercolor="#6699CC" cellpadding="0" cellspacing="0">
			<thead style="text-align: center">
				<tr CLASS="TABLEBULE2" style="height: 30px;">
					<th CLASS="TABLEBULE2" style="width:150px" rowspan="1" align="center">�ӴѺ </th>
					<th CLASS="TABLEBULE2" style="width:100px" rowspan="1" ><a href="http://10.253.13.25/employee/" target="_blank" align="center">�Ţ��Шӵ��</a></th>
					<th CLASS="TABLEBULE2" style="width:250px" rowspan="1">���� - ���ʡ��</th>
				    <th CLASS="TABLEBULE2" style="width:90px" rowspan="1">���˹�-�дѺ</th>
					<th CLASS="TABLEBULE2" style="width:350px" rowspan="1">�ѧ�Ѵ</th>
					<th CLASS="TABLEBULE2" style="width:100px" rowspan="1">�ӹǹ�Թ</th>
					<th CLASS="TABLEBULE2" style="width:100px"  rowspan="1">����Ѻ</th>
					<th CLASS="TABLEBULE2" style="width:100px"  rowspan="1">�Ѻ�ͧ</th>
				
				</tr>				
			</thead>
			<tbody id="dataTable">
			</tbody>
			<tr id="temprow" style="visibility:hidden;position:absolute">
			<td id="seqData" align="center"><input type="text"  name="seqData"  maxlength="10" readonly="readonly"  style="text-align:right;width:100%;" onkeyup="if(this.value < 0) this.value = '';if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);"/><input type="hidden" align="center" name="keySeq" /></td>
			<td id="empCode" align="center"><input type="text" maxlength="6" name="empCode" readonly="readonly" style="width:100%" onchange="whenSelectEmpOptionInRow(this);"  onkeyup="if(this.value < 0) this.value = '';if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);"></td>
			<td id="name" align="center"><input type="text"  name="name" readonly="readonly"  style="width:100%;background-color:silver;"/><input type="hidden"  name="codeSeq" /></td>
			<td id="oldPositionShort" align="center"><input type="text"  name="oldPositionShort" readonly="readonly" style="width:100%;background-color:silver;"/></td>
			<td id="orgDesc" align="center"><input type="text"  name="orgDesc" readonly="readonly" style="width:100%;background-color:silver;"/></td>
			<td id="totAmt" align="center"><input type="text" maxlength="12" name="totAmt" readonly="readonly" style="width:100%" onkeyup="if(this.value < 0) this.value = '';if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);"></td>
		    <td id="newGwork" align="center"><input type="text"  name="newGwork" readonly="readonly" style="width:100%;background-color:silver;"/></td>
		    <!-- <td id="newGwork" align="center" width="40" background-color:silver; readonly="readonly" >
						<select name="newGwork" style="width:100%;"" >
													<option value="" selected="selected"  ></option>
													<option value="1">���ͧ</option>
													<option value="2">��ͺ����</option>
						</select></td>-->
		    <td id="approveF" align="center" width="40">
						<select name="approveF" style="width:100%;"" >
						                            <option value="Y" <% if(approveF.equals("Y")){%> selected="selected"  <%} %> >�Ѻ�ͧ</option>
													<option value="N" <% if(approveF.equals("N")){%> selected="selected"  <%} %>  >����Ѻ�ͧ</option>													
						</select></td>	
		</tr>
	</table>
	</div>
	<div>
<table width="770" align="center"  cellpadding="2" cellspacing="0" >
	<tr>
		<td align="right">
			<input type="hidden" name="page" value="<%=pageEdit%>">
			<input type="hidden" name="maxPage">
			<input type="hidden" name="countData" >
			<input type="hidden" name="dataPerPage" value="10">
			<input type="button" disabled="disabled" class=" button " value="First" name="first" onclick="onFirst(whenShowDataTable);"/>
			<input type="button" disabled="disabled" class=" button " value="<<" name="previous" onclick="onPrevious(whenShowDataTable);"/>
			<input type="text"  name="showPage" style="text-align:right;width: 40;" 
				    onkeyup="onCheckPageNAN(this.value);" onchange="onChangeGoPage(whenShowDataTable);" onkeypress="onKeyGoPage(event,whenShowDataTable);" 
			/>
			/
			<input type="text"  name="showMaxPage" readonly="readonly" style="width: 40;border-style : none;background-color : transparent;text-align:right;font-weight:bold;"/>
			<input type="button" disabled="disabled" class=" button " value=">>" name="next" onclick="onNext(whenShowDataTable);" />
			<input type="button" disabled="disabled" class=" button " value="Last" name="last" onclick="onLast(whenShowDataTable);"/>
		</td>
	</tr>
</table>
</div>
	</td>
  </tr>
</table>


<table width="100%" CLASS="TABLEBULE2" >
	<tr CLASS="TABLEBULE2" >
		<td align="left" >&nbsp;
			<input type="Button" class=" button " value="��ŧ" id="confirmData" name="confirmData" onclick="onUpdate();"/>
		</td>
	</tr>
</table>
</body>
</html>
<SCRIPT LANGUAGE="JavaScript">
<!--
function addVisualRow(){
		var tab = $('dataTable');
	
		var table = document.getElementById("table");
		var tempRow = document.getElementById("temprow");
	
		// Insert two rows.
	   	var oTable = table;
	   	var oRowsCheck = table.rows;
	   	var rrCheck = oRowsCheck.length - 1;
	   	var idx = oTable.rows.length;
	   
	   	var str=" " ;
		var from = "[0]";
		
		if(tab.rows.length > 0){
			if(idx > (2 + tab.rows.length)){
				if(oRowsCheck[rrCheck].cells["empCode"].childNodes[0].value != null && oRowsCheck[rrCheck].cells["empCode"].childNodes[0].value != '' ){
					// Insert cells into row.
					var oRow1=oTable.insertRow(idx);
					var to = "("+(oRow1.rowIndex+tempRow.rowIndex)+")";
					oRow1.id = tempRow.id;
					for(i=0;i<tempRow.cells.length;i++){
						var oCell=oRow1.insertCell(i);
						// Add regular HTML values to the cells.
						
						
						oCell.id = tempRow.cells[i].id;
						oCell.innerHTML=tempRow.cells[i].innerHTML;  
						str = oCell.innerHTML;
						if(str.indexOf(from)>0){
							oCell.innerHTML=str.replace(from, to);
						}
					}
				}else{
					alert('��سҡ�͡�Ţ��Шӵ�ǡ�͹���������ŵ�ǶѴ�');
				
				}
			}else{
					var oRow1=oTable.insertRow(idx);
					var to = "("+(oRow1.rowIndex+tempRow.rowIndex)+")";
					oRow1.id = tempRow.id;
					for(i=0;i<tempRow.cells.length;i++){
						var oCell=oRow1.insertCell(i);
						// Add regular HTML values to the cells.
						
						
						oCell.id = tempRow.cells[i].id;
						oCell.innerHTML=tempRow.cells[i].innerHTML;  
						str = oCell.innerHTML;
						if(str.indexOf(from)>0){
							oCell.innerHTML=str.replace(from, to);
						}
			
				}
			}
		}else{
			if(idx > 2){
				//if(oRowsCheck[rrCheck].cells["empCode"].childNodes[0].value != null && oRowsCheck[rrCheck].cells["empCode"].childNodes[0].value != ''){
				if(oRowsCheck[rrCheck].cells["empCode"].childNodes[0].value != null && oRowsCheck[rrCheck].cells["empCode"].childNodes[0].value != ''){
					// Insert cells into row.
					var oRow1=oTable.insertRow(idx);
					var to = "("+(oRow1.rowIndex+tempRow.rowIndex)+")";
					oRow1.id = tempRow.id;
					for(i=0;i<tempRow.cells.length;i++){
						var oCell=oRow1.insertCell(i);
						// Add regular HTML values to the cells.
						
						
						oCell.id = tempRow.cells[i].id;
						oCell.innerHTML=tempRow.cells[i].innerHTML;  
						str = oCell.innerHTML;
						if(str.indexOf(from)>0){
							oCell.innerHTML=str.replace(from, to);
						}
					}
				}else{
					alert('��سҡ�͡�Ţ��Шӵ�ǡ�͹���������ŵ�ǶѴ�');
				
				}
			}else{
					var oRow1=oTable.insertRow(idx);
					var to = "("+(oRow1.rowIndex+tempRow.rowIndex)+")";
					oRow1.id = tempRow.id;
					for(i=0;i<tempRow.cells.length;i++){
						var oCell=oRow1.insertCell(i);
						// Add regular HTML values to the cells.
						oCell.id = tempRow.cells[i].id;
						oCell.innerHTML=tempRow.cells[i].innerHTML;  
						str = oCell.innerHTML;
						if(str.indexOf(from)>0){
							oCell.innerHTML=str.replace(from, to);
						}
			
				}
			}
		}
	
	
}

//this function removeVisualRow() used page UPDATE ONLY!!!
	var chDelete = false;
	var chcCon = false;
	function removeVisualRow(){
		var tab = $('dataTable');
		var row;
		var empList=[];
		var frm = document.forms["searchForm"];
		var chk = frm.elements["chk"];
		
			
		var table = document.getElementById("table");
		var tdName;
		var chkName;
		var num;
		if(parseInt(DWRUtil.getValue("dataLength")) != null && parseInt(DWRUtil.getValue("dataLength")) > 0){
			num = 1 + parseInt(DWRUtil.getValue("dataLength"));
		}else{
			num = 1;
		}
		
		var oRows = table.rows;
		if(tdName == null)tdName="flag";
		if(chkName == null)chkName="chk";
		//alert(oRows.length+" : "+num);
		
		
		
		for(x=oRows.length-1;x > num;x--){
				if (oRows[x].cells[tdName].childNodes[0].checked){
						chcCon = true;	
				}
			
			}
		if(tab.rows.length>1){
				for(i=0; i<tab.rows.length; i++){
					row = tab.rows[i];	
					if (chk[i].checked){
							chcCon = true;
						}
					
						
				}
			}else{
				if(tab.rows.length==1){
					row = tab.rows[0];	
					if (chk[0].checked){
							chcCon = true;
						}
				  
				}	
			}
		
		
		
		
		
		
		
		
		if(chcCon){
			var answer = confirm("��ͧ���ź������ �������?");
			if( answer ){
				for(i=oRows.length-1;i > num;i--){
						if (oRows[i].cells[tdName].childNodes[0].checked){
								table.deleteRow(i);		
								chDelete = true;	
						}
					
					}
					
				DWREngine.beginBatch();
					//alert('123435  :'+tab.rows.length);
				    if(tab.rows.length>1){
						for(i=0; i<tab.rows.length; i++){
							row = tab.rows[i];	
							if (chk[i].checked){
									
									//alert('BB'+rowDelete);
									apTable.keySeq = parseInt(frm.elements["keySeq"][i].value);
									ApTableService.deleteApTable(apTable, {callback:onDeleteCallback,errorHandler:function(message) { alert('�Դ��ͼԴ��Ҵ����ǡѺ��õԴ����������');}});
									chDelete = true;
								}
							
								
						}
					}else{
						if(tab.rows.length==1){
							row = tab.rows[0];	
							if (chk[0].checked){
									
									//alert('CC'+rowDelete);
									apTable.keySeq = parseInt(frm.elements["keySeq"][0].value);
									ApTableService.deleteApTable(apTable, {callback:onDeleteCallback,errorHandler:function(message) { alert('�Դ��ͼԴ��Ҵ����ǡѺ��õԴ����������');}});
									chDelete = true;
								}
						  
						}	
					}
					
				DWREngine.endBatch();
					
		
			}
		}
		
		if(chDelete){
			alert('ź���������º����');
			DeletCompleaseData();
		}
	}
//***********************

	function onDeleteCallback(){
		//whenQueryData();
	}
	
	function DeletCompleaseData(){
		if(parseInt(DWRUtil.getValue("dataLength")) != null && parseInt(DWRUtil.getValue("dataLength")) > 0){
			whenShowDataTable();
		}
	}
	
	

//-->
	function whenShowReport(){
		var frm = document.forms[0];
		var workYear = DWRUtil.getValue("workYear");
		var workMonth = DWRUtil.getValue("workMonth");
		var comB1=dojo.widget.byId("volumeCbo").textInputNode.value;
		
			
		if(comB1!=''){
			dojo.widget.byId("volumeCbo").comboBoxSelectionValue.value = splitCombo(dojo.widget.byId("volumeCbo").textInputNode.value);
			volumeCbo = splitCombo(dojo.widget.byId("volumeCbo").textInputNode.value);
			
		}
	
		forController(volumeCbo);
		frm.action="pnEmpPromoteReport.htm?reqCode=doPrintReport";
		frm.target = "_blank";
		frm.submit();
		frm.target = "_self";
	}
	function whenShowLevelReport(){
		var frm = document.forms[0];
		var workYear = DWRUtil.getValue("workYear");
		var workMonth = DWRUtil.getValue("workMonth");
		var comB1=dojo.widget.byId("volumeCbo").textInputNode.value;
		
			
		if(comB1!=''){
			dojo.widget.byId("volumeCbo").comboBoxSelectionValue.value = splitCombo(dojo.widget.byId("volumeCbo").textInputNode.value);
			volumeCbo = splitCombo(dojo.widget.byId("volumeCbo").textInputNode.value);
			
		}
		
		forController(volumeCbo);
		frm.action="pnEmpPromoteLevelReport.htm?reqCode=doPrintReport";
		frm.target = "_blank";
		frm.submit();
		frm.target = "_self";
	}
	function forController(volumeCbo){
		DWRUtil.setValue("volumeSet",volumeCbo);
		
	
	}
</SCRIPT>
