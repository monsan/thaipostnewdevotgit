<%@ page language="java" contentType="text/html;charset=TIS-620" %>
<%@page import="java.text.DecimalFormat"%>
<%@page import="javax.swing.text.Document"%>
<%@ taglib prefix="c" uri="/WEB-INF/tld/c.tld"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.Date"%>
<%@ page import="com.ss.tp.security.ProcessResult" %>
<%@ page import="com.ss.tp.security.UserInfo" %>
<%@ page import="com.ss.tp.dto.DefaultYearSectionVO" %>

<%
	Calendar now = Calendar.getInstance(Locale.US);	
	UserInfo uf =  (UserInfo)request.getSession().getAttribute("UserLogin");	
	
	DefaultYearSectionVO defaultYear = (DefaultYearSectionVO)session.getAttribute("DefaultYearAndSection");
	
	
	java.util.Date dd = new java.util.Date();
	java.text.SimpleDateFormat fmd = new java.text.SimpleDateFormat("dd/MM/yyyy",new java.util.Locale("th","TH"));
	String date = fmd.format(dd);
	String userId =  uf.getUserId();
	String ouCode =  uf.getOuCode();
	String hidPage = request.getParameter("hidPage")==null?"":request.getParameter("hidPage");
	String pageEdit = request.getParameter("pageEdit")==null?"0":request.getParameter("pageEdit");
	//String year = request.getParameter("hidWorkYear")==null?String.valueOf(now.get(Calendar.YEAR)+543):request.getParameter("hidWorkYear");
	//String month = request.getParameter("hidWorkMonth")==null?String.valueOf(now.get(Calendar.MONTH)+1):request.getParameter("hidWorkMonth");
	
	if (request.getSession().getAttribute("processResult") != null){
		ProcessResult processResult = (ProcessResult) request.getSession().getAttribute("processResult");
		//year =  request.getParameter("hidYear")==null?String.valueOf(now.get(Calendar.YEAR)+543):request.getParameter("hidYear");
		//month =  request.getParameter("hidMonth")==null?String.valueOf(now.get(Calendar.MONTH)+1):request.getParameter("hidMonth");
	}
%>
<html>
<head>
<title>���觢����� �.��ا�� ��.</title>
<!-- Include -->
<script type="text/javascript" src="dwr/engine.js"></script>
<script type="text/javascript" src="dwr/util.js"></script>
<script type="text/javascript" src="script/payroll_util.js"></script>
<!-- Javascript Script File -->
<SCRIPT type="text/javascript" src="dwr/interface/SuUserOrganizationService.js"></SCRIPT>
<SCRIPT type="text/javascript" src="dwr/interface/PnEmployeeService.js"></SCRIPT>
<SCRIPT type="text/javascript" src="dwr/interface/ApTableService.js"></SCRIPT>
<SCRIPT type="text/javascript" src="dwr/interface/ApConfirmDataService.js"></SCRIPT>


<script type="text/javascript" src="script/gridScript.js"></script>
<script type="text/javascript" src="script/payrollComboBox.js"></script>
<script type="text/javascript" src="page/NavigatePage.jsp"></script>
<script type="text/javascript" src="script/dojo.js"></script>
<script type="text/javascript" src="script/dateCalendar.js"></script>
<script type="text/javascript" src="script/json.js"></script>

<script type="text/javascript">
	//Load Dojo's code relating to widget managing functions
	dojo.require("dojo.widget.*");
	dojo.require("dojo.widget.Menu2");
	dojo.require("dojo.widget.Button");
	dojo.require("dojo.widget.ComboBox");
	dojo.require("dojo.widget.DropDownButton");
	dojo.require("dojo.widget.SortableTable");
	dojo.require("dojo.widget.ContentPane");
	dojo.require("dojo.widget.LayoutContainer");
	dojo.require("dojo.widget.SortableTable");
	dojo.require("dojo.widget.Toolbar");
	dojo.require("dojo.widget.html.*");
	dojo.require("dojo.widget.Menu2");
	dojo.hostenv.writeIncludes();
	
	//Event
	dojo.require("dojo.event.*");
</script>

<script type="text/javascript">
     var statusConfirm = 'N';

	// =========================== Start LOV ===========================
   
        function onLoadVolumeFromCallback()
    {
    	try
    	{
	    	var cboSourceFrom = [];
	     	var cboFrom = dojo.widget.byId("volumeCboFrom");
	     	//var workYear = DWRUtil.getValue("workYear");
			//var workMonth = DWRUtil.getValue("workMonth");
	     
	     
			     	
			     	<c:forEach items="${VolumeInSecurity}" var="result" >		 
						cboSourceFrom.push(["<c:out value='${result}' />","<c:out value='${result}' />"]);
					</c:forEach>
	     	
	     	cboFrom.dataProvider.setData(cboSourceFrom);
	     
	   
     	}catch(e)
     	{
     		alert(e.message);
    	}
	} 
     
   
 
     
   
   function whenSelectVolumeFromOption()
	{
    	DWRUtil.useLoadingMessage("Loading ...");
		var cboFrom = dojo.widget.byId("volumeCboFrom");
		whenFetchVolumeFrom(cboFrom.textInputNode.value);
	}
	
	function whenFetchVolumeFrom(volume)
	{
     	DWRUtil.useLoadingMessage("Loading ...");
     	var cboFrom = dojo.widget.byId("volumeCboFrom");
		     	
		    
		     	
		     	var cboSourceFrom = [];
		     	
		     	<c:forEach items="${VolumeInSecurity}" var="result" >			     	
						cboSourceFrom.push(["<c:out value='${result}' />","<c:out value='${result}' />"]);
				</c:forEach>	
		     	
		     	cboFrom.dataProvider.setData(cboSourceFrom);
		     	
		     	//if( $("isConfirm").value == 'true' )
					//document.forms['searchForm'].elements['add'].disabled = true;
			
     	
    }
	
	
    
	 function onLoadVolumeToCallback()
	    {
	    	try
	    	{
		    	var cboSourceTo = [];
		     	var cboTo = dojo.widget.byId("volumeCboTo");
		     	//var workYear = DWRUtil.getValue("workYear");
				//var workMonth = DWRUtil.getValue("workMonth");
		     
		     
				     	
				     	<c:forEach items="${VolumeInSecurity}" var="result" >		 
							cboSourceTo.push(["<c:out value='${result}' />","<c:out value='${result}' />"]);
						</c:forEach>
		     	
		     	cboTo.dataProvider.setData(cboSourceTo);
		     
		   
	     	}catch(e)
	     	{
	     		alert(e.message);
	    	}
		} 
	     
	   
	 
	     
	   
	   function whenSelectVolumeToOption()
		{
	    	DWRUtil.useLoadingMessage("Loading ...");
			var cboTo = dojo.widget.byId("volumeCboTo");
			whenFetchVolumeTo(cboTo.textInputNode.value);
		}
		
		function whenFetchVolumeTo(volume)
		{
	     	DWRUtil.useLoadingMessage("Loading ...");
	     	var cboTo = dojo.widget.byId("volumeCboTo");
			     	
			    
			     	
			     	var cboSourceTo = [];
			     	
			     	<c:forEach items="${VolumeInSecurity}" var="result" >			     	
							cboSourceTo.push(["<c:out value='${result}' />","<c:out value='${result}' />"]);
					</c:forEach>	
			     	
			     	cboTo.dataProvider.setData(cboSourceTo);
			     	
			     	//if( $("isConfirm").value == 'true' )
						//document.forms['searchForm'].elements['add'].disabled = true;
				
	     	
	    }
  
    
    
	
    
   

 
	function init()
	{
		$("workYear").value = '<%= defaultYear.getYear() %>';
		$("workMonth").value = '<%= defaultYear.getMonth() %>';
		//var cboEmpFrom = dojo.widget.byId("empFromCbo");
	    var cboVolumeFrom = dojo.widget.byId("volumeCboFrom");
		var cboVolumeTo = dojo.widget.byId("volumeCboTo");
	
		//dojo.event.connect(cboEmpFrom, "selectOption", "whenSelectEmpOption");
		dojo.event.connect(cboVolumeFrom, "selectOption", "whenSelectVolumeFromOption");
		dojo.event.connect(cboVolumeTo, "selectOption", "whenSelectVolumeToOption");
		
		
		
		//whenCountDataAll();
		
	}
	
  	dojo.addOnLoad(init);

  	 dojo.addOnLoad(onLoadVolumeFromCallback);
     dojo.addOnLoad(onLoadVolumeToCallback);
 	
	
	// =========================== End LOV ===========================
	
	
	function query(){
		if(confirm("��س��׹�ѹ����觢����Ÿ�Ҥ���¾ҳԪ�� ?")){
		   statusConfirm = 'Y';
		   whenCountDataOthMoneyHandler();
		}
	}
	function queryData()
	{
		$("allData").value = '';
		$("allMoney").value = '';
		$("empMoney").value = '';
		$("othMoney").value = '';
		
	
		document.forms['searchForm'].elements['add'].disabled = true;
		DWRUtil.useLoadingMessage("Loading ...");
		
		whenCountDataAll();
	}
	function whenInsertApConfirmDataHandler(data){
	}
	
	var allOrder = 0;
	
	function whenCountDataAll()
	{
		allOrder = 0;
		var workYear = DWRUtil.getValue("workYear");
		var workMonth = DWRUtil.getValue("workMonth");
		var volumeCbo = '';
		
		
		
		
		if(dojo.widget.byId("volumeCbo").textInputNode.value != '')
		{
			volumeCbo = dojo.widget.byId("volumeCbo").textInputNode.value;
		}else
		{
			volumeCbo = '';
		}
		ApTableService.confirmData
		(
			'<%=ouCode%>',
			workYear,
			workMonth,
			volumeCbo,
			'<%= userId %>',
			{callback:whenCountDataAllHandler,errorHandler:function(message) { alert('�Դ��ͼԴ��Ҵ����ǡѺ��õԴ����������');}}
		);
	}
	
	function whenCountDataAllHandler(data)
	{
		DWRUtil.useLoadingMessage("Loading ...");
		
		allOrder += parseInt(data);
		$("allData").value = allOrder;
		
		whenCountDataMoney();
	}
	
	function whenCountDataMoney()
	{
		
		var workYear = DWRUtil.getValue("workYear");
		var workMonth = DWRUtil.getValue("workMonth");
		var volumeCbo = '';
		
		
		
		
		
		if(dojo.widget.byId("volumeCbo").textInputNode.value != '')
		{
			volumeCbo = dojo.widget.byId("volumeCbo").textInputNode.value;
		}else
		{
			volumeCbo = '';
		}
		ApTableService.sumAllMoneyData
		(
			'<%=ouCode%>',
			workYear,
			workMonth,
			volumeCbo,
			'<%= userId %>',
			{callback:whenCountDataMoneyHandler,errorHandler:function(message) { alert('�Դ��ͼԴ��Ҵ����ǡѺ��õԴ���������� 7');}}
		);
	}
	
	function whenCountDataMoneyHandler(data)
	{
		var allMon = 0.00;
		DWRUtil.useLoadingMessage("Loading ...");
		allMon += data;
		$("allMoney").value =formatCurrency(allMon);
		//allOrder += parseInt(data);
		
		whenCountDataEmpMoney();
	}
	
	function whenCountDataEmpMoney()
	{
		
		var workYear = DWRUtil.getValue("workYear");
		var workMonth = DWRUtil.getValue("workMonth");
		var volumeCbo = '';
		
		
		
		
		
		if(dojo.widget.byId("volumeCbo").textInputNode.value != '')
		{
			volumeCbo = dojo.widget.byId("volumeCbo").textInputNode.value;
		}else
		{
			volumeCbo = '';
		}
		
		ApTableService.sumMoneyData
		(
			'<%=ouCode%>',
			workYear,
			workMonth,
			volumeCbo,
			'<%= userId %>',
			'1',
			{callback:whenCountDataEmpMoneyHandler,errorHandler:function(message) { alert('�Դ��ͼԴ��Ҵ����ǡѺ��õԴ���������� 6');}}
		);
	}
	
	function whenCountDataEmpMoneyHandler(data)
	{
		var empM = 0.00;
		DWRUtil.useLoadingMessage("Loading ...");
		empM += data;
		$("empMoney").value = formatCurrency(empM);
		//allOrder += parseInt(data);
		
		whenCountDataOthMoney();
	}
	function whenCountDataOthMoney()
	{
		
		var workYear = DWRUtil.getValue("workYear");
		var workMonth = DWRUtil.getValue("workMonth");
		var volumeCbo = '';
		
		
		
		
		
		if(dojo.widget.byId("volumeCbo").textInputNode.value != '')
		{
			volumeCbo = dojo.widget.byId("volumeCbo").textInputNode.value;
		}else
		{
			volumeCbo = '';
		}
		
		ApTableService.sumMoneyData
		(
			'<%=ouCode%>',
			workYear,
			workMonth,
			volumeCbo,
			'<%= userId %>',
			'2',
			{callback:whenCountDataOthMoneyHandler,errorHandler:function(message) { alert('�Դ��ͼԴ��Ҵ����ǡѺ��õԴ���������� 5');}}
		);
	}
	
	
	function whenCountDataOthMoneyHandler()
	{
		//var othM = 0.00;
		
		var volumeCboFrom = '';
		var volumeCboTo = '';
		
		DWRUtil.useLoadingMessage("Loading ...");
		//othM += data;
		//$("othMoney").value = formatCurrency(othM);
		
		if(dojo.widget.byId("volumeCboFrom").textInputNode.value != '')
		{
			volumeCboFrom = dojo.widget.byId("volumeCboFrom").textInputNode.value;
		}else
		{
			volumeCboFrom = '';
		}
		
		if(dojo.widget.byId("volumeCboTo").textInputNode.value != '')
		{
			volumeCboTo = dojo.widget.byId("volumeCboTo").textInputNode.value;
		}else
		{
			volumeCboTo = '';
		}
		if (statusConfirm == 'Y'){		
			
				DWRUtil.useLoadingMessage("Loading ...");
				    //alert('ApTableService.updateConfirmDataBankKTB');
				    //alert(volumeCboFrom);
				    
					ApTableService.updateConfirmDataBankSCB
					(
						'<%=ouCode%>',
						DWRUtil.getValue("workYear"),
						DWRUtil.getValue("workMonth"),
						volumeCboFrom,
						volumeCboTo,
						'<%= userId %>',
						{callback:whenUpdateConfirmDataHandler,errorHandler:function(message) { alert('�Դ��ͼԴ��Ҵ����ǡѺ��õԴ���������� 2');}}
					);		
					 var confirmDataVo = { 	ouCode:'<%=ouCode%>',
				             yearPn:DWRUtil.getValue("workYear"),
				             monthPn:DWRUtil.getValue("workMonth"),
				             volumeSetFrom:volumeCboFrom,
					         volumeSetTo:volumeCboTo,
				             userId:'<%=userId%>',
				             flag:'4' 
					};
					
		ApConfirmDataService.insertApConfirmData
		(
			confirmDataVo,
			{callback:whenInsertApConfirmDataHandler,errorHandler:function(message) { alert('�Դ��ͼԴ��Ҵ����ǡѺ��õԴ���������� 3');}}
		);
			
		}
	}
	

	function whenUpdateConfirmDataHandler(data)
	{
		//allOrder = 0;
		//whenShowReport();
		  //var confirmDataVo = { 	ouCode:'<%=ouCode%>',
							   //          yearPn:DWRUtil.getValue("workYear"),
							  //           monthPn:DWRUtil.getValue("workMonth"),
							  //           volumeSetFrom:volumeCboFrom,
								//         volumeSetTo:volumeCboTo,
							   //          userId:'<%=userId%>',
							    //         flag:'4' 
								//};
								
					//ApConfirmDataService.insertApConfirmData
					//(
						//confirmDataVo,
						//{callback:whenInsertApConfirmDataHandler,errorHandler:function(message) { alert('�Դ��ͼԴ��Ҵ����ǡѺ��õԴ���������� 3');}}
					//);
		alert('��ӡ����Ŵ����������Ѻ��Ҥ���¾ҳԪ��� ���º����');
		whenShowReport();
		//document.forms['searchForm'].elements['add'].disabled = false;
	}
	
	

	function addCommas(nStr)
	{
		nStr += '';
		x = nStr.split('.');
		x1 = x[0];
		x2 = x.length > 1 ? '.' + x[1] : '';
		var rgx = /(\d+)(\d{3})/;
		while (rgx.test(x1)) {
			x1 = x1.replace(rgx, '$1' + ',' + '$2');
		}
		return x1 + x2;
	}
	
	function formatCurrency(num) {
	    num = isNaN(num) || num === '' || num === null ? 0.00 : num;
	    return addCommas(parseFloat(num).toFixed(2));
	}

	
	
</script>
<%
	
	
	String keySeq  = request.getParameter("keySeq");
%>
</head>
<body>
<table width="100%">
	<tr>
		<td class="font-head">
			[ CTAPTR002 ] ����������Ź��觸�Ҥ���¾ҳԪ��  ��.
		</td>
	</tr>
</table>
<form name="searchForm" action="" method="post">
<!-- <input type="hidden" name="workMonth" />-->
<input type="hidden" name="dataLength"> 
<br/><br/><br/><br/>
<table align="center" border="1" bordercolor="#6699CC">
<tr><td>
<BR/> 
<table width="330" border="0" align="center" cellspacing="1">

  		<tr>
    	<td class="font-field" align="right">��Шӻ�</td>
    	<td align="left">
    		<input type="text" name="workYear" value="" size="2" maxlength="4" style="width: 70px;text-align: center;" onkeyup="if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);"/>
    	</td>
	    <td class="font-field" align="right">��͹</td>
		<td align="left">
			<select name="workMonth" >	
				<option value="0">- - - - - - - - - -</option>
				<option value="1">���Ҥ�</option>
				<option value="2">����Ҿѹ��</option>
				<option value="3">�չҤ�</option>
				<option value="4">����¹</option>
				<option value="5">����Ҥ�</option>
				<option value="6">�Զع�¹</option>
				<option value="7">�á�Ҥ�</option>
				<option value="8">�ԧ�Ҥ�</option>
				<option value="9">�ѹ��¹</option>
				<option value="10">���Ҥ�</option>
				<option value="11">��Ȩԡ�¹</option>
				<option value="12">�ѹ�Ҥ�</option>
			</select>
			<script>
				document.forms["searchForm"].workMonth.value = document.forms["searchForm"].workMonth.value;
			</script>
		</td>
		<td>&nbsp</td>
  	</tr>  
 	<tr>
  	<td class="font-field" align="right">�ش�������&nbsp;</td>
	    <td align="left"><SELECT  dojoType="ComboBox" widgetId="volumeCboFrom" style="width:100"  forceValidOption="true" ></SELECT></td>
	    <input type="hidden" name="volumeSetFrom" >	  
	    <td class="font-field" align="right">�ش����ش&nbsp;</td>
	    <td align="left"><SELECT  dojoType="ComboBox" widgetId="volumeCboTo" style="width:100"  forceValidOption="true"  ></SELECT></td>
	    <input type="hidden" name="volumeSetTo" >	  	   
     </tr>  
</table>
 <center><FONT color="#6699CC">__________________________________________________________________</FONT></center>
 <br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<CENTER><input type="button" class="button" style="width:80px" value="�������§ҹ" name="add" onclick="query();"/></CENTER>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</td></tr>
</table>

</form>
</body>
</html>
<SCRIPT LANGUAGE="JavaScript">
function whenShowReport(){
	var frm = document.forms[0];
	var workYear = DWRUtil.getValue("workYear");
	var workMonth = DWRUtil.getValue("workMonth");
	var comB1=dojo.widget.byId("volumeCboFrom").textInputNode.value;
	var comB2=dojo.widget.byId("volumeCboTo").textInputNode.value;
	
		
	if(comB1!=''){
		dojo.widget.byId("volumeCboFrom").comboBoxSelectionValue.value = splitCombo(dojo.widget.byId("volumeCboFrom").textInputNode.value);
		volumeCboFrom = splitCombo(dojo.widget.byId("volumeCboFrom").textInputNode.value);
		
	}
	
	if(comB2!=''){
		dojo.widget.byId("volumeCboTo").comboBoxSelectionValue.value = splitCombo(dojo.widget.byId("volumeCboTo").textInputNode.value);
		volumeCboTo = splitCombo(dojo.widget.byId("volumeCboTo").textInputNode.value);
		
	}
	
	forController(volumeCboFrom,volumeCboTo);
	frm.action="apScbReport.htm?reqCode=doPrintReport";
	frm.target = "_blank";
	frm.submit();
	frm.target = "_self";
	
}
function forController(volumeCboFrom,volumeCboTo){
	DWRUtil.setValue("volumeSetFrom",volumeCboFrom);
	DWRUtil.setValue("volumeSetTo",volumeCboTo);
	
	//whenCountDataOthMoneyHandler();
}

</SCRIPT>
