<%@ page language="java" contentType="text/html;charset=TIS-620" %>
<%@page import="java.text.DecimalFormat"%>
<%@page import="javax.swing.text.Document"%>
<%@ taglib prefix="c" uri="/WEB-INF/tld/c.tld"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.Date"%>
<%@ page import="com.ss.tp.security.ProcessResult" %>
<%@ page import="com.ss.tp.security.UserInfo" %>
<%@ page import="com.ss.tp.dto.DefaultYearSectionVO" %>

<%
Calendar now = Calendar.getInstance(Locale.US);	
UserInfo uf =  (UserInfo)request.getSession().getAttribute("UserLogin");	


java.util.Date dd = new java.util.Date();
java.text.SimpleDateFormat fmd = new java.text.SimpleDateFormat("dd/MM/yyyy",new java.util.Locale("th","TH"));
String date = fmd.format(dd);
String userId =  uf.getUserId();
String ouCode =  uf.getOuCode();
String hidPage = request.getParameter("hidPage")==null?"":request.getParameter("hidPage");
String pageEdit = request.getParameter("pageEdit")==null?"0":request.getParameter("pageEdit");
String year = request.getParameter("hidWorkYear")==null?String.valueOf(now.get(Calendar.YEAR)+543):request.getParameter("hidWorkYear");
String month = request.getParameter("hidWorkMonth")==null?String.valueOf(now.get(Calendar.MONTH)+1):request.getParameter("hidWorkMonth");

if (request.getSession().getAttribute("processResult") != null){
	ProcessResult processResult = (ProcessResult) request.getSession().getAttribute("processResult");
	year =  request.getParameter("hidYear")==null?String.valueOf(now.get(Calendar.YEAR)+543):request.getParameter("hidYear");
	month =  request.getParameter("hidMonth")==null?String.valueOf(now.get(Calendar.MONTH)+1):request.getParameter("hidMonth");
}
%>
<html>
<head>
<title>�ͺ�����¡���ԡ����ѡ�Ҿ�Һ��</title>
<!-- Include -->
<script type="text/javascript" src="dwr/engine.js"></script>
<script type="text/javascript" src="dwr/util.js"></script>
<script type="text/javascript" src="script/payroll_util.js"></script>
<!-- Javascript Script File -->
<SCRIPT type="text/javascript" src="dwr/interface/SuUserOrganizationService.js"></SCRIPT>
<SCRIPT type="text/javascript" src="dwr/interface/PnEmployeeService.js"></SCRIPT>
<SCRIPT type="text/javascript" src="dwr/interface/ApTableService.js"></SCRIPT>


<script type="text/javascript" src="script/gridScript.js"></script>
<script type="text/javascript" src="script/payrollComboBox.js"></script>
<script type="text/javascript" src="page/NavigatePage.jsp"></script>
<script type="text/javascript" src="script/dojo.js"></script>
<script type="text/javascript" src="script/dateCalendar.js"></script>
<script type="text/javascript" src="script/json.js"></script>

<script type="text/javascript">
	//Load Dojo's code relating to widget managing functions
	dojo.require("dojo.widget.*");
	dojo.require("dojo.widget.Menu2");
	dojo.require("dojo.widget.Button");
	dojo.require("dojo.widget.ComboBox");
	dojo.require("dojo.widget.DropDownButton");
	dojo.require("dojo.widget.SortableTable");
	dojo.require("dojo.widget.ContentPane");
	dojo.require("dojo.widget.LayoutContainer");
	dojo.require("dojo.widget.SortableTable");
	dojo.require("dojo.widget.Toolbar");
	dojo.require("dojo.widget.html.*");
	dojo.require("dojo.widget.Menu2");
	dojo.hostenv.writeIncludes();
	
	//Event
	dojo.require("dojo.event.*");
</script>

<script type="text/javascript">

	// =========================== Start LOV ===========================
    var myUpdate = new Array();
	var count = 0;
	var rowModify ;
	var rowModifyAMT ;
	var lRowNumber;
	var canSave = true;
	
	 
    function whenSelectEmpOption()
    {
		DWRUtil.useLoadingMessage("Loading ...");
		var empCode = DWRUtil.getValue("empCbo")
		//alert(empCode);
		//var cbo = dojo.widget.byId("empCbo");
		
		whenFetchEmployeeDetail(empCode);
	 } 
	 
	 function whenFetchEmployeeDetail(empCode)
	 {
		DWRUtil.useLoadingMessage("Loading ...");
		
		ApTableService.findByEmpCodeDetail( empCode, '<%=ouCode%>', {callback:whenFetchEmployeeDetailCallback,errorHandler:function(message) { alert('�Դ��ͼԴ��Ҵ����ǡѺ��õԴ����������');}});
	 }

	 function whenFetchEmployeeDetailCallback(data)
	 {
	 	if(data.empCode != null && data.empCode != ''){
			$("name").value = data.name;
			$("positionShort").value = data.oldPositionShort;
			$("orgCode").value = data.orgCode;
			$("orgDesc").value = checkNull(data.orgDesc,'STRING');
			$("codeSeq").value = data.codeSeq;	
			
			}else{
			$("name").value = '';
			$("positionShort").value = '';
			$("orgCode").value = '';
	    	$("orgDesc").value = '';
			$("codeSeq").value = '';	
			
			alert('�Ţ��Шӵ�����١��ͧ �����ѧ����բ������ԡ����ѡ�Ҿ�Һ��');
		}	
	 }
	 
	
	function whenShowDataTable()
	{	
		myUpdate = new Array();
		
	    	var frm = document.forms[0];
			var workYear = DWRUtil.getValue("workYear");
			//var workMonth = DWRUtil.getValue("workMonth");
			
			
			//alert('query');
			// Query by Criteria
			ApTableService.findByEmpCriteriaList
			(
				'<%=userId%>',
				DWRUtil.getValue("empCbo"),
				'<%=ouCode%>',
				workYear,
				DWRUtil.getValue("page"),
				DWRUtil.getValue("dataPerPage"),
				{callback:whenListDataTableHandler,errorHandler:function(message) { alert('�Դ��ͼԴ��Ҵ����ǡѺ��õԴ����������');}}
			);
		
	}
	
	var cellFuncs = [
        function(data) { return writeTextDisplay("workYear",data.yearPn,150,"center","codeSeq",data.codeSeq);},
        function(data) { return writeMDisplay("workMonth",data.monthPn,150,"center","codeSeq",data.codeSeq);},   
        function(data) { return writeTextDisplay("volumeSet",data.volumeSet,150,"center","codeSeq",data.codeSeq);},
        function(data) { return writeText("totAmt",data.totAmt,12,"right",data.keySeq);},
		function(data) { return writeHidden("seqData",data.seqData,200,12,"right","keySeq",data.keySeq);},		
		function(data) { return writeGDisplay("newGwork",data.newGworkCode,200,"right","codeSeq",data.codeSeq);},
		function(data) { 
			if(data.approveFlag != null && data.approveFlag != ''){
				return  writeADisplay("approveF",data.approveFlag,200,"right","codeSeq",data.codeSeq);
			}else{
				return writeADisplay("",data.keySeq);
			}	
			},			
		function(data) { 
			if(data.bankFlag != null && data.bankFlag != ''){
				return writeBDisplay("bankF",data.bankFlag,200,"right","codeSeq",data.codeSeq);
			}else{
				return writeBDisplay("",data.keySeq);
			}	
			},
		function(data) { 
			if(data.bankDate != null && data.bankDate != ''){
				return writeDateDisplay("bankD",formatDate(data.bankDate,"dd/MM/yyyy"),200,"right","codeSeq",data.codeSeq);
			}else{
				return writeDateDisplay("","");
			}	
			}
	];
	
	

	function writeText(inname,emp,maxlength,textalign,key)
	{
		return "<div align='center' ><input type='text' name = '"+inname+"' readonly='true'  onChange='addListUpdate("+key+");checkValueInRowUpdate("+key+");'  value='"+emp+"' align='center' maxlength='"+maxlength+"' style='text-align:"+textalign+";width:100%;' onkeyup='if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);'/></div>";
	}
	function writeTextDisplay(inname,emp,maxlength,textalign,nameSeq,codeseq)
	{
		return "<div align='center' style='background-color:#CCCCCC;'><input type='text' readonly='true' name = '"+inname+"' value='"+emp+"' align='center' maxlength='"+maxlength+"' style='text-align:"+textalign+";width:100%;background-color:transparent;' /><input type='hidden' name = '"+nameSeq+"' value='"+codeseq+"'  /></div>";
	}
	
	function writeDateDisplay(inname,emp,maxlength,textalign,nameSeq,codeseq)
	{
		return "<div align='center' style='background-color:#CCCCCC;'><input type='text' readonly='true' name = '"+inname+"' value='"+emp+"' align='center' maxlength='"+maxlength+"' style='text-align:"+textalign+";width:100%;background-color:transparent;' /><input type='hidden' name = '"+nameSeq+"' value='"+codeseq+"'  /></div>";
	}
	function writeTextID(inname,emp,maxlength,textalign,key)
	{
		return "<div align='center' ><input type='text' name = '"+inname+"' maxlength='6' onChange='whenSelectEmpOptionInRowUpdate("+key+");addListUpdate("+key+")' value='"+emp+"' align='center' maxlength='"+maxlength+"' style='text-align:"+textalign+";width:100%;'  onkeyup='if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);'/></div>";
	}
	
	
	function writeHidden(inname,emp,size,maxlength,textalign,nameHide,empHide)
	{
				return "<div align='center'><input type='text' name = '"+inname+"' readonly='true'  onchange='addListUpdate("+empHide+")' value='"+emp+"' align='center' maxlength='"+maxlength+"' style='text-align:"+textalign+";width:100%' onkeyup='if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);'/><input type='hidden' name = '"+nameHide+"' value='"+empHide+"'  /></div>";
	}
	function writeGDisplay(inname,emp,maxlength,textalign,nameSeq,codeseq)
	{
        var lmp ;
		
		if(emp != null && emp != ''){			
				lmp = emp;		
		}else{
			lmp = '';			
		}
	
		 if(lmp=='1'){
			return "<div align='center' style='background-color:#CCCCCC;'><input type='text' readonly='true' name = '"+inname+"' value='���ͧ' align='center' maxlength='"+maxlength+"' style='text-align:"+textalign+";width:100%;background-color:transparent;' /><input type='hidden' name = '"+nameSeq+"' value='"+codeseq+"'  /></div>";
		} else if(lmp=='2'){
			return "<div align='center' style='background-color:#CCCCCC;'><input type='text' readonly='true' name = '"+inname+"' value='��ͺ����' align='center' maxlength='"+maxlength+"' style='text-align:"+textalign+";width:100%;background-color:transparent;' /><input type='hidden' name = '"+nameSeq+"' value='"+codeseq+"'  /></div>";	
		}
		
	}
	
	function writeMDisplay(inname,emp,maxlength,textalign,nameSeq,codeseq)
	{
		var lmp ;
		
		if(emp != null && emp != ''){			
				lmp = emp;		
		}else{
			lmp = '';			
		}
		
		    if(lmp=='1'){
				return "<div align='center' style='background-color:#CCCCCC;'><input type='text' readonly='true' name = '"+inname+"' value='���Ҥ�' align='center' maxlength='"+maxlength+"' style='text-align:"+textalign+";width:100%;background-color:transparent;' /><input type='hidden' name = '"+nameSeq+"' value='"+codeseq+"'  /></div>";
			} else if(lmp=='2'){
				return "<div align='center' style='background-color:#CCCCCC;'><input type='text' readonly='true' name = '"+inname+"' value='����Ҿѹ��' align='center' maxlength='"+maxlength+"' style='text-align:"+textalign+";width:100%;background-color:transparent;' /><input type='hidden' name = '"+nameSeq+"' value='"+codeseq+"'  /></div>";	
			}else if(lmp=='3'){
				return "<div align='center' style='background-color:#CCCCCC;'><input type='text' readonly='true' name = '"+inname+"' value='�չҤ�' align='center' maxlength='"+maxlength+"' style='text-align:"+textalign+";width:100%;background-color:transparent;' /><input type='hidden' name = '"+nameSeq+"' value='"+codeseq+"'  /></div>";	
			}else if(lmp=='4'){
				return "<div align='center' style='background-color:#CCCCCC;'><input type='text' readonly='true' name = '"+inname+"' value='����¹' align='center' maxlength='"+maxlength+"' style='text-align:"+textalign+";width:100%;background-color:transparent;' /><input type='hidden' name = '"+nameSeq+"' value='"+codeseq+"'  /></div>";	
			}else if(lmp=='5'){
				return "<div align='center' style='background-color:#CCCCCC;'><input type='text' readonly='true' name = '"+inname+"' value='����Ҥ�' align='center' maxlength='"+maxlength+"' style='text-align:"+textalign+";width:100%;background-color:transparent;' /><input type='hidden' name = '"+nameSeq+"' value='"+codeseq+"'  /></div>";	
			}else if(lmp=='6'){
				return "<div align='center' style='background-color:#CCCCCC;'><input type='text' readonly='true' name = '"+inname+"' value='�Զع�¹' align='center' maxlength='"+maxlength+"' style='text-align:"+textalign+";width:100%;background-color:transparent;' /><input type='hidden' name = '"+nameSeq+"' value='"+codeseq+"'  /></div>";	
			}else if(lmp=='7'){
				return "<div align='center' style='background-color:#CCCCCC;'><input type='text' readonly='true' name = '"+inname+"' value='�á�Ҥ�' align='center' maxlength='"+maxlength+"' style='text-align:"+textalign+";width:100%;background-color:transparent;' /><input type='hidden' name = '"+nameSeq+"' value='"+codeseq+"'  /></div>";	
			}else if(lmp=='8'){
				return "<div align='center' style='background-color:#CCCCCC;'><input type='text' readonly='true' name = '"+inname+"' value='�ԧ�Ҥ�' align='center' maxlength='"+maxlength+"' style='text-align:"+textalign+";width:100%;background-color:transparent;' /><input type='hidden' name = '"+nameSeq+"' value='"+codeseq+"'  /></div>";	
			}else if(lmp=='9'){
				return "<div align='center' style='background-color:#CCCCCC;'><input type='text' readonly='true' name = '"+inname+"' value='�ѹ��¹' align='center' maxlength='"+maxlength+"' style='text-align:"+textalign+";width:100%;background-color:transparent;' /><input type='hidden' name = '"+nameSeq+"' value='"+codeseq+"'  /></div>";	
			}else if(lmp=='10'){
				return "<div align='center' style='background-color:#CCCCCC;'><input type='text' readonly='true' name = '"+inname+"' value='���Ҥ�' align='center' maxlength='"+maxlength+"' style='text-align:"+textalign+";width:100%;background-color:transparent;' /><input type='hidden' name = '"+nameSeq+"' value='"+codeseq+"'  /></div>";	
			}else if(lmp=='11'){
				return "<div align='center' style='background-color:#CCCCCC;'><input type='text' readonly='true' name = '"+inname+"' value='��Ȩԡ�¹' align='center' maxlength='"+maxlength+"' style='text-align:"+textalign+";width:100%;background-color:transparent;' /><input type='hidden' name = '"+nameSeq+"' value='"+codeseq+"'  /></div>";	
			}else if(lmp=='12'){
				return "<div align='center' style='background-color:#CCCCCC;'><input type='text' readonly='true' name = '"+inname+"' value='�ѹ�Ҥ�' align='center' maxlength='"+maxlength+"' style='text-align:"+textalign+";width:100%;background-color:transparent;' /><input type='hidden' name = '"+nameSeq+"' value='"+codeseq+"'  /></div>";	
			}
	}
	
	function writeADisplay(inname,emp,maxlength,textalign,nameSeq,codeseq)
	{
        var lmp ;
		
		if(emp != null && emp != ''){			
				lmp = emp;		
		}else{
			lmp = '';			
		}
		var mm0 = '';
		var mm1 = '';
		var mm2 = '';
		
		
		 if(lmp=='Y'){
			return "<div align='center' style='background-color:#CCCCCC;'><input type='text' readonly='true' name = '"+inname+"' value='�Ѻ�ͧ' align='center' maxlength='"+maxlength+"' style='text-align:"+textalign+";width:100%;background-color:transparent;' /><input type='hidden' name = '"+nameSeq+"' value='"+codeseq+"'  /></div>";
		} else if(lmp=='N'){
			return "<div align='center' style='background-color:#CCCCCC;'><input type='text' readonly='true' name = '"+inname+"' value='����Ѻ�ͧ' align='center' maxlength='"+maxlength+"' style='text-align:"+textalign+";width:100%;background-color:transparent;' /><input type='hidden' name = '"+nameSeq+"' value='"+codeseq+"'  /></div>";	
		}
		
	}
	function writeBDisplay(inname,emp,maxlength,textalign,nameSeq,codeseq)
	{
        var lmp ;
		
		if(emp != null && emp != ''){			
				lmp = emp;		
		}else{
			lmp = '';			
		}
		var mm0 = '';
		var mm1 = '';
		var mm2 = '';
		
		
		 if(lmp=='Y'){
			return "<div align='center' style='background-color:#CCCCCC;'><input type='text' readonly='true' name = '"+inname+"' value='�͹' align='center' maxlength='"+maxlength+"' style='text-align:"+textalign+";width:100%;background-color:transparent;' /><input type='hidden' name = '"+nameSeq+"' value='"+codeseq+"'  /></div>";
		} else if(lmp=='N'){
			return "<div align='center' style='background-color:#CCCCCC;'><input type='text' readonly='true' name = '"+inname+"' value='����͹' align='center' maxlength='"+maxlength+"' style='text-align:"+textalign+";width:100%;background-color:transparent;' /><input type='hidden' name = '"+nameSeq+"' value='"+codeseq+"'  /></div>";	
		}
		
	}
	
	
	function whenListDataTableHandler(data)
	{
		$("dataLength").value = data.length;
		countData();
		
		if(data.length > 0){
			DWRUtil.removeAllRows("dataTable");
			DWRUtil.addRows("dataTable",data,cellFuncs);
			//countData();
			if(DWRUtil.getValue("showMaxPage") == ''){
				countData();
			}else{
				onCheckButt("searchForm");
			}
		}else{
			alert('��辺������');
			DWRUtil.removeAllRows("dataTable");
			if(DWRUtil.getValue("showMaxPage") == ''){
				countData();
			}else{
				onCheckButt("searchForm");
			}
		}
	}
	
	
	var apTable = {keySeq:null, ouCode:null, yearPn:null, monthPn:null,volumeSet:null,empCode:null,codeSeq:null,seqData:null,newGworkCode:null,totAmt:null,updBy:null,updDate:null,creBy:null,creDate:null};
	
	var allRowUpdate = 0;
	
	

	
	
	

	
	 function countData(){
		myUpdate = new Array();
		
	    	var frm = document.forms[0];
			var workYear = DWRUtil.getValue("workYear");
			//var workMonth = DWRUtil.getValue("workMonth");
			
			
			//alert('query');
			// Query by Criteria
		ApTableService.countEmpData
		(
				'<%=userId%>',
				DWRUtil.getValue("empCbo"),
				'<%=ouCode%>',
				workYear,
			{callback:countDataHandler,errorHandler:function(message) { alert('�Դ��ͼԴ��Ҵ����ǡѺ��õԴ����������');}}
		);
	}
	
	function countDataHandler(data)
	{
		DWRUtil.setValue("countData",data);
		onCheckButt("searchForm");
	    
	}
	 
</script>
<%	
	String keySeq  = request.getParameter("keySeq");
%>
</head>
<body>

<table width="100%">
	<tr>
		<td class="font-head">
			[ CTAPQY001 ] �ͺ�����¡���ԡ����ѡ�Ҿ�Һ�� ��.
		</td>
	</tr>
</table>
<form name="searchForm" action="security.htm?reqCode=CTAPPM001" method="post">

<input type="hidden" name="dataLength"> 
 
<table width="800" border="0" cellspacing="0" cellpadding="0" align="center">

      <tr>
          <td  align="center">
				<table width="750" border="1" cellpadding="1" bordercolor="#6699CC" cellspacing="1"  align="center">
        		        <td class="font-field" align="right">��Шӻ�</td>
							<td align="left"><input type="text" name="workYear"
								 value="<%=year%>"  readonly="readonly" size="2" maxlength="4"
								style="width: 70px; text-align: center;"
								onkeyup="if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);" />
							</td>
        			<tr>
         				<td class="font-field"  align="right" width="100">�Ţ��Шӵ��</td>
          				<td align="left" width="15"><input type="text" name="empCbo" maxlength="6" style="width:150" onchange="whenSelectEmpOption();"></td>
          				<td align="left"><input type="text" name="name" readonly="readonly" style="width:260;background-color:silver;"/></td>
        				<td  class="font-field"  align="right" width="65">���˹�</td>
          				<td  align="left"><input type="text" name="positionShort" value="" readonly="readonly" style="width: 150;text-align: center;background-color:silver;"  /></td>
          				
       			 	</tr>
       			 	<tr>
          				<td  class="font-field"  align="right" width="100">�ѧ�Ѵ��Ժѵԧҹ</td>
          				<td  align="left" width="30"><INPUT type="text" name="orgCode" readonly="readonly" maxlength="20"  style="width:150;background-color: silver;"  /></td>
          				<td colspan="3" align="left"><input type="text" name="orgDesc" readonly="readonly" style="width:485;background-color: silver;"/></td>
       			 	</tr>
       	    <td><input type="Button" value="����" class=" button " onclick="whenShowDataTable();" /></td>
  	    		
     </tr>
       			 	
     		 </table>
          </td>
      </tr>
    	&nbsp;
	 	&nbsp;
	  	&nbsp;
	  <tr>
          <td align="center" >
				<div style="height:345px;width:783;overflow:auto;">
				 &nbsp;
	  			&nbsp;
	  			&nbsp;
				<table id="table"  bordercolor="#6699CC" align="center" style="text-align: center;width:750px" border="1" cellpadding="0" cellspacing="0" >
					<thead style="text-align: center">
						<tr CLASS="TABLEBULE2" style="height:30px;">
							<th CLASS="TABLEBULE2" style="width:150px" align="center">��</th>
							<th CLASS="TABLEBULE2" style="width:200px" align="center">��͹</th>
							<th CLASS="TABLEBULE2" style="width:150px" align="center">�ش���</th>
							<th CLASS="TABLEBULE2" style="width:200px" align="center"><center>�ӹǹ�Թ</center></th>
							<th CLASS="TABLEBULE2" style="width:150px" align="center">�Ţ��ҧ�ԧ</th>
							<th CLASS="TABLEBULE2" style="width:150px" align="center">����Ѻ</th>
							<th CLASS="TABLEBULE2" style="width:150px" align="center">�Ѻ�ͧ</th>
							<th CLASS="TABLEBULE2" style="width:150px" align="center">��Ҹ�Ҥ��</th>
							<th CLASS="TABLEBULE2" style="width:150px" align="center">�ѹ����觢�������鸹Ҥ��</th>
						</tr>
						
					</thead>
					<tbody id="dataTable">
					</tbody>
									
					<tr id="temprow" style="visibility:hidden;position:absolute">
							<td id="workYear"><input type="text" value="workYear" name="workYear" size="6" maxlength="4" align="center" style="text-align:center;" onkeyup="if(this.value < 0) this.value = '';if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);"/></td>
							<td id="workMonth" align="center"><input type="text"  name="workMonth" readonly="readonly" style="width:100%;background-color:silver;"/></td>
							<td id="volumeSet" ><input type="text"  name="volumeSet"  maxlength="12" readonly="readonly" style="text-align:right;width:100%;" onkeyup="if(this.value < 0) this.value = '';if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);"/></td>
							<td id="totAmt" align="center"><input type="text" maxlength="12" name="totAmt" readonly="readonly" style="width:100%" onkeyup="if(this.value < 0) this.value = '';if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);"></td>
							<td id="seqData" ><input type="text"  name="seqData" maxlength="12" align="center" style="text-align:right;width:100%;" onkeyup="if(this.value < 0) this.value = '';if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);" /><input type="hidden"  name="keySeq" /><input type="hidden"  name="codeSeq" /></td>
						    <td id="newGwork" align="center"><input type="text"  name="newGwork" readonly="readonly" style="width:100%;background-color:silver;"/></td>
		 				    <td id="approveF" align="center"><input type="text"  name="approveF" readonly="readonly" style="width:100%;background-color:silver;"/></td>
		 				    <td id="bankF" align="center"><input type="text"  name="bankF" readonly="readonly" style="width:100%;background-color:silver;"/></td>	
		 				    <td id="bankD" align="center"><input type="text"  name="bankD" readonly="readonly" style="width:100%;background-color:silver;"/></td>							
					</tr>
					
					</table>
				</div>
           </td>
      </tr>
          
</table>&nbsp;

	</div>
	<div>
<table width="770" align="center"  cellpadding="2" cellspacing="0" >
	<tr>
		<td align="right">
			<input type="hidden" name="page" value="<%=pageEdit%>">
			<input type="hidden" name="maxPage">
			<input type="hidden" name="countData" >
			<input type="hidden" name="dataPerPage" value="10">
			<input type="button" disabled="disabled" class=" button " value="First" name="first" onclick="onFirst(whenShowDataTable);"/>
			<input type="button" disabled="disabled" class=" button " value="<<" name="previous" onclick="onPrevious(whenShowDataTable);"/>
			<input type="text"  name="showPage" style="text-align:right;width: 40;" 
				    onkeyup="onCheckPageNAN(this.value);" onchange="onChangeGoPage(whenShowDataTable);" onkeypress="onKeyGoPage(event,whenShowDataTable);" 
			/>
			/
			<input type="text"  name="showMaxPage" readonly="readonly" style="width: 40;border-style : none;background-color : transparent;text-align:right;font-weight:bold;"/>
			<input type="button" disabled="disabled" class=" button " value=">>" name="next" onclick="onNext(whenShowDataTable);" />
			<input type="button" disabled="disabled" class=" button " value="Last" name="last" onclick="onLast(whenShowDataTable);"/>
		</td>
	</tr>
</table>
</div>
	</td>
  </tr>
</table>
</body>
</html>
<SCRIPT LANGUAGE="JavaScript">
<!--
function addVisualRow(){
		var tab = $('dataTable');
	
		var table = document.getElementById("table");
		var tempRow = document.getElementById("temprow");
	
		// Insert two rows.
	   	var oTable = table;
	   	var oRowsCheck = table.rows;
	   	var rrCheck = oRowsCheck.length - 1;
	   	var idx = oTable.rows.length;
	   
	   	var str=" " ;
		var from = "[0]";
		
		if(tab.rows.length > 0){
			if(idx > (2 + tab.rows.length)){
				if(oRowsCheck[rrCheck].cells["empCode"].childNodes[0].value != null && oRowsCheck[rrCheck].cells["empCode"].childNodes[0].value != '' ){
					// Insert cells into row.
					var oRow1=oTable.insertRow(idx);
					var to = "("+(oRow1.rowIndex+tempRow.rowIndex)+")";
					oRow1.id = tempRow.id;
					for(i=0;i<tempRow.cells.length;i++){
						var oCell=oRow1.insertCell(i);
						// Add regular HTML values to the cells.
						
						
						oCell.id = tempRow.cells[i].id;
						oCell.innerHTML=tempRow.cells[i].innerHTML;  
						str = oCell.innerHTML;
						if(str.indexOf(from)>0){
							oCell.innerHTML=str.replace(from, to);
						}
					}
				}else{
					alert('��سҡ�͡�Ţ��Шӵ�ǡ�͹���������ŵ�ǶѴ�');
				
				}
			}else{
					var oRow1=oTable.insertRow(idx);
					var to = "("+(oRow1.rowIndex+tempRow.rowIndex)+")";
					oRow1.id = tempRow.id;
					for(i=0;i<tempRow.cells.length;i++){
						var oCell=oRow1.insertCell(i);
						// Add regular HTML values to the cells.
						
						
						oCell.id = tempRow.cells[i].id;
						oCell.innerHTML=tempRow.cells[i].innerHTML;  
						str = oCell.innerHTML;
						if(str.indexOf(from)>0){
							oCell.innerHTML=str.replace(from, to);
						}
			
				}
			}
		}else{
			if(idx > 2){
				//if(oRowsCheck[rrCheck].cells["empCode"].childNodes[0].value != null && oRowsCheck[rrCheck].cells["empCode"].childNodes[0].value != ''){
				if(oRowsCheck[rrCheck].cells["empCode"].childNodes[0].value != null && oRowsCheck[rrCheck].cells["empCode"].childNodes[0].value != ''){
					// Insert cells into row.
					var oRow1=oTable.insertRow(idx);
					var to = "("+(oRow1.rowIndex+tempRow.rowIndex)+")";
					oRow1.id = tempRow.id;
					for(i=0;i<tempRow.cells.length;i++){
						var oCell=oRow1.insertCell(i);
						// Add regular HTML values to the cells.
						
						
						oCell.id = tempRow.cells[i].id;
						oCell.innerHTML=tempRow.cells[i].innerHTML;  
						str = oCell.innerHTML;
						if(str.indexOf(from)>0){
							oCell.innerHTML=str.replace(from, to);
						}
					}
				}else{
					alert('��سҡ�͡�Ţ��Шӵ�ǡ�͹���������ŵ�ǶѴ�');
				
				}
			}else{
					var oRow1=oTable.insertRow(idx);
					var to = "("+(oRow1.rowIndex+tempRow.rowIndex)+")";
					oRow1.id = tempRow.id;
					for(i=0;i<tempRow.cells.length;i++){
						var oCell=oRow1.insertCell(i);
						// Add regular HTML values to the cells.
						oCell.id = tempRow.cells[i].id;
						oCell.innerHTML=tempRow.cells[i].innerHTML;  
						str = oCell.innerHTML;
						if(str.indexOf(from)>0){
							oCell.innerHTML=str.replace(from, to);
						}
			
				}
			}
		}
	
	
}

//this function removeVisualRow() used page UPDATE ONLY!!!
	var chDelete = false;
	var chcCon = false;
	function removeVisualRow(){
		var tab = $('dataTable');
		var row;
		var empList=[];
		var frm = document.forms["searchForm"];
		var chk = frm.elements["chk"];
		
			
		var table = document.getElementById("table");
		var tdName;
		var chkName;
		var num;
		if(parseInt(DWRUtil.getValue("dataLength")) != null && parseInt(DWRUtil.getValue("dataLength")) > 0){
			num = 1 + parseInt(DWRUtil.getValue("dataLength"));
		}else{
			num = 1;
		}
		
		var oRows = table.rows;
		if(tdName == null)tdName="flag";
		if(chkName == null)chkName="chk";
		//alert(oRows.length+" : "+num);
		
		
		
		for(x=oRows.length-1;x > num;x--){
				if (oRows[x].cells[tdName].childNodes[0].checked){
						chcCon = true;	
				}
			
			}
		if(tab.rows.length>1){
				for(i=0; i<tab.rows.length; i++){
					row = tab.rows[i];	
					if (chk[i].checked){
							chcCon = true;
						}
					
						
				}
			}else{
				if(tab.rows.length==1){
					row = tab.rows[0];	
					if (chk[0].checked){
							chcCon = true;
						}
				  
				}	
			}
		
		
		
		
		
		
		
		
		if(chcCon){
			var answer = confirm("��ͧ���ź������ �������?");
			if( answer ){
				for(i=oRows.length-1;i > num;i--){
						if (oRows[i].cells[tdName].childNodes[0].checked){
								table.deleteRow(i);		
								chDelete = true;	
						}
					
					}
					
				DWREngine.beginBatch();
					//alert('123435  :'+tab.rows.length);
				    if(tab.rows.length>1){
						for(i=0; i<tab.rows.length; i++){
							row = tab.rows[i];	
							if (chk[i].checked){
									
									//alert('BB'+rowDelete);
									apTable.keySeq = parseInt(frm.elements["keySeq"][i].value);
									ApTableService.deleteApTable(apTable, {callback:onDeleteCallback,errorHandler:function(message) { alert('�Դ��ͼԴ��Ҵ����ǡѺ��õԴ����������');}});
									chDelete = true;
								}
							
								
						}
					}else{
						if(tab.rows.length==1){
							row = tab.rows[0];	
							if (chk[0].checked){
									
									//alert('CC'+rowDelete);
									apTable.keySeq = parseInt(frm.elements["keySeq"][0].value);
									ApTableService.deleteApTable(apTable, {callback:onDeleteCallback,errorHandler:function(message) { alert('�Դ��ͼԴ��Ҵ����ǡѺ��õԴ����������');}});
									chDelete = true;
								}
						  
						}	
					}
					
				DWREngine.endBatch();
					
		
			}
		}
		
		if(chDelete){
			alert('ź���������º����');
			DeletCompleaseData();
		}
	}
//***********************

	function onDeleteCallback(){
		//whenQueryData();
	}
	
	function DeletCompleaseData(){
		if(parseInt(DWRUtil.getValue("dataLength")) != null && parseInt(DWRUtil.getValue("dataLength")) > 0){
			whenShowDataTable();
		}
	}
	
	

//-->
	function whenShowReport(){
		var frm = document.forms[0];
		var workYear = DWRUtil.getValue("workYear");
		var workMonth = DWRUtil.getValue("workMonth");
		var comB1=dojo.widget.byId("volumeCbo").textInputNode.value;
		
			
		if(comB1!=''){
			dojo.widget.byId("volumeCbo").comboBoxSelectionValue.value = splitCombo(dojo.widget.byId("volumeCbo").textInputNode.value);
			volumeCbo = splitCombo(dojo.widget.byId("volumeCbo").textInputNode.value);
			
		}
	
		forController(volumeCbo);
		frm.action="pnEmpPromoteReport.htm?reqCode=doPrintReport";
		frm.target = "_blank";
		frm.submit();
		frm.target = "_self";
	}
	function whenShowLevelReport(){
		var frm = document.forms[0];
		var workYear = DWRUtil.getValue("workYear");
		var workMonth = DWRUtil.getValue("workMonth");
		var comB1=dojo.widget.byId("volumeCbo").textInputNode.value;
		
			
		if(comB1!=''){
			dojo.widget.byId("volumeCbo").comboBoxSelectionValue.value = splitCombo(dojo.widget.byId("volumeCbo").textInputNode.value);
			volumeCbo = splitCombo(dojo.widget.byId("volumeCbo").textInputNode.value);
			
		}
		
		forController(volumeCbo);
		frm.action="pnEmpPromoteLevelReport.htm?reqCode=doPrintReport";
		frm.target = "_blank";
		frm.submit();
		frm.target = "_self";
	}
	function forController(volumeCbo){
		DWRUtil.setValue("volumeSet",volumeCbo);
		
	
	}
</SCRIPT>
